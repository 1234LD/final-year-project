 <?php

use Illuminate\Http\Request;

use App\Http\Resources\menuResource;
use App\Http\Resources\menuCollection;
use App\Menu;

use App\Http\Resources\menu_itemCollection;
use App\Menu_Item;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
|--------------------------------------------------------------------------
| API Routes for general User
|--------------------------------------------------------------------------
*/

Route::get('userComposer','api_viewBuild_controller@build_user_data');

Route::get('indexComposer','api_viewBuild_controller@build_index_data');

Route::get('chef_panelComposer/{id}','api_viewBuild_controller@build_chef_panel_data');

Route::get('updateprofile/{id}','apiController@profile_data_populate');
Route::get('chef_profile_populate/{id}','api_chef_controller@chef_data_populate');


 Route::post('update-data-profile','apiController@get_admin_data_update');

 Route::post('update-chef-profile','api_chef_controller@get_chef_data_update');




 Route::get('customer_all_orders/{id}','api_order_controller@get_all_customer_orders');




 Route::get('admin_dashboard','api_viewBuild_controller@build_admin_dashboard');

Route::get('showU','apiController@index');

Route::get('users','apiController@index'); // get all the details of the users ...

Route::post('login_request','apiController@show'); // login the user ...

Route::post('create/customer','apiController@create_customer'); // create the new user as a customer ...

Route::post('create/chef','apiController@create_chef'); // create the new user  as a chef ...

Route::put('update_password','apiController@update_password'); // update the password of the user ...

Route::put('update_picture','apiController@update_picture'); // update the email of the user ...

Route::put('update_status','apiController@update_status'); // to update the current status of the account ...

Route::delete('user/{id}','apiController@destroy'); // delete the existing user ...

Route::post('forgot/password','apiController@forgot_password');

Route::post('reset_pswd','apiController@reset_pswd');

Route::get('top_meals/ratings','apiController@get_top_meals'); // to get the top meals based on ratings ...

Route::get('quickest_meal','apiController@get_quickest_meals'); // to get the quickest meals based on time ...

Route::get('analysis','apiController@get_analysis'); // to get the analysis between self-delivery and take-away ...

/*
|--------------------------------------------------------------------------
| API Routes for Chef
|--------------------------------------------------------------------------
*/

Route::post('chef/{id}','api_chef_controller@store'); // create new chef ...

Route::get('search/{lat}/{lng}','api_chef_controller@nearSearching');


Route::get('all_chefs','api_chef_controller@get_all_chefs');


Route::get('verified_chefs','api_chef_controller@get_verfied_chefs');



Route::get('blocked_chefs','api_chef_controller@get_blocked_chefs');
//Route::post('search','api_chef_controller@nearSearching') ; //for searching nearby chef...

Route::put('chef_edit/{id}','api_chef_controller@edit');

Route::put('chef','api_chef_controller@update'); // update existing chef ...

Route::delete('chef/{id}','api_chef_controller@destory'); // delete the chef ...

Route::get('chef_profile/{id}/{username}','api_chef_controller@view_complete_profile'); // to get the complete chef profile along with his menu ....

Route::get('total_sales/{id}','api_chef_controller@get_total_sales'); // to get the total sales of meals today ....

Route::get('total_sales/items/{id}','api_chef_controller@get_total_sale_items'); // to get the total sales per item today ....

Route::get('gross_rate/{id}','api_chef_controller@get_gross_total'); // to get the total gross rate of the today's sale ....

Route::get('chef_analysis/{id}','api_chef_controller@get_analysis'); // to get the analysis between self-delivery and take-away ...

Route::get('all_feedback/{id}','api_chef_controller@get_all_feedback'); // to get all the feedback of the chef ...

Route::get('flagged_feedback/{id}','api_chef_controller@get_flagged_feedback'); // to get flagged feedback of the chef ...

Route::get('flagged_orders/{id}','api_chef_controller@get_flagged_orders'); // To get flagged orders of the chef ...

Route::get('chef_all_orders/{id}/{name}','api_chef_controller@get_all_orders'); // To get all orders of the chef ...


/*
|--------------------------------------------------------------------------
| API Routes for Super Admin
|--------------------------------------------------------------------------
*/

Route::get('view_admin/{id}','api_superAdmin_controller@get_admin_details'); // To get the details of the admin ...

Route::get('view_all_admins','api_superAdmin_controller@get_all_admin_details'); // To get the details of all admins ...

Route::post('admin/create','api_superAdmin_controller@store'); // create user as Admin ...

Route::get('delete_admin/{id}','api_superAdmin_controller@destroy_admin'); // To delete the record of the admin ...

Route::get('block_admin/{id}','api_superAdmin_controller@block_admin'); // To block the account of admin ...

Route::get('unblock_admin/{id}','api_superAdmin_controller@unblock_admin'); // To unblock the account of admin ...


/*
|--------------------------------------------------------------------------
| API Routes for Admin
|--------------------------------------------------------------------------
*/

// CRUD for admin profile ...

Route::put('admin','api_admin_controller@update'); // to update the profile of admin ...

Route::post('block_request','api_admin_controller@block_user'); // to block the user's account ...

Route::post('unblock_request','api_admin_controller@unblock_user'); // to unblock the user's account ...

Route::get('total_sale/per_day','api_admin_controller@get_total_sales'); // to get the total sales of meal per day

Route::get('profit/per_day','api_admin_controller@get_profit'); // to get the profit per day ...

Route::get('registered_users/per_day','api_admin_controller@get_registered_users'); // to get the total registered users today ...

Route::get('online_chefs','api_admin_controller@get_online_chef'); // to get the total number of online chef at the moment ...

Route::get('top_meals','api_admin_controller@get_top_meals'); // to get the top meals based on number of orders ...

Route::get('top_users','api_admin_controller@get_top_users'); // to get the top users ...

Route::get('top_chefs/orders','api_admin_controller@get_top_ordered_chefs'); // to get the chefs based on orders ...

Route::get('top_chefs/rating','api_admin_controller@get_top_rated_chefs'); // to get the top chefs based on rating ...

Route::get('total_meals/searched','api_admin_controller@get_total_meals_searched'); // to get the total number of meals searched today ...

Route::get('order/view/{id}','api_admin_controller@get_order'); // to get the details of the particular order ...

Route::get('user/profile/{id}','api_admin_controller@search_user'); // To get the details of user account ...

Route::get('user/all','api_admin_controller@view_all_users'); // To get the details of all user account ...

Route::get('verify/menu_item','api_admin_controller@verify_menu_item'); // To verify the menu item of the chef ...

Route::get('unverify/menu_item','api_admin_controller@un_verify_menu_item'); // To un verify the menu item of the chef ...

Route::get('increment','api_admin_controller@search'); // to get the total number of online chef ...


/*
|--------------------------------------------------------------------------
| API Routes for Complete Menu of Admin and Chef
|--------------------------------------------------------------------------
*/

// C-R-U-D for Menu ...

Route::get('pop_menu','api_menu_controller@pop_up_menu'); // to popup the menu details for the chef ...

Route::post('menu','api_menu_controller@view_menu'); // get the details of the complete menu ...

Route::post('menu/create','api_menu_controller@create_menu'); // create the menu name for the chef ...

Route::put('menu/{id}','api_menu_controller@update_menu'); // update the existing menu name for the chef ...

Route::get('all_meals', 'api_menu_controller@get_regular_meals');


Route::get('admin/all_quick_meals', 'api_menu_controller@get_admin_meals');

Route::get('admin/all_expire_meals','api_menu_controller@get_admin_expire_meals');

Route::get('admin/all_free_meals','api_menu_controller@get_admin_free_meals');

Route::get('admin/all_review_meals','api_rating_reviews_controller@get_admin_review_meals');

Route::get('admin/all_flagged_meals','api_rating_reviews_controller@get_admin_flagged_reviews');

Route::get('admin/all_nonflagged_meals','api_rating_reviews_controller@get_admin_nonflagged_reviews');

Route::get('admin/all_expire_meals','api_menu_controller@get_admin_expire_meals');

Route::get('admin/all_free_meals','api_menu_controller@get_admin_free_meals');

Route::get('admin/all_review_meals','api_rating_reviews_controller@get_admin_review_meals');


//
//Route::get('admin/all_flagged_meals','api_rating_reviews_controller@get_admin_flagged_reviews');

Route::get('admin/all_nonflagged_meals','api_rating_reviews_controller@get_admin_nonflagged_reviews');

//Route::get('meal_page/{id}','api_menu_controller@view_category_item');

// C-R-U-D for Category ...

Route::post('category','api_menu_controller@view_category'); // get the details of the category ...

Route::post('category/create','api_menu_controller@create_category'); //  create the complete category ...

Route::put('update_meal','api_menu_controller@update_category_item'); //  create the complete category ...

Route::delete('category','api_menu_controller@destroy_category'); // delete the existing category ...

 Route::get('all_categories' ,'api_menu_controller@get_all_category' );

 Route::post('admin/add_categories', 'api_menu_controller@get_add_category');

// C-R-U-D for Category Items ...

Route::get('all_regular_meals/{id}/{username}','api_menu_controller@get_all_regular_meals'); // to get all regular meals of chef ...

Route::get('all_quick_meals/{id}/{username}','api_menu_controller@get_all_quick_meals'); // to get all quick meals of chef ...

Route::get('expired_quick_meals/{id}/{username}','api_menu_controller@get_expired_quick_meals'); // to get expired quick meals of chef ...

Route::get('free_meals/{id}/{username}','api_menu_controller@get_free_meals'); // to get free meals of chef ...


Route::get('meal_page/{username}/{id}','api_menu_controller@view_category_item'); // get the details of the category ...

Route::post('category_item','api_menu_controller@create_category_items'); // create the new item in the category ...

Route::put('category_item/update','api_menu_controller@update_category_item'); // update the existing item ...

Route::delete('category_item/delete/{id}','api_menu_controller@destroy_category_item'); // delete the existing item ...

// C-R-U-D for Item Recipes ...

Route::post('item_recipe','api_menu_controller@view_item_recipe'); // to view the recipe of the item ...

Route::post('item_recipe/create','api_menu_controller@create_item_recipe'); // to create the item recipe of an item ...

Route::put('item_recipe','api_menu_controller@update_item_recipe'); // to update the item recipe of an item ....

Route::delete('item_recipe','api_menu_controller@destroy_item_recipe'); // to delete the item recipe of an item ...


// C-R-U-D operations for Item Images ...

Route::post('item_images','api_menu_controller@view_item_images'); // to view the images of an item ...

Route::post('item_images/create','api_menu_controller@create_item_images'); // to create the images of an item ...

Route::put('item_images','api_menu_controller@update_item_images'); // to update the existing images of an item ...

Route::delete('item_images','api_menu_controller@destroy_item_images'); // to delete the image of an item ...


/*
|--------------------------------------------------------------------------
| API Routes for Orders
|--------------------------------------------------------------------------
*/

Route::post('showarray','api_order_controller@index');

 Route::get('pending_orders','api_order_controller@get_pending_admin_order');

 Route::get('admin/ready_orders','api_order_controller@get_admin_ready_orders');

 Route::get('admin/deliver_orders','api_order_controller@get_admin_deliver_orders');

 Route::get('admin/cancelled_orders','api_order_controller@get_admin_canceled_orders');


 Route::get('admin/declined_orders','api_order_controller@get_admin_declined_orders');


 Route::get('admin/paid_orders','api_payment_controller@get_admin_paid_orders');

 Route::get('admin/unpaid_orders','api_payment_controller@get_admin_unpaid_orders');

 Route::get('admin/flagged_orders','api_order_controller@get_admin_flagged_orders');


Route::get('all_orders/{id}/{usertype}','api_order_controller@get_all_orders'); // to view the details of all chef orders ....

Route::get('customer_order/{id}/{usertype}','api_order_controller@get_all_order'); // to view the details of all customer orders ....

Route::get('pending_orders/{id}/{username}','api_order_controller@get_pending_orders'); // to get the details of chef's pending orders ...

Route::get('ready_orders/{id}/{username}','api_order_controller@get_ready_orders'); // to get the details of chef's ready orders ...

Route::get('processing_orders/{id}/{username}','api_order_controller@get_processing_orders'); // to get the details of chef's processing orders ...

Route::get('process_orders','api_order_controller@get_admin_process_orders');

Route::get('delivered_orders/{id}/{username}','api_order_controller@get_delivered_orders'); // to get the details of chef's delivered orders ...

Route::get('cancelled_orders/{id}/{username}','api_order_controller@get_cancelled_orders'); // to get the details of chef's cancelled orders ...

Route::get('declined_orders/{id}/{username}','api_order_controller@get_declined_orders'); // to get the details of chef's declined orders ...

Route::get('flagged_orders/{id}/{username}','api_order_controller@get_flagged_orders'); // to get the details of chef's flagged orders ...

Route::post('orders/create','api_order_controller@create_order'); // to create the new order of customer ...

Route::get('user/{id}/order/{order_id}/order_status','api_order_controller@update_order_status'); // accepting rejecting the status from chef side ...

Route::get('customer/{id}/order/{order_id}/order_status','api_order_controller@order_status_ByCustomer'); // accepting rejecting the status from chef side ...

// C-R-U-D operations for testing

Route::get('category_details/{id}',function($id){
        return new menu_itemCollection(Menu_Item::with('menu','item_recipe')->get());
});


/*
|--------------------------------------------------------------------------
| API Routes for Payment
|--------------------------------------------------------------------------
*/

Route::get('paid_orders/{id}/{username}','api_payment_controller@get_paid_orders'); // to get the details of paid orders ...

Route::get('unpaid_orders/{id}/{username}','api_payment_controller@get_unpaid_orders'); // to get the details of unpaid orders ...

/*
|--------------------------------------------------------------------------
| API Routes for Review
|--------------------------------------------------------------------------
*/

Route::get('all_reviews/{id}/{username}','api_rating_reviews_controller@get_all_reviews'); // to get the details of all reviews ...

Route::get('flagged_reviews/{id}/{username}','api_rating_reviews_controller@get_flagged_reviews'); // to get the details of flagged reviews ...

Route::get('nonflagged_reviews/{id}/{username}','api_rating_reviews_controller@get_nonflagged_reviews'); // to get the details of nonflagged reviews ...

/*
|--------------------------------------------------------------------------
| API Routes for Quick Meal
|--------------------------------------------------------------------------
*/

Route::get('free_quick_meal/{username}/{id}','api_quick_meal_controller@update_meal_price'); // method to free the quick meal ...

Route::post('store/quick_meal','api_quick_meal_controller@post_quick_meal'); // store the ad of the quick meal ...

Route::get('edit/quick_meal/{username}/{id}','api_quick_meal_controller@edit_quick_meal'); // update the quick meal ...

Route::put('update/quick_meal','api_quick_meal_controller@update_quick_meal'); // update the quick meal ...

Route::get('drop/quick_meal/{id}','api_quick_meal_controller@drop_quick_meal'); // drop the quick meal ...

Route::post('testing','api_menu_controller@test');

Route::post('testingIng','api_menu_controller@testIngredients');

// C-R-U-D operation for customer
Route::put('customerpic/{id}','api_customer_controller@edit') ;

Route::get('all_customer','api_customer_controller@get_all_customers');

Route::get('all_blocked_customer','api_customer_controller@get_blocked_customers');

Route::get('test','api_menu_controller@test');


/*
|--------------------------------------------------------------------------
| API Routes for general User
|--------------------------------------------------------------------------
*/

Route::get('/filter/response/{lat}/{lng}','api_filter_sorting_controller@response_filter_sort'); // to get the filtered data....

/*
|--------------------------------------------------------------------------
|  MAP API Routes for Orders
|--------------------------------------------------------------------------
*/

Route::post('maproute','api_payment_controller@maproute');


 /*
 |--------------------------------------------------------------------------
 |  MAP API Routes for API
 |--------------------------------------------------------------------------
 */

Route::post('ratings','api_rating_reviews_controller@store');

Route::post('maproute','api_payment_controller@maproute');


 Route::get('chef-customer-info/{id}','api_payment_controller@location_info');

