<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Admin;
use App\Menu;
use App\users;
use App\chef;


/*
|--------------------------------------------------------------------------
| View Routes for General
|--------------------------------------------------------------------------
*/

Route::get('/getJob','loginController@demo_job');



Route::get('/', function () {
    return view('index-logged-in');
});

Route::get('/logout',function(){
    return view('index-logged-out');
});

Route::get('/meal_page', function(){
    return view('meal-page');
});

Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('/order_confirmation', function () {
        return view('order-confirmation');
    });

    Route::get('/payment', function () {
        return view('payment');
    });
});

Route::get('/search_results', function(){
    return view('search-results');
});

Route::get('/reset_password', function(){
    return view('forgot_password_page');
});

Route::get('/expired_reset_password', function(){
    return view('expired_reset_password');
});
/*
|--------------------------------------------------------------------------
| View Routes for User
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('/user/dashboard', function () {
        return view('user.user-dashboard');
    });

    Route::get('/user/my_orders', function () {
        return view('user.user-my-orders');
    });

    Route::get('user/profile', function () {
        return view('user.user-profile');
    });

});

Route::get('chef/{id}/{username}/chef_page','web_chef_controller@view_complete_profile');

Route::get('meal/{username}/{id}/details','web_menu_controller@view_complete_meal');


/*
|--------------------------------------------------------------------------
| View Routes for Chef Dashboard
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('chef/add_category', function () {
        return view('chef.chef-add-category');
    });

    Route::get('chef/{id}/add_meal', 'web_view_controller@view_add_meal');

    Route::get('chef/add_quick_meal', function () {
        return view('chef.chef-add-quick-meal');
    });


    Route::get('chef/all_categories/{name}/{id}', function () {
        return view('chef.chef-all-categories');
    });

});


        //  ------- FOR ORDER REQUESTS -------  //


Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('chef/{id}/{name}/all_orders', 'web_view_controller@get_chef_all_orders'); // to get the all orders of the chef ...chef login

    Route::get('chef/{id}/{name}/pending_orders', 'web_view_controller@get_chef_pending_orders'); // to get the pending orders of the chef ...chef login

    Route::get('chef/{id}/{name}/ready_orders', 'web_view_controller@get_chef_ready_orders'); // to get the ready orders of the chef ... chef login

    Route::get('chef/{id}/{name}/delivered_orders', 'web_view_controller@get_chef_delivered_orders'); // to get the delivered orders of the chef .. chef login

    Route::get('chef/{id}/{name}/completed_orders', 'web_view_controller@get_chef_completed_orders'); // to get the completed orders of the chef ...

    Route::get('chef/{id}/{name}/cancelled_orders', 'web_view_controller@get_chef_cancelled_orders'); // to get the cancelled orders of the chef ...

    Route::get('chef/{id}/{name}/declined_orders', 'web_view_controller@get_chef_declined_orders'); // to get the declined orders of the chef ...

    Route::get('chef/{id}/{name}/processing_orders', 'web_view_controller@get_chef_processing_orders'); /// to get the processing orders of the chef ...

    Route::get('chef/{id}/{name}/flagged_orders', 'web_view_controller@get_chef_flagged_orders'); // to get the flagged orders of the chef ...

    Route::get('chef/{id}/{name}/paid_orders', 'web_view_controller@get_chef_paid_orders'); // to get the flagged orders of the chef ...

    Route::get('chef/{id}/{name}/unpaid_orders', 'web_view_controller@get_chef_unpaid_orders'); // to get the flagged orders of the chef ...

});

        //  ------- FOR REVIEW REQUESTS -------  //

Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('chef/{id}/{name}/all_reviews', 'web_view_controller@get_chef_all_reviews'); // to get the all reviews of the chef ...

    Route::get('chef/{id}/{name}/flagged_reviews', 'web_view_controller@get_chef_flagged_reviews'); // to get the flagged reviews of the chef ...

    Route::get('chef/{id}/{name}/nonflagged_reviews', 'web_view_controller@get_chef_nonflagged_reviews'); // to get the non flagged reviews of the chef ...

});

        //  ------- FOR MEALS REQUESTS -------  //

Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('chef/{id}/{name}/all_regular_meals', 'web_view_controller@get_chef_all_regular_meals'); // to get all regular meals of the chef ...

    Route::get('chef/{id}/{name}/all_quick_meals', 'web_view_controller@get_chef_all_quick_meals'); // to get all quick meals of the chef ...

    Route::get('chef/{id}/{name}/expired_quick_meals', 'web_view_controller@get_chef_expired_quick_meals'); // to get expired quick meals of the chef ...

    Route::get('chef/{id}/{name}/free_meals', 'web_view_controller@get_chef_free_meals'); // to get free meals of the chef ...

    Route::get('chef/{id}/{name}/all_categories', 'Web_admin_view_controller@get_chef_all_categories');

    Route::get('edit_quick_meal/{username}/meal/{id}','web_quick_meal_controller@edit_quick_meal');
});


Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('chef/all_quick_meals/{name}/{id}', function () {
        return view('chef.chef-all-quick-meals');
    });

    Route::get('chef/expired_quick_meals/{name}/{id}', function () {
        return view('chef.chef-expired-quick-meals');
    });

    Route::get('chef/free_meals/{name}/{id}', function () {
        return view('chef.chef-free-meals');
    });
});

Route::get('chef/location_thank_you',function(){
    return view('chef.chef-location-thank-you');
});

Route::get('chef/location',function(){
    return view('chef.chef-location');
});

Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('chef/panel', function () {
        return view('chef.chef-panel');
    });
});

//Route::get('chef/profile',function(){
//    return view('chef.chef-profile');
//});


//Route::get('chef/update_category',function(){
//    return view('chef.chef-update-category');
//});

/*
|--------------------------------------------------------------------------
| View Routes for Admin Dashboard
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('/admin/add_admin', function () {
        return view('admin.admin-add-admin'); //user type //login
    });
//
//Route::get('/admin/all_blocked_customers',function(){
//    return view('admin.admin-blocked-customers');});
//
//Route::get('/admin/all_admins',function(){
//    return view('admin.admin-all-admins');
//});

    Route::get('/admin/all_chefs', function () {
        return view('admin.admin-all-chefs');
    });
//
    Route::get('/admin/add_categories', function () {
        return view('admin.admin-add-category');
    });
//
//Route::get('/admin/all_orders',function(){
//    return view('admin.admin-all-orders');
//});

    Route::get('/admin/all_quick_meals', function () {
        return view('admin.admin-all-quick-meals');
    });

    Route::get('/admin/all_regular_meals', function () {
        return view('admin.admin-all-regular-meals');
    });

    Route::get('/admin/all_reviews', function () {
        return view('admin.admin-all-reviews');
    });

    Route::get('/admin/dashboard', function () {
        return view('admin.admin_dashboard');
    });

    Route::get('/admin_expired_quick_meals', function () {
        return view('admin.admin-expired-quick-meals');
    });

    Route::get('/admin_free_meals', function () {
        return view('admin.admin-free-meals');
    });

    Route::get('/admin_profile', function () {
        return view('admin.admin-profile');
    });
});

/*
|--------------------------------------------------------------------------
| View Routes for Admin Dashboard
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['check_session','web']],function() {
    Route::get('/chef/panel', function () {
        return view('chef.chef-panel');
    });
});

Route::get('/chef_page',function(){
    return view('chef_page');
})->name('chef_page');

Route::group(['middleware' => ['check_session','web']],function()
{
    Route::get('/chef_panel',function(){
        return view('chef_panel');
    })->name('chef_panel');
});

Route::get('/t','loginController@index')->name('t');

/*
|--------------------------------------------------------------------------
| WEB Routes for general User
|--------------------------------------------------------------------------
*/

// Routes to open the direct views ...

Route::get('/userLogin',function(){  return view('login'); }); // to open the login form ...

Route::get('/userSignup', function(){ return view('signup');  }); // to open the sign up form ...



// Routes to send requests on api for some response ...

Route::post('/register_customer','loginController@registerCustomer');// request to register the user as a customer ....

Route::get('/general/user-profile','Web_admin_view_controller@get_user_data_population');


Route::get('/chef/user-profile/{id}','Web_admin_view_controller@get_chef_data_population');

Route::post('/register_chef','loginController@registerChef');// request to register the user as a chef ....

Route::post('/justlogin','loginController@check'); // Login running in genuine gui....

Route::get('logout_request','loginController@logout_request'); // to flush the Session ID of the current logged in user ...

Route::group(['middleware' => ['check_session','web']],function() {

    Route::put('update_password', 'loginController@update_password'); // to send the request on api to update password ...

    Route::put('update_picture', 'loginController@update_picture'); // to send the request on api to update picture ...

});

Route::get('update_status', 'loginController@update_status'); // to send the request on api to update picture ...

Route::get('/forgot_password/request',function()
{
    return view('expired_reset_password');
});

Route::post('submit/forgot_password/request','loginController@forgotPswd');

Route::group(['middleware' => ['check_url_validity']],function()
{
    Route::get('reset_password/{email}/{time}',function($email,$time)
    {
        $data = ['email'=>$email ,'time'=> $time];
        return view('forgot_password_page',compact('data'));
    });
});


Route::post('submit/reset_password/request/{email}','loginController@reset_password');

Route::get('/send/mail','loginController@mail');

/*
|--------------------------------------------------------------------------
| WEB Routes for Admin
|--------------------------------------------------------------------------
*/

// Routes to open the direct views ...

// Routes to send requests on api for some response ...
//All admin routes needs  login and user type middleware

Route::get('/adminlogin',function(){  return view('login'); });

Route::group(['middleware' => ['check_session','web']],function() {

    Route::post('admin/create', 'web_admin_controller@create_admin'); // request to create the admin ...

    Route::put('admin', 'web_admin_controller@update_status'); // request to update the current status of admin ...

    Route::get('admin/delete/{id}', 'web_admin_controller@destroy_admin'); // request to delete the admin ...

    Route::get('admin/block/{id}', 'web_admin_controller@block_user'); // to block the user's account ...

    Route::get('admin/unblock/{id}', 'web_admin_controller@unblock_user'); // to unblock the user's account ...

    Route::get('/admin/all_admins', 'web_admin_controller@get_all_admin_details');

    Route::post('/admin/createadmin', 'Web_superadmin_controller@store');

    Route::get('/admin/all_customers', 'Web_admin_view_controller@get_all_customer');

    Route::get('admin/verify-unverify/{id}', 'web_chef_controller@chef_verify_unverify');

    Route::get('/admin/all_blocked_customers', 'Web_admin_view_controller@get_all_block_customer');

    Route::get('/admin/all_chefs', 'Web_admin_view_controller@get_all_chef');

    Route::get('/admin/online_chef', 'Web_admin_view_controller@get_online_chef');

    Route::get('/admin/online_chef', 'Web_admin_view_controller@get_online_chef');

    Route::get('/admin/all_orders', 'Web_admin_view_controller@get_all_orders');

    Route::get('/admin/cancelled', 'Web_admin_view_controller@get_admin_cancel_orders');

    Route::get('/admin/Declined', 'Web_admin_view_controller@get_admin_Declined_orders');

    Route::get('/admin/Paid', 'Web_admin_view_controller@get_admin_Paid_orders');

    Route::get('/admin/unPaid', 'Web_admin_view_controller@get_admin_unpaid_orders');

    Route::get('/admin/flagged-order', 'Web_admin_view_controller@get_admin_flagged_orders');

    Route::get('/admin/all_quick_meals', 'Web_admin_view_controller@get_admin_quick_orders');

    Route::get('/admin/all_expired_meals', 'Web_admin_view_controller@get_admin_expire_orders');

    Route::get('/admin/all_free_meals', 'Web_admin_view_controller@get_admin_free_orders');

    Route::get('admin/all_review', 'Web_admin_view_controller@get_admin_allreview_orders');

    Route::get('/admin/flagged', 'Web_admin_view_controller@get_admin_flagged_reviews');

    Route::get('/admin/non_flagged', 'Web_admin_view_controller@get_admin_nonflagged_reviews');

    Route::get('/admin/admin-profile/{id}', 'Web_admin_view_controller@get_admin_profile_load');

    Route::post('admin/update-profile/{id}', 'Web_admin_view_controller@get_admin_profile_update');

    Route::post('user/update-profile/{id}', 'Web_admin_view_controller@get_user_profile_update');

    Route::post('admin/update-profile', 'Web_admin_view_controller@get_admin_profile_update');

    Route::post('user/update-profile', 'Web_admin_view_controller@get_user_profile_update');

    Route::post('chef/update-profile/{id}', 'Web_admin_view_controller@get_chef_profile_update');

    Route::get('admin/verify-meal/{id}', 'Web_admin_view_controller@get_meal_recomend');

    Route::get('admin/delete-meal/{id}', 'Web_admin_view_controller@get_meal_delete');

});

Route::get('/admin/logged-out','loginController@get_admin_logout');

Route::get('/general/user-logout','loginController@get_user_logout');

Route::get('/general/user-all-orders','Web_admin_view_controller@get_user_all_orders');

Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('/accept/order/{id}/{status}', 'Web_chef_controller@get_order_accepted');


});

Route::get('/accept/order/{id}/{status}','web_chef_controller@get_order_accepted');




Route::get('/all_pop_up/{id}','Web_admin_view_controller@setValue');

Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('/admin/all_regular_meals', 'Web_admin_view_controller@get_regular_meals');

    Route::get('/admin/pending_orders', 'Web_admin_view_controller@get_admin_pending_orders');

    Route::get('/admin/ready_orders', 'Web_admin_view_controller@get_ready_orders');

    Route::get('/admin/all_categories', 'Web_admin_view_controller@get_all_categrories');

    Route::get('/admin/processing', 'Web_admin_view_controller@get_processsing_order');

    Route::get('/admin/deliver', 'Web_admin_view_controller@get_delivered_order');

    Route::get('/admin/verified_chef', 'Web_admin_view_controller@get_verified_chef');

    Route::get('/admin/blocked_chef', 'Web_admin_view_controller@get_blocked_chef');

    Route::get('all_orders', 'web_admin_controller@get_all_orders'); // request to get the all orders ...


});

Route::get('all_orders/','web_admin_controller@get_all_orders'); // request to get the all orders ...


/*
|--------------------------------------------------------------------------
| WEB Routes for Chef
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('/chef_registration', 'web_chef_controller@create'); // to open the chef registration form ...// login register

    Route::post('/chef_reg', 'web_chef_controller@check'); // to send the request on api for registration as chef ...

});

Route::get('/basic_email', 'MailController@basic_mail'); // to send the confirmation mail to chef ...

Route::get('/cheflist' , 'web_chef_controller@load');

Route::get('/chef_complete_profile/{id}','web_chef_controller@view_complete_profile');

/*
|--------------------------------------------------------------------------
| WEB Routes for Menu of Admin and Chef
|--------------------------------------------------------------------------
*/

// Routes to open the direct views ...

Route::get('/image_uploader',function(){ return view('item_image_upload'); });

Route::get('/image_show',function(){ return view('Item_imageShow'); });

// Routes to send requests on api for some response ...

// C-R-U-D routes for Menu ...

Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('pop_menu', 'web_menu_controller@pop_up_menu'); // send request to get the details of the public menu ...

    Route::post('menu', 'web_menu_controller@view_menu'); // send request to get the details of the menu for chef or admin ...

    Route::post('menu/create', 'web_menu_controller@create_menu'); // send request to create the menu name for the chef ...

    Route::put('menu/{id}', 'web_menu_controller@update_menu'); // send request to update the menu name of the chef ...

});
// C-R-U-D routes for Category ...

Route::post('category','web_menu_controller@view_category'); // send request to view the complete category details ....

Route::group(['middleware' => ['check_session','web']],function() {

//    Route::get('chef/{id}/{username}/add_meals',function($id,$username){
//        return view('chef.chef-add-meal');
//    });

    Route::post('chef/add_meal/create', 'web_menu_controller@create_category'); // send request to create the complete category ...

    Route::get('edit_meal/chef/{username}/meal/{id}', 'web_menu_controller@edit_category_item'); // send request to update the meal of the chef ...

    Route::post('update_meal/chef/{username}/category/{menu_id}/meal/{id}', 'web_menu_controller@update_category_item'); // send request to update the meal of the chef ...

    Route::get('meal/{id}/delete', 'web_menu_controller@delete_category_item'); // send request to delete the meal of the chef ...

    Route::delete('category/{id}', 'web_menu_controller@destroy_category'); // send request to delete the complete category ...

    Route::post('/admin/add_category', 'Web_admin_view_controller@get_add_category');
    Route::post('/chef/add_category', 'Web_admin_view_controller@chef_add_category');

    Route::post('chef/update-info', 'imageupload@store');
    Route::post('/chef/add_category', 'Web_admin_view_controller@chef_add_category');

    Route::get('/remove-category/{id} ', 'Web_admin_view_controller@delete_category');

});

// C-R-U-D routes for Category Items ...

Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('category_item/{id}', 'web_menu_controller@view_category_item'); // send request to view the item details ...

    Route::post('category_item', 'web_menu_controller@create_category_item'); // send request to create the item ...

    Route::put('category_item', 'web_menu_controller@update_category_item'); // send request to update the item ...

    Route::delete('category_item/{id}', 'web_menu_controller@destroy_category_item'); // send request to delete the complete item ...

});
// C-R-U-D routes for Item _ Recipe ...

Route::group(['middleware' => ['check_session','web']],function() {

    Route::post('item_recipe', 'web_menu_controller@view_item_recipe'); // send request to get the details of the item recipe ...

    Route::post('item_recipe/create', 'web_menu_controller@create_item_recipe'); // send request to create the item recipe ...

    Route::put('item_recipe', 'web_menu_controller@update_item_recipe'); // send request to update the existing item _ recipe ...

    Route::delete('item_recipe/{id}', 'web_menu_controller@destroy_item_recipe'); // send request to delete the existing item recipe ...

});
// C-R-U-D routes for item _ images ...

Route::group(['middleware' => ['check_session','web']],function() {

    Route::post('/item_images', 'web_menu_controller@view_item_images'); // send request to get the images of the item ...

    Route::post('/item_images/create', 'web_menu_controller@create_item_images'); // send request to create the images of the item ...

    Route::put('/item_images', 'web_menu_controller@update_item_images'); // send request to update the existing item image ...

    Route::delete('/item_images', 'web_menu_controller@destroy_item_images'); // send request to delete the existing item image ...

});
/*
|--------------------------------------------------------------------------
| WEB Routes for Order of Customer and Chef
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('order', function () {
        return view('order_form');
    });

    Route::get('orders', 'web_order_controller@view_orders');

    Route::post('order/create', 'web_order_controller@create_order'); // send request to create the order ....

    Route::post('order', 'web_order_controller@update_order'); // send request to view the details of the order ...

    Route::get('user/{id}/order/{order_id}/order_status', 'web_order_controller@update_order_status'); // Order status of chef and customer ...

    Route::get('getOrder', 'web_order_controller@view_order');

    Route::get('/re-order/{order}', 'web_order_controller@re_order');

Route::get('viewOrder',function(){
    return view('order_history');
});

});

/*
|--------------------------------------------------------------------------
| WEB Routes for Filter Sorting
|--------------------------------------------------------------------------
*/

Route::get('filter/request/{lat}/{lng}','web_filter_sorting_controller@request_filter_sort'); // to send request on api for filtered data ...

/*
|--------------------------------------------------------------------------
| WEB Routes for Quick Meal
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => ['check_session','web']],function() {

    Route::get('chef/add_quick_meal', function () {
        return view('chef.chef-add-quick-meal');
    });

    Route::get('hello', function () {
        return view('search-results');
    });

    Route::get('update_meal_price/{username}/meal/{id}', 'web_quick_meal_controller@update_meal_price');

    Route::post('create/quick_meal/{id}', 'web_quick_meal_controller@create_quick_meal'); // to create the quick meal ...

    Route::post('update/quick_meal/{username}/meal/{id}', 'web_quick_meal_controller@update_quick_meal'); // update the quick meal ...

    Route::delete('drop/quick_meal', 'web_quick_meal_controller@drop_quick_meal'); // drop the quick meal ...

});
/*
|--------------------------------------------------------------------------
| WEB Routes for testing
|--------------------------------------------------------------------------
*/

Route::get('/check',function(){
    return view('checksum');
});

Route::get('/home','loginController@go_home'); // checking the current session ...

Route::get('dragable' , function (){return view('dragablemap');});

Route::get('/flush','loginController@flush_do'); // flushing the session ...

Route::get('/flush','loginController@flush_do'); // flushing the session ...

Route::get('/location' , 'web_nearby_search_controller@ind');

Route::get('address' , 'web_nearby_search_controller@change_address');

//Route::get('/near_search','web_nearby_search_controller@searching') ;

Route::get('/near_search','web_nearby_search_controller@scopeDistance');

//Route::get('/near_search','web_nearby_search_controller@locate') ;

Route::get('places',function(){return view('place');});

//Route::get('near/','web_nearby_search_controller@scopeDistance');

//Route::get('near/','web_nearby_search_controller@scopeDistance');
Route::get('near' , 'web_nearby_search_controller@scopeDistance');

// -----------------------
//      image route
// -----------------------

Route::get('chefprofile','imageupload@ind');

Route::post('picture','imageupload@store');

Route::get('/showimage','imageupload@show');

// One to One relationship ...

Route::get('/admin/{id}/menu',function($id)
{
    return Admin::find($id)->menu->category_name;
});

Route::get('/open',function(){ return view('try_order'); });

Route::post('/showresult','web_menu_controller@wrap');


////////////////payment api route///

Route::get('start','web_Paymentcontroller@index');

Route::get('payments', 'web_Paymentcontroller@payment');


///////////// Direction api route ////////////////

Route::get('direction', 'web_direction_controller@create');

Route::get('direct','web_direction_controller@rote') ;

Route::get('direct','web_Paymentcontroller@route') ;


/////////////////web routes for customer////////////

Route::get('customer', 'web_customer_controller@create') ;

Route::post('userprofile','web_customer_controller@store_image') ;// storing image of customer


//////////// WEB ROUTES FOR ORDER ////////////////
////////////We routes for recbook login /////////

Route::get('facebook', 'loginController@redirectToProvider');

Route::get('userLogin/facebook/callback', 'loginController@handleProviderCallback');

Route::get('/facepass',function(){ return view('facepass'); });

Route::post('facepassed' , 'loginController@createuser');

//Route::get('auth/facebook', 'Auth\LoginController@redirectToProvider');
//Route::get('auth/facebook/callback', 'Auth\LoginController@handleProviderCallback');

/////     quick meal routes 	//////

//Route::get('/quick_meals',function(){  return view('quick_meal'); }); // to open the quick meal form ...
//Route::post('quick_meals','web_quick_meal_controller@add_quick_meal');

Route::post('/add_cart','web_cart_controller@addToCart');

//Route::post('quick_meals','api_quick_meal_controller@store');
//Route::post('quick_meals','api_quick_meal_controller@store');

/*
|--------------------------------------------------------------------------
| WEB Routes for Rating and reviews
|--------------------------------------------------------------------------
*/

Route::get('feedback-rating/{id}', 'web_rating_reviews_controller@check');


///////////// Resoon routes /////////////////////
Route::get('decline-reason/{id}/{status}', 'web_chef_controller@get_decline_reasons');
//
Route::get('flag-rating/{id}', 'web_rating_reviews_controller@get_order_flag');



Route::get('/address-info/{order}','web_view_controller@get_address_info') ;

