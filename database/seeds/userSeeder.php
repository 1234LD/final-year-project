<?php

use Illuminate\Database\Seeder;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\users::class,150)->create()->each(function($u){
            $u->chefs()->save(factory(\App\chef::class)->make());
        });

    }
}
