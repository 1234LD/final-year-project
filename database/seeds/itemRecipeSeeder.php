<?php

use Illuminate\Database\Seeder;

class itemRecipeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Item_Recipes::class,3)->create()->make();
    }
}
