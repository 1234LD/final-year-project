<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

    //    $this->call(userSeeder::class);
    //    $this->call(customerSeeder::class);
    //    $this->call(adminSeeder::class);
    //    $this->call(menuSeeder::class);
        $this->call(itemRecipeSeeder::class);

        Model::reguard();
    }
}
