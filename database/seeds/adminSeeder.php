<?php

use Illuminate\Database\Seeder;

class adminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\users::class,3)->create()->each(function($a_u){
            $a_u->admins()->save(factory(\App\Admin::class)->make());
        });
    }
}
