<?php

use Illuminate\Database\Seeder;

class customerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\users::class,150)->create()->each(function($c_u){
            $c_u->customers()->save(factory(\App\Customer::class)->make());
        });
    }
}
