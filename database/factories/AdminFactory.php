<?php

use Faker\Generator as Faker;

$factory->define(\App\Admin::class, function (Faker $faker) {
    return [
        'users_id' => factory(\App\users::class),
        'online_status'=>$faker->text(8)
    ];
});
