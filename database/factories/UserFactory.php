<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\users::class, function (Faker $faker) {
    return [
        'username' => $faker->name,
        'email' => $faker->email,
        'password' => \Illuminate\Support\Facades\Hash::make(str_random(15)),
        'usertype' => $faker->text(8),
        'facebook_ID' => $faker->text(10),
        'gmail' => $faker->text(10),
        'twitter' => $faker->text(10),
        'account_status' => $faker->boolean(true),
        'Activation' => $faker->text(8),
    ];
});
