<?php

use Faker\Generator as Faker;

$factory->define(\App\chef::class, function (Faker $faker) {
    return [
        'users_id' => factory(\App\users::class),
        'cnic'=>$faker->text(13),
        'phoneNumber'=>$faker->text(10),
        'city'=>$faker->text(8),
        'streetaddress'=>$faker->text(10),
        'state'=>$faker->text(10),
        'postalcode'=>$faker->randomNumber(),
        'workingHrs'=>$faker->randomNumber(2),
        'chef_level'=>$faker->text(5),
        'description'=>$faker->text(150),
        'delivery_type'=>$faker->text(10),
        'online_status'=>$faker->text(8),
        'start_time'=>$faker->time(),
        'end_time'=>$faker->time(),
    ];
});
