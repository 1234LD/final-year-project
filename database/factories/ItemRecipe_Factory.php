<?php

use Faker\Generator as Faker;

$factory->define(\App\Item_Recipes::class, function (Faker $faker) {
    return [
        'menu__item_id' =>factory(\App\Menu_Item::class),
        'description' =>$faker->text(100),
        'ingredient_name' =>$faker->text(50),
        'ingredient_qty' =>$faker->text(50),
    ];
});
