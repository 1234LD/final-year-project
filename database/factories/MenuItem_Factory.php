<?php

use Faker\Generator as Faker;

$factory->define(\App\Menu_Item::class, function (Faker $faker) {
    return [
        'menu_id' => factory(\App\Menu::class),
        'item_option' =>$faker->text(100),
        'item_name' =>$faker->text(15),
        'price' => $faker->randomNumber(4),
        'item_price' =>$faker->randomNumber(5),
        'item_time' =>$faker->time(),
    ];
});
