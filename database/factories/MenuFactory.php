<?php

use Faker\Generator as Faker;

$factory->define(\App\Menu::class, function (Faker $faker) {
    return [
        'admin_id' => factory(\App\Admin::class),
        'category_name' => $faker->text(20),
    ];
});
