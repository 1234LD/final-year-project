<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChefTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chef', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('users_id')->unsigned()->nullable();
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('cnic','15');
            $table->integer('chef_rating')->nullable();
            $table->string('phoneNumber','12');
            $table->string('city','150')->nullable();

            $table->string('streetaddress','255')->nullable();
            $table->string('state','150')->nullable();
            $table->string('postalcode','150')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();

            $table->string('menu_name',200)->nullable();
            $table->integer('workingHrs')->nullable();
            $table->string('chef_level')->nullable();
            $table->string('description','250')->nullable();
            $table->string('delivery_type',250)->nullable();
            $table->string('online_status')->nullable();
            $table->string('picture','100')->nullable();
            $table->boolean('verify',false)->nullable();
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
           // $table->string('Activation')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chef');
    }
}
