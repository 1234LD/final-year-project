<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned()->nullable();
            $table->integer('chef_id')->unsigned()->nullable();

            $table->foreign('customer_id')->references('id')->on('customer')->onDelete('cascade');
            $table->foreign('chef_id')->references('id')->on('chef')->onDelete('cascade');

            $table->double('sub_total')->nullable();
            $table->string('delivery_type','100')->nullable();
            $table->string('order_status','150')->nullable();
            $table->double('delivery_fare')->nullable();
            $table->double('grand_total')->nullable();
            $table->boolean('flag',false)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
