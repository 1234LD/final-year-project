<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('menu_id')->unsigned()->nullable();
            $table->integer('chef_id')->unsigned()->nullable();

            $table->foreign('menu_id')->references('id')->on('menu')->onDelete('cascade');
            $table->foreign('chef_id')->references('id')->on('chef')->onDelete('cascade');

            $table->string('item_name',200)->nullable();
            $table->string('item_price',1000)->nullable();
            $table->string('item_option',1000)->nullable();
            $table->integer('item_time')->nullable();
            $table->integer('rating')->nullable();
            $table->integer('item_quantity')->nullable();
            $table->boolean('meal_status',true)->nullable();
            $table->boolean('verify',false)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_items');
    }
}
