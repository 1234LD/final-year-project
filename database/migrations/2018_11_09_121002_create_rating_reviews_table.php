<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingReviewsTable extends Migration
{
    /**
     * Run the migrations ...
     */
    public function up()
    {
        Schema::create('rating_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned()->nullable();
            $table->integer('chef_id')->unsigned()->nullable();
            $table->integer('orders_id')->unsigned()->nullable();
            $table->integer('menu__item_id')->unsigned()->nullable();

            $table->foreign('customer_id')->references('id')->on('customer')->onDelete('cascade');
            $table->foreign('chef_id')->references('id')->on('chef')->onDelete('cascade');
            $table->foreign('orders_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('menu__item_id')->references('id')->on('menu_items')->onDelete('cascade');

            $table->string('headline')->nullable();
            $table->string('feedback')->nullable();
            $table->float('rating')->nullable();
            $table->string('status')->nullable();
            $table->tinyInteger('approved')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations ...
     */
    public function down()
    {
        Schema::dropIfExists('rating_reviews');
    }
}
