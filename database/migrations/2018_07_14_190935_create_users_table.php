<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->string('full_name',50)->nullable();
            $table->string('username',50)->nullable();
            $table->string('email',100)->nullable();
            $table->string('password',200)->nullable();
            $table->string('phone_number',20)->nullable();
            $table->string('usertype',10)->nullable();

            $table->double('facebook_ID',100)->nullable();
            $table->string('gmail',100)->nullable();
            $table->string('twitter',100)->nullable();
            $table->string('image','250')->nullable();
            $table->boolean('account_status',true)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
