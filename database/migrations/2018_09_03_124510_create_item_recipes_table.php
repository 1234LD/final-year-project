<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu__item_id')->unsigned()->nullable();
            $table->string('description',1000)->nullable();
            $table->string('ingredient_name',1000)->nullable();
            $table->string('ingredient_qty',1000)->nullable();

            $table->foreign('menu__item_id')->references('id')->on('menu_items')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_recipes');
    }
}
