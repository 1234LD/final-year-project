<!doctype html>
<html lang="en">

@include('includes.header')
<body>
@include('includes.body-front')

<!-- Wrapper -->
<div id="wrapper">
@include('includes.header-dashboard')
@include('includes.top-bar-loggedin')
<!-- Content
    ================================================== -->
    <div class="sign-in-form forgot-password-page">

        <div class="popup-tabs-container">
            <div class="popup-tab-content" id="forgot-password-03">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Reset your account password!</h3>
                </div>

                <!-- Form 03 -->
                <form action="{{url('submit/reset_password/request/'.$data['email'])}}" method="post" id="forgot-password-form-03">
                    {!! csrf_field() !!}
                    <div class="input-with-icon-left" title="Should be at least 8 characters long" data-tippy-placement="bottom">
                        <i class="icon-material-outline-lock"></i>
                        <input type="password" class="input-text with-border" name="password-register" id="fp-password-register" placeholder="New Password" required/>
                    </div>

                    <div class="input-with-icon-left">
                        <i class="icon-material-outline-lock"></i>
                        <input type="password" class="input-text with-border" name="password" id="fp-password-repeat-register" placeholder="Repeat Password" required/>
                    </div>
                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="forgot-password-form-03">Reset Password <i class="icon-material-outline-arrow-right-alt"></i></button>


            </div>
        </div>
    </div>
    @include('includes.footer')

</div>
@include('includes.body-scripts')

</body>
</html>