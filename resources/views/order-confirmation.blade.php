<!doctype html>
<html lang="en">

@include('includes.header')
<body>
@include('includes.body-front')

<!-- Wrapper -->
<div id="wrapper">

<!-- Content
================================================== -->
<div id="titlebar" class="gradient"> </div>

<!-- Container -->
<div class="container">
	<div class="row">
		<div class="col-md-12">

			<div class="order-confirmation-page">
				<div class="breathing-icon c-done"><i class="icon-feather-check"></i></div>
				<h2 class="margin-top-30">Your order# {{ $output->order->id }} has been placed!</h2>
				<p>Once your order is confirmed by the chef, you will be notified.</p>
				<a href="{{ URL::asset('/general/user-all-orders')}}" class="button ripple-effect-dark button-sliding-icon margin-top-30">My Orders<i class="icon-material-outline-arrow-right-alt"></i></a>
			</div>

		</div>
	</div>
</div>
<!-- Container / End -->


</div>
<!-- Wrapper / End -->
@include('includes.body-scripts')

</body>
</html>