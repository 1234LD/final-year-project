@extends('layouts.master_layout')
@section('content')
<!-- Page Content
================================================== -->
<div class="full-page-container with-map">
@include('general_boxes.filters')
<?php //$urls = $results->getUrlRange(1, 4); dd($urls[1]); ?>

	<!-- Full Page Content -->
	<div class="full-page-content-container" data-simplebar>
		<div class="full-page-content-inner">
			@if (Request::is('filter/request/*'))
			<h3 class="page-title">Search Results</h3>
            @endif
			@include('general_boxes.search-sorting')
			@include('general_boxes.result-quick-meals')
			@include('general_boxes.pagination')
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Full Page Content / End -->
		@include('includes.map')
</div>
@include('general_boxes.pin-location')
<script src="{{ URL::asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ URL::asset('js/infobox.min.js') }}"></script>
<script src="{{ URL::asset('js/markerclusterer.js') }}"></script>
<script>
    (function($){"use strict";function mainMap(){var ib=new InfoBox();function locationData(foodpole_xitemURL,foodpole_sourceLogo,foodpole_sourceName,foodpole_xitemTitle,verifiedBadge){return(''+
        '<a href="'+foodpole_xitemURL+'" class="foodpole-xitem-listing">'+
        '<div class="infoBox-close"><i class="icon-feather-x"></i></div>'+
        '<div class="foodpole-xitem-listing-details">'+
        '<div class="'+verifiedBadge+'-badge"></div>'+
        '<div class="foodpole-xitem-listing-foodpole-source-logo">'+
        '<img src="'+foodpole_sourceLogo+'" alt="">'+
        '</div>'+
        '<div class="foodpole-xitem-listing-description">'+
        '<h4 class="foodpole-xitem-listing-foodpole-source map-status '+foodpole_sourceName+'">'+foodpole_sourceName+'</h4>'+
        '<h3 class="foodpole-xitem-listing-title">Chef '+foodpole_xitemTitle+'</h3>'+
        '</div>'+
        '</div>'+
        '</a>')}

            <?php

//            $results->items() = $results->items();
            //                dd(collect($results->items())->unique('chef_id'));
            $locations = "";
            //                for ($j = 0; $j < sizeof(collect($results->items())->unique('chef_id')); $j++){
            //
            //				}
//                $count = sizeof($results->items());
//                $perpage = $results->perPage();
//                $pagesGone = ($results->currentPage() - 1);
//                $i = ($perpage * $pagesGone);
                $chef_ids = collect($results->items())->unique('chef_id');
                $chefs = $chef_ids->toArray();
                $number = count($chefs);

             foreach($chefs as $index => $me) {
                $url = URL::to('chef/'.$chefs[$index]['chef_id'].'/'.$chefs[$index]['username'].'/chef_page');
                $img = '';
                if (($chefs[$index]['chef_image']) == null) {
                    $img = URL::asset('images/food-placeholder.jpg');
                } else {
                    $img = URL::asset('chefimages/'.$chefs[$index]['chef_image'].'');
                }
                $chefname = $chefs[$index]['full_name'];
                $mealname = $chefs[$index]['item_name'];
                $verified = "not-verified";
                if ($chefs[$index]['chef_verify_status']) {
                    $verified = "verified";
                }
                $lat = $chefs[$index]['latitude'];
                $lng = $chefs[$index]['longitude'];
                $status = $chefs[$index]['online_status'];
                $rand = null;
                $other = '';
                $locations .= "[locationData('" . $url . "','" . $img . "','" . $status . "','" . $chefname . "','" . $verified . "'),'" . $lat . "','" . $lng . "','" . $rand . "','" . $other . "']";
//          $locations .= "[locationData('".$url.','.$img.','.$chefname.','.$mealname.','.$status.'),'.$lat.','.$lng.',5,'.$other.']';
                if ($index != $number - 1 ) {
                    $locations .= ",";
                }
//                }
//                $i++;
////                dd($chef_ids);
            } ?>
        var locations=[<?php echo ($locations); ?>];var mapZoomAttr=$('#map').attr('data-map-zoom');var mapScrollAttr=$('#map').attr('data-map-scroll');if(typeof mapZoomAttr!==typeof undefined&&mapZoomAttr!==false){var zoomLevel=parseInt(mapZoomAttr);}else{var zoomLevel=5;}
        if(typeof mapScrollAttr!==typeof undefined&&mapScrollAttr!==false){var scrollEnabled=parseInt(mapScrollAttr);}else{var scrollEnabled=false;}
        var map=new google.maps.Map(document.getElementById('map'),{zoom:zoomLevel,scrollwheel:scrollEnabled,center:new google.maps.LatLng(33.5651, 73.0169),mapTypeId:google.maps.MapTypeId.ROADMAP,zoomControl:false,mapTypeControl:false,scaleControl:false,panControl:false,navigationControl:false,streetViewControl:false,gestureHandling:'cooperative',styles:[{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#747474"},{"lightness":"23"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"color":"#f38eb0"}]},{"featureType":"poi.government","elementType":"geometry.fill","stylers":[{"color":"#ced7db"}]},{"featureType":"poi.medical","elementType":"geometry.fill","stylers":[{"color":"#ffa5a8"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#c7e5c8"}]},{"featureType":"poi.place_of_worship","elementType":"geometry.fill","stylers":[{"color":"#d6cbc7"}]},{"featureType":"poi.school","elementType":"geometry.fill","stylers":[{"color":"#c4c9e8"}]},{"featureType":"poi.sports_complex","elementType":"geometry.fill","stylers":[{"color":"#b1eaf1"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":"100"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"},{"lightness":"100"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffd4a5"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffe9d2"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"weight":"3.00"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"weight":"0.30"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#747474"},{"lightness":"36"}]},{"featureType":"road.local","elementType":"labels.text.stroke","stylers":[{"color":"#e9e5dc"},{"lightness":"30"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":"100"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#d2e7f7"}]}]});var boxText=document.createElement("div");boxText.className='map-box'
        var currentInfobox;var boxOptions={content:boxText,disableAutoPan:false,alignBottom:true,maxWidth:0,pixelOffset:new google.maps.Size(-160,0),zIndex:null,boxStyle:{width:"320px"},closeBoxMargin:"0",closeBoxURL:"",infoBoxClearance:new google.maps.Size(25,25),isHidden:false,pane:"floatPane",enableEventPropagation:false,};var markerCluster,overlay,i;var allMarkers=[];var clusterStyles=[{textColor:'white',url:'',height:50,width:50}];var markerIco;for(i=0;i<locations.length;i++){markerIco=locations[i][4];var overlaypositions=new google.maps.LatLng(locations[i][1],locations[i][2]),overlay=new CustomMarker(overlaypositions,map,{marker_id:i},markerIco);allMarkers.push(overlay);google.maps.event.addDomListener(overlay,'click',(function(overlay,i){return function(){ib.setOptions(boxOptions);boxText.innerHTML=locations[i][0];ib.close();ib.open(map,overlay);currentInfobox=locations[i][3];google.maps.event.addListener(ib,'domready',function(){$('.infoBox-close').click(function(e){e.preventDefault();ib.close();$('.map-marker-container').removeClass('clicked infoBox-opened');});});}})(overlay,i));}
        var options={imagePath:'images/',styles:clusterStyles,minClusterSize:3};markerCluster=new MarkerClusterer(map,allMarkers,options);google.maps.event.addDomListener(window,"resize",function(){var center=map.getCenter();google.maps.event.trigger(map,"resize");map.setCenter(center);});var zoomControlDiv=document.createElement('div');var zoomControl=new ZoomControl(zoomControlDiv,map);function ZoomControl(controlDiv,map){zoomControlDiv.index=1;map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);controlDiv.style.padding='5px';controlDiv.className="zoomControlWrapper";var controlWrapper=document.createElement('div');controlDiv.appendChild(controlWrapper);var zoomInButton=document.createElement('div');zoomInButton.className="custom-zoom-in";controlWrapper.appendChild(zoomInButton);var zoomOutButton=document.createElement('div');zoomOutButton.className="custom-zoom-out";controlWrapper.appendChild(zoomOutButton);google.maps.event.addDomListener(zoomInButton,'click',function(){map.setZoom(map.getZoom()+1);});google.maps.event.addDomListener(zoomOutButton,'click',function(){map.setZoom(map.getZoom()-1);});}
        $("#geoLocation, .input-with-icon.location a").click(function(e){e.preventDefault();geolocate();});function geolocate(){if(navigator.geolocation){navigator.geolocation.getCurrentPosition(function(position){var pos=new google.maps.LatLng(position.coords.latitude,position.coords.longitude);map.setCenter(pos);map.setZoom(12);});}}}
        var map=document.getElementById('map');if(typeof(map)!='undefined'&&map!=null){google.maps.event.addDomListener(window,'load',mainMap);}
        function singleListingMap(){var myLatlng=new google.maps.LatLng({lng:$('#singleListingMap').data('longitude'),lat:$('#singleListingMap').data('latitude'),});var single_map=new google.maps.Map(document.getElementById('singleListingMap'),{zoom:15,center:myLatlng,scrollwheel:false,zoomControl:false,mapTypeControl:false,scaleControl:false,panControl:false,navigationControl:false,streetViewControl:false,styles:[{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#747474"},{"lightness":"23"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"color":"#f38eb0"}]},{"featureType":"poi.government","elementType":"geometry.fill","stylers":[{"color":"#ced7db"}]},{"featureType":"poi.medical","elementType":"geometry.fill","stylers":[{"color":"#ffa5a8"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#c7e5c8"}]},{"featureType":"poi.place_of_worship","elementType":"geometry.fill","stylers":[{"color":"#d6cbc7"}]},{"featureType":"poi.school","elementType":"geometry.fill","stylers":[{"color":"#c4c9e8"}]},{"featureType":"poi.sports_complex","elementType":"geometry.fill","stylers":[{"color":"#b1eaf1"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":"100"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"},{"lightness":"100"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffd4a5"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffe9d2"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"weight":"3.00"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"weight":"0.30"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#747474"},{"lightness":"36"}]},{"featureType":"road.local","elementType":"labels.text.stroke","stylers":[{"color":"#e9e5dc"},{"lightness":"30"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":"100"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#d2e7f7"}]}]});$('#streetView').click(function(e){e.preventDefault();single_map.getStreetView().setOptions({visible:true,position:myLatlng});});var zoomControlDiv=document.createElement('div');var zoomControl=new ZoomControl(zoomControlDiv,single_map);function ZoomControl(controlDiv,single_map){zoomControlDiv.index=1;single_map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);controlDiv.style.padding='5px';var controlWrapper=document.createElement('div');controlDiv.appendChild(controlWrapper);var zoomInButton=document.createElement('div');zoomInButton.className="custom-zoom-in";controlWrapper.appendChild(zoomInButton);var zoomOutButton=document.createElement('div');zoomOutButton.className="custom-zoom-out";controlWrapper.appendChild(zoomOutButton);google.maps.event.addDomListener(zoomInButton,'click',function(){single_map.setZoom(single_map.getZoom()+1);});google.maps.event.addDomListener(zoomOutButton,'click',function(){single_map.setZoom(single_map.getZoom()-1);});}
            var singleMapIco="<i class='"+$('#singleListingMap').data('map-icon')+"'></i>";new CustomMarker(myLatlng,single_map,{marker_id:'1'},singleMapIco);}
        var single_map=document.getElementById('singleListingMap');if(typeof(single_map)!='undefined'&&single_map!=null){google.maps.event.addDomListener(window,'load',singleListingMap);}
        function CustomMarker(latlng,map,args,markerIco){this.latlng=latlng;this.args=args;this.markerIco=markerIco;this.setMap(map);}
        CustomMarker.prototype=new google.maps.OverlayView();CustomMarker.prototype.draw=function(){var self=this;var div=this.div;if(!div){div=this.div=document.createElement('div');div.className='map-marker-container';div.innerHTML='<div class="marker-container">'+
            '<div class="marker-card">'+
            '</div>'+
            '</div>'
            google.maps.event.addDomListener(div,"click",function(event){$('.map-marker-container').removeClass('clicked infoBox-opened');google.maps.event.trigger(self,"click");$(this).addClass('clicked infoBox-opened');});if(typeof(self.args.marker_id)!=='undefined'){div.dataset.marker_id=self.args.marker_id;}
            var panes=this.getPanes();panes.overlayImage.appendChild(div);}
            var point=this.getProjection().fromLatLngToDivPixel(this.latlng);if(point){div.style.left=(point.x)+'px';div.style.top=(point.y)+'px';}};CustomMarker.prototype.remove=function(){if(this.div){this.div.parentNode.removeChild(this.div);this.div=null;$(this).removeClass('clicked');}};CustomMarker.prototype.getPosition=function(){return this.latlng;};})(this.jQuery);


</script>

@endsection
