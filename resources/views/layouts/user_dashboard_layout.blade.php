<!doctype html>
<html lang="en">

@include('includes.header')

<body>

@include('includes.body-dashboard')

<div id="wrapper">
    @include('includes.header-dashboard')
    @include('includes.top-bar-loggedin')

    <div class="dashboard-container">
        @include('general_boxes.dashboard-sidebar-user')

        @yield('content')

    </div>

</div>

@include('includes.body-scripts')
</body>
</html>