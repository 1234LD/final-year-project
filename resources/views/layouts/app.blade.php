<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document - @yield('title')</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
<div>Hello World</div>
    @yield('content')
</div>

</body>
</html>