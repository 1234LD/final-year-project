<!doctype html>
<html lang="en">

    @include('includes.header')

    <body>


        @include('includes.body-front')

        <div id="wrapper">
            @include('includes.header-front')

            {{--@if(Session::has('user_id'))--}}
                @include('includes.top-bar-loggedin')
            {{--@else--}}
                {{--@include('includes.top-bar-loggedout')--}}
            {{--@endif--}}

            @yield('content')
            @if (!Request::is('filter/request/*'))
            @include('includes.footer')
            @endif
        </div>

        @include('includes.body-scripts')
    </body>
</html>