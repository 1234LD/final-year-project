<!doctype html>
<html lang="en">

@include('includes.header')

<body>

@include('includes.body-dashboard')

<div id="wrapper">
    @include('includes.header-dashboard')
    @include('includes.top-bar-loggedin')

    <div class="dashboard-container">
        @if(Session::get('user_type') == config('constants.DEFAULT_CHEF_TYPE'))
        @include('chef.chef-boxes.dashboard-sidebar')
        @endif

        @yield('content')

    </div>

</div>

@include('includes.body-scripts')
</body>
</html>