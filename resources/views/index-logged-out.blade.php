@extends('layouts.master_layout')
@section('content')
<!-- Intro Banner
================================================== -->
<!-- add class "disable-gradient" to enable consistent background overlay -->
<div class="intro-banner" data-background-image="images/app-background.jpg">
	<div class="container">
		
		<!-- Intro Headline -->
		<div class="row">
			<div class="col-md-12">
				<div class="banner-headline">
					<h3>
						<strong>Find home cooked meals any time, any where.</strong>
						<br>
						<span>Thousands of foodies use <strong class="color">Foodpole</strong> for a healthy & tasty lifestyle.</span>
					</h3>
				</div>
			</div>
		</div>
		
		<!-- Search Bar -->
		<div class="row">
			<div class="col-md-12">
				<div class="intro-banner-search-form margin-top-95">

					<!-- Search Field -->
					<div class="intro-search-field with-autocomplete">
						<label for="autocomplete-input" class="field-title ripple-effect">Where?</label>
						<div class="input-with-icon">
							<input id="autocomplete-input" type="text" placeholder="Your Location">
							<i class="icon-material-outline-location-on"></i>
						</div>
					<div class="intro-search-button locationbutton">
						<a href="#small-dialog-2" id ="markLocation" class="popup-with-zoom-anim"><button class="popup-with-zoom-anim button ripple-effect location-btn"></button></a>
					</div>						
					</div>

					<!-- Search Field -->
					<div class="intro-search-field">
						<label for ="intro-keywords" class="field-title ripple-effect">What would you like to have?</label>
						<input id="intro-keywords" type="text" placeholder="Food Title or Keywords">
					</div>

					<!-- Button -->
					<div class="intro-search-button">
						<button class="button ripple-effect" onclick="window.location.href='#'">Search</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Stats -->
		<div class="row">
			<div class="col-md-12">
				<ul class="intro-stats margin-top-45 hide-under-992px">
					<li>
						<strong class="counter">2,986</strong>
						<span>Orders Placed</span>
					</li>
					<li>
						<strong class="counter">3,543</strong>
						<span>Meals Served</span>
					</li>
					<li>
						<strong class="counter">1,232</strong>
						<span>Chefs Cooking</span>
					</li>
				</ul>
			</div>
		</div>

	</div>
</div>

<!-- Features Cities -->
<div class="section margin-top-65 margin-bottom-65">
	<div class="container">
		<div class="row">

			<!-- Section Headline -->
			<div class="col-xl-12">
				<div class="section-headline centered margin-top-0 margin-bottom-45">
					<h3>We are operating in</h3>
				</div>
			</div>

			<div class="col-xl-3 col-md-6">
				<!-- Photo Box -->
				<a href="foodpole-xitems-list-layout-1.html" class="photo-box" data-background-image="images/featured-city-01.jpg">
					<div class="photo-box-content">
						<h3>Islamabad</h3>
						<span class="hide">376 Chefs</span>
					</div>
				</a>
			</div>
			
			<div class="col-xl-3 col-md-6">
				<!-- Photo Box -->
				<a href="foodpole-xitems-list-layout-full-page-map.html" class="photo-box" data-background-image="images/featured-city-02.jpg">
					<div class="photo-box-content">
						<h3>Rawalpindi</h3>
						<span class="hide">645 Chefs</span>
					</div>
				</a>
			</div>
			
			<div class="col-xl-3 col-md-6">
				<!-- Photo Box -->
				<a href="foodpole-xitems-grid-layout-full-page.html" class="photo-box" data-background-image="images/featured-city-03.jpg">
					<div class="photo-box-content">
						<h3>Lahore</h3>
						<span class="hide">832 Chefs</span>
					</div>
				</a>
			</div>

			<div class="col-xl-3 col-md-6">
				<!-- Photo Box -->
				<a href="foodpole-xitems-list-layout-2.html" class="photo-box" data-background-image="images/featured-city-04.jpg">
					<div class="photo-box-content">
						<h3>Karachi</h3>
						<span class="hide">513 Chefs</span>
					</div>
				</a>
			</div>

		</div>
	</div>
</div>
<!-- Features Cities / End -->


<!-- Content
================================================== -->


<!-- Features Meals -->
<div class="section gray padding-top-65 padding-bottom-75">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				
				<!-- Section Headline -->
				<div class="section-headline margin-top-0 margin-bottom-35">
					<h3>Popular Meals</h3>
					<a href="foodpole-xitems-list-layout-full-page-map.html" class="headline-link">All Nearby Meals</a>
				</div>
				
				<!-- foodpole-xitems Container -->
				<div class="listings-container compact-list-layout margin-top-35">
					
					<!-- foodpole-xitem Listing -->
					<a href="#" class="foodpole-xitem-listing with-apply-button">

						<!-- foodpole-xitem Listing Details -->
						<div class="foodpole-xitem-listing-details">

							<!-- Logo -->
							<div class="foodpole-xitem-listing-foodpole-source-logo thumb">
								<img src="images/foodpole-source-logo-01.png" alt="">
							</div>

							<!-- Details -->
							<div class="foodpole-xitem-listing-description">
								<h3 class="foodpole-xitem-listing-title">Chicken Biryani</h3>

								<!-- foodpole-xitem Listing Footer -->
								<div class="foodpole-xitem-listing-footer">
									<ul>
										<li><i class="icon-material-outline-business"></i> Jurry Abbas <div class="verified-badge" title="Verified Chef" data-tippy-placement="top"></div></li>
										<li><i class="icon-material-outline-location-on"></i> Shalley Valley, Rawalpindi</li>
										<li><i class="icon-material-outline-star-border"></i> 4.5</li>
										<li><i class="icon-material-outline-access-time"></i> 30 min</li>
									</ul>
								</div>
							</div>

							<!-- Apply Button -->
							<span class="list-apply-button ripple-effect">Place Order</span>
						</div>
					</a>	


					<!-- foodpole-xitem Listing -->
					<a href="#" class="foodpole-xitem-listing with-apply-button">

						<!-- foodpole-xitem Listing Details -->
						<div class="foodpole-xitem-listing-details">

							<!-- Logo -->
							<div class="foodpole-xitem-listing-foodpole-source-logo thumb">
								<img src="images/foodpole-source-logo-05.png" alt="">
							</div>

							<!-- Details -->
							<div class="foodpole-xitem-listing-description">
								<h3 class="foodpole-xitem-listing-title">Zinger Burger</h3>

								<!-- foodpole-xitem Listing Footer -->
								<div class="foodpole-xitem-listing-footer">
									<ul>
										<li><i class="icon-material-outline-business"></i> Hamza Dabeer</li>
										<li><i class="icon-material-outline-location-on"></i> Commercial Market, Rawalpindi</li>
										<li><i class="icon-material-outline-star-border"></i> 4.5</li>
										<li><i class="icon-material-outline-access-time"></i> 45 min</li>
									</ul>
								</div>
							</div>

							<!-- Apply Button -->
							<span class="list-apply-button ripple-effect">Place Order</span>
						</div>
					</a>
					<!-- foodpole-xitem Listing -->
					<a href="#" class="foodpole-xitem-listing with-apply-button">

						<!-- foodpole-xitem Listing Details -->
						<div class="foodpole-xitem-listing-details">

							<!-- Logo -->
							<div class="foodpole-xitem-listing-foodpole-source-logo thumb">
								<img src="images/foodpole-source-logo-02.png" alt="">
							</div>

							<!-- Details -->
							<div class="foodpole-xitem-listing-description">
								<h3 class="foodpole-xitem-listing-title">Beef Stakes</h3>

								<!-- foodpole-xitem Listing Footer -->
								<div class="foodpole-xitem-listing-footer">
									<ul>
										<li><i class="icon-material-outline-business"></i> Naimat Shabbir <div class="verified-badge" title="Verified Chef" data-tippy-placement="top"></div></li>
										<li><i class="icon-material-outline-location-on"></i> Phase 2 Bahria Town, Rawalpindi</li>
										<li><i class="icon-material-outline-star-border"></i> 4.5</li>
										<li><i class="icon-material-outline-access-time"></i> 60 min</li>
									</ul>
								</div>
							</div>

							<!-- Apply Button -->
							<span class="list-apply-button ripple-effect">Place Order</span>
						</div>
					</a>
					

					<!-- foodpole-xitem Listing -->
					<a href="#" class="foodpole-xitem-listing with-apply-button">

						<!-- foodpole-xitem Listing Details -->
						<div class="foodpole-xitem-listing-details">

							<!-- Logo -->
							<div class="foodpole-xitem-listing-foodpole-source-logo thumb">
								<img src="images/foodpole-source-logo-03.png" alt="">
							</div>

							<!-- Details -->
							<div class="foodpole-xitem-listing-description">
								<h3 class="foodpole-xitem-listing-title">Daal Maash</h3>

								<!-- foodpole-xitem Listing Footer -->
								<div class="foodpole-xitem-listing-footer">
									<ul>
										<li><i class="icon-material-outline-business"></i> Wajid Raza</li>
										<li><i class="icon-material-outline-location-on"></i> Satellite Town, Rawalpindi</li>
										<li><i class="icon-material-outline-star-border"></i> 4.5</li>
										<li><i class="icon-material-outline-access-time"></i> 45 min</li>
									</ul>
								</div>
							</div>

							<!-- Apply Button -->
							<span class="list-apply-button ripple-effect">Place Order</span>
						</div>
					</a>

					<!-- foodpole-xitem Listing -->
					<a href="#" class="foodpole-xitem-listing with-apply-button">

						<!-- foodpole-xitem Listing Details -->
						<div class="foodpole-xitem-listing-details">

							<!-- Logo -->
							<div class="foodpole-xitem-listing-foodpole-source-logo thumb">
								<img src="images/foodpole-source-logo-05.png" alt="">
							</div>

							<!-- Details -->
							<div class="foodpole-xitem-listing-description">
								<h3 class="foodpole-xitem-listing-title">Chinese Chowmin</h3>

								<!-- foodpole-xitem Listing Footer -->
								<div class="foodpole-xitem-listing-footer">
									<ul>
										<li><i class="icon-material-outline-business"></i> Imran Khan</li>
										<li><i class="icon-material-outline-location-on"></i> Banni Gaala, Islamabad</li>
										<li><i class="icon-material-outline-star-border"></i> 4.5</li>
										<li><i class="icon-material-outline-access-time"></i> 15 min</li>
									</ul>
								</div>
							</div>

							<!-- Apply Button -->
							<span class="list-apply-button ripple-effect">Place Order</span>
						</div>
					</a>

				</div>
				<!-- foodpole-xitems Container / End -->

			</div>
		</div>
	</div>
</div>
<!-- Featured foodpole-xitems / End -->

<!-- Intro Banner
================================================== -->
<!-- add class "disable-gradient" to enable consistent background overlay -->
<div class="app-banner" app-background-image="images/app-background-3.jpg">
	<div class="container">
		
		<!-- Intro Headline -->
		<div class="row">
			<div class="col-md-12">
				<div class="banner-headline">
					<h3>
						<strong>Order on the go!</strong>
						<br>
						<span>Download <strong class="color">Foodpole</strong> for your smartphone and enjoy healthy meals.</span>
					</h3>
				</div>
			</div>
		</div>
		

		<!-- Stats -->
		<div class="row">
			<div class="col-md-12">
				<ul class="intro-stats margin-top-45">
					<li>
						<strong>Play Store</strong>
						<span><a href="#">Download</a></span>
					</li>
					<li>
						<strong>Appstore</strong>
						<span><a href="#">Download</a></span>
					</li>
				</ul>
			</div>
		</div>

	</div>
</div>

<!-- Highest Rated foodpole-users -->
<div class="section gray padding-top-65 padding-bottom-70 full-width-carousel-fix">
	<div class="container">
		<div class="row">

			<div class="col-xl-12">
				<!-- Section Headline -->
				<div class="section-headline margin-top-0 margin-bottom-25">
					<h3>Popular Chefs</h3>
					<a href="foodpole-users-grid-layout.html" class="headline-link">All Nearby Chefs</a>
				</div>
			</div>

			<div class="col-xl-12">
				<div class="default-slick-carousel foodpole-users-container foodpole-users-grid-layout">

					<!--foodpole-user -->
					<div class="foodpole-user">

						<!-- Overview -->
						<div class="foodpole-user-overview">
							<div class="foodpole-user-overview-inner">
								
								<!-- Bookmark Icon -->
								<span class="bookmark-icon"></span>
								
								<!-- Avatar -->
								<div class="foodpole-user-avatar">
									<div class="verified-badge"></div>
									<a href="single-foodpole-user-profile.html"><img src="images/user-avatar-big-01.jpg" alt=""></a>
								</div>

								<!-- Name -->
								<div class="foodpole-user-name">
									<h4><a href="single-foodpole-user-profile.html">Jurry Abbas</a></h4>
									<span>Rawalpindi</span>
								</div>

								<!-- Rating -->
								<div class="foodpole-user-rating">
									<div class="star-rating" data-rating="5.0"></div>
								</div>
							</div>
						</div>
						
						<!-- Details -->
						<div class="foodpole-user-details">
							<div class="foodpole-user-details-list">
								<ul>
									<li>Location <strong><i class="icon-material-outline-location-on"></i> Shalley Valley</strong></li>
									<li>Orders <strong>109</strong></li>
									<li>Items <strong>16</strong></li>
								</ul>
							</div>
							<a href="single-foodpole-user-profile.html" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
						</div>
					</div>
					<!-- foodpole-user / End -->

					<!--foodpole-user -->
					<div class="foodpole-user">

						<!-- Overview -->
						<div class="foodpole-user-overview">
							<div class="foodpole-user-overview-inner">
								
								<!-- Bookmark Icon -->
								<span class="bookmark-icon"></span>
								
								<!-- Avatar -->
								<div class="foodpole-user-avatar">
									<div class="verified-badge"></div>
									<a href="single-foodpole-user-profile.html"><img src="images/user-avatar-big-02.jpg" alt=""></a>
								</div>

								<!-- Name -->
								<div class="foodpole-user-name">
									<h4><a href="#">Naimat Shabbir</a></h4>
									<span>Rawalpindi</span>
								</div>

								<!-- Rating -->
								<div class="foodpole-user-rating">
									<div class="star-rating" data-rating="5.0"></div>
								</div>
							</div>
						</div>
						
						<!-- Details -->
						<div class="foodpole-user-details">
							<div class="foodpole-user-details-list">
								<ul>
									<li>Location <strong><i class="icon-material-outline-location-on"></i> Bahria Town</strong></li>
									<li>Orders <strong>95</strong></li>
									<li>Items <strong>11</strong></li>
								</ul>
							</div>
							<a href="single-foodpole-user-profile.html" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
						</div>
					</div>
					<!-- foodpole-user / End -->

					<!--foodpole-user -->
					<div class="foodpole-user">

						<!-- Overview -->
						<div class="foodpole-user-overview">
							<div class="foodpole-user-overview-inner">
								<!-- Bookmark Icon -->
								<span class="bookmark-icon"></span>
								
								<!-- Avatar -->
								<div class="foodpole-user-avatar">
									<a href="single-foodpole-user-profile.html"><img src="images/user-avatar-placeholder.png" alt=""></a>
								</div>

								<!-- Name -->
								<div class="foodpole-user-name">
									<h4><a href="#">Hamza Dabeer</a></h4>
									<span>Islamabad</span>
								</div>

								<!-- Rating -->
								<div class="foodpole-user-rating">
									<div class="star-rating" data-rating="4.9"></div>
								</div>
							</div>
						</div>
						
						<!-- Details -->
						<div class="foodpole-user-details">
							<div class="foodpole-user-details-list">
								<ul>
									<li>Location <strong><i class="icon-material-outline-location-on"></i> F-10 Markaz</strong></li>
									<li>Orders <strong>88</strong></li>
									<li>Items <strong>19</strong></li>
								</ul>
							</div>
							<a href="single-foodpole-user-profile.html" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
						</div>
					</div>
					<!-- foodpole-user / End -->

					<!--foodpole-user -->
					<div class="foodpole-user">

						<!-- Overview -->
						<div class="foodpole-user-overview">
							<div class="foodpole-user-overview-inner">
								
								<!-- Bookmark Icon -->
								<span class="bookmark-icon"></span>
								
								<!-- Avatar -->
								<div class="foodpole-user-avatar">
									<div class="verified-badge"></div>
									<a href="single-foodpole-user-profile.html"><img src="images/user-avatar-big-01.jpg" alt=""></a>
								</div>

								<!-- Name -->
								<div class="foodpole-user-name">
									<h4><a href="single-foodpole-user-profile.html">Jurry Abbas</a></h4>
									<span>Rawalpindi</span>
								</div>

								<!-- Rating -->
								<div class="foodpole-user-rating">
									<div class="star-rating" data-rating="5.0"></div>
								</div>
							</div>
						</div>
						
						<!-- Details -->
						<div class="foodpole-user-details">
							<div class="foodpole-user-details-list">
								<ul>
									<li>Location <strong><i class="icon-material-outline-location-on"></i> Shalley Valley</strong></li>
									<li>Orders <strong>109</strong></li>
									<li>Items <strong>16</strong></li>
								</ul>
							</div>
							<a href="single-foodpole-user-profile.html" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
						</div>
					</div>
					<!-- foodpole-user / End -->

					<!--foodpole-user -->
					<div class="foodpole-user">

						<!-- Overview -->
						<div class="foodpole-user-overview">
							<div class="foodpole-user-overview-inner">
								
								<!-- Bookmark Icon -->
								<span class="bookmark-icon"></span>
								
								<!-- Avatar -->
								<div class="foodpole-user-avatar">
									<div class="verified-badge"></div>
									<a href="single-foodpole-user-profile.html"><img src="images/user-avatar-big-02.jpg" alt=""></a>
								</div>

								<!-- Name -->
								<div class="foodpole-user-name">
									<h4><a href="#">Naimat Shabbir</a></h4>
									<span>Rawalpindi</span>
								</div>

								<!-- Rating -->
								<div class="foodpole-user-rating">
									<div class="star-rating" data-rating="5.0"></div>
								</div>
							</div>
						</div>
						
						<!-- Details -->
						<div class="foodpole-user-details">
							<div class="foodpole-user-details-list">
								<ul>
									<li>Location <strong><i class="icon-material-outline-location-on"></i> Bahria Town</strong></li>
									<li>Orders <strong>95</strong></li>
									<li>Items <strong>11</strong></li>
								</ul>
							</div>
							<a href="single-foodpole-user-profile.html" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
						</div>
					</div>
					<!-- foodpole-user / End -->

					<!--foodpole-user -->
					<div class="foodpole-user">

						<!-- Overview -->
						<div class="foodpole-user-overview">
							<div class="foodpole-user-overview-inner">
								<!-- Bookmark Icon -->
								<span class="bookmark-icon"></span>
								
								<!-- Avatar -->
								<div class="foodpole-user-avatar">
									<a href="single-foodpole-user-profile.html"><img src="images/user-avatar-placeholder.png" alt=""></a>
								</div>

								<!-- Name -->
								<div class="foodpole-user-name">
									<h4><a href="#">Hamza Dabeer</a></h4>
									<span>Islamabad</span>
								</div>

								<!-- Rating -->
								<div class="foodpole-user-rating">
									<div class="star-rating" data-rating="4.9"></div>
								</div>
							</div>
						</div>
						
						<!-- Details -->
						<div class="foodpole-user-details">
							<div class="foodpole-user-details-list">
								<ul>
									<li>Location <strong><i class="icon-material-outline-location-on"></i> F-10 Markaz</strong></li>
									<li>Orders <strong>88</strong></li>
									<li>Items <strong>19</strong></li>
								</ul>
							</div>
							<a href="single-foodpole-user-profile.html" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
						</div>
					</div>
					<!-- foodpole-user / End -->


				</div>
			</div>

		</div>
	</div>
</div>
<!-- Highest Rated foodpole-users / End-->
@include('general_boxes.pin-location')

<!-- Wrapper / End -->
@include('general_boxes.signin')

	@endsection
