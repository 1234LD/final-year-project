@extends('layouts.app')

@section('content')

    @if (session('status'))
        <div class="alert alert-danger">
            <center> {{ session('status') }} </center>
        </div>
    @endif

    <h1>Forgot Password ?</h1>

    <div class="well">

        <form action="{{url('submit/forgot_password/request')}}" method="POST" class="popup-form form-horizontal">
            {!! csrf_field() !!}
            <fieldset>
                <legend>Request for resetting the password</legend>

                <!-- Email -->
                <div class="form-group">
                    <div class="col-lg-10">
                        <input type="email" name="email" class="form-control form-white" placeholder="Enter email" required>
                    </div>
                </div>

                <!-- Submit Button -->
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-submit btn-lg btn-info pull-right">Send request</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>

@endsection
