@extends('layouts.chef_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>All Orders</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Orders</a></li>
						<li>All Orders</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">

				@include('chef.chef-boxes.chef-search-orders')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">


						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-feather-shopping-bag"></i> @if($orders != null){{ count($orders->data) }}@else 0 @endif Orders</h3>
						</div>


						<div class="content">
							<ul class="dashboard-box-list">
								@if($orders != null)
								@foreach($orders->data as $order_item)
								<li>
									<!-- Overview -->
									<div class="foodpole-user-overview manage-candidates">
										<div class="foodpole-user-overview-inner">

											<!-- Avatar -->

											<div class="foodpole-user-avatar">
												<a href="#"><img src="{{URL::asset('images/food-placeholder.jpg') }}" alt=""></a>
											</div>

											<!-- Name -->
											<div class="foodpole-user-name">
												<h4><a href="#">Order# {{ $order_item->id }}</a>
													@if($order_item->flag == true)
														<span class="dashboard-status-button red">Flagged</span>
													@else
														@if($order_item->order_status == config('constants.DEFAULT_ORDER_DELIVERED'))
															<span class="dashboard-status-button green">{{ $order_item->order_status }}</span>
															@if($order_item->payment != null && $order_item->payment->payment_status == true)
																<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
															@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
																<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															@endif
														@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_PENDING'))
															<span class="dashboard-status-button yellow">{{ $order_item->order_status }}</span>
															@if($order_item->payment != null && $order_item->payment->payment_status == true)
																<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
															@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
																<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															@endif
														@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_DECLINED'))
															<span class="dashboard-status-button red">{{ $order_item->order_status }}</span>
															@if($order_item->payment != null && $order_item->payment->payment_status == true)
																<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
															@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
																<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															@endif
														@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_CANCELLED'))
															<span class="dashboard-status-button red">{{ $order_item->order_status }}</span>
															@if($order_item->payment != null && $order_item->payment->payment_status == true)
																<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
															@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
																<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															@endif
														@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_READY'))
															<span class="dashboard-status-button green">{{ $order_item->order_status }}</span>
															@if($order_item->payment != null && $order_item->payment->payment_status == true)
																<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
															@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
																<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															@endif
														@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_COMPLETED'))
															<span class="dashboard-status-button green">{{ $order_item->order_status }}</span>
															@if($order_item->payment != null && $order_item->payment->payment_status == true)
																<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
															@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
																<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															@endif
														@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_PROCESSING'))
															<span class="dashboard-status-button green">{{ $order_item->order_status }}</span>
															@if($order_item->payment != null && $order_item->payment->payment_status == true)
																<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
															@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
																<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															@endif
														@endif
													@endif</h4>

												<!-- Details -->
												<span class="foodpole-user-detail-item"><strong>Subtotal:</strong> Rs. {{ $order_item->sub_total }}</span>
												{{--<span class="foodpole-user-detail-item"><strong>GST (17%):</strong> Rs. 210</span>--}}
												<span class="foodpole-user-detail-item"><strong>Delivery:</strong> Rs. {{ $order_item->delivery_fare }}</span>
												<span class="foodpole-user-detail-item"><strong>Grand Total:</strong> Rs. {{ $order_item->grand_total }}</span>
												
												<!-- Details -->
												<span class="foodpole-user-detail-item o-time-stamp">{{ \Carbon\Carbon::parse($order_item->created_at->date)->toDayDateTimeString() }}</span>
												
												
												<!-- fpOffer Details -->
												<ul class="dashboard-task-info fpOffer-info">
													@foreach($order_item->order_line_items->data as $item)
													<li><strong>{{ $item->menu_item->item_name }}</strong><span>{{ $item->item_option }} - x{{ $item->quantity }} - Rs.{{ $item->sub_total }}</span></li>
													@endforeach

												</ul>

												<!-- Buttons -->
												@if($order_item->order_status == config('constants.DEFAULT_ORDER_READY') ||
													$order_item->order_status == config('constants.DEFAULT_ORDER_PROCESSING') ||
												    $order_item->order_status == config('constants.DEFAULT_ORDER_PENDING') ||$order_item->order_status == config('constants.DEFAULT_ORDER_DECLINED' )
												    || $order_item->order_status == config('constants.DEFAULT_ORDER_CANCELLED'))
													<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">
                                                        @if($order_item->order_status == config('constants.DEFAULT_ORDER_READY')  && $order_item->flag == false)
															<a  href="{{ URL::to('/accept/order/'.$order_item->id.'/'.config('constants.DEFAULT_ORDER_DELIVERED')) }}"><span class="button ripple-effect order-green"><i class="icon-material-outline-check-circle"></i> Delivered</span></a>
															{{--<a href="#" target="_blank" class="button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Customer Info</a>--}}
															<a href="{{ URL::to('/address-info/'.$order_item->id)}}"  class="popup-with-zoom-anim button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Customer Info</a>
                                                        @elseif($order_item->order_status == config('constants.DEFAULT_ORDER_PENDING')  && $order_item->flag == false)


                                                            {{--<a href="{{ URL::to('/accept/order/'.$order_item->id.'/'.config('constants.DEFAULT_ORDER_PENDING')) }}" class="button ripple-effect order-green"><i class="icon-material-outline-access-time"></i> Accept</a>--}}
                                                            {{--<button  class="button ripple-effect order-red" onclick="chefReason($(this))"><i class="icon-line-awesome-times-circle-o"></i> Decline</button>--}}

                                                            <a href="{{ URL::to('/accept/order/'.$order_item->id.'/'.config('constants.DEFAULT_ORDER_PROCESSING')) }}" class="button ripple-effect order-green"><i class="icon-material-outline-access-time"></i> Accept</a>
                                                            <button  class="button ripple-effect order-red" onclick="chefReason($(this))"><i class="icon-line-awesome-times-circle-o"></i> Decline</button>

                                                            {{--<a href="#" target="_blank" class="popup-with-zoom-anim button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Customer Info</a>--}}
															<a href="{{ URL::to('/address-info/'.$order_item->id)}}"  class="popup-with-zoom-anim button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Customer Info</a>
                                                        @elseif($order_item->order_status == config('constants.DEFAULT_ORDER_PROCESSING') && $order_item->flag == false)
															<a href="{{ URL::to('/accept/order/'.$order_item->id.'/'.config('constants.DEFAULT_ORDER_READY')) }}" class="button ripple-effect order-green"><i class="icon-material-outline-check-circle"></i> Ready</a>
															{{--<a href="#" target="_blank" class="button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Customer Info</a>--}}
															<a href="{{ URL::to('/address-info/'.$order_item->id)}}"  class="popup-with-zoom-anim button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Customer Info</a>
														@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_DECLINED'))
															<div class="freelancer-rating margin-top-25">
																<h4>Your Reason</h4>
															 	<p>{{$order_item->reason->reason}}</p>
															</div>

														@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_CANCELLED'))
															<div class="freelancer-rating margin-top-25">
																<h4>Customer Reason</h4>
																<p>{{$order_item->reason->reason}}</p>
															</div>
															@elseif($order_item->reason->order_status == "flagged")
																<div class="freelancer-rating margin-top-25">
																	<h4>Your Reason</h4>
																	<p>{{$order_item->reason->reason}}</p>
																</div>
														@endif
                                                    </div>

												@endif


												@include('general_boxes.chef-reason-area')
												@include('general_boxes.chef-comment-box')
											</div>
										</div>
									</div>
								</li>
								@endforeach

								@endif

							</ul>
						</div>
					</div>
				</div>

			</div>
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->


@include('chef.chef-boxes.chef-response')
@include('chef.chef-boxes.decline-order')
@include('customer.customer-boxes.customer-route')

@endsection
