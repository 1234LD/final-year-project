@extends('layouts.chef_dashboard_layout')
@section('content')

    <!-- Dashboard Content
	================================================== -->
    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <!-- Dashboard Headline -->
            <div class="dashboard-headline">
                <h3>Edit Regular Meal</h3>

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="{{ URL::to('/') }}">Home</a></li>
                        <li><a href="#">Chef Panel</a></li>
                        <li>Add Regular Meal</li>
                    </ul>
                </nav>
            </div>

            <!-- Row -->
            <div class="row">

                <form id="f-add-meal" action="{{ URL::to('update_meal/chef/'.$meal->data[0]->chef->user->username.'/category/'.$meal->data[0]->menu->id.'/meal/'.$meal->data[0]->id) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div id="test1" class="dashboard-box margin-top-0">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-reorder"></i> Category</h3>
                            </div>

                            <div class="content with-padding">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>Category</h5>
                                            <input type="text"  name="category" class="with-border cat-field" value="{{ $meal->data[0]->menu->category_name }}" readonly>
                                        </div>
                                    </div>

                                    {{--<div class="col-xl-6">--}}
                                        {{--<div class="submit-field">--}}
                                            {{--<h5>Select Category</h5>--}}
                                            {{--<select class="selectpicker with-border all-cats" title="Select Category">--}}
                                                {{--<option>None</option>--}}
                                                {{--<option>Hamburger</option>--}}
                                                {{--<option>Anday wala burger</option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-restaurant"></i> Meal# </h3>
                                <div class="keyword-input-container meal-plus-cont">
                                    {{--<a class="keyword-input-button ripple-effect meal-plus meal-item-add" data-tippy-placement="left" title="Add another Meal"><i class="icon-material-outline-add"></i></a>--}}
                                </div>
                            </div>

                            <div class="content with-padding padding-bottom-0">

                                <div class="row">

                                    <div class="col-auto avatar-cont">
                                        <div class="avatar-wrapper new-meal" data-tippy-placement="bottom" title="Upload Featured Photo">
                                            @if($meal->data[0]->item_image->item_image != null)
                                                <img class="profile-pic" src="{{ URL::asset('item_images/'.$meal->data[0]->item_image->item_image[0]) }}" alt="" />
                                            @else
                                                <img class="profile-pic" src="{{ URL::asset('images/food-placeholder.jpg') }}" alt="" />
                                            @endif
                                            <div class="upload-button"></div>
                                            <input class="file-upload" type="file" name="menu_item[0][item_image][0]" accept="image/*"/>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="row">

                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Title</h5>
                                                    <input type="text" name="menu_item[0][item_name]" value="{{ $meal->data[0]->item_name }}" class="with-border">
                                                </div>
                                            </div>

                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>More Photos</h5>

                                                    <!-- Upload Button -->
                                                    <div class="uploadButton margin-top-0">
                                                        <input class="uploadButton-input" type="file" name="menu_item[0][item_image][1]" accept="image/*" id="upload" multiple/>
                                                        <label class="uploadButton-button ripple-effect" for="upload" onclick="updateNames($(this))">Upload Photos</label>
                                                        <span class="uploadButton-file-name">Maximum file size: 10 MB</span>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-restaurant"></i> Options</h3>
                                <div class="keyword-input-container meal-plus-cont">
                                    <a class="keyword-input-button ripple-effect meal-plus add-option" data-tippy-placement="left" title="Add Option"><i class="icon-material-outline-add"></i></a>
                                </div>
                            </div>

                            <div class="content with-padding padding-bottom-0 options-cont">
                                <?php $counter = count($meal->data[0]->item_price) ?>
                                <div class="hide qawsed">{{ $counter }}</div>
                                <div class="row">

                                    <div class="col">

                                        @for ($i = 0; $i < count($meal->data[0]->item_price); $i++)
                                            @for ($j = $i; $j <= $i; $j++)
                                        <div class="row">

                                            {{--@foreach($meal->data[0]->item_option as $key => $option)--}}
                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Option Name</h5>
                                                    <input type="text" class="with-border" name="menu_item[0][option_name][{{$j}}]" value="{{ $meal->data[0]->item_option[$j] }}">
                                                </div>
                                            </div>
                                            {{--@endforeach--}}

                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Price Rs.</h5>
                                                    <input type="number" class="with-border" name="menu_item[0][option_price][{{ $i }}]" value="{{ $meal->data[0]->item_price[$i] }}">
                                                </div>
                                            </div>
                                        </div>
                                            @endfor
                                        @endfor
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-restaurant"></i> Ingredients</h3>
                                <div class="keyword-input-container meal-plus-cont">
                                    <a class="keyword-input-button ripple-effect meal-plus add-ingredient" data-tippy-placement="left" title="Add Ingredient"><i class="icon-material-outline-add"></i></a>
                                </div>
                            </div>

                            <div class="content with-padding padding-bottom-0 ingredients-cont">
                                <?php $counter = count($meal->data[0]->item_recipe->ingredient_name) ?>
                                <div class="hide rftgyh">{{ $counter }}</div>
                                <div class="row">
                                    <div class="col">

                                        @for ($i = 0; $i < count($meal->data[0]->item_recipe->ingredient_name); $i++)
                                            @for ($j = $i; $j <= $i; $j++)
                                        <div class="row">

                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Name</h5>
                                                    <input type="text" class="with-border" name="menu_item[0][ingredient_name][{{ $i }}]"  value="{{ $meal->data[0]->item_recipe->ingredient_name[$i] }}">
                                                </div>
                                            </div>

                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Quantity</h5>
                                                    <input type="text" class="with-border" name="menu_item[0][ingredient_qty][{{ $j }}]" value="{{ $meal->data[0]->item_recipe->ingredient_qty[$j] }}">
                                                </div>
                                            </div>

                                        </div>
                                            @endfor
                                            @endfor
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-restaurant"></i> Recipe</h3>
                            </div>

                            <div class="content with-padding padding-bottom-0">

                                <div class="row">

                                    <div class="col">
                                        <div class="row">

                                            <div class="col-xl-12">
                                                <div class="submit-field">
                                                    <textarea name="menu_item[0][description]" cols="30" rows="5" class="with-border">{{ $meal->data[0]->item_recipe->description }}</textarea>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

            </div>

            <!-- Button -->
            <div class="col-xl-12">
                <button class="button ripple-effect big margin-top-30" onclick="document.getElementById('f-add-meal').submit();">Update</button>
            </div>
            </form>
            <!-- Row / End -->
            <div class="dashboard-footer-spacer"></div>
            @include('includes.footer-dashboard')

        </div>
    </div>
    <!-- Dashboard Content / End -->

    <div class="hide xwnmokp">0</div>

@endsection