@extends('layouts.chef_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Update Category</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Categories</a></li>
						<li>Update Category</li>
					</ul>
				</nav>
			</div>
	
					
				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div id="test1" class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-reorder"></i> Category</h3>
						</div>

						<div class="content with-padding">
							<div class="row">
								<div class="col-xl-6">
									<div class="submit-field">
										<h5>Update Category</h5>
										<input type="text" class="with-border" placeholder="e.g. Zinger Burger">
									</div>
								</div>
								
								<div class="col-xl-6">
									<div class="submit-field">
										<h5>Select Category</h5>
										<select class="selectpicker with-border" data-size="7" title="Select Category">
											<option selected>None of existing</option>
											<option>Hamburger</option>
											<option>Anday wala burger</option>
										</select>
									</div>
								</div>								
							</div>
						</div>
					</div>
				</div>
				
				<!-- Button -->
				<div class="col-xl-12">
					<a href="#" class="button ripple-effect big margin-top-30">Save</a>
				</div>				

			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->



@endsection