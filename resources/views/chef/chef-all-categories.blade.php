@extends('layouts.chef_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>All Categories</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{URL::to('/')}}">Home</a></li>
						<li><a href="#">Categories</a></li>
						<li>All Categories</li>
					</ul>
				</nav>
			</div>

		@include('admin.admin-boxes.admin-search-category')

		<!-- Dashboard Box -->
			<div class="col-xl-12">
				<div class="dashboard-box margin-top-25">

					<!-- Headline -->
					<div class="headline">
						<h3><i class="icon-material-outline-business-center"></i>Number of Categories {{count($allcategories)}}</h3>
					</div>

					<div class="content">
						<ul class="dashboard-box-list">
							@foreach($allcategories as $category)
								<li>
									<!-- foodpole-xitem Listing -->
									<div class="foodpole-xitem-listing">

										<!-- foodpole-xitem Listing Details -->
										<div class="foodpole-xitem-listing-details">

											<!-- Logo -->
											<div class="foodpole-xitem-listing-foodpole-source-logo thumb">
												<img src="{{URL::asset('images/food-placeholder.jpg')}}" alt="">
											</div>

											<!-- Details -->
											<div class="foodpole-xitem-listing-description">
												<h3 class="foodpole-xitem-listing-title">{{$category->category_name}}</h3>
											</div>
										</div>
									</div>
									<!-- Buttons -->
									<div class="buttons-to-right">
										{{--<a href="#" class="button red ripple-effect">Edit</a>--}}
										<a href="{{URL::to('remove-category/'.$category->id)}}" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
									</div>
								</li>
							@endforeach
					{{--<ul>--}}
						{{--<li><a href="#">Home</a></li>--}}
						{{--<li><a href="#">Categories</a></li>--}}
						{{--<li>All Categories</li>--}}
					{{--</ul>--}}
				{{--</nav>--}}
			{{--</div>--}}

			{{--@include('chef.chef-boxes.chef-search-category')--}}

				{{--<!-- Dashboard Box -->--}}
				{{--<div class="col-xl-12">--}}
					{{--<div class="dashboard-box margin-top-25">--}}

						{{--<!-- Headline -->--}}
						{{--<div class="headline">--}}
							{{--<h3><i class="icon-material-outline-business-center"></i> 5 Categories</h3>--}}
						{{--</div>--}}

						{{--<div class="content">--}}
							{{--<ul class="dashboard-box-list">--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/food-placeholder.jpg" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Chicken Biryani</h3>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/food-placeholder.jpg" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Nali Nehari</h3>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/food-placeholder.jpg" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Chicken Pulao</h3>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/food-placeholder.jpg" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Chinese Soup</h3>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">								--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/food-placeholder.jpg" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Badami Kulfa</h3>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>								--}}

							</ul>
						</div>
					</div>
				</div>

			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->



@endsection