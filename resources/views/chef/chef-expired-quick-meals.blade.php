@extends('layouts.chef_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Expired Quick Meals</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Meals</a></li>
						<li>Expired Quick Meals</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">

				@include('chef.chef-boxes.chef-search-meals')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-business-center"></i> <?php  $count = 0;
                                foreach ($meals->data as $index){
                                    if($index!= null)
                                    {
                                        if($index->menu->category_name == config('constants.DEFAULT_QUICK-MEAL_CATEGORY')){ $count++;}
                                    }
                                }?>{{ $count }} Meals</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								@foreach($meals->data as $meal_item)
									@if($meal_item != null)
								<li>
									<!-- foodpole-xitem Listing -->
									<div class="foodpole-xitem-listing">

										<!-- foodpole-xitem Listing Details -->
										<div class="foodpole-xitem-listing-details">

											<!-- Logo -->
											<div class="foodpole-xitem-listing-foodpole-source-logo thumb">
												@if($meal_item->item_image == null)
													<img src="{{ URL::asset('images/food-placeholder.jpg') }}" alt="">
												@else
													<img src="{{ URL::asset('item_images/'.$meal_item->item_image->item_image[0]) }}" alt="">
												@endif
											</div>

											<!-- Details -->
											<div class="foodpole-xitem-listing-description">
												<h3 class="foodpole-xitem-listing-title">{{ $meal_item->item_name }} </h3>

												<!-- foodpole-xitem Listing Footer -->
												<div class="foodpole-xitem-listing-footer">
													<ul>
														<li><i class="icon-feather-image"></i> {{ $meal_item->menu->category_name }}</li>
														<li><i class="icon-line-awesome-money"></i> Rs. {{ $meal_item->price }}</li>
														<li><i class="icon-feather-align-justify"></i> {{ $meal_item->item_quantity }} Plates</li>


														<li><i class="icon-material-outline-access-time"></i> Expired  {{ \Illuminate\Support\Carbon::parse($meal_item->updated_at->date)->diffForHumans() }}</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<!-- Buttons -->
									<div class="buttons-to-right">
										{{--@if($meal_item->price != 0)--}}
											{{--<a href="{{ URL::to('update_meal_price/'.Session::get('username').'/meal/'.$meal_item->id) }}" class="button red ripple-effect">Free</a>--}}
										{{--@endif--}}
										{{--<a href="{{ URL::to('meal/'.Session::get('username').'/'.$meal_item->id.'/details') }}" class="button red ripple-effect">View</a>--}}
										{{--<a href="{{ URL::to('edit_quick_meal/'.Session::get('username').'/meal/'.$meal_item->id) }}" class="button red ripple-effect">Edit</a>--}}
										<a href="{{ URL::to('meal/'.$meal_item->id.'/delete') }}" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
									</div>
								</li>
									@endif
								@endforeach
							</ul>
						</div>
					</div>
				</div>
</div>
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->



@endsection