@extends('layouts.chef_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Ready Orders</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Orders</a></li>
						<li>Ready</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">

				@include('chef.chef-boxes.chef-search-orders')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">


						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-feather-shopping-bag"></i> {{ count($orders->data) }} Order</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								@foreach($orders->data as $order_item)
								<li>
									<!-- Overview -->
									<div class="foodpole-user-overview manage-candidates">
										<div class="foodpole-user-overview-inner">

											<!-- Avatar -->
											<div class="foodpole-user-avatar">
												<a href="#"><img src="{{ URL::asset('images/food-placeholder.jpg') }}" alt=""></a>
											</div>

											<!-- Name -->
											<div class="foodpole-user-name">
												<h4><a href="#">Order# {{ $order_item->id }}</a> <span class="dashboard-status-button green">{{ $order_item->order_status }}</span>
													@if($order_item->payment != null && $order_item->payment->payment_status == true)
														<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
													@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
														<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
													@endif</h4>

												<!-- Details -->
												<span class="foodpole-user-detail-item"><strong>Subtotal:</strong> Rs. {{ $order_item->sub_total }}</span>
												{{--<span class="foodpole-user-detail-item"><strong>GST (17%):</strong> Rs. 210</span>--}}
												<span class="foodpole-user-detail-item"><strong>Delivery:</strong> Rs. {{ $order_item->delivery_fare }}</span>
												<span class="foodpole-user-detail-item"><strong>Grand Total:</strong> Rs. {{ $order_item->grand_total }}</span>
												

												<!-- fpOffer Details -->
												<ul class="dashboard-task-info fpOffer-info">
													@foreach($order_item->order_line_items->data as $item)
														<li><strong>{{ $item->menu_item->item_name }}</strong><span>{{ $item->item_option }} - x{{ $item->quantity }} - Rs.{{ $item->sub_total }}</span></li>
													@endforeach
												</ul>												

												<!-- Buttons -->
												<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">
													<a  href="{{ URL::to('/accept/order/'.$order_item->id.'/'.config('constants.DEFAULT_ORDER_DELIVERED')) }}"><span class="button ripple-effect order-green"><i class="icon-material-outline-check-circle"></i> Delivered</span></a>
													{{--<span class="button ripple-effect order-green"><i class="icon-material-outline-check-circle"></i> Delivered</span>	--}}
													{{--<a href="{{ URL::to('/address-info/'.$order_item->id)}}"class="button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Customer Info</a>--}}
                                                    <a href="{{ URL::to('/address-info/'.$order_item->id)}}" target="_blank" class="button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Customer Info</a>
													{{--<a href="{{ URL::to('/address-info/'.$order_item->id)}}" target="_blank" class="button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Customer Info</a>--}}
												</div>
											</div>
										</div>
									</div>
								</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>

			</div>
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->




@endsection