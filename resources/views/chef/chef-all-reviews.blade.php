@extends('layouts.chef_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>All Reviews</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Reviews</a></li>
						<li>All Reviews</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">

				@include('chef.chef-boxes.chef-search-reviews')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">


						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-feather-shopping-bag"></i> {{ count($reviews->data) }} Reviews</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								@foreach(collect($reviews->data)->unique('orders_id') as $review_item)
								<li>
									<!-- Overview -->
									<div class="foodpole-user-overview manage-candidates">
										<div class="foodpole-user-overview-inner">

											<!-- Avatar -->
											<div class="foodpole-user-avatar">
												<a href="#"><img src="{{ URL::asset('images/food-placeholder.jpg') }}" alt=""></a>
											</div>

											<!-- Name -->
											<div class="foodpole-user-name">
												<h4><a href="#">Order# {{ $review_item->orders_id }}</a> <span class="dashboard-status-button green">by {{ $review_item->order->customer->user->full_name }}</span></h4>

												<!-- Details -->
												<span class="foodpole-user-detail-item o-time-stamp">{{ \Carbon\Carbon::parse($review_item->created_at->date)->toDayDateTimeString() }}</span>
												<?php $avg = 0; $sum = 0; $count = 0;?>
												<ul class="dashboard-task-info fpOffer-info">
													@foreach($review_item->order->order_line_items->data as $item)
														<li><strong>{{ $item->menu_item->item_name }}</strong><div class="star-rating margin-top-5 or-item-rating" data-rating="{{ $item->menu_item->rating }}"></div></li>
														<?php $sum += $item->menu_item->rating; $count++; ?>
													@endforeach
												</ul>
												
												<!-- Rating -->
												<div class="foodpole-user-rating margin-top-25">
												    <h4>Customer Review</h4>
													<div class="star-rating margin-top-10" data-rating="{{round($avg = $sum/$count,1) }}"></div>
													<p> {{ $review_item->feedback }}</p>
												</div>
												
												<!-- Buttons -->
												{{--<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">--}}
													{{--<a href="#small-dialog-4" class="popup-with-zoom-anim button dark ripple-effect order-blue"><i class="icon-material-outline-thumb-up"></i> Leave a Comment</a>												--}}
												{{--</div>												--}}
											</div>
										</div>
									</div>
								</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>

			</div>
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->


@include('chef.chef-boxes.chef-response')
@include('chef.chef-boxes.decline-order')
@include('customer.customer-boxes.customer-route')

@endsection
