@extends('layouts.chef_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Add Quick Meal</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Chef Panel</a></li>
						<li>Add Quick Meal</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">

                <form action="{{ URL::to('create/quick_meal/'.Session::get('user_id')) }}" id="quick_meal_fm" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div id="test1" class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-lock"></i> Verify Phone Number</h3>
						</div>

						<div class="content with-padding">
							<div class="row">
								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Phone Number</h5>
										<input type="number" name="phone_number" class="with-border" placeholder="(0333)-(1234567)">
									</div>
								</div>
								
								<div class="col-xl-2">
									<div class="submit-field code-btn">
										<a href="#" class="button ripple-effect big margin-top-40">Code</a>
									</div>
								</div>

								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Code</h5>
										<input type="number" name="code" class="with-border">
									</div>
								</div>
								
								<div class="col-xl-2">
									<div class="submit-field code-btn">
										<a href="#" class="button ripple-effect big margin-top-40">Verify</a>
									</div>
								</div>								
							</div>
						</div>
					</div>
				</div>
				
				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-restaurant"></i> Meal</h3>
						</div>

						<div class="content with-padding padding-bottom-0">

							<div class="row">

								<div class="col-auto avatar-cont">
									<div class="avatar-wrapper" data-tippy-placement="bottom" title="Upload Featured Photo">
										<img class="profile-pic" src="images/food-placeholder.jpg" alt="" />
										<div class="upload-button"></div>
										<input class="file-upload" name="item_image[0]" type="file"accept="image/*"/>
									</div>
								</div>

								<div class="col">
									<div class="row">

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Title</h5>
												<input type="text" name="item_name" class="with-border" placeholder="Zafrani Biryani">
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Price Rs. (Per serving)</h5>
												<input type="number" name="price" class="with-border" placeholder="150">
											</div>
										</div>
										
										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Quantity or Servings</h5>
												<input type="number" name="quantity"  class="with-border" placeholder="10">
											</div>
										</div>										

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>More Photos</h5>
												
												<!-- Upload Button -->
												<div class="uploadButton margin-top-0">
													<input class="uploadButton-input" name="item_image[1]" type="file" accept="image/*" id="upload" multiple/>
													<label class="uploadButton-button ripple-effect" for="upload">Upload Photos</label>
													<span class="uploadButton-file-name">Maximum file size: 10 MB</span>
												</div>

											</div>
										</div>

										<div class="col-xl-12">
											<div class="submit-field">
												<h5>Description</h5>
												<textarea cols="30" rows="5" name="description" class="with-border">Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.</textarea>
											</div>
										</div>
										
										<div class="col-xl-12">
											<div class="submit-field">
												<h5>Meal free after post expires <label class="switch switch-qm"><input type="checkbox" name="switch" checked=""><span class="switch-button"></span></label></h5>
											</div>
										</div>										

									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				
				<!-- Button -->
				<div class="col-xl-12">
					<button id="quick_meal_fm" class="button ripple-effect big margin-top-30">Post Meal</button>
				</div>

				</form>
			</div>
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->



@endsection