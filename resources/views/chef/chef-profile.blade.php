@extends('layouts.chef_dashboard_layout')
@section('content')
<?php Session::put('status', $data->data[0]->chef->online_status );
 ?>
	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Settings</h3>
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Account</a></li>
						<li>Settings</li>
					</ul>
				</nav>
			</div>

			<!-- Row -->
			<form method="post" id="info-form"  action="{{URL::to('chef/update-profile/'.$data->data[0]->u_id)}}"  enctype="multipart/form-data">
				{{ csrf_field()}}
				<div class="row">

					<!-- Dashboard Box -->
					<div class="col-xl-12">
						<div class="dashboard-box margin-top-0">

							<!-- Headline -->
							<div class="headline">
								<h3><i class="icon-material-outline-account-circle"></i> My Account</h3>
								<!-- User Status Switcher -->
									<div class="status-switch chef-page-switch" id="snackbar-user-status">
										{{--<a href="{{ URL::to('update_status/'.config('constants.DEFAULT_STATUS_ONLINE')) }}">--}}
										<label class="user-online online @if(Session::get('status') == config('constants.DEFAULT_STATUS_ONLINE')) current-status @endif">Online</label>

										{{--<a href="{{ URL::to('update_status/'.config('constants.DEFAULT_STATUS_BUSY')) }}">--}}

										<label class="user-busy	busy @if(Session::get('status') == config('constants.DEFAULT_STATUS_BUSY')) current-status @endif">Busy</label>


										<!-- Status Indicator -->
										<span class="status-indicator" aria-hidden="true"></span>
									</div>
							</div>

							<div class="content with-padding padding-bottom-0">

								<div class="row">
									<div class="col-auto avatar-cont">
										<div class="avatar-wrapper" data-tippy-placement="bottom" title="Change Avatar">
											@if($data->data[0]->chef->picture == null)
												<img class="profile-pic" src="images/user-avatar-placeholder.png" alt="" />
											@else
												<img class="profile-pic" src="{{ URL::asset('chefimages/'.$data->data[0]->chef->picture)}}" alt="" />
											@endif

											<div class="upload-button"></div>
											<input class="file-upload" type="file" name="image_name" accept="image/*"/>
											{{--<input type="hidden" name="_token" value="{{csrf_token()}}"><br>--}}
										</div>

									{{--<div class="col-auto avatar-cont">--}}
										{{--<div class="avatar-wrapper" data-tippy-placement="bottom" title="Change Avatar">--}}

												{{--<img class="profile-pic" src="{{ URL::asset('images/user-avatar-placeholder.png')}}" alt="" />--}}
											{{--@else--}}
												{{--<img class="profile-pic" src="{{ URL::asset('chefimages/'.$data->data[0]->chef->picture)}}" alt="" />--}}
											{{--@endif--}}
												{{--<div class="upload-button"></div>--}}
												{{--<input class="file-upload" type="file" name="image_name" accept="image/*"/>--}}
											{{----}}
										{{--</div>--}}
									</div>

									<div class="col">
										<div class="row">

											<div class="col-xl-6">
												<div class="submit-field">
													<h5>Full Name</h5>
													<input type="text" name="name" data-validation="custom" data-validation-regexp="^([a-zA-Z]+)$" class="with-border" value="{{$data->data[0]->full_name}}">
												</div>
											</div>

											<div class="col-xl-6">
												<div class="submit-field">
													<h5>Username</h5>
													<input type="text" name="username" class="with-border"  value="{{$data->data[0]->username}} " readonly>
												</div>
											</div>

											<div class="col-xl-6">
												<div class="submit-field">
													<h5>Email</h5>
													<input type="email" name="email" class="with-border" value="{{$data->data[0]->email}}" readonly>
												</div>
											</div>

											<div class="col-xl-6">
												<div class="submit-field">
													<h5>Phone</h5>
													<input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"   name="numb"class="with-border" value="{{$data->data[0]->phone_number}}">
												</div>
											</div>

										</div>
									</div>
								</div>

							</div>
						</div>
					</div>


					<!-- Dashboard Box -->
					<div class="col-xl-12">
						<div id="test1" class="dashboard-box">

							<!-- Headline -->
							<div class="headline">
								<h3><i class="icon-material-outline-lock"></i> Password & Security</h3>
							</div>

							<div class="content with-padding">
								<div class="row">
									{{--<div class="col-xl-4">--}}
									{{--<div class="submit-field">--}}
									{{--<h5>Current Password</h5>--}}
									{{--<input type="password"  name="pswd"  value=""  class="with-border">--}}
									{{--</div>--}}
									{{--</div>--}}

									<div class="col-xl-6">
										<div class="submit-field">
											<h5>New Password</h5>
											<input type="password" name="pass" class="with-border" value="">
										</div>
									</div>

									<div class="col-xl-6">
										<div class="submit-field">
											<h5>Repeat New Password</h5>
											<input type="password" name="pass" class="with-border " value="">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Button -->
					{{--<div class="col-xl-12">--}}
						{{--<button class="button ripple-effect big margin-top-30"  id="profile-form">  Save Changes</button>--}}
					{{--</div>--}}
				<!-- Dashboard Box -->
					<div class="col-xl-12">
						<div class="dashboard-box">

							<!-- Headline -->
							<div class="headline">
								<h3><i class="icon-material-outline-account-circle"></i> Personal Info</h3>
							</div>

							<div class="content with-padding padding-bottom-0">

								<div class="row">

									<div class="col">
										<div class="row">

											<div class="col-xl-6">

												<div class="submit-field">
													<h5>CNIC# </h5>
													<input type="number" name="cnic" class="with-border" value="{{$data->data[0]->chef->cnic}}" placeholder="3820171348875" >
												</div>
											</div>

											<div class="col-xl-6">
												<div class="submit-field">
													<h5>City</h5>
													<input type="text" name="city" class="with-border" value="{{$data->data[0]->chef->city}}">
												</div>
											</div>

											<div class="col-xl-12">
												<div class="submit-field">
													<h5>Street Address</h5>
													<input type="text" class="with-border" name="address" value="{{$data->data[0]->chef->streetaddress}}" placeholder="105/1A feroz street mughalabad" >
												</div>
											</div>

											<div class="col-xl-6">
												<div class="submit-field">
													<h5>State</h5>
													<input type="text" class="with-border"  name="state" value="{{$data->data[0]->chef->state}}" placeholder="Punjab">
												</div>
											</div>

											<div class="col-xl-6">
												<div class="submit-field">
													<h5>Postal Code</h5>
													<input type="number" class="with-border" name="postalcode" value="{{$data->data[0]->chef->postalcode}}" placeholder="46000">
												</div>
											</div>

											<div class="col-xl-6">
												<div class="submit-field">
													<h5>Start-time</h5>
													<input type="time" class="with-border" name="starttime" value="{{$data->data[0]->chef->start_time}}" placeholder="03:00 PM">
												</div>
											</div>
											<div class="col-xl-6">
												<div class="submit-field">
													<h5>End-time</h5>
													<input type="time" class="with-border" name="endtime" value="{{$data->data[0]->chef->end_time}}" placeholder="03:00 PM - 06:00PM">
												</div>
											</div>
											<div class="col-xl-6">
												<div class="submit-field">
													<h5>Delivery Mode</h5>
													<select class="selectpicker with-border" name="deliverymode" data-size="7" title="Select Mode">
														<option selected>{{$data->data[0]->chef->delivery_type}}</option>
														<option>Home Delivery</option>
														<option>Take away</option>
														<option>Both</option>
													</select>
												</div>
											</div>

											<div class="col-xl-12">
												<div class="submit-field">
													<h5>About Me</h5>
													<textarea cols="30" rows="5" name="aboutme" class="with-border">{{$data->data[0]->chef->description}}</textarea>
												</div>
											</div>

										</div>
									</div>
								</div>

							</div>
						</div>
						<!-- Button -->
						<div class="col-xl-12">
							<button class="button ripple-effect big margin-top-30"  id="info-form">  Save Changes</button>

						</div>

					</div>
				</div>



			</form>
				</div>

			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

			{{--<li><a href="#">Home</a></li>--}}
						{{--<li><a href="#">Account</a></li>--}}
						{{--<li>Settings</li>--}}
					{{--</ul>--}}
				{{--</nav>--}}
			{{--</div>--}}
	{{----}}
			{{--<!-- Row -->--}}
			{{--<div class="row">--}}

				{{--<!-- Dashboard Box -->--}}
				{{--<div class="col-xl-12">--}}
					{{--<div class="dashboard-box margin-top-0">--}}

						{{--<!-- Headline -->--}}
						{{--<div class="headline">--}}
							{{--<h3><i class="icon-material-outline-account-circle"></i> My Account</h3>--}}
						{{--</div>--}}

						{{--<div class="content with-padding padding-bottom-0">--}}

							{{--<div class="row">--}}

								{{--<div class="col-auto avatar-cont">--}}
									{{--<div class="avatar-wrapper" data-tippy-placement="bottom" title="Change Avatar">--}}
										{{--<img class="profile-pic" src="images/user-avatar-placeholder.png" alt="" />--}}
										{{--<div class="upload-button"></div>--}}
										{{--<input class="file-upload" type="file" accept="image/*"/>--}}
									{{--</div>--}}
								{{--</div>--}}

								{{--<div class="col">--}}
									{{--<div class="row">--}}

										{{--<div class="col-xl-6">--}}
											{{--<div class="submit-field">--}}
												{{--<h5>Full Name</h5>--}}
												{{--<input type="text" class="with-border" value="Jurry Abbas">--}}
											{{--</div>--}}
										{{--</div>--}}
										{{----}}
										{{--<div class="col-xl-6">--}}
											{{--<div class="submit-field">--}}
												{{--<h5>Username</h5>--}}
												{{--<input type="text" class="with-border" value="jurryabbas">--}}
											{{--</div>--}}
										{{--</div>											--}}
										{{----}}
										{{--<div class="col-xl-6">--}}
											{{--<div class="submit-field">--}}
												{{--<h5>Email</h5>--}}
												{{--<input type="email" class="with-border" value="jurryabbas@foodpole.com">--}}
											{{--</div>--}}
										{{--</div>										--}}

										{{--<div class="col-xl-6">--}}
											{{--<div class="submit-field">--}}
												{{--<h5>Phone <span class="verified-badge vb-small" title="Verified Number" data-tippy-placement="top"></span></h5>--}}
												{{--<input type="number" class="with-border" value="03335583655">--}}
											{{--</div>--}}
										{{--</div>--}}
										{{----}}
										{{--<div class="col-xl-6">--}}
											{{--<div class="submit-field">--}}
												{{--<h5>CNIC# <span class="verified-badge vb-small" title="Verified CNIC" data-tippy-placement="top"></span></h5>--}}
												{{--<input type="number" class="with-border" value="38201-7134887-5">--}}
											{{--</div>--}}
										{{--</div>	--}}

										{{--<div class="col-xl-6">--}}
											{{--<div class="submit-field">--}}
												{{--<h5>City</h5>--}}
												{{--<input type="text" class="with-border" value="Rawalpindi">--}}
											{{--</div>--}}
										{{--</div>	--}}

										{{--<div class="col-xl-12">--}}
											{{--<div class="submit-field">--}}
												{{--<h5>Street Address</h5>--}}
												{{--<input type="text" class="with-border" value="105/1A feroz street mughalabad">--}}
											{{--</div>--}}
										{{--</div>--}}

										{{--<div class="col-xl-6">--}}
											{{--<div class="submit-field">--}}
												{{--<h5>State</h5>--}}
												{{--<input type="text" class="with-border" value="Punjab">--}}
											{{--</div>--}}
										{{--</div>--}}

										{{--<div class="col-xl-6">--}}
											{{--<div class="submit-field">--}}
												{{--<h5>Postal Code</h5>--}}
												{{--<input type="number" class="with-border" value="46000">--}}
											{{--</div>--}}
										{{--</div>--}}

										{{--<div class="col-xl-6">--}}
											{{--<div class="submit-field">--}}
												{{--<h5>Working hours</h5>--}}
												{{--<input type="text" class="with-border" value="03:00 PM - 06:00PM">--}}
											{{--</div>--}}
										{{--</div>	--}}

										{{--<div class="col-xl-6">--}}
											{{--<div class="submit-field">--}}
												{{--<h5>Delivery Mode</h5>--}}
												{{--<select class="selectpicker with-border" data-size="7" title="Select Mode">--}}
													{{--<option selected>Both</option>--}}
													{{--<option>Home Delivery</option>--}}
													{{--<option>Take Away</option>--}}
												{{--</select>--}}
											{{--</div>--}}
										{{--</div>		--}}

										{{--<div class="col-xl-12">--}}
											{{--<div class="submit-field">--}}
												{{--<h5>About Me</h5>--}}
												{{--<textarea cols="30" rows="5" class="with-border">Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.</textarea>--}}
											{{--</div>--}}
										{{--</div>										--}}

									{{--</div>--}}
								{{--</div>--}}
							{{--</div>--}}

						{{--</div>--}}
					{{--</div>--}}
				{{--</div>--}}


				{{--<!-- Dashboard Box -->--}}
				{{--<div class="col-xl-12">--}}
					{{--<div id="test1" class="dashboard-box">--}}

						{{--<!-- Headline -->--}}
						{{--<div class="headline">--}}
							{{--<h3><i class="icon-material-outline-lock"></i> Password & Security</h3>--}}
						{{--</div>--}}

						{{--<div class="content with-padding">--}}
							{{--<div class="row">--}}
								{{--<div class="col-xl-4">--}}
									{{--<div class="submit-field">--}}
										{{--<h5>Current Password</h5>--}}
										{{--<input type="password" class="with-border">--}}
									{{--</div>--}}
								{{--</div>--}}

								{{--<div class="col-xl-4">--}}
									{{--<div class="submit-field">--}}
										{{--<h5>New Password</h5>--}}
										{{--<input type="password" class="with-border">--}}
									{{--</div>--}}
								{{--</div>--}}

								{{--<div class="col-xl-4">--}}
									{{--<div class="submit-field">--}}
										{{--<h5>Repeat New Password</h5>--}}
										{{--<input type="password" class="with-border">--}}
									{{--</div>--}}
								{{--</div>--}}

								{{--<div class="col-xl-12">--}}
									{{--<div class="checkbox">--}}
										{{--<input type="checkbox" id="two-step" checked>--}}
										{{--<label for="two-step"><span class="checkbox-icon"></span> Enable Two-Step Verification via Email</label>--}}
									{{--</div>--}}
								{{--</div>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
				{{--</div>--}}
				{{----}}
				{{--<!-- Button -->--}}
				{{--<div class="col-xl-12">--}}
					{{--<a href="#" class="button ripple-effect big margin-top-30">Save Changes</a>--}}
				{{--</div>--}}

			{{--</div>--}}
			{{--<!-- Row / End -->--}}
			{{--<div class="dashboard-footer-spacer"></div>--}}
			{{--@include('includes.footer-dashboard')--}}

		</div>
	</div>
	<!-- Dashboard Content / End -->



@endsection