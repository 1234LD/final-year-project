<!doctype html>
<html lang="en">
@include('includes.header')

<body>

@include('includes.body-front')

<!-- Wrapper -->
<div id="wrapper">

<!-- Content
================================================== -->
<div id="titlebar" class="gradient"> </div>

<!-- Container -->
<div class="container">
	<div class="row">
		<div class="col-md-12">

			<div class="order-confirmation-page">
				<div class="breathing-icon"><i class="icon-line-awesome-location-arrow"></i></div>
				<h2 id="chef-loc" class="margin-top-30">Please enable access to verify location</h2>
				<a href="#" class="button ripple-effect-dark button-sliding-icon margin-top-30">Confirm Address<i class="icon-material-outline-arrow-right-alt"></i></a>
			</div>

		</div>
	</div>
</div>
<!-- Container / End -->


</div>
<!-- Wrapper / End -->
@include('includes.body-scripts')
</body>
</html>