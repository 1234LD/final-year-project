@extends('layouts.chef_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Add Regular Meal</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Chef Panel</a></li>
						<li>Add Regular Meal</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">
				
				<form id="f-add-meal" action="{{ URL::to('chef/add_meal/create') }}" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-reorder"></i> Category</h3>
						</div>

						<div class="content with-padding">
							<div class="row">
								<div class="col-xl-6">
									<div class="submit-field">
										<h5>New Category</h5>
										<input type="text"  name="category" class="with-border cat-field" placeholder="e.g. Zinger Burger">
									</div>
								</div>
								
								<div class="col-xl-6">
									<div class="submit-field">
										<h5>Select Category</h5>
										<select class="selectpicker with-border all-cats" title="Select Category">
											<option>None</option>
											@foreach($categories as $category)
											<option>{{ $category->category_name }}</option>
											@endforeach
										</select>
									</div>
								</div>								
							</div>
						</div>
					</div>
				</div>				
				
				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-restaurant"></i> Meal# 1</h3>
							<div class="keyword-input-container meal-plus-cont">
								<a class="keyword-input-button ripple-effect meal-plus meal-item-add" data-tippy-placement="left" title="Add another Meal"><i class="icon-material-outline-add"></i></a>
							</div>	
						</div>

						<div class="content with-padding padding-bottom-0">

							<div class="row">

								<div class="col-auto avatar-cont">
									<div class="avatar-wrapper new-meal" data-tippy-placement="bottom" title="Upload Featured Photo">
										<img class="profile-pic" src="images/food-placeholder.jpg" alt="" />
										<div class="upload-button"></div>
										<input class="file-upload" type="file" name="menu_item[0][item_image][0]" accept="image/*"/>
									</div>
								</div>

								<div class="col">
									<div class="row">

										<div class="col-xl-4">
											<div class="submit-field">
												<h5>Title</h5>
												<input type="text" name="menu_item[0][item_name]" class="with-border" placeholder="e.g. Mc Zinger Burger">
											</div>
										</div>


										<div class="col-xl-4">
											<div class="submit-field">
												<h5>Preparation Time (minutes)</h5>
												<input type="number" name="menu_item[0][item_time]" class="with-border" placeholder="e.g. 60">
											</div>
										</div>
										
										<div class="col-xl-4">
											<div class="submit-field">
												<h5>More Photos</h5>
												
												<!-- Upload Button -->
												<div class="uploadButton margin-top-0">
													<input class="uploadButton-input meal-0" type="file" name="menu_item[0][item_image][1]" accept="image/*" id="upload" multiple/>
													<label class="uploadButton-button ripple-effect" for="upload">Upload Photos</label>
													<span class="uploadButton-file-name">Maximum file size: 10 MB</span>
												</div>

											</div>
										</div>							

									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				
				
				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-restaurant"></i> Options</h3>
							<div class="keyword-input-container meal-plus-cont">
								<a class="keyword-input-button ripple-effect meal-plus add-option" data-tippy-placement="left" title="Add Option"><i class="icon-material-outline-add"></i></a>
							</div>					
						</div>

						<div class="content with-padding padding-bottom-0 options-cont">
						<div class="hide qawsed">0</div>
							<div class="row">

								<div class="col">
									<div class="row">

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Option Name</h5>
												<input type="text" class="with-border" name="menu_item[0][option_name][0]" placeholder="e.g. Single Patty Burger" required/>
											</div>
										</div>									

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Price Rs.</h5>
												<input type="number" class="with-border" name="menu_item[0][option_price][0]" placeholder="e.g. 300" required/>
											</div>
										</div>									
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>				

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-restaurant"></i> Ingredients</h3>
							<div class="keyword-input-container meal-plus-cont">
								<a class="keyword-input-button ripple-effect meal-plus add-ingredient" data-tippy-placement="left" title="Add Ingredient"><i class="icon-material-outline-add"></i></a>
							</div>								
						</div>

						<div class="content with-padding padding-bottom-0 ingredients-cont">
						<div class="hide rftgyh">0</div>
							<div class="row">
								<div class="col">
									<div class="row">

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Name</h5>
												<input type="text" class="with-border" name="menu_item[0][ingredient_name][0]"  placeholder="e.g. Mustard Sauce">
											</div>
										</div>
										
										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Quantity</h5>
												<input type="text" class="with-border" name="menu_item[0][ingredient_qty][0]" placeholder="e.g. 1 tea spoon">
											</div>
										</div>										

									</div>
								</div>
							</div>							

						</div>
					</div>
				</div>			

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-restaurant"></i> Recipe</h3>
						</div>

						<div class="content with-padding padding-bottom-0">

							<div class="row">

								<div class="col">
									<div class="row">									

										<div class="col-xl-12">
											<div class="submit-field">
												<textarea name="menu_item[0][description]" cols="30" rows="5" class="with-border">Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.</textarea>
											</div>
										</div>									

									</div>
								</div>
							</div>

						</div>
					</div>
				</div>

			</div>

            <!-- Button -->
            <div class="col-xl-12">
                <button class="button ripple-effect big margin-top-30" onclick="document.getElementById('f-add-meal').submit();">Save</button>
            </div>
            </form>
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->

    <div class="hide xwnmokp">0</div>

@endsection