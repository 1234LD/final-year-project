@extends('layouts.chef_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Paid Orders</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Orders</a></li>
						<li>Paid</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">

				@include('chef.chef-boxes.chef-search-orders')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">


						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-feather-shopping-bag"></i> {{ count($orders->data) }} Order</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								@foreach($orders->data as $order_item)
								<li>
									<!-- Overview -->
									<div class="foodpole-user-overview manage-candidates">
										<div class="foodpole-user-overview-inner">

											<!-- Avatar -->
											<div class="foodpole-user-avatar">
												<a href="#"><img src="{{ URL::asset('images/food-placeholder.jpg') }}" alt=""></a>
											</div>

											<!-- Name -->
											<div class="foodpole-user-name">
												<h4><a href="#">Order# {{ $order_item->orders_id }}</a>

													@if($order_item->order->flag == true)
														<span class="dashboard-status-button red">Flagged</span>
													@elseif($order_item->order->order_status == config('constants.DEFAULT_ORDER_READY') ||
															$order_item->order->order_status == config('constants.DEFAULT_ORDER_DELIVERED') ||
															$order_item->order->order_status == config('constants.DEFAULT_ORDER_PROCESSING'))
														<span class="dashboard-status-button green">{{ $order_item->order->order_status }}</span>
													@elseif($order_item->order->order_status == config('constants.DEFAULT_ORDER_CANCELLED') || $order_item->order->order_status == config('constants.DEFAULT_ORDER_DECLINED'))
														<span class="dashboard-status-button red">{{ $order_item->order->order_status }}</span>
													@elseif($order_item->order->order_status == config('constants.DEFAULT_ORDER_PENDING'))
														<span class="dashboard-status-button yellow">{{ $order_item->order->order_status }}</span>
													@endif
													<span class="dashboard-status-button green">{{ $order_item->payment_status }}</span></h4>

												<!-- Details -->
												<span class="foodpole-user-detail-item"><strong>Subtotal:</strong> Rs. {{ $order_item->order->sub_total }}</span>
												{{--<span class="foodpole-user-detail-item"><strong>GST (17%):</strong> Rs. 210</span>--}}
												<span class="foodpole-user-detail-item"><strong>Delivery:</strong> Rs. {{ $order_item->order->delivery_fare }}</span>
												<span class="foodpole-user-detail-item"><strong>Grand Total:</strong> Rs. {{ $order_item->order->grand_total }}</span>

												<!-- Details -->
												<span class="foodpole-user-detail-item o-time-stamp">{{ \Carbon\Carbon::parse($order_item->order->created_at->date)->toDayDateTimeString() }}</span>

												<!-- fpOffer Details -->
												<ul class="dashboard-task-info fpOffer-info">
													@foreach($order_item->order->order_line_items->data as $item)
														<li><strong>{{ $item->menu_item->item_name }}</strong><span>{{ $item->item_option }} - x{{ $item->quantity }} - Rs.{{ $item->sub_total }}</span></li>
													@endforeach
												</ul>												

												<!-- Buttons -->
												@if($order_item->order->order_status == config('constants.DEFAULT_ORDER_READY') ||
													$order_item->order->order_status == config('constants.DEFAULT_ORDER_PROCESSING') ||
												    $order_item->order->order_status == config('constants.DEFAULT_ORDER_PENDING'))
													<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">
														@if($order_item->order->order_status == config('constants.DEFAULT_ORDER_READY')  && $order_item->order->flag == false)
															<span class="button ripple-effect order-green"><i class="icon-material-outline-check-circle"></i> Delivered</span>
															<a href="#small-dialog-1"  class="popup-with-zoom-anim button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Customer Info</a>
														@elseif($order_item->order->order_status == config('constants.DEFAULT_ORDER_PENDING')  && $order_item->order->flag == false)
															<a href="#" class="button ripple-effect order-green"><i class="icon-material-outline-access-time"></i> Accept</a>
															<a href="#small-dialog-3"  class="popup-with-zoom-anim button ripple-effect order-red"><i class="icon-line-awesome-times-circle-o"></i> Decline</a>
															<a href="#small-dialog-1"  class="popup-with-zoom-anim button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Customer Info</a>
														@elseif($order_item->rder->order_status == config('constants.DEFAULT_ORDER_PROCESSING') && $order_item->order->flag == false)
															<span class="button ripple-effect order-green"><i class="icon-material-outline-check-circle"></i> Ready</span>
															<a href="#small-dialog-1"  class="popup-with-zoom-anim button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Customer Info</a>
														@endif
													</div>
												@endif
											</div>
										</div>
									</div>
								</li>
									@endforeach
							</ul>
						</div>
					</div>
				</div>

			</div>
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->

	@include('customer.customer-boxes.customer-route')
	@include('chef.chef-boxes.decline-order')
	@include('chef.chef-boxes.chef-response')


@endsection