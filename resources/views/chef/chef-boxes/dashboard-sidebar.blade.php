	<!-- Dashboard Sidebar
	================================================== -->
	<div class="dashboard-sidebar">
		<div class="dashboard-sidebar-inner" data-simplebar>
			<div class="dashboard-nav-container">

				<!-- Responsive Navigation Trigger -->
				<a href="#" class="dashboard-responsive-nav-trigger">
					<span class="hamburger hamburger--collapse" >
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</span>
					<span class="trigger-title">Dashboard Navigation</span>
				</a>
				
				<!-- Navigation -->
				<div class="dashboard-nav">
					<div class="dashboard-nav-inner">

						<ul data-submenu-title="Start">
							<li><a href="{{ URL::to('chef/panel') }}"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>
						</ul>
						
						<ul data-submenu-title="Organize and Manage">
							<li><a href="#"><i class="icon-feather-shopping-bag"></i> Orders</a>
								<ul>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/all_orders') }}">All</a></li>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/pending_orders') }}">Pending Response</a></li>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/ready_orders') }}">Ready</a></li>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/processing_orders') }}">Processing</a></li>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/delivered_orders') }}">Delivered</a></li>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/cancelled_orders') }}">Cancelled</a></li>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/declined_orders') }}">Declined</a></li>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/paid_orders') }}">Paid</a></li>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/unpaid_orders') }}">Unpaid</a></li>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/flagged_orders') }}">Flagged</a></li>
								</ul>	
							</li>
							<li><a href="#"><i class="icon-material-outline-rate-review"></i> Reviews</a>
								<ul>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/all_reviews') }}">All</a></li>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/flagged_reviews') }}">Flagged</a></li>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/nonflagged_reviews') }}">Non-flagged</a></li>
								</ul>	
							</li>	
							<li><a href="#"><i class="icon-material-outline-restaurant"></i> Meals</a>
								<ul>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/all_regular_meals') }}">All Regular Meals</a></li>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/all_quick_meals') }}">All Quick Meals</a></li>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/add_meal') }}">Add Regular Meal</a></li>
									<li><a href="{{ URL::to('chef/add_quick_meal') }}">Add Quick Meal</a></li>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/expired_quick_meals') }}">Expired Quick Meals</a></li>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/free_meals') }}">Free Meals</a></li>
								</ul>	
							</li>	
							<li><a href="#"><i class="icon-material-outline-reorder"></i> Categories</a>
								<ul>
									<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/all_categories') }}">All</a></li>
									{{--<li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/add_category') }}">Add New</a></li>	--}}
									<li>
										<a href="{{ URL::to('chef/add_category') }}">Add New</a></li>

								</ul>	
							</li>								
						</ul>

						<ul data-submenu-title="Account">
							<li class="active"><a href="{{ URL::asset('/chef/user-profile/'.Session::get('user_id'))}}"><i class="icon-material-outline-settings"></i> Settings</a></li>
							<li><a href="{{ URL::asset('/logout_request')}}"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
						</ul>
						
					</div>
				</div>
				<!-- Navigation / End -->

			</div>
		</div>
	</div>
	<!-- Dashboard Sidebar / End -->