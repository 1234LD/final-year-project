<!-- Highest Rated foodpole-users -->
<div class="section padding-bottom-70 full-width-carousel-fix">
	<div class="container">
		<div class="row">

			<div class="col-xl-12">
				<!-- Section Headline -->
				<div class="section-headline margin-top-0 margin-bottom-25">
					<h3>Meals by {{ $chef_menu->data[0]->user->full_name }}</h3>
				</div>
			</div>

			<div class="col-xl-12">
				<div class="default-slick-carousel foodpole-users-container foodpole-users-grid-layout">

					@foreach($chef_menu->data[0]->menu_items->data as $item)
					<!--Result -->
					<div class="foodpole-user">

						<!-- Overview -->
						<div class="foodpole-user-overview">
							<div class="foodpole-user-overview-inner">
								
								<!-- Bookmark Icon -->
								<span class="bookmark-icon"></span>
								
								<!-- Avatar -->
								<div class="foodpole-user-avatar">
									@if($item->verified)
									<div class="verified-badge rec-meal" title="FoodPole Recommended" data-tippy-placement="right"></div>
									@endif
									@if($item->item_image == null)
										<a href="#"><img src="{{ URL::asset('images/food-placeholder.jpg') }}" alt=""></a>
									@else
										<a href="#"><img src="{{ URL::asset('item_images/'.$item->item_image->item_image[0]) }}" alt=""></a>
									@endif
								</div>

								<!-- Name -->
								<div class="foodpole-user-name">
									<h4><a href="#">{{ $item->item_name }}</a></h4>
									<span>{{ $chef_menu->data[0]->user->full_name }}</span>
								</div>

								<!-- Rating -->
								<div class="foodpole-user-rating">
									<div class="star-rating" data-rating="{{ $item->rating }}"></div>
								</div>
							</div>
						</div>
						
						<!-- Details -->
						<div class="foodpole-user-details">
							<div class="foodpole-user-details-list search-results-dt">
								<ul>
									<li>Price <strong>Rs. {{ $item->price }}</strong></li>
									<li>Ready in <strong>{{ $item->item_time }} min</strong></li>
									@if($chef_menu->data[0]->delivery_type == config('constants.DEFAULT_BOTH_TYPE'))
										<li>Mode <strong>Delivery & Take Away</strong></li>
									@else
										<li>Mode <strong>{{ $chef_menu->data[0]->delivery_type }}</strong></li>
									@endif
								</ul>
							</div>
							<a href="{{ URL::to('meal/'.$chef_menu->data[0]->user->username.'/'.$item->id).'/details' }}" class="button button-sliding-icon ripple-effect ">More Details<i class="icon-material-outline-arrow-right-alt"></i></a>
						</div>
					</div>
					<!-- Result / End -->
					@endforeach

				</div>
			</div>

		</div>
	</div>
</div>
@include('general_boxes.meal-options-pop-up')