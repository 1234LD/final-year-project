@extends('layouts.chef_dashboard_layout')
@section('content')
	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Add Category</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Categories</a></li>
						<li>Add Category</li>
					</ul>
				</nav>
			</div>
	
					
				<!-- Dashboard Box -->
			<form method="post" id="login-form"  action="{{URL::to('/admin/add_category')}}">
				{{ csrf_field() }}
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-reorder"></i> Category</h3>
						</div>

						<div class="content with-padding">
							<div class="row">
								<div class="col-xl-12">
									<div class="submit-field">
										<h5>New Category</h5>
										<input type="text"  name="category_name" class="with-border" placeholder="e.g. Zinger Burger">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Button -->
				<div class="col-xl-12">
					<button class="button ripple-effect big margin-top-30" id="login-form" type="submit">Save</button>
				</div>


			</form>
				{{--<div class="col-xl-12">--}}
					{{--<div id="test1" class="dashboard-box margin-top-0">--}}

						{{--<!-- Headline -->--}}
						{{--<div class="headline">--}}
							{{--<h3><i class="icon-material-outline-reorder"></i> Category</h3>--}}
						{{--</div>--}}

						{{--<div class="content with-padding">--}}
							{{--<div class="row">--}}
								{{--<div class="col-xl-12">--}}
									{{--<div class="submit-field">--}}
										{{--<h5>New Category</h5>--}}
										{{--<input type="text" class="with-border" placeholder="e.g. Zinger Burger">--}}
									{{--</div>--}}
								{{--</div>								--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
				{{--</div>--}}
				{{----}}
				{{--<!-- Button -->--}}
				{{--<div class="col-xl-12">--}}
					{{--<a href="#" class="button ripple-effect big margin-top-30">Save</a>--}}
				{{--</div>				--}}

			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->



@endsection