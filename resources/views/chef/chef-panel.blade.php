@extends('layouts.chef_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Howdy, {{ Session::get('full_name') }}!</h3>
				<span>We are glad to see you again!</span>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li>Chef Panel</li>
						<li>Dashboard</li>
					</ul>
				</nav>
			</div>
	
			<!-- Fun Facts Container -->
			<div class="fun-facts-container">
				<div class="fun-fact" data-fun-fact-color="#36bd78">
					<div class="fun-fact-text">
						<span>Orders Served</span>
						<h4>{{ $renderedArray->orders }}</h4>
					</div>
					<div class="fun-fact-icon"><i class="icon-material-outline-restaurant"></i></div>
				</div>
				<div class="fun-fact" data-fun-fact-color="#b81b7f">
					<div class="fun-fact-text ff-revenue">
						<span>Revenue Rs.</span>
						@if($renderedArray->revenue == 0)
							<h4>{{ $renderedArray->revenue }}</h4>
						@else
							<h4>{{ $renderedArray->revenue[0]->Total }}</h4>
						@endif
					</div>
					<div class="fun-fact-icon"><i class="icon-line-awesome-money"></i></div>
				</div>
				<div class="fun-fact" data-fun-fact-color="#efa80f">
					<div class="fun-fact-text">
						<span>Self-delivery</span>

						<h4 class="percentage-symbol">{{ round($renderedArray->analysis->self_delivery) }}</h4>

					</div>
					<div class="fun-fact-icon"><i class="icon-material-outline-rate-review"></i></div>
				</div>

				<!-- Last one has to be hidden below 1600px, sorry :( -->
				<div class="fun-fact" data-fun-fact-color="#2a41e6">
					<div class="fun-fact-text">
						<span>Take away</span>

						<h4 class="percentage-symbol">{{ round($renderedArray->analysis->take_away) }}</h4>

					</div>
					<div class="fun-fact-icon"><i class="icon-feather-trending-up"></i></div>
				</div>
			</div>
			
			<!-- Row -->
			<div class="row">

				<div class="col-xl-12 hide">
					<!-- Dashboard Box -->
					<div class="dashboard-box main-box-in-row">
						<div class="headline">
							<h3><i class="icon-feather-bar-chart-2"></i> Delivery / Take Away Analysis</h3>
							<div class="sort-by">
								<select class="selectpicker hide-tick">
									<option>Last 6 Months</option>
									<option>This Year</option>
									<option>This Month</option>
								</select>
							</div>
						</div>
						<div class="content">
							<!-- Chart -->
							<div class="chart">
								<canvas id="chart" width="100" height="45"></canvas>
							</div>
						</div>
					</div>
					<!-- Dashboard Box / End -->
				</div>
				
				
			</div>
			<!-- Row / End -->
			
			<!-- Row -->
			<div class="row">

				<!-- Dashboard Box -->
				<div class="@if($renderedArray->meal_sale == null)col-xl-12 @else col-xl-6 @endif hide">
					<div class="dashboard-box">
						<table class="basic-table">
							<tr>
								<th>Peak Time Slot</th>
								<th>No. of Orders</th>
							</tr>
							
							<tr>
								<td data-label="Column 1">12:00 PM - 03:00 PM</td>
								<td data-label="Column 2">23</td>
							</tr>

							<tr>
								<td data-label="Column 1">03:00 PM - 06:00 PM</td>
								<td data-label="Column 2">22</td>
							</tr>

							<tr>
								<td data-label="Column 1">06:00 PM - 09:00 PM</td>
								<td data-label="Column 2">21</td>
							</tr>

							<tr>
								<td data-label="Column 1">09:00 PM - 12:00 AM</td>
								<td data-label="Column 2">19</td>
							</tr>

							<tr>
								<td data-label="Column 1">12:00 AM - 03:00 AM</td>
								<td data-label="Column 2">15</td>
							</tr>							
						</table>
					</div>
				</div>

					<div class="col-xl-12 @if($renderedArray->meal_sale == null) hide @endif">

					<div class="dashboard-box">
						<table class="basic-table">
							<tr>
								<th>Meals</th>
								<th>Orders</th>
								<th>Revenue</th>
							</tr>

							@if($renderedArray->meal_sale != null)
							@foreach($renderedArray->meal_sale as $meal)
								<tr>
									<td data-label="Column 1"><a href="{{ URL::to(''.$meal->id) }}">{{ $meal->item_name }}</a></td>
									<td data-label="Column 2">{{ $meal->total_order }}</td>
									<td data-label="Column 3">{{ $meal->price }}</td>
								</tr>
							@endforeach
							@endif
						</table>
					</div>
				</div>
				{{--@endif--}}

			</div>
			<!-- Row / End -->			

			<!-- Row -->
			<div class="row">

				<!-- Dashboard Box -->
				<div class="col-xl-6">
					<div class="dashboard-box">						
						<table class="basic-table">
							<tr>
								<th>Quickest Meals</th>
								<th>Time</th>
								<th>Chef</th>								
							</tr>

							@foreach($renderedArray->quick_meal as $quick)
								<tr>
									<td data-label="Column 1"><a href="{{ URL::to(''.$quick->itemID) }}">{{ $quick->item_name }}</a></td>
									<td data-label="Column 2">{{ $quick->item_time }} min</td>
									<td data-label="Column 3"><a href="{{ URL::to(''.$quick->chefID) }}">{{ $quick->full_name }}</a></td>
								</tr>
							@endforeach

						</table>						
					</div>
				</div>
				
				<div class="col-xl-6">
					<div class="dashboard-box">
						<table class="basic-table">
							<tr>
								<th>Cheapest Meals</th>
								<th>Price</th>
								<th>Chef</th>								
							</tr>

							@foreach($renderedArray->cheap_meal as $cheap)
								<tr>
									<td data-label="Column 1"><a href="#">{{ $cheap->item_name }}</a></td>
									<td data-label="Column 2">Rs. {{ $cheap->price }}</td>
									<td data-label="Column 3"><a href="#">{{ $cheap->full_name }}</a></td>
								</tr>
							@endforeach

						</table>
					</div>
				</div>				

			</div>
			<!-- Row / End -->
			
        @include('general_boxes.top-meals')
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->



@endsection