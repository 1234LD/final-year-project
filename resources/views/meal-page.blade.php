@extends('layouts.master_layout')
@section('content')
<?php //dd($meal->data[0]->chef); ?>
	@if(Session::has('customer_cart') )
		<?php $chefInCart;
		foreach(Session::get('customer_cart') as $user){
			$chefInCart = $user[0]['chef_id'];
		}?>
	@if($chefInCart != $meal->data[0]->chef->id)
			<h3 class="m-message">Kindly place separate orders for each chef. Thank you!</h3>

	@endif
	@endif
<!-- Titlebar
================================================== -->
	@if($meal->data[0]->item_image == null)
<div class="single-page-header" data-background-image="{{ URL::to('images/app-background.jpg') }}">
	@else
		<div class="single-page-header" data-background-image="{{ URL::to('item_images/'.$meal->data[0]->item_image->item_image[0]) }}">
			@endif
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="single-page-header-inner">
					<div class="left-side">
						@if($meal->data[0]->item_image == null)
							<div class="header-image"><a href=""><img src="{{ URL::to('images/food-placeholder.jpg') }}" alt=""></a></div>
						@else
							<div class="header-image"><a href=""><img src="{{ URL::to('item_images/'.$meal->data[0]->item_image->item_image[0]) }}" alt=""></a></div>
						@endif
						<div class="header-details margin-top-20">
							<h3>{{ $meal->data[0]->item_name }}</h3>
							<h5>{{ $meal->data[0]->chef->user->full_name }}</h5>
							<ul>
								<li><i class="icon-material-outline-location-on"></i>{{ $meal->data[0]->chef->city }}, {{ $meal->data[0]->chef->state }}</li>
								@if($meal->data[0]->rating != null)
								<li><div class="star-rating" data-rating="{{ $meal->data[0]->rating }}"></div></li>
								@endif
								<li><i class="icon-material-outline-access-time"></i> {{ $meal->data[0]->item_time }} min</li>
								@if($meal->data[0]->verified)
									<li><div class="verified-badge-with-title rec-meal-op">FoodPole Recommended</div></li>
								@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Page Content
================================================== -->
<div class="container">
	<div class="row">
		
		<!-- Content -->
		<div class="col-xl-8 col-lg-8 content-right-offset">

		@if($meal->data[0]->item_image->item_image != null)
			<div class=" slider-main-cont">
			@include('general_boxes.slider')
			</div>
		@endif
			<!-- Ingredients -->
			<div class="single-page-section margin-top-50">
				<h3>Ingredients</h3>
				<div class="task-tags">

					{{--@foreach($meal->data[0]->item_recipe as $key => $item)--}}
{{--{{ $item }}--}}
						{{--@switch($key)--}}
							{{--@case('item_option')--}}
								{{--@foreach($key as $i)--}}
									{{--{{ $i }} ---}}
							{{--@break--}}
							{{--@case('item_price')--}}
								{{--@foreach($key as $o)--}}
									{{--{{ $o }} --}}
							{{--@break--}}
							{{--@endforeach @endforeach--}}

						{{--@endswitch--}}

					{{--@endforeach--}}
					@if($meal->data[0]->menu->category_name != config('constants.DEFAULT_QUICK-MEAL_CATEGORY') )
						@for ($i = 0; $i < count($meal->data[0]->item_recipe->ingredient_name); $i++)
							@for ($j = $i; $j <= $i; $j++)
								<span>{{ $meal->data[0]->item_recipe->ingredient_name[$j] }} - {{ $meal->data[0]->item_recipe->ingredient_qty[$i] }}</span>
							@endfor
						@endfor
					@endif
				</div>
			</div>

			@if($meal->data[0]->item_recipe != null)
			<!-- Recipe -->
			<div class="single-page-section">
				<h3 class="margin-bottom-25">Recipe</h3>
				<p>{{ $meal->data[0]->item_recipe->description }}</p>
			</div>
			@endif
			
			<div class="clearfix"></div>
			
			<!-- foodpole-users offerings -->
			<div class="boxed-list margin-bottom-60">
				@if($meal->data[0]->ratings->data != null)
				<div class="boxed-list-headline">
					<h3><i class="icon-material-outline-group"></i> Customers Feedback</h3>
				</div>
				@endif
				<ul class="boxed-list-ul">
					@if($meal->data[0]->ratings->data != null)
						@foreach($meal->data[0]->ratings->data as $key => $rating)
							<li>
								<div class="fpOffer">
									<!-- Avatar -->
									<div class="fpOffers-avatar">
										<div class="foodpole-user-avatar">
											<div class="verified-badge" title="Verified Customer" data-tippy-placement="right"></div>
											@if($rating->customer->user->image != null)
												<a href="#"><img src="{{ URL::asset('chefimages/'.$rating->customer->user->image) }}" alt=""></a>
											@else
												<a href="#"><img src="{{ URL::asset('images/user-avatar-big-01.jpg') }}" alt=""></a>
											@endif
										</div>
									</div>

									<!-- Content -->
									<div class="fpOffers-content">
										<!-- Name -->
										<div class="foodpole-user-name">
											<h4><a href="#">{{ $rating->customer->user->full_name }}</a></h4>
											<div class="clearfix"></div>
											{{--@foreach(collect($arrFeed)->unique('orders_id') as $index)--}}
												<p class="feedback-content margin-top-10">{{ $rating->feedback }} </p>
											{{--@endforeach--}}
											<div class="star-rating" data-rating="{{ $rating->rating }}"></div>
										</div>
									</div>

                                <?php date_default_timezone_set('Asia/Karachi') ?>
								<!-- fpOffer -->
									<div class="fpOffers-fpOffer">
										<div class="fpOffer-rate">
											<div class="rate">{{ \Carbon\Carbon::parse($rating->created_at->date)->toFormattedDateString() }}</div>
											<span>{{ \Carbon\Carbon::parse($rating->created_at->date)->format('g:i A') }}</span>
										</div>
									</div>
								</div>
							</li>
						@endforeach
					@endif
				</ul>
			</div>

		</div>
		

		<!-- Sidebar -->
		<div class="col-xl-4 col-lg-4">
			<div class="sidebar-container">

				@if($meal->data[0]->menu->category_name == config('constants.DEFAULT_QUICK-MEAL_CATEGORY'))
				<div class="countdown green margin-bottom-35">{{ $meal->data[0]->item_quantity }} servings left</div>
				@endif

				<div class="sidebar-widget">
					<div class="offerings-widget">

						@if(Session::get('user_id') != $meal->data[0]->chef->user->id)
							<div class="offerings-headline"><h3>Order Now!</h3></div>
						@else
							<div class="offerings-headline"><h3>Your Meal Options</h3></div>
						@endif
						<div class="offerings-inner">
							<form id="snackbar-place-fpOffer" method="POST" @if(Session::get('user_id') != $meal->data[0]->chef->user->id) action="{{ URL::to('/add_cart') }}" @endif>

								{{ csrf_field() }}
							<!-- Fields -->
							<div class="offerings-fields meal-page-fields">
								<div class="offerings-field meal-page-field">
									@if(!($meal->data[0]->menu->category_name == config('constants.DEFAULT_QUICK-MEAL_CATEGORY')))
									<select class="selectpicker default" name="selection">
										@if($meal->data[0]->menu->category_name != config('constants.DEFAULT_QUICK-MEAL_CATEGORY') )
											@for ($i = 0; $i < count($meal->data[0]->item_price); $i++)
												@for ($j = $i; $j <= $i; $j++)
													<option value="{{ $meal->data[0]->item_option[$j].'^'.$meal->data[0]->item_price[$i] }}">{{ $meal->data[0]->item_option[$j] }} - Rs. {{ $meal->data[0]->item_price[$i] }}</option>
												@endfor
											@endfor
										@else
											<option value = "{{ 'nill'.'^'.$meal->data[0]->price }}">Rs. {{ $meal->data[0]->price }}</option>
										@endif
									{{--<select class="selectpicker default">--}}

										{{--<option selected>Single - Rs. {{ $i }}</option>--}}
										{{--<option>Double + 4 Kebabs + 1.5 Ltr Pepsi - Rs. 250<option>--}}
										{{--@endforeach--}}
									</select>
										@endif
								</div>
								<input type="hidden" name="chef_id" value="{{ $meal->data[0]->chef->id }}" class="hide">
								<input type="hidden" name="delivery_type" value="{{ $meal->data[0]->chef->delivery_type }}" class="hide">
								<input type="hidden" name="item_id" value="{{ $meal->data[0]->id }}" class="hide">
								<input type="hidden" name="item_name" value="{{$meal->data[0]->item_name }}" class="hide">
								@if($meal->data[0]->item_image != null)
								<input type="hidden" name="item_image" value="{{$meal->data[0]->item_image->item_image[0] }}" class="hide">
								@else
								<input type="hidden" name="item_image" value="{{'images/food-placeholder.jpg'}}" class="hide">
								@endif

								<div class="offerings-field meal-page-field">
									<!-- Quantity Buttons -->
									<div class="qtyButtons">
										<div class="qtyDec"></div>

										@if($meal->data[0]->menu->category_name != config('constants.DEFAULT_QUICK-MEAL_CATEGORY'))
										<input type="number" name="qty" value="1" min="1" max="50">
										@else
										<input type="number" name="qty" value="1" min="1" max="{{ $meal->data[0]->item_quantity }}">
										@endif

										{{--<input type="number" name="qty" value="1">--}}

										<div class="qtyInc"></div>
									</div>
								</div>
							</div>

							<!-- Button -->

							@if(Session::has('user_id'))
								@if(Session::get('user_id') != $meal->data[0]->chef->user->id)
									@if(Session::has('customer_cart'))
										<?php $chefInCart;
											foreach(Session::get('customer_cart') as $user){
												$chefInCart = $user[0]['chef_id'];
											}
										?>
											@if(($chefInCart == $meal->data[0]->chef->id))
												@if($meal->data[0]->chef->online_status == config('constants.DEFAULT_STATUS_ONLINE'))
													<button id="snackbar-place-fpOffer" class="button ripple-effect move-on-hover full-width margin-top-0 go_cart"><span>Add to order</span></button>
												@else
													<a id="snackbar-place-fpOffer" class="button ripple-effect move-on-hover full-width margin-top-0 {{ $meal->data[0]->chef->online_status }}-cart-btn"><span>{{ $meal->data[0]->chef->online_status }}</span></a>
												@endif
											@endif
									@else
										@if($meal->data[0]->chef->online_status == config('constants.DEFAULT_STATUS_ONLINE'))
											<button id="snackbar-place-fpOffer" class="button ripple-effect move-on-hover full-width margin-top-0 go_cart"><span>Add to order</span></button>
										@else
											<a id="snackbar-place-fpOffer" class="button ripple-effect move-on-hover full-width margin-top-0 {{ $meal->data[0]->chef->online_status }}-cart-btn"><span>{{ $meal->data[0]->chef->online_status }}</span></a>
										@endif
									@endif
								@endif
							@else
								@if(Session::has('customer_cart'))
									<?php $chefInCart;
									foreach(Session::get('customer_cart') as $user){
										$chefInCart = $user[0]['chef_id'];
									}
									?>
									@if(($chefInCart == $meal->data[0]->chef->id))
										@if($meal->data[0]->chef->online_status == config('constants.DEFAULT_STATUS_ONLINE'))
											<button id="snackbar-place-fpOffer" class="button ripple-effect move-on-hover full-width margin-top-0 go_cart"><span>Add to order</span></button>
										@else
											<a id="snackbar-place-fpOffer" class="button ripple-effect move-on-hover full-width margin-top-0 {{ $meal->data[0]->chef->online_status }}-cart-btn"><span>{{ $meal->data[0]->chef->online_status }}</span></a>
										@endif
									@endif
								@else
									@if($meal->data[0]->chef->online_status == config('constants.DEFAULT_STATUS_ONLINE'))
										<button id="snackbar-place-fpOffer" class="button ripple-effect move-on-hover full-width margin-top-0 go_cart"><span>Add to order</span></button>
									@else
										<a id="snackbar-place-fpOffer" class="button ripple-effect move-on-hover full-width margin-top-0 {{ $meal->data[0]->chef->online_status }}-cart-btn"><span>{{ $meal->data[0]->chef->online_status }}</span></a>
									@endif
								@endif
							@endif
							</form>
						</div>
						@if(!Session::has('user_id'))
						<div class="offerings-signup">Don't have an account? <a href="#small-dialog-6" class="register-tab sign-in popup-with-zoom-anim">Sign Up</a></div>
						@endif
					</div>
				</div>

				<!-- Sidebar Widget -->
				<div class="sidebar-widget">
					<h3>Bookmark or Share</h3>

					<!-- Bookmark Button -->
					<button class="bookmark-button margin-bottom-25">
						<span class="bookmark-icon"></span>
						<span class="bookmark-text">Bookmark</span>
						<span class="bookmarked-text">Bookmarked</span>
					</button>
					<!-- Copy URL -->
					<div class="copy-url">
						<input id="copy-url" type="text" value="" class="with-border">
						<button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>
					</div>


					<!-- Share Buttons -->
					<div class="share-buttons margin-top-25">
						<div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
						<div class="share-buttons-content">
							<span>Interesting? <strong>Share It!</strong></span>
							<ul class="share-buttons-icons">
								<li><a href="http://www.facebook.com/dialog/share?app_id=2605732326105450&display=popup&href={{ URL::to('meal/'.$meal->data[0]->chef->user->username.'/'.$meal->data[0]->id).'/details' }}&name={{ $meal->data[0]->chef->user->full_name }} - Chef&caption=Hey! Check my food art at FoodPole&description={{ $meal->data[0]->chef->description }}&redirect_uri=http://www.foodpole.com/" target="_blank" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"><i class="icon-brand-facebook-f"></i></a></li>
								<li><a href="https://twitter.com/home?status={{ URL::to('meal/'.$meal->data[0]->chef->user->username.'/'.$meal->data[0]->id).'/details' }}" target="_blank" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
								<li><a href="https://plus.google.com/share?url={{ URL::to('meal/'.$meal->data[0]->chef->user->username.'/'.$meal->data[0]->id).'/details' }}" target="_blank" data-button-color="#dd4b39" title="Share on Google Plus" data-tippy-placement="top"><i class="icon-brand-google-plus-g"></i></a></li>
								<li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{ URL::to('meal/'.$meal->data[0]->chef->user->username.'/'.$meal->data[0]->id).'/details' }}&title=&summary=&source=" target="_blank" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"><i class="icon-brand-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>


<!-- Spacer -->
<div class="margin-top-15"></div>
<!-- Spacer / End-->

<!-- Wrapper / End -->



	@endsection