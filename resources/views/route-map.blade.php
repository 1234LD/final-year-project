<!doctype html>
<html lang="en">

@include('includes.header')
<body>
@include('includes.body-front')

<!-- Wrapper -->
<div id="wrapper">
@include('includes.header-dashboard')
@include('includes.top-bar-loggedin')
    <!-- Content
    ================================================== -->
    <div id="map-canvas" class="route-map"></div>

    <div class="route-details" data-simplebar>
        <div class="route-container">

            <!-- FOR CUSTOMER & ADMIN ================================================== -->

            <div class="welcome-text">

                <div class="breathing-icon c-done"><i class="icon-material-outline-map"></i></div>


                <h3>Point A: {{$customer_user->full_name}}</h3>
                {{--<span>Address: Phase 2, Bahria Town, Rawalpindi</span> to get the lat long of customer kindly user {{customer_detail->latitude}}  --}}

                <span>Phone: {{$customer_user->phone_number}}</span>
            </div>

            <!-- FOR CUSTOMER & ADMIN ================================================== -->


            <!-- FOR CHEF & ADMIN ================================================== -->

            <div class="welcome-text">
                <h3>Point B: Chef {{$chef_user->full_name}}</h3>
                <span>Address:  {{$chef_detail->streetaddress}},{{$chef_detail->city}},{{$chef_detail->state}}</span>
                <span>Phone: {{$chef_detail->phoneNumber}}</span>
            </div>

            <!-- FOR CHEF & ADMIN ================================================== -->

            <div class="welcome-text margin-top-50">
                <h3>Step by Step Instructions</h3>
            </div>
            <div id="route" class="margin-top-0"></div>
        </div>
    </div>

</div>
@include('includes.body-scripts')
<script>

    function initMap() {

        var pointA = new google.maps.LatLng("<?php echo $customer_detail->latitude ?>" , "<?php echo $customer_detail->longitude ?>"),
            pointB = new google.maps.LatLng("<?php echo $chef_detail->latitude ?>" , "<?php echo $chef_detail->longitude ?>"),
            myOptions = {
                center: pointA,
                zoom: 18
            },
            map = new google.maps.Map(document.getElementById('map-canvas'), myOptions),
            // Instantiate a directions service.
            directionsService = new google.maps.DirectionsService,
            directionsDisplay = new google.maps.DirectionsRenderer({
                preserveViewport: true,
                map: map
            });

        // get route from A to B
        calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB);
    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB) {
        directionsService.route({
            origin: pointA,
            destination: pointB,
            travelMode: google.maps.TravelMode.WALKING
        }, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                pointsArray = response.routes[0].legs[0].steps;
                route = "";
                for (var i in pointsArray) {
                    route += "<li>"+pointsArray[i].instructions+"</li>" ;
                }
                document.getElementById("route").innerHTML = "<ul>"+route+"</ul>";
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }

    initMap();

</script>

</body>
</html>