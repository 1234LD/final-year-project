<!-- Logo -->
<div id="logo">
	<a href="{{ URL::to('/') }}"><img src="{{ URL::asset('images/logo.png') }}" alt=""></a>
</div>

@if (session('login_failed'))
	<div class="alert alert-danger">
		<center><h3> {{ session('login_failed') }} </h3></center>
	</div>
@endif

{{--@if (session('email_status'))--}}
	{{--<div class="alert alert-danger">--}}
		{{--<center> {{ session('email_status') }} </center>--}}
	{{--</div>--}}
{{--@endif--}}

@if(session('session_status'))
	<div class="alert alert-danger">
		<center><h3> {{ session('session_status') }} </h3></center>
	</div>
@endif

@if (session('signup_failed'))
	<div class="alert alert-danger">
		<center><h3> {{ session('signup_failed') }} </h3></center>
	</div>
@endif

@if (session('signup_success'))
	<div class="alert alert-success">
		<center><h3> {{ session('signup_success') }} </h3></center>
	</div>
@endif

@if(session('response_failed'))
	<div class="alert alert-danger">
		<center><h3> {{ session('response_failed') }} </h3></center>
	</div>
@endif

@if(session('response_success'))
	<div class="alert alert-success">
		<center><h3> {{ session('response_success') }} </h3></center>
	</div>
@endif




