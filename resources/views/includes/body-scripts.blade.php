<!-- Scripts
================================================== -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@if (!Request::is('filter/request/*'))
<script src="{{ URL::asset('js/jquery-3.3.1.min.js') }}"></script>
@endif

<script src="{{ URL::asset('js/jquery-migrate-3.0.0.min.js') }}"></script>
<script src="{{ URL::asset('js//jquery.form-validator.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="{{ URL::asset('js/jquery-thumbnail-cut.js') }}"></script>
<script src="{{ URL::asset('js/mmenu.min.js') }}"></script>
<script src="{{ URL::asset('js/tippy.all.min.js') }}"></script>
<script src="{{ URL::asset('js/simplebar.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap-slider.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap-select.min.js') }}"></script>
<script src="{{ URL::asset('js/snackbar.js') }}"></script>
<script src="{{ URL::asset('js/clipboard.min.js') }}"></script>
<script src="{{ URL::asset('js/counterup.min.js') }}"></script>
<script src="{{ URL::asset('js/magnific-popup.min.js') }}"></script>
<script src="{{ URL::asset('js/slick.min.js') }}"></script>
<script src="{{ URL::asset('js/custom.js') }}"></script>
<script src="{{ URL::asset('js/cart.js') }}"></script>
<script src="{{ URL::asset('js/photoswipe.min.js') }}"></script>
<script src="{{ URL::asset('js/photoswipe-ui-default.min.js') }}"></script>
<script src="{{ URL::asset('js/swiper.min.js') }}"></script>
<script src="{{ URL::asset('js/gallery.js') }}"></script>
<script src="{{ URL::asset('js/add-meal.js') }}"></script>
<!-- Chart.js // documentation: http://www.chartjs.org/docs/latest/ -->
<script src="{{ URL::asset('js/chart.min.js') }}"></script>
<!-- Google API -->
<script>
    $.validate();
</script>

{{--if (\Request::is('companies/*')) {--}}
{{--// will match URL /companies/999 or /companies/create--}}
{{--}--}}


<script>
	var categoryy = '';
    var delivery_type = '';
	var ratting;
    var url;

    $(document).ready(function() {
        setTimeout(function() {
            $('.m-message').slideUp(1000);
        }, 3000); // <-- time in milliseconds

        setTimeout(function() {
            $('.alert.alert-danger').slideUp(1000);
        }, 3000); // <-- time in milliseconds

        setTimeout(function() {
            $('.alert.alert-success').slideUp(1000);
        }, 3000); // <-- time in milliseconds

        $('.filter-category').on('change', function() {

            if(this.options[this.selectedIndex].value != '')
            {
                categoryy = this.options[this.selectedIndex].value;
			}

        });

        $('.filter-rating').on('change', function() {
            ratting = $('.filter-rating input[name=rating]:checked').val();

        });
        // sending latitude and longitude on search results ...
        $('.intro-search-button button.search_click').click(function()
        {
            var lat = parseFloat($('.latlong span#blat').text());
            var lng = parseFloat($('.latlong span#blong').text());

            var keyword = $('.intro-search-field input[name=keyword]').val();

            var distance = 15;

            if($('.intro-search-field input[name=keyword]').val() == '')
            {
                window.location= 'filter/request/'+lat+'/'+lng+'?distance='+500;
                // alert('YESSSS');
            }
            else
            {
                window.location= 'filter/request/'+lat+'/'+lng+'?distance='+500+'&keyword='+keyword;
                // alert('NOOOOO');
            }
        });

        // $('.all-cats').on('change', function() {
        //     var cat = this.options[this.selectedIndex].value;
        // });

        $('.sidebar-search-button-container button.search_click').click(function()
        {

            var lat = parseFloat($('.latlong span#blat').text());
            var lng = parseFloat($('.latlong span#blong').text());
            var keywordd = $('.filter-keyword input[name=keyword]').val();
            var distance = $('.filter-distance input[name=distance]').val();
            var rating = parseInt($('.filter-rating input[name=rating]').val());
            var preparation_max_time = parseInt($('.filter-time input[name=preparation_max_time]').val());

            var minprice = parseInt($(".filter-price .min-slider-handle").attr("aria-valuenow"));

            var maxprice = parseInt($(".filter-price .max-slider-handle").attr("aria-valuenow"));



            if(($('.filter-mode input[id=mode1]').is(":checked")) && !($('.filter-mode input[id=mode2]').is(":checked"))){

                delivery_type = '{{ config('constants.DEFAULT_SELF_DELIVERY') }}';
            }
            else if(($('.filter-mode input[id=mode2]').is(":checked")) && !($('.filter-mode input[id=mode1]').is(":checked"))){
                delivery_type = '{{ config('constants.DEFAULT_TAKE-AWAY_DELIVERY') }}';
            }
            else if(($('.filter-mode input[id=mode1]').is(":checked")) && ($('.filter-mode input[id=mode2]').is(":checked"))){
                delivery_type = '{{ config('constants.DEFAULT_BOTH_DELIVERY') }}';
            }
            else if(!($('.filter-mode input[id=mode1]').is(":checked")) && !($('.filter-mode input[id=mode2]').is(":checked"))){
                delivery_type = null;
            }

            url = '<?php echo(URL::to('/'));?>/filter/request/'+lat+'/'+lng;

            url += '?distance='+distance;

            if(keywordd != '')
            {
                url += '&keyword='+keywordd;
            }
			if(categoryy != '')
			{
			    url += '&category='+categoryy;
			}
			if(delivery_type == '{{ config('constants.DEFAULT_SELF_DELIVERY') }}' || delivery_type == '{{ config('constants.DEFAULT_TAKE-AWAY_DELIVERY') }}' ||
				delivery_type == '{{ config('constants.DEFAULT_BOTH_DELIVERY') }}')
			{
                url += '&delivery_type='+delivery_type;
			}
            url += '&price_min='+minprice+'&price_max='+maxprice;
            url += '&rating='+rating;
            url += '&preparation_min_time='+0+'&preparation_max_time='+preparation_max_time;


             window.location = url;


                // if($('.intro-search-field input[name=keyword]').val() != ''){url += '&keyword='+keywordd;}
                // if($('.filter-rating input[name=rating]').val() != ''){url += '&rating='+rating;}
                // if($('.filter-time input[name=preparation_max_time]').val() != ''){url += '&preparation_min_time='+0+'&preparation_max_time='+preparation_max_time;}
                // if($(".filter-price .max-slider-handle").attr("aria-valuenow") != 1000){url += '&price_min='+minprice+'&price_max='+maxprice;}
                // if(categoryy != ''){url += '&category='+categoryy;}

                // window.location.replace('filter/');

            // }
            // if($('.intro-search-field input[name=keyword]').val() == '')
            // {
            //     window.location= 'filter/request/'+lat+'/'+lng+'?distance='+5;
            //     // alert('YESSSS');
            // }
            // else
            // {
            //     window.location= 'filter/request/'+lat+'/'+lng+'?distance='+5+'&keyword='+keyword;
            //     // alert('NOOOOO');
            // }
        });

    });
</script>

@if (Request::is('admin/dashboard') || Request::is('chef/panel') || Request::is('user/dashboard'))
<script>
	Chart.defaults.global.defaultFontFamily = "Nunito";
	Chart.defaults.global.defaultFontColor = '#888';
	Chart.defaults.global.defaultFontSize = '14';

	var ctx = document.getElementById('chart-t-r-q').getContext('2d');

	var chart = new Chart(ctx, {
		type: 'bar',

		// The data for our dataset
		data: {
			labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			// Information about the dataset
	   		datasets: [{
				label: "Total Meals",
				backgroundColor: 'rgba(42,65,232,0.08)',
				borderColor: '#2a41e8',
				borderWidth: "3",
				data: [196,132,215,362,210,252,196,132,215,362,210,252],
				pointRadius: 5,
				pointHoverRadius:5,
				pointHitRadius: 10,
				pointBackgroundColor: "#fff",
				pointHoverBackgroundColor: "#fff",
				pointBorderWidth: "2",
			},
			{
				label: "Quick Meals",
				backgroundColor: 'rgba(54, 189, 120, 0.08)',
				borderColor: '#36bd78',
				borderWidth: "3",
				data: [150,100,200,280,200,230,150,100,200,280,200,230],
				pointRadius: 5,
				pointHoverRadius:5,
				pointHitRadius: 10,
				pointBackgroundColor: "#fff",
				pointHoverBackgroundColor: "#fff",
				pointBorderWidth: "2",
			},
			{
				label: "Regular Meals",
				backgroundColor: 'rgba(236, 35, 39, 0.08)',
				borderColor: '#ec2327',
				borderWidth: "3",
				data: [46,32,15,35,10,22,46,32,15,35,10,22],
				pointRadius: 5,
				pointHoverRadius:5,
				pointHitRadius: 10,
				pointBackgroundColor: "#fff",
				pointHoverBackgroundColor: "#fff",
				pointBorderWidth: "2",
			}]
		},

		// Configuration options
		options: {

		    layout: {
		      padding: 10,
		  	},

			legend: { display: false },
			title:  { display: false },

			scales: {
				yAxes: [{
					scaleLabel: {
						display: false
					},
					gridLines: {
						 borderDash: [6, 10],
						 color: "#d8d8d8",
						 lineWidth: 1,
	            	},
				}],
				xAxes: [{
					scaleLabel: { display: false },
					gridLines:  { display: false },
				}],
			},

		    tooltips: {
		      backgroundColor: '#333',
		      titleFontSize: 13,
		      titleFontColor: '#fff',
		      bodyFontColor: '#fff',
		      bodyFontSize: 13,
		      displayColors: false,
		      xPadding: 10,
		      yPadding: 10,
		      intersect: false
		    }
		},


});

</script>

<script>
	Chart.defaults.global.defaultFontFamily = "Nunito";
	Chart.defaults.global.defaultFontColor = '#888';
	Chart.defaults.global.defaultFontSize = '14';

	var ctx = document.getElementById('chart-u-c-c').getContext('2d');

	var chart = new Chart(ctx, {
		type: 'line',

		// The data for our dataset
		data: {
			labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			// Information about the dataset
	   		datasets: [{
				label: "Users",
				backgroundColor: 'rgba(42,65,232,0.08)',
				borderColor: '#2a41e8',
				borderWidth: "3",
				data: [196,132,215,362,210,252,196,132,215,362,210,252],
				pointRadius: 5,
				pointHoverRadius:5,
				pointHitRadius: 10,
				pointBackgroundColor: "#fff",
				pointHoverBackgroundColor: "#fff",
				pointBorderWidth: "2",
			},
			{
				label: "Customers",
				backgroundColor: 'rgba(54, 189, 120, 0.08)',
				borderColor: '#36bd78',
				borderWidth: "3",
				data: [150,100,200,280,200,230,150,100,200,280,200,230],
				pointRadius: 5,
				pointHoverRadius:5,
				pointHitRadius: 10,
				pointBackgroundColor: "#fff",
				pointHoverBackgroundColor: "#fff",
				pointBorderWidth: "2",
			},
			{
				label: "Chefs",
				backgroundColor: 'rgba(236, 35, 39, 0.08)',
				borderColor: '#ec2327',
				borderWidth: "3",
				data: [46,32,15,35,10,22,46,32,15,35,10,22],
				pointRadius: 5,
				pointHoverRadius:5,
				pointHitRadius: 10,
				pointBackgroundColor: "#fff",
				pointHoverBackgroundColor: "#fff",
				pointBorderWidth: "2",
			}]
		},

		// Configuration options
		options: {

		    layout: {
		      padding: 10,
		  	},

			legend: { display: false },
			title:  { display: false },

			scales: {
				yAxes: [{
					scaleLabel: {
						display: false
					},
					gridLines: {
						 borderDash: [6, 10],
						 color: "#d8d8d8",
						 lineWidth: 1,
	            	},
				}],
				xAxes: [{
					scaleLabel: { display: false },
					gridLines:  { display: false },
				}],
			},

		    tooltips: {
		      backgroundColor: '#333',
		      titleFontSize: 13,
		      titleFontColor: '#fff',
		      bodyFontColor: '#fff',
		      bodyFontSize: 13,
		      displayColors: false,
		      xPadding: 10,
		      yPadding: 10,
		      intersect: false
		    }
		},


});

</script>

<script>
	Chart.defaults.global.defaultFontFamily = "Nunito";
	Chart.defaults.global.defaultFontColor = '#888';
	Chart.defaults.global.defaultFontSize = '14';

	var ctx = document.getElementById('chart-sales-profit').getContext('2d');

	var chart = new Chart(ctx, {
		type: 'bar',

		// The data for our dataset
		data: {
			labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			// Information about the dataset
	   		datasets: [{
				label: "Sales/Orders",
				backgroundColor: 'rgba(42,65,232,0.08)',
				borderColor: '#2a41e8',
				borderWidth: "3",
				data: [196,132,215,362,210,252,196,132,215,362,210,252],
				pointRadius: 5,
				pointHoverRadius:5,
				pointHitRadius: 10,
				pointBackgroundColor: "#fff",
				pointHoverBackgroundColor: "#fff",
				pointBorderWidth: "2",
			},
			{
				label: "Revenue",
				backgroundColor: 'rgba(54, 189, 120, 0.08)',
				borderColor: '#36bd78',
				borderWidth: "3",
				data: [4500,2600,6890,4800,5020,6070,4500,2600,6890,4800,5020,6070],
				pointRadius: 5,
				pointHoverRadius:5,
				pointHitRadius: 10,
				pointBackgroundColor: "#fff",
				pointHoverBackgroundColor: "#fff",
				pointBorderWidth: "2",
			},
			{
				label: "Profit",
				backgroundColor: 'rgba(236, 35, 39, 0.08)',
				borderColor: '#ec2327',
				borderWidth: "3",
				data: [1200,550,2650,1390,2100,2900,2460,890,2650,1390,2100,2900],
				pointRadius: 5,
				pointHoverRadius:5,
				pointHitRadius: 10,
				pointBackgroundColor: "#fff",
				pointHoverBackgroundColor: "#fff",
				pointBorderWidth: "2",
			}]
		},

		// Configuration options
		options: {

		    layout: {
		      padding: 10,
		  	},

			legend: { display: false },
			title:  { display: false },

			scales: {
				yAxes: [{
					scaleLabel: {
						display: false
					},
					gridLines: {
						 borderDash: [6, 10],
						 color: "#d8d8d8",
						 lineWidth: 1,
	            	},
				}],
				xAxes: [{
					scaleLabel: { display: false },
					gridLines:  { display: false },
				}],
			},

		    tooltips: {
		      backgroundColor: '#333',
		      titleFontSize: 13,
		      titleFontColor: '#fff',
		      bodyFontColor: '#fff',
		      bodyFontSize: 13,
		      displayColors: false,
		      xPadding: 10,
		      yPadding: 10,
		      intersect: false
		    }
		},


});

</script>

<script>
	Chart.defaults.global.defaultFontFamily = "Nunito";
	Chart.defaults.global.defaultFontColor = '#888';
	Chart.defaults.global.defaultFontSize = '14';

	var ctx = document.getElementById('chart').getContext('2d');

	var chart = new Chart(ctx, {
		type: 'line',

		// The data for our dataset
		data: {
			labels: ["January", "February", "March", "April", "May", "June"],
			// Information about the dataset
	   		datasets: [{
				label: "Delivery",
				backgroundColor: 'rgba(42,65,232,0.08)',
				borderColor: '#2a41e8',
				borderWidth: "3",
				data: [196,132,215,362,210,252],
				pointRadius: 5,
				pointHoverRadius:5,
				pointHitRadius: 10,
				pointBackgroundColor: "#fff",
				pointHoverBackgroundColor: "#fff",
				pointBorderWidth: "2",
			},
			{
				label: "Take away",
				backgroundColor: 'rgba(236, 35, 39, 0.08)',
				borderColor: '#ec2327',
				borderWidth: "3",
				data: [201,105,300,200,150,201],
				pointRadius: 5,
				pointHoverRadius:5,
				pointHitRadius: 10,
				pointBackgroundColor: "#fff",
				pointHoverBackgroundColor: "#fff",
				pointBorderWidth: "2",
			}]
		},

		// Configuration options
		options: {

		    layout: {
		      padding: 10,
		  	},

			legend: { display: false },
			title:  { display: false },

			scales: {
				yAxes: [{
					scaleLabel: {
						display: false
					},
					gridLines: {
						 borderDash: [6, 10],
						 color: "#d8d8d8",
						 lineWidth: 1,
	            	},
				}],
				xAxes: [{
					scaleLabel: { display: false },
					gridLines:  { display: false },
				}],
			},

		    tooltips: {
		      backgroundColor: '#333',
		      titleFontSize: 13,
		      titleFontColor: '#fff',
		      bodyFontColor: '#fff',
		      bodyFontSize: 13,
		      displayColors: false,
		      xPadding: 10,
		      yPadding: 10,
		      intersect: false
		    }
		},


});

</script>
@endif
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
// Snackbar for user status switcher
$('#snackbar-user-status label').click(function() {

	Snackbar.show({
		text: 'Your status has been changed!',
		pos: 'bottom-center',
		showAction: false,
		actionText: "Dismiss",
		duration: 3000,
		textColor: '#fff',
		backgroundColor: '#383838'
	});

    setTimeout(location.reload.bind(location), 1000);

});

$('#snackbar-user-status label.busy').click(function() {
    var status = '{{ config('constants.DEFAULT_STATUS_BUSY') }}';
    $.ajax({

        // url:'http://' + window.location.hostname + window.location.pathname + '/update_status',

        url: 'http://' + window.location.hostname + '/foodpoleAPI/public/update_status',

        type:'GET',
        data:'status='+status,
        success: function(response){ // What to do if we succeed
           // if(data == "success")
           // alert(response);
        },
        error: function(response){
           // alert('Error : '+response);
        }
    });
});

$('#snackbar-user-status label.online').click(function() {
    var status = '{{ config('constants.DEFAULT_STATUS_ONLINE') }}';

    $.ajax({

        // url:'http://' + window.location.hostname + window.location.pathname + '/update_status',

        url: 'http://' + window.location.hostname + '/foodpoleAPI/public/update_status/',

        type:'GET',
        data:'status='+status,
        success: function(response){ // What to do if we succeed
            // if(data == "success")
            // alert(response);
        },
        error: function(response){
             // alert('Error : '+response);
        }
    });
});

</script>


<!-- Google Autocomplete -->
<script>
	function initAutocomplete() {
		 var options = {
		  // types: ['(locality)'],
		  //componentRestrictions: {country: "pk"}
		 };

		 var input = document.getElementById('autocomplete-input');
		 var autocomplete = new google.maps.places.Autocomplete(input, options);

        var inputCustomer = document.getElementById('autocomplete-input-customer');
        var autocompleteCustomer = new google.maps.places.Autocomplete(inputCustomer, options);

        var inputChef = document.getElementById('autocomplete-input-chef');
        var autocompleteChef = new google.maps.places.Autocomplete(inputChef, options);
	}

	// Autocomplete adjustment for homepage
	if ($('.intro-banner-search-form')[0]) {
	    setTimeout(function(){
	        $(".pac-container").prependTo(".intro-search-field.with-autocomplete");
	    }, 300);
	}

</script>

<!-- Drag & Drop Location Popup -->
<script>

$('#updateLocation').click(function () {
  m = $("#exactMark").val();
  $("#autocomplete-input").val(m);
    $("#autocomplete-input-customer").val(m);
    $("#autocomplete-input-chef").val(m);
});
</script>

     <script type="text/javascript" charset="utf-8">

     $(document).ready(function() {
         var currgeocoder;

         //Set geo location lat and long

         navigator.geolocation.getCurrentPosition(function(position, html5Error) {

             geo_loc = processGeolocationResult(position);
             currLatLong = geo_loc.split(",");
             initializeCurrent(currLatLong[0], currLatLong[1]);

        });

        //Get geo location result

       function processGeolocationResult(position) {
             html5Lat = position.coords.latitude; //Get latitude
             html5Lon = position.coords.longitude; //Get longitude
             html5TimeStamp = position.timestamp; //Get timestamp
             html5Accuracy = position.coords.accuracy; //Get accuracy in meters
             return (html5Lat).toFixed(8) + ", " + (html5Lon).toFixed(8);
       }

        //Check value is present or not & call google api function

        function initializeCurrent(latcurr, longcurr) {
             currgeocoder = new google.maps.Geocoder();
             console.log(latcurr + "-- ######## --" + longcurr);
             if (latcurr != '' && longcurr != '') {
				 $("#blat").html(latcurr);
				 $("#blong").html(longcurr);
                 $('input[name="lat"]').val(latcurr);
                 $('input[name="lng"]').val(longcurr);
                 var myLatlng = new google.maps.LatLng(latcurr, longcurr);
                 return getCurrentAddress(myLatlng);
             }
       }

        //Get current address

         function getCurrentAddress(location) {
              currgeocoder.geocode({
                  'location': location

            }, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(results[0]);
                    $("#autocomplete-input").val(results[0].formatted_address);
                    $("#autocomplete-input-customer").val(results[0].formatted_address);
                    $("#autocomplete-input-chef").val(results[0].formatted_address);
					$("#chef-loc").html(results[0].formatted_address);
					$("#exactMark").val(results[0].formatted_address);
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
         }


    });

    </script>

<script>

</script>

<script>//the maps api is setup above
//var x = 19.9974533;
//var y = 73.7898023;
$("#markLocation").click(function () {
var x = $("#blat").text();
var y = $("#blong").text();
    var latlng = new google.maps.LatLng(Number(x),Number(y)); //Set the default location of map

    var map = new google.maps.Map(document.getElementById('mark-map'), {

        center: latlng,

        zoom: 19, //The zoom value for map

        mapTypeId: google.maps.MapTypeId.ROADMAP });



    var marker = new google.maps.Marker({

        position: latlng,

        map: map,

        title: 'Place the marker for exact location!', //The title on hover to display

        draggable: true //this makes it drag and drop
    });


    google.maps.event.addListener(marker, 'dragend', function (a) {

        console.log(a);

        document.getElementById('blat').innerHTML = a.latLng.lat().toFixed(8); //Place the value in input box
		document.getElementById('blong').innerHTML = a.latLng.lng().toFixed(8);
        $('input[name="lat"]').val(a.latLng.lat().toFixed(8));
        $('input[name="lat"]').val(a.latLng.lng().toFixed(8));
            var lat = Number(document.getElementById("blat").innerHTML);
            var lng = Number(document.getElementById("blong").innerHTML);
            var latlng = new google.maps.LatLng(lat, lng);
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
						$("#exactMark").val(results[0].formatted_address);
                    }
                }
            });

    });


});


//# sourceURL=pen.js
</script>

<script>
    function chefReason (poo) {
        if ($(poo).closest(".foodpole-user-name").find($(".reason-area.reason-only")).is(':hidden')) {
            ($(poo).closest(".foodpole-user-name").find($(".reason-area.reason-only")).show()).slideDown(1000);
        } else {
            ($(poo).closest(".foodpole-user-name").find($(".reason-area.reason-only"))).hide().slideUp(1000);
        }
    }

    function chefComment (poo) {
        if ($(poo).closest(".foodpole-user-name").find($(".reason-area.comment-box")).is(':hidden')) {
            ($(poo).closest(".foodpole-user-name").find($(".reason-area.comment-box")).show()).slideDown(1000);
        } else {
            ($(poo).closest(".foodpole-user-name").find($(".reason-area.comment-box"))).hide().slideUp(1000);
        }
    }

    function customerReason (poo) {
        if ($(poo).closest(".foodpole-user-name").find($(".reason-area.reason-only")).is(':hidden')) {
            ($(poo).closest(".foodpole-user-name").find($(".reason-area.reason-only")).show()).slideDown(1000);
        } else {
            ($(poo).closest(".foodpole-user-name").find($(".reason-area.reason-only"))).hide().slideUp(1000);
        }
    }

    function adminReason (poo) {
        if ($(poo).closest(".foodpole-user-name").find($(".reason-area.reason-only")).is(':hidden')) {
            ($(poo).closest(".foodpole-user-name").find($(".reason-area.reason-only")).show()).slideDown(1000);
        } else {
            ($(poo).closest(".foodpole-user-name").find($(".reason-area.reason-only"))).hide().slideUp(1000);
        }
    }

    function adminComment (poo) {
        if ($(poo).closest(".foodpole-user-name").find($(".reason-area.comment-box")).is(':hidden')) {
            ($(poo).closest(".foodpole-user-name").find($(".reason-area.comment-box")).show()).slideDown(1000);
        } else {
            ($(poo).closest(".foodpole-user-name").find($(".reason-area.comment-box"))).hide().slideUp(1000);
        }
    }

    function customerReview (poo) {
        if ($(poo).closest(".foodpole-user-name").find($(".reason-area.customer-review")).is(':hidden')) {
            ($(poo).closest(".foodpole-user-name").find($(".reason-area.customer-review")).show()).slideDown(1000);
        } else {
            ($(poo).closest(".foodpole-user-name").find($(".reason-area.customer-review"))).hide().slideUp(1000);
        }
    }

    function customerFlag (poo) {
        if ($(poo).closest(".foodpole-user-name").find($(".reason-area.customer-flag")).is(':hidden')) {
            ($(poo).closest(".foodpole-user-name").find($(".reason-area.customer-flag")).show()).slideDown(1000);
        } else {
            ($(poo).closest(".foodpole-user-name").find($(".reason-area.customer-flag"))).hide().slideUp(1000);
        }
    }

</script>

<script>//the maps api is setup above
    //var x = 19.9974533;
    //var y = 73.7898023;
    $("#markLocation-customer").click(function () {
		if($(".customer-pin").is(':hidden')) {
            $(".customer-pin").show(function () {
                var x = $("#blat").text();
                var y = $("#blong").text();
                var latlng = new google.maps.LatLng(Number(x), Number(y)); //Set the default location of map

                var map = new google.maps.Map(document.getElementById('mark-map-customer'), {

                    center: latlng,

                    zoom: 19, //The zoom value for map

                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });


                var marker = new google.maps.Marker({

                    position: latlng,

                    map: map,

                    title: 'Place the marker for exact location!', //The title on hover to display

                    draggable: true //this makes it drag and drop
                });


                google.maps.event.addListener(marker, 'dragend', function (a) {

                    console.log(a);

                    document.getElementById('blat').innerHTML = a.latLng.lat().toFixed(8); //Place the value in input box
                    document.getElementById('blong').innerHTML = a.latLng.lng().toFixed(8);
                    $('input[name="lat"]').val(a.latLng.lat().toFixed(8));
                    $('input[name="lng"]').val(a.latLng.lng().toFixed(8));
                    var lat = Number(document.getElementById("blat").innerHTML);
                    var lng = Number(document.getElementById("blong").innerHTML);
                    var latlng = new google.maps.LatLng(lat, lng);
                    var geocoder = geocoder = new google.maps.Geocoder();
                    geocoder.geocode({'latLng': latlng}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $("#autocomplete-input-customer").val(results[0].formatted_address);
                                $("#autocomplete-input-chef").val(results[0].formatted_address);
                                $("#autocomplete-input").val(results[0].formatted_address);
                            }
                        }
                    });

                });
            }).slideDown(600);

        } else {
            $(".customer-pin").slideUp(600);
		}


    });


    //# sourceURL=pen.js
</script>

<script>//the maps api is setup above
    //var x = 19.9974533;
    //var y = 73.7898023;
    $("#markLocation-chef").click(function () {
        if($(".chef-pin").is(':hidden')) {
            $(".chef-pin").show(function () {
                var x = $("#blat").text();
                var y = $("#blong").text();
                var latlng = new google.maps.LatLng(Number(x), Number(y)); //Set the default location of map

                var map = new google.maps.Map(document.getElementById('mark-map-chef'), {

                    center: latlng,

                    zoom: 19, //The zoom value for map

                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });


                var marker = new google.maps.Marker({

                    position: latlng,

                    map: map,

                    title: 'Place the marker for exact location!', //The title on hover to display

                    draggable: true //this makes it drag and drop
                });


                google.maps.event.addListener(marker, 'dragend', function (a) {

                    console.log(a);

                    document.getElementById('blat').innerHTML = a.latLng.lat().toFixed(8); //Place the value in input box
                    document.getElementById('blong').innerHTML = a.latLng.lng().toFixed(8);
                    $('input[name="lat"]').val(a.latLng.lat().toFixed(8));
                    $('input[name="lng"]').val(a.latLng.lng().toFixed(8));
                    var lat = Number(document.getElementById("blat").innerHTML);
                    var lng = Number(document.getElementById("blong").innerHTML);
                    var latlng = new google.maps.LatLng(lat, lng);
                    var geocoder = geocoder = new google.maps.Geocoder();
                    geocoder.geocode({'latLng': latlng}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $("#autocomplete-input-chef").val(results[0].formatted_address);
                                $("#autocomplete-input-customer").val(results[0].formatted_address);
                                $("#autocomplete-input").val(results[0].formatted_address);
                            }
                        }
                    });

                });
            }).slideDown(600);

        } else {
            $(".chef-pin").slideUp(600);
        }


    });


    //# sourceURL=pen.js
</script>

@if (!Request::is('filter/request/*'))
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMShCNsf6wPh0TEq14vLG2X2uUmcFuh5E&amp;libraries=places&amp;callback=initAutocomplete"></script>
	<!-- Maps -->


	{{--<script src="{{ URL::asset('js/infobox.min.js') }}"></script>--}}
	{{--<script src="{{ URL::asset('js/markerclusterer.js') }}"></script>--}}

	{{--<script src="{{ URL::asset('js/route.js') }}"></script>--}}
@endif




