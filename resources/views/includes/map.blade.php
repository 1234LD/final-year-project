	<!-- Full Page Map -->
	<div class="full-page-map-container">
		
		<!-- Enable Filters Button -->
		<div class="filter-button-container">
			<button class="enable-filters-button">
				<i class="enable-filters-button-icon"></i>
				<span class="show-text">Show Filters</span>
				<span class="hide-text">Hide Filters</span>
			</button>
			<div class="filter-button-tooltip">Click to expand sidebar with filters!</div>
		</div>
		
		<!-- Map -->
	    <div id="map" data-map-zoom="12" data-map-scroll="true"></div>
	</div>

	<!-- Wrapper / End -->
	<!-- Full Page Map / End -->