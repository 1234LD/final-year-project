<?php //dd(Session::get('customer_cart')); ?>
	<!-- Header -->
	<div id="header">
		<div class="container">
			
			<!-- Left Side Content -->
			<div class="left-side">

				@include('includes.logo')
				
			</div>
			<!-- Left Side Content / End -->


            <!-- Right Side Content / End -->
			<div class="right-side">

				<div class="post-quick-meal">
					@if(Session::has('user_id'))
					<button class="button ripple-effect" onclick="window.location.href='{{URL::to('chef/add_quick_meal')}}'">Post Quick meal</button>
					@else
					<a href="#small-dialog-6" class="login-tab popup-with-zoom-anim button ripple-effect">Post Quick meal</a>
						@endif
				</div>
				
				<!--  User Notifications -->
				<div class="header-widget">
					
					{{--<!-- Notifications -->--}}
					{{--<div class="header-notifications">--}}

						{{--<!-- Trigger -->--}}
						{{--<div class="header-notifications-trigger">--}}
							{{--<a class="notification-count" href="#"><i class="icon-feather-bell"></i><span>1</span></a>--}}
						{{--</div>--}}

						{{--<!-- Dropdown -->--}}
						{{--<div class="header-notifications-dropdown">--}}

							{{--<div class="header-notifications-headline">--}}
								{{--<h4>Notifications</h4>--}}
								{{--<button class="mark-as-read ripple-effect-dark" title="Mark all as read" data-tippy-placement="left">--}}
									{{--<i class="icon-feather-check-square"></i>--}}
								{{--</button>--}}
							{{--</div>--}}

							{{--<div class="header-notifications-content">--}}
								{{--<div class="header-notifications-scroll" data-simplebar>--}}
									{{--<ul>--}}
										{{--<!-- Notification -->--}}
										{{--<li class="notifications-not-read">--}}
											{{--<a href="#">--}}
												{{--<span class="notification-icon"><i class="icon-material-outline-restaurant"></i></span>--}}
												{{--<span class="notification-text">--}}
													{{--<strong>Chef. Jurry</strong> has prepared your <span class="color">Order #11567</span>--}}
												{{--</span>--}}
											{{--</a>--}}
										{{--</li>--}}

										{{--<!-- Notification -->--}}
										{{--<li>--}}
											{{--<a href="#">--}}
												{{--<span class="notification-icon"><i class="icon-line-awesome-money"></i></span>--}}
												{{--<span class="notification-text">--}}
													{{--<strong>Dear Customer</strong> please clear payment for <span class="color">Order #11567</span>--}}
												{{--</span>--}}
											{{--</a>--}}
										{{--</li>--}}
									{{--</ul>--}}
								{{--</div>--}}
							{{--</div>--}}

						{{--</div>--}}

					{{--</div>--}}
					
					<!-- Messages -->
					<div class="header-notifications">
						<div class="header-notifications-trigger">

							<a class="total-cart-items" href=""><i class="icon-feather-shopping-bag"></i>
                                <?php


                                function removeItem() {
//                                    if(Session::has('customer_cart')[$_GET['removeMeal']]){
//                                        dd("Found");
//                                    }else{
//                                        dd("NOT FOUND");
//                                    }
//									dd(Session::get('customer_cart')[$_GET['removeMeal']]);
//                                    $getCart = Session::get('customer_cart');
//                                    dd($getCart[$_GET['removeMeal']]);
//                                    unset($getCart[$_GET['removeMeal']]);
//                                    Session::set('customer_cart', $getCart);
//									unset($_SESSION('customer_cart')[$_GET['removeMeal']]);
//									if([$_GET['removeMeal']] == 0){
//
//                                    }
									$dot = implode([$_GET['removeMeal'][0]]);
									$line = 'customer_cart.' . $dot;
//									dd($line);
                                    Session::forget($line);
                                }


                                if (isset($_GET['removeMeal'])) {
                                    if(Session::has('customer_cart')){
//
//                                    if(isset(Session::get('customer_cart')[$_GET['removeMeal']])){
//                                        dd("Found");
//                                    }else{
//                                        dd("NOT FOUND");
//                                    }

                                    if(isset(Session::get('customer_cart')[$_GET['removeMeal']])){
                                    removeItem();
                                    }
                                    }
                                }

                                if (isset($_GET['emptybag'])){
                                if(Session::has('customer_cart')){
                                    Session::forget('customer_cart');
                                    }
								}
                                ?>
								@if(Session::has('customer_cart'))
									<?php
									$cartCount = 0;
									foreach(Session::get('customer_cart') as $items)
									{
									    $cartCount += $items[0]['quantity'];
                                }
                                ?>
										@if($cartCount > 0) <span><?php echo $cartCount;?></span>@else <?php  Session::forget('customer_cart'); ?> @endif
								@endif</a>

						</div>

						<!-- Dropdown -->
						<div class="header-notifications-dropdown">

							<div class="header-notifications-headline">
								<h4>Meal Bag</h4>
								@if(Session::has('customer_cart'))
								<a href="?emptybag" class="ripple-effect-dark clear-cart" title="Remove all meals" data-tippy-placement="left">
									<i class="icon-material-outline-delete"></i>
								</a>
									@endif
							</div>
<?php  $global_mode = ''; $chefID = '';?>
							@if(Session::has('customer_cart'))
							<div class="header-notifications-content cart-content">
								<div class="header-notifications-scroll" data-simplebar>

									<ul class="meal-bag">
									    <span class="meal-bag-items">


												<?php

											if(Session::has('customer_cart')) {
                                            $counter = 0;
											    foreach(Session::get('customer_cart') as $cart) {

$global_mode = $cart[0]['delivery_type']; $chefID = $cart[0]['chef_id']?>
												<li class="meal-item">
												<span class="notification-avatar meal-icon thumb"><img src="{{ URL::asset('item_images/'.$cart[0]['item_image']) }}" alt=""></span>
												<a href="?removeMeal=<?php echo $counter; ?>" class="icon-material-outline-delete remove remove-cart-item cart-item-<?php echo $counter; ?>"></a>
												<div class="notification-text">
													<strong>{{ $cart[0]['item_name'] }}</strong>
													<strong class="item_id hide">{{ $cart[0]['item_id'] }}</strong>
													<strong class="chef_id hide" >{{ $cart[0]['chef_id'] }}</strong>
													<p class="notification-msg-text">{{ $cart[0]['item_option'] }} - Rs.
														<span class="price">{{ $cart[0]['item_price'] }}</span>
														<span class="option hide" >{{ $cart[0]['item_option'] }}</span>
													</p>
													<span class="color">Rs. 
														<span class="full-price">{{ $cart[0]['subtotal'] }}</span>
													</span>
													<div class="quantities">
														<span class="qt">{{ $cart[0]['quantity'] }}</span>
													</div>
												</div>
											</li>
                                                <?php $counter++; }
                                                } ?>

										</span>
										<!-- Notification -->

										<li class="cart-info-text">
											<div class="notification-text cart-info-inner">
												<span class="cart-info">
													<strong>Subtotal: </strong>
													<span class="color subtotal">Rs. 
														<span>0</span>
													</span>
												</span>
												{{--<span class="cart-info">--}}
													{{--<strong>GST 17%: </strong>--}}
													{{--<span class="color tax">Rs. --}}
														{{--<span>0</span>--}}
													{{--</span>--}}
												{{--</span>--}}
												<span class="cart-info">
													<strong>Delivery: </strong>
													<span class="color delivery">Rs. 
														<span>25</span>
													</span>
												</span>
												<span class="cart-info mode-container">
													<select class="color mode">
														@if($global_mode == config('constants.DEFAULT_BOTH_DELIVERY'))
															<option value="Home Delivery">Home Delivery</option>
															<option value="Take away">Take away</option>
														@else
															<option value="{{ $global_mode }}">{{ $global_mode }}</option>
														@endif
													</select>
												</span>													
												<span class="cart-info">
													<strong>Grand Total: </strong>
													<span class="color total">Rs. 
														<span>0</span>
													</span>
												</span>
												
											</div>
										</li>
									</ul>
								</div>
							</div>
								@if(Session::has('user_id'))
									@if(Session::get('chef_id') != $chefID)
							<button class="header-notifications-button ripple-effect button-sliding-icon button full-width cart-checkout">Checkout<i class="icon-material-outline-arrow-right-alt"></i></button>
										@else
										<?php
										?>

										<a href="#small-dialog-3" class="popup-with-zoom-anim header-notifications-button ripple-effect button-sliding-icon button full-width">Checkout<i class="icon-material-outline-arrow-right-alt"></i></a>
										@endif
									@else
									<a href="#small-dialog-6" class="popup-with-zoom-anim header-notifications-button ripple-effect button-sliding-icon button full-width">Checkout<i class="icon-material-outline-arrow-right-alt"></i></a>
									@endif
							@else

							<div class="header-notifications-content cart-content">
								<div class="header-notifications-scroll" data-simplebar>

									<ul class="meal-bag">
										<span class="no-meal">No meals!</span>
									</ul>
								</div>
							</div>
								@endif
						</div>
					</div>

				</div>
				<!--  User Notifications / End -->

			@if(Session::has('user_id'))
				<!-- User Menu -->
				<div class="header-widget">

					<!-- Messages -->
					<div class="header-notifications user-menu">
						<div class="header-notifications-trigger">
							@if(Session::get('picture') == null)
							<a href="#"><div class="user-avatar @if(Session::get('status') == config('constants.DEFAULT_STATUS_ONLINE')) status-online @else status-busy @endif"><img src="{{ URL::asset('images/user-avatar-placeholder.png') }}" alt=""></div></a>
							@else
								{{--@if(Session::get('user_type') == config('constants.DEFAULT_CHEF_TYPE'))--}}
									<a href="#"><div class="user-avatar @if(Session::get('status') == config('constants.DEFAULT_STATUS_ONLINE')) status-online @else status-busy @endif"><img src="{{ URL::asset('chefimages/'.Session::get('picture')) }}" alt=""></div></a>
								{{--@else--}}
									{{--<a href="#"><div class="user-avatar status-online"><img src="{{ URL::asset('userimages/'.Session::get('picture')) }}" alt=""></div></a>--}}
								{{--@endif--}}
							@endif
						</div>

						<!-- Dropdown -->
						<div class="header-notifications-dropdown">

							<!-- User Status -->
							<div class="user-status">

								<!-- User Name / Avatar -->
								<div class="user-details">
									@if(\Illuminate\Support\Facades\Session::get('picture') == null)
										<div class="user-avatar status-online"><img src="{{ URL::asset('images/user-avatar-placeholder.png') }}" alt=""></div>
									@else
										{{--@if(Session::get('user_type') == config('constants.DEFAULT_CHEF_TYPE'))--}}
											<div class="user-avatar status-online"><img src="{{ URL::asset('chefimages/'.Session::get('picture')) }}" alt=""></div>
										{{--@else--}}
											{{--<div class="user-avatar status-online"><img src="{{ URL::asset('userimages/'.Session::get('picture')) }}" alt=""></div>--}}
										{{--@endif--}}
									@endif
									<div class="user-name">
										{{ Session::get('full_name') }} <span>{{ Session::get('user_type') }}
											{{--@if(Session::get('user_type') == config('constants.DEFAULT_SUPER-ADMIN_TYPE'))--}}
												{{--Super Admin--}}
										{{--@elseif (Session::get('user_type') == config('constants.DEFAULT_ADMIN_TYPE'))--}}
											{{--Admin--}}
										{{--@elseif (Session::get('user_type') == config('constants.DEFAULT_CHEF_TYPE'))--}}
											{{--Chef--}}
										{{--@elseif (Session::get('user_type') == config('constants.DEFAULT_QUICK-CHEF_TYPE'))--}}
											{{--Quick Chef--}}
											{{--@elseif (Session::get('user_type') == config('constants.DEFAULT_GENERAL_TYPE'))--}}
											{{--General--}}
											{{--@elseif (Session::get('user_type') == config('constants.DEFAULT_CUSTOMER_TYPE'))--}}
											{{--Customer--}}
											{{--@endif--}}
										</span>
									</div>
								</div>

                            {{--@if(Session::get('user_type') == config('constants.DEFAULT_CHEF_TYPE'))--}}
								{{--<!-- User Status Switcher -->--}}
								{{--<div class="status-switch" id="snackbar-user-status">--}}
									{{--<a href="{{ URL::to('update_status/'.config('constants.DEFAULT_STATUS_ONLINE')) }}">--}}
									{{--<label class="user-online current-status online">Online</label>--}}

									{{--<a href="{{ URL::to('update_status/'.config('constants.DEFAULT_STATUS_BUSY')) }}">--}}

										{{--<label class="user-busy	curent-status busy">Busy</label>--}}


									{{--<!-- Status Indicator -->--}}
									{{--<span class="status-indicator" aria-hidden="true"></span>--}}
								{{--</div>--}}
								{{--@endif--}}
						</div>

						<ul class="user-menu-small-nav">
                            @if(Session::get('user_type') == config('constants.DEFAULT_ADMIN_TYPE') || Session::get('user_type') == config('constants.DEFAULT_SUPER-ADMIN_TYPE'))
							    <li><a href="{{ URL::asset('/admin/dashboard') }}"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>
                            @else
                                <li><a href="{{ URL::asset('/user/dashboard') }}"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>
                            @endif


							@if(Session::get('user_type') == config('constants.DEFAULT_CHEF_TYPE'))
								<li><a href="{{ URL::asset('/chef/panel') }}"><i class="icon-material-outline-school"></i> Chef Panel</a></li>
							@endif

                            @if(Session::get('user_type') == config('constants.DEFAULT_QUICK-CHEF_TYPE'))
                                    <li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/all_orders') }}"><i class="icon-feather-shopping-bag"></i> Orders</a></li>
                                    <li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/all_quick_meals') }}"><i class="icon-material-outline-restaurant"></i> Meals</a></li>
                                    <li><a href="{{ URL::to('chef/'.Session::get('user_id').'/'.Session::get('username').'/all_reviews') }}"><i class="icon-material-outline-rate-review"></i> Reviews</a></li>
                            @endif

									{{--@if(Session::get('user_type') == config('constants.DEFAULT_ADMIN_TYPE') || Session::get('user_type') == config('constants.DEFAULT_SUPER-ADMIN_TYPE'))--}}
										{{--<li><a href="{{ URL::asset('/admin/admin-profile')}}"><i class="icon-material-outline-settings"></i> Settings</a></li>--}}
										{{--<li><a href="{{ URL::asset('/admin/logged-out')}}"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>--}}
									{{--@elseif(Session::get('user_type') == config('constants.DEFAULT_CHEF_TYPE'))--}}
										{{--<li><a href="{{ URL::asset('/chef/user-profile')}}"><i class="icon-material-outline-settings"></i> Settings</a></li>--}}
										{{--<li><a href="{{ URL::asset('/logout_request')}}"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>--}}

							@if(Session::get('user_type') == config('constants.DEFAULT_ADMIN_TYPE') || Session::get('user_type') == config('constants.DEFAULT_SUPER-ADMIN_TYPE'))
							        <li><a href="{{ URL::asset('/admin/admin-profile/'.Session::get('user_id'))}}"><i class="icon-material-outline-settings"></i> Settings</a></li>
							@elseif(Session::get('user_type') == config('constants.DEFAULT_CHEF_TYPE'))
									<li><a href="{{ URL::asset('/chef/user-profile/'.Session::get('user_id'))}}"><i class="icon-material-outline-settings"></i> Settings</a></li>
							@else
									<li><a href="{{ URL::asset('/general/user-profile')}}"><i class="icon-material-outline-settings"></i> Settings </a></li>
									{{--<li><a href="{{ URL::asset('/general/user-logout')}}"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>--}}
							@endif
								<li><a href="{{ URL::asset('logout_request')}}"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
						</ul>

						</div>
					</div>

				</div>
				<!-- User Menu / End -->
@else
					<div class="header-widget">
						<a href="#small-dialog-6" class="popup-with-zoom-anim log-in-button"><i class="icon-feather-log-in"></i> <span>Log In / Register</span></a>
					</div>
				@endif
			</div>
			<!-- Right Side Content / End -->

		</div>
	</div>
	<!-- Header / End -->
	@include('general_boxes.signin')
	@include('general_boxes.own-meal')
</header>
<div class="clearfix"></div>
<!-- Header Container / End -->