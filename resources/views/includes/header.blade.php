<head>

<!-- Basic Page Needs
================================================== -->
<title>Foodpole - @yield('title')</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="_token" content="{{ csrf_token() }}">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" href="{{ asset('css/colors/red.css') }}">
<link rel="stylesheet" href="{{ asset('css/photoswipe.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/default-skin.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/swiper.min.css') }}">
<!-- Google Api
================================================== -->
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDMShCNsf6wPh0TEq14vLG2X2uUmcFuh5E"></script>

</head>