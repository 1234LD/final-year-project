<!-- Result List Container -->
<div class="foodpole-users-container foodpole-users-grid-layout margin-top-35">
	
	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<div class="verified-badge" title="Verified Chef" data-tippy-placement="right"></div>
					<a href="#"><img src="images/user-avatar-placeholder.png" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Jurry Abbas</a></h4>
					<span>Shalley Valley, Rawalpindi</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->

	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<a href="#"><img src="images/user-avatar-placeholder.png" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Jurry Abbas</a></h4>
					<span>Shalley Valley, Rawalpindi</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->
	
	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<a href="#"><img src="images/user-avatar-placeholder.png" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Jurry Abbas</a></h4>
					<span>Shalley Valley, Rawalpindi</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->

	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<div class="verified-badge" title="Verified Chef" data-tippy-placement="right"></div>
					<a href="#"><img src="images/user-avatar-placeholder.png" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Jurry Abbas</a></h4>
					<span>Shalley Valley, Rawalpindi</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->

	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<a href="#"><img src="images/user-avatar-placeholder.png" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Jurry Abbas</a></h4>
					<span>Shalley Valley, Rawalpindi</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->

	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<div class="verified-badge" title="Verified Chef" data-tippy-placement="right"></div>
					<a href="#"><img src="images/user-avatar-placeholder.png" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Jurry Abbas</a></h4>
					<span>Shalley Valley, Rawalpindi</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->

	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<a href="#"><img src="images/user-avatar-placeholder.png" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Jurry Abbas</a></h4>
					<span>Shalley Valley, Rawalpindi</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->

	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<div class="verified-badge" title="Verified Chef" data-tippy-placement="right"></div>
					<a href="#"><img src="images/user-avatar-placeholder.png" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Jurry Abbas</a></h4>
					<span>Shalley Valley, Rawalpindi</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->

</div>
<!-- foodpole-users Container / End -->