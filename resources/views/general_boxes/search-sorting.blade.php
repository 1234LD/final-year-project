<div class="notify-box margin-top-15">
	<div class="switch-container">
		<label class="switch"><input type="checkbox"><span class="switch-button"></span><span class="switch-text">Show free meals only</span></label>
	</div>

	<div class="sort-by">
		<span>Pick by:</span>
		<select class="selectpicker hide-tick">
			<option>Verified Meals</option>
			<option>Online Chefs</option>
			<option>Verified Chefs</option>
			<option>Busy Chefs</option>
		</select>
	</div>
</div>