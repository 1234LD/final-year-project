<!-- Sign In Popup
================================================== -->
<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs order-otp">

	<!--Tabs -->
	<div class="sign-in-form">

		<ul class="popup-tabs-nav">
		</ul>

		<div class="popup-tabs-container">
			
			<!-- Order OTP Verification 01 -->
			<div class="popup-tab-content" id="forgot-password-01">
				
				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>Please verify your phone number!</h3>
				</div>
					
				<!-- Form 01 -->
				<form method="post" id="forgot-password-form-01">
					<div class="input-with-icon-left">
						<i class="icon-line-awesome-mobile"></i>
						<input type="email" class="input-text with-border" name="emailphone" id="emailphone" placeholder="(XXX)-(XXXXXXX)" required/>
					</div>
				</form>
				
				<!-- Button -->
				<button class="button full-width button-sliding-icon ripple-effect forgot-password-02" type="submit" form="forgot-password-form-01">Send Code <i class="icon-material-outline-arrow-right-alt"></i></button>


			</div>

			<!-- Forgot Password 02 -->
			<div class="popup-tab-content" id="forgot-password-02">
				
				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>Enter code received in  sms!</h3>
				</div>
					
				<!-- Form 02 -->
				<form method="post" id="forgot-password-form-02">
					<div class="input-with-icon-left">
						<i class="icon-material-outline-textsms"></i>
						<input type="number" class="input-text with-border" name="code" id="code" placeholder="Enter code" required/>
					</div>
				</form>
				
				<!-- Button -->
				<button class="button full-width button-sliding-icon ripple-effect forgot-password-03" type="submit" form="forgot-password-form-02">Submit <i class="icon-material-outline-arrow-right-alt"></i></button>

			</div>		
				
			<!-- Forgot Password 03 -->
			<div class="popup-tab-content" id="forgot-password-03">
				
				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>Phone Verified!</h3>
					<span class="phone-successful"><i class="icon-material-outline-check-circle"></i></span>
				</div>

			</div>				

		</div>
	</div>
</div>
<!-- Sign In Popup / End -->