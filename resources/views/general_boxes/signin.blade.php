<!-- Sign In Popup
================================================== -->
<div id="small-dialog-6" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

	<!--Tabs -->
	<div class="sign-in-form">

		<ul class="popup-tabs-nav">
			<li><a href="#login">Log In</a></li>
			<li><a href="#register">Register</a></li>
			<li><a href="#forgot-password-01">Forgot Password</a></li>
			<li class="hide"><a href="#forgot-password-02">Submit Code</a></li>
			<li class="hide"><a href="#forgot-password-03">Reset Password</a></li>	
			<li class="hide"><a href="#customer-register">Customer / Quick Meal</a></li>
			<li class="hide"><a href="#chef-register">Chef</a></li>				
		</ul>

		<div class="popup-tabs-container">

			<!-- Login -->
			<div class="popup-tab-content" id="login">
				
				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>We're glad to see you again!</h3>
					<span>Don't have an account? <a href="#" class="register-tab">Sign Up!</a></span>
				</div>
					
				<!-- Form -->
				<form method="post" id="login-form" action="{{ URL::to('/justlogin') }}">
					{{ csrf_field() }}
					<div class="input-with-icon-left">
						<i class="icon-feather-user"></i>
						<input type="email"  class="input-text with-border" name="email" data-validation-error-msg="You did not enter a valid e-mail" data-validation="email" id="login-emailphone" placeholder="Email Address"/>
					</div>

					<div class="input-with-icon-left">
						<i class="icon-material-outline-lock"></i>
						<input type="password" class="input-text with-border" name="password" id="login-password" placeholder="Password"/>
					</div>
				</form>
				<a href="#" class="forgot-password">Forgot Password?</a>
				
				<!-- Button -->
				<button class="button full-width button-sliding-icon ripple-effect" type="submit" form="login-form">Log In <i class="icon-material-outline-arrow-right-alt"></i></button>
				
				{{--<!-- Social Login -->--}}
				{{--<div class="social-login-separator"><span>or</span></div>--}}
				{{--<div class="social-login-buttons">--}}
					{{--<button class="facebook-login ripple-effect"><i class="icon-brand-facebook-f"></i> Log In via Facebook</button>--}}
					{{--<button class="google-login ripple-effect"><i class="icon-brand-google-plus-g"></i> Log In via Google+</button>--}}
				{{--</div>--}}

			</div>
			
			<!-- Forgot Password 01 -->
			<div class="popup-tab-content" id="forgot-password-01">
				
				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>Let's recover your account!</h3>
					<span>Don't have an account? <a href="#" class="register-tab">Sign Up!</a></span>
				</div>
					
				<!-- Form 01 -->
				<form method="POST" id="forgot-password-form-01" action="{{ URL::to('submit/forgot_password/request') }}">
                    {{ csrf_field() }}
					<div class="input-with-icon-left">
						<i class="icon-feather-user"></i>
						<input type="email" class="input-text with-border" name="email" data-validation-error-msg="You did not enter a valid e-mail" data-validation="email" id="fp-emailphone" placeholder="Email Address" required/>
					</div>
				</form>
				
				<!-- Button -->
				<button class="button full-width button-sliding-icon ripple-effect" type="submit" form="forgot-password-form-01">Send<i class="icon-material-outline-arrow-right-alt"></i></button>

				{{--<!-- Social Login -->--}}
				{{--<div class="social-login-separator"><span>or</span></div>--}}
				{{--<div class="social-login-buttons">--}}
					{{--<button class="facebook-login ripple-effect"><i class="icon-brand-facebook-f"></i> Log In via Facebook</button>--}}
					{{--<button class="google-login ripple-effect"><i class="icon-brand-google-plus-g"></i> Log In via Google+</button>--}}
				{{--</div>--}}

			</div>

			<!-- Forgot Password 02 -->
			{{--<div class="popup-tab-content" id="forgot-password-02">--}}
				{{----}}
				{{--<!-- Welcome Text -->--}}
				{{--<div class="welcome-text">--}}
					{{--<h3>Enter code received in email or sms!</h3>--}}
					{{--<span>Don't have an account? <a href="#" class="register-tab">Sign Up!</a></span>--}}
				{{--</div>--}}
					{{----}}
				{{--<!-- Form 02 -->--}}
				{{--<form method="post" id="forgot-password-form-02">--}}
                    {{--{{ csrf_field() }}--}}
                    {{--<div class="input-with-icon-left">--}}
						{{--<i class="icon-material-outline-textsms"></i>--}}
						{{--<input type="number" class="input-text with-border" name="code" id="fp-code" placeholder="Enter code" required/>--}}
					{{--</div>--}}
				{{--</form>--}}
				{{----}}
				{{--<!-- Button -->--}}
				{{--<button class="button full-width button-sliding-icon ripple-effect forgot-password-03" type="submit" form="forgot-password-form-02">Submit <i class="icon-material-outline-arrow-right-alt"></i></button>--}}

				{{--<!-- Social Login -->--}}
				{{--<div class="social-login-separator"><span>or</span></div>--}}
				{{--<div class="social-login-buttons">--}}
					{{--<button class="facebook-login ripple-effect"><i class="icon-brand-facebook-f"></i> Log In via Facebook</button>--}}
					{{--<button class="google-login ripple-effect"><i class="icon-brand-google-plus-g"></i> Log In via Google+</button>--}}
				{{--</div>--}}

			{{--</div>		--}}

			<!-- Forgot Password 03 -->
			{{--<div class="popup-tab-content" id="forgot-password-03">--}}
				{{----}}
				{{--<!-- Welcome Text -->--}}
				{{--<div class="welcome-text">--}}
					{{--<h3>Reset your account password!</h3>--}}
					{{--<span>Don't have an account? <a href="#" class="register-tab">Sign Up!</a></span>--}}
				{{--</div>--}}
					{{----}}
				{{--<!-- Form 03 -->--}}
				{{--<form method="post" id="forgot-password-form-03">--}}
                    {{--{{ csrf_field() }}--}}
					{{--<div class="input-with-icon-left" title="Should be at least 8 characters long" data-tippy-placement="bottom">--}}
						{{--<i class="icon-material-outline-lock"></i>--}}
						{{--<input type="password" class="input-text with-border" name="password-register" id="fp-password-register" placeholder="New Password" required/>--}}
					{{--</div>--}}

					{{--<div class="input-with-icon-left">--}}
						{{--<i class="icon-material-outline-lock"></i>--}}
						{{--<input type="password" class="input-text with-border" name="password-repeat-register" id="fp-password-repeat-register" placeholder="Repeat Password" required/>--}}
					{{--</div>--}}
				{{--</form>--}}
				{{----}}
				{{--<!-- Button -->--}}
				{{--<button class="button full-width button-sliding-icon ripple-effect" type="submit" form="forgot-password-form-03">Reset Password <i class="icon-material-outline-arrow-right-alt"></i></button>--}}
				{{----}}
				{{--<!-- Social Login -->--}}
				{{--<div class="social-login-separator"><span>or</span></div>--}}
				{{--<div class="social-login-buttons">--}}
					{{--<button class="facebook-login ripple-effect"><i class="icon-brand-facebook-f"></i> Log In via Facebook</button>--}}
					{{--<button class="google-login ripple-effect"><i class="icon-brand-google-plus-g"></i> Log In via Google+</button>--}}
				{{--</div>--}}

			{{--</div>					--}}

			<!-- Register -->
			<div class="popup-tab-content" id="register">
				
				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>Let's create your account!</h3>
				</div>

				<!-- Account Type -->
				<div class="account-type">
					<div>
						<input type="radio" name="account-type-radio" id="customer-radio" class="account-type-radio" checked="checked"/>
						<label for="customer-radio" class="ripple-effect-dark customer-register"><i class="icon-material-outline-account-circle"></i> Customer</label>
					</div>

					<div>
						<input type="radio" name="account-type-radio" id="chef-radio" class="account-type-radio"/>
						<label for="chef-radio" class="ripple-effect-dark chef-register"><i class="icon-material-outline-business-center"></i> Chef</label>
					</div>
				</div>
					
				<!-- Form 01 -->
				<span class="popup-tab-content" id="customer-register">

				<form method="POST" id="01-register-account-form" action="{{ URL::to('/register_customer') }}">
                    	{{ csrf_field() }}
					<div class="input-with-icon-left">
						<i class="icon-feather-user"></i>
						<input type="text" class="input-text with-border" data-validation="custom" data-validation-regexp="^([a-zA-Z]+)$" name="fullname-register" id="customer-fullname-register" placeholder="Full Name" required/>
					</div>
					
					<div class="input-with-icon-left">
						<i class="icon-feather-link-2"></i>
						<input type="text" class="input-text with-border" pattern="^([a-zA-Z0-9)$" data-validation="length alphanumeric" data-validation-length="5-10" name="username-register" id="customer-username-register" placeholder="Username" required/>
					</div>					
					
					<div class="input-with-icon-left">
						<i class="icon-material-baseline-mail-outline"></i>
						<input type="email" class="input-text with-border" data-validation-error-msg="You did not enter a valid e-mail" data-validation="email" name="emailaddress-register" id="customer-emailaddress-register" placeholder="Email Address" required/>
					</div>

					<div class="input-with-icon-left" title="Should be at least 8 characters long" data-tippy-placement="bottom">
						<i class="icon-material-outline-lock"></i>
						<input type="password" class="input-text with-border"  name="password-confirmation" data-validation="length" data-validation-length="5-12" id="customer-password-register" placeholder="Password" required/>
					</div>

					<div class="input-with-icon-left">
						<i class="icon-material-outline-lock"></i>
						<input type="password" class="input-text with-border" name="password-repeat-register" data-validation="length" data-validation-length="5-12" id="customer-password-repeat-register" placeholder="Repeat Password" required/>
					</div>

					@include('general_boxes.customer-pin')

						<div class="input-with-icon-left location-margin">
							<i class="icon-material-outline-location-on"></i>
							<input id="autocomplete-input-customer" type="text" class="input-text with-border" placeholder="Your Location" name="location" required/>
							<span id="markLocation-customer" class="margin-bottom-20 button ripple-effect phone-btn mark-loc" >Pin <i class="icon-material-outline-my-location"></i></span>
						</div>

					<input type="hidden" name="lat" value="" required/>
					<input type="hidden" name="lng" value="" required/>

				</form>
				
				<!-- Button -->
				<button class="margin-top-10 button full-width button-sliding-icon ripple-effect" type="submit" form="01-register-account-form">Register <i class="icon-material-outline-arrow-right-alt"></i></button>
				</span>
				
				<!-- Form 02 -->
				<span class="popup-tab-content" id="chef-register" style="display:none;">
				<form method="post" id="02-register-account-form" action="{{ URL::to('/register_chef') }}">
						{{ csrf_field() }}
                    <div class="input-with-icon-left">
						<i class="icon-feather-user"></i>
						<input type="text" class="input-text with-border" data-validation="custom" data-validation-regexp="^([a-zA-Z]+)$" name="fullname-register" id="chef-fullname-register" placeholder="Full Name" required/>
					</div>
					
					<div class="input-with-icon-left">
						<i class="icon-feather-link-2"></i>
						<input type="text" class="input-text with-border" pattern="^([a-zA-Z0-9)$" data-validation="length alphanumeric" data-validation-length="5-10" name="username-register" id="chef-username-register" placeholder="Username" required/>
					</div>					
					
					<div class="input-with-icon-left">
						<i class="icon-material-baseline-mail-outline"></i>
						<input type="email" class="input-text with-border" data-validation-error-msg="You did not enter a valid e-mail" data-validation="email" name="emailaddress-register" id="chef-emailaddress-register" placeholder="Email Address" required/>
					</div>

					<div class="input-with-icon-left" title="Should be at least 8 characters long" data-tippy-placement="bottom">
						<i class="icon-material-outline-lock"></i>
						<input type="password" class="input-text with-border" name="password-register" data-validation="length" data-validation-length="5-12" id="chef-password-register" placeholder="Password" required/>
					</div>

					<div class="input-with-icon-left">
						<i class="icon-material-outline-lock"></i>
						<input type="password" class="input-text with-border" name="password-repeat-register" data-validation="length" data-validation-length="5-12" id="chef-password-repeat-register" placeholder="Repeat Password" required/>
					</div>
					
					<div class="input-with-icon-left">
						<i class="icon-line-awesome-mobile"></i>
						<input type="number" class="input-text with-border" name="phone-register" id="chef-phone-register" placeholder="Phone" required/>
						{{--<button class="button button-sliding-icon ripple-effect phone-btn">Send<i class="icon-material-outline-arrow-right-alt"></i></button>--}}
					</div>

					{{--<div class="input-with-icon-left">--}}
						{{--<i class="icon-material-outline-textsms"></i>--}}
						{{--<input type="number" class="input-text with-border" name="code-register" id="chef-code-register" placeholder="Code" required/>--}}
					{{--</div>--}}

					@include('general_boxes.chef-pin')

					<div class="input-with-icon-left location-margin">
							<i class="icon-material-outline-location-on"></i>
							<input id="autocomplete-input-chef" type="text" class="input-text with-border" placeholder="Your Location" name="location" required/>
							<span id="markLocation-chef" class="margin-bottom-20 button ripple-effect phone-btn mark-loc" >Pin <i class="icon-material-outline-my-location"></i></span>
					</div>

					<input type="hidden" name="lat" value="" required/>
					<input type="hidden" name="lng" value="" required/>
				</form>
				
				<!-- Button -->
				<button class="margin-top-10 button full-width button-sliding-icon ripple-effect" type="submit" form="02-register-account-form">Register <i class="icon-material-outline-arrow-right-alt"></i></button>
				</span>				
				
				{{--<!-- Social Login -->--}}
				{{--<div class="social-login-separator"><span>or</span></div>--}}
				{{--<div class="social-login-buttons">--}}
					{{--<button class="facebook-login ripple-effect"><i class="icon-brand-facebook-f"></i> Register via Facebook</button>--}}
					{{--<button class="google-login ripple-effect"><i class="icon-brand-google-plus-g"></i> Register via Google+</button>--}}
				{{--</div>--}}

			</div>
			

		</div>
	</div>
</div>
<!-- Sign In Popup / End -->
<script>
    $(document).ready(function () {
        $('#login-form').validate({ // initialize the plugin
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                number: {
                    required: true,
                    digits: true

                },
                minlength: {
                    required: true,
                    minlength: 5

                },
                maxlength: {
                    required: true,
                    maxlength: 8

                },
                minvalue: {
                    required: true,
                    min: 1

                },
                maxvalue: {
                    required: true,
                    max: 100

                },
                range: {
                    required: true,
                    range: [20, 40]

                },
                url: {
                    required: true,
                    url: true
                },
                filename: {
                    required: true,
                    extension: "jpeg|png"
                },
            }
        });
    });
</script>