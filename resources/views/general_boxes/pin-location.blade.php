<!-- Drag & Drop Location Popup -->
<!-- ================================================== -->
<div id="small-dialog-2" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

	<!--Tabs -->
	<div class="sign-in-form location-popup">

		<div class="popup-tabs-container">

			<!-- Tab -->
			<div class="popup-tab-content" id="tab2">

				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>Drag & Pin Location</h3>
				</div>
				<div id="mark-map"></div>
				<input id="exactMark" type="text" class="search-query" readonly>

				<!-- Button -->
				<button id="updateLocation" title="Close" class="mfp-close button full-width button-sliding-icon ripple-effect updateLoc-btn" type="submit" form="leave-review-form">Update Location <i class="icon-material-outline-arrow-right-alt"></i></button>

			</div>

		</div>
	</div>
</div>
<!-- Drag & Drop Popup / End -->