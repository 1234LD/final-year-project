<!-- Sign In Popup
================================================== -->
<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs order-otp">

	<!--Tabs -->
	<!--Result -->
	<div class="foodpole-user pop-up-meal">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<div class="verified-badge rec-meal" title="FoodPole Recommended" data-tippy-placement="right"></div>
					<a href="#"><img src="images/food-placeholder.jpg" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Sindhi Biryani</a></h4>
					<span>Jurry Abbas</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<!-- Fields -->
			<div class="offerings-fields">
				<div class="offerings-field">
					<select class="selectpicker default">
						<option selected>Single + 2 Kebabs + 500ml Pepsi - Rs. 150</option>
						<option>Double + 4 Kebabs + 1.5 Ltr Pepsi - Rs. 250<option>
					</select>
				</div>			
				<div class="offerings-field">
					<!-- Quantity Buttons -->
					<div class="qtyButtons">
						<div class="qtyDec"></div>
						<input type="number" name="qtyInput" value="1" min="1" max="50">
						<div class="qtyInc"></div>
					</div>
				</div>
			</div>
			<a href="#" class="button button-sliding-icon ripple-effect">Add Now <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->
</div>
<!-- Sign In Popup / End -->