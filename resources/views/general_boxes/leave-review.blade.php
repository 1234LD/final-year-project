<!-- Leave a Review for foodpole-user Popup
================================================== -->
<div id="small-dialog-2" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

	<!--Tabs -->
	<div class="sign-in-form">

		<ul class="popup-tabs-nav">
		</ul>

		<div class="popup-tabs-container">

			<!-- Tab -->
			<div class="popup-tab-content" id="tab2">
				
				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>Leave a Review {{$xid}}</h3>
					<span>Rate <a href="#"> {{ $orders->data[0]->chef->user->full_name }}</a>'s meals for your order# <a href="#">{{$orders->data[0]->id}}</a> </span>
				</div>
					
				<!-- Form -->
				<form method="post" id="leave-review-form">

					{{--<div class="feedback-yes-no">--}}
						{{--<strong>Was it prepared almost on time?</strong>--}}
						{{--<div class="radio">--}}
							{{--<input id="radio-1" name="radio" type="radio" required>--}}
							{{--<label for="radio-1"><span class="radio-label"></span> Yes</label>--}}
						{{--</div>--}}

						{{--<div class="radio">--}}
							{{--<input id="radio-2" name="radio" type="radio" required>--}}
							{{--<label for="radio-2"><span class="radio-label"></span> No</label>--}}
						{{--</div>--}}
					{{--</div>--}}

					{{--<div class="feedback-yes-no">--}}
						{{--<strong>Was this delivered on time?</strong>--}}
						{{--<div class="radio">--}}
							{{--<input id="radio-3" name="radio2" type="radio" required>--}}
							{{--<label for="radio-3"><span class="radio-label"></span> Yes</label>--}}
						{{--</div>--}}

						{{--<div class="radio">--}}
							{{--<input id="radio-4" name="radio2" type="radio" required>--}}
							{{--<label for="radio-4"><span class="radio-label"></span> No</label>--}}
						{{--</div>--}}
					{{--</div>--}}


                      {{--@foreach($orders->data-> as $item)--}}
                      {{--@foreach(order_line_items->data->menu_item as  $name)--}}
					<div class="feedback-yes-no">
						<strong>nihari</strong>
						<div class="leave-rating">
							<input type="radio" name="item1" id="item1-radio-1" value="1" required>
							<label for="item1-radio-1" class="icon-material-outline-star"></label>
							<input type="radio" name="item1" id="item1-radio-2" value="2" required>
							<label for="item1-radio-2" class="icon-material-outline-star"></label>
							<input type="radio" name="item1" id="item1-radio-3" value="3" required>
							<label for="item1-radio-3" class="icon-material-outline-star"></label>
							<input type="radio" name="item1" id="item1-radio-4" value="4" required>
							<label for="item1-radio-4" class="icon-material-outline-star"></label>
							<input type="radio" name="item1" id="item1-radio-5" value="5" required>
							<label for="item1-radio-5" class="icon-material-outline-star"></label>
						</div><div class="clearfix"></div>
					</div>
					{{--@endforeach--}}
					{{--<div class="feedback-yes-no">--}}
						{{--<strong>Rate Nali Nehari:</strong>--}}
						{{--<div class="leave-rating">--}}
							{{--<input type="radio" name="item2" id="item2-radio-1" value="1" required>--}}
							{{--<label for="item2-radio-1" class="icon-material-outline-star"></label>--}}
							{{--<input type="radio" name="item2" id="item2-radio-2" value="2" required>--}}
							{{--<label for="item2-radio-2" class="icon-material-outline-star"></label>--}}
							{{--<input type="radio" name="item2" id="item2-radio-3" value="3" required>--}}
							{{--<label for="item2-radio-3" class="icon-material-outline-star"></label>--}}
							{{--<input type="radio" name="item2" id="item2-radio-4" value="4" required>--}}
							{{--<label for="item2-radio-4" class="icon-material-outline-star"></label>--}}
							{{--<input type="radio" name="item2" id="item2-radio-5" value="5" required>--}}
							{{--<label for="item2-radio-5" class="icon-material-outline-star"></label>--}}
						{{--</div><div class="clearfix"></div>--}}
					{{--</div>		--}}

					{{--<div class="feedback-yes-no">--}}
						{{--<strong>Rate Chicken Pulao:</strong>--}}
						{{--<div class="leave-rating">--}}
							{{--<input type="radio" name="item3" id="item3-radio-1" value="1" required>--}}
							{{--<label for="item3-radio-1" class="icon-material-outline-star"></label>--}}
							{{--<input type="radio" name="item3" id="item3-radio-2" value="2" required>--}}
							{{--<label for="item3-radio-2" class="icon-material-outline-star"></label>--}}
							{{--<input type="radio" name="item3" id="item3-radio-3" value="3" required>--}}
							{{--<label for="item3-radio-3" class="icon-material-outline-star"></label>--}}
							{{--<input type="radio" name="item3" id="item3-radio-4" value="4" required>--}}
							{{--<label for="item3-radio-4" class="icon-material-outline-star"></label>--}}
							{{--<input type="radio" name="item3" id="item3-radio-5" value="5" required>--}}
							{{--<label for="item3-radio-5" class="icon-material-outline-star"></label>--}}
						{{--</div><div class="clearfix"></div>--}}
					{{--</div>						--}}

					<textarea class="with-border" placeholder="Comment" name="message2" id="message2" cols="7" required></textarea>

				</form>
				
				<!-- Button -->
				<button class="button full-width button-sliding-icon ripple-effect" type="submit" form="leave-review-form">Submit <i class="icon-material-outline-arrow-right-alt"></i></button>

			</div>

		</div>
	</div>
</div>
<!-- Leave a Review Popup / End -->