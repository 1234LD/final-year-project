<!-- Result List Container -->
<div class="foodpole-users-container foodpole-users-grid-layout margin-top-35">

@if($results->items())
	@foreach($results->items() as $key=>$meal_item)
	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				@if($results->items()[$key]['verify'] && $results->items()[$key]['verified'])
				<span class="bookmark-icon bookmarked"></span>
				@endif
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					@if($results->items()[$key]['verified'])
						<div class="verified-badge rec-meal" title="FoodPole Recommended" data-tippy-placement="right"></div>
					@endif
					@if($results->items()[$key]['item_image'] == null)
						<a href="{{ URL::to('meal/'.$results->items()[$key]['username'].'/'.$results->items()[$key]['menu_item_id']).'/details' }}"><img src="{{ URL::asset('images/food-placeholder.jpg') }}" alt=""></a>
					@else
						<?php $exploded_image = explode(config('constants.DEFAULT_GUM'),$results->items()[$key]['item_image']); ?>

						<a href="{{ URL::to('meal/'.$results->items()[$key]['username'].'/'.$results->items()[$key]['menu_item_id']).'/details' }}"><img src="{{ URL::asset('item_images/'.$exploded_image[0]) }}" alt=""></a>
					@endif
				</div>
				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="{{ URL::to('meal/'.$results->items()[$key]['username'].'/'.$results->items()[$key]['menu_item_id']).'/details' }}">{{ $results->items()[$key]['item_name'] }}</a></h4>
					<span><a href="{{ URL::to('chef/'.$results->items()[$key]['users_id'].'/'.$results->items()[$key]['username'].'/chef_page') }}">{{ $results->items()[$key]['full_name'] }}</a></span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					@if($results->items()[$key]['item_rating'] != null)
					<div class="star-rating" data-rating="{{ $results->items()[$key]['item_rating'] }}"></div>
					@endif
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Price <strong>Rs. {{ $results->items()[$key]['price'] }}</strong></li>
					@if($results->items()[$key]['category_name'] == config('constants.DEFAULT_QUICK-MEAL_CATEGORY'))
						<li>Servings Left <strong>{{ $results->items()[$key]['item_quantity'] }}</strong></li>
					@else
						<li>Ready in <strong>{{ $results->items()[$key]['item_time'] }} mins</strong></li>
					@endif
					<li>Mode
						@if($results->items()[$key]['delivery_type'] == config('constants.DEFAULT_BOTH_DELIVERY'))
							<strong>Delivery & Take Away</strong>
						@else
							<strong>{{ $results->items()[$key]['delivery_type'] }}</strong>
						@endif</li>
				</ul>
			</div>
{{--<<<<<<< Updated upstream--}}
			{{--<a href="{{ 'meal/'.$results->items()[$key]->menu_item_id.'/details' }}" class="button button-sliding-icon ripple-effect">Meal Details <i class="icon-material-outline-arrow-right-alt"></i></a>--}}
{{--=======--}}
	{{--<<<<<<< Updated upstream--}}
			<a href="{{ URL::to('meal/'.$results->items()[$key]['username'].'/'.$results->items()[$key]['menu_item_id']).'/details' }}" class="button button-sliding-icon ripple-effect">Meal Details <i class="icon-material-outline-arrow-right-alt"></i></a>
{{--=======--}}
			{{--<a href="{{ URL::to('meal/'.$results->items()[$key]->menu_item_id.'/details') }}" class="button button-sliding-icon ripple-effect">Meal Details <i class="icon-material-outline-arrow-right-alt"></i></a>--}}
{{-->>>>>>> Stashed changes--}}
{{-->>>>>>> Stashed changes--}}
		</div>
	</div>
	<!-- Result / End -->



	@endforeach
@else
	<img class="no-results-found" src="{{ URL::to('images/no-results.jpg') }}" alt="No results found" />
@endif
        <!-- Sign In Popup / End -->
        {{--<script>--}}
            {{--var cclass;--}}
            {{--$(".item-order-pop").click(function () {--}}

                {{--var id1 = $(this).parent().parent().children(".item-id").text();--}}
                {{--cclass = "."+id1;--}}
                {{--console.log(cclass);--}}
                {{--$(this).closest("body").find(cclass).removeClass('hide');--}}
            {{--});--}}
            {{--$(document).click(function(e){--}}

                {{--if($(e.target).closest('#sign-in-dialog').length == 0) return false;--}}
                {{--$(cclass).addClass('hide');--}}
            {{--});--}}
        {{--</script>--}}
	{{--@include('general_boxes.meal-options-pop-up')--}}

</div>
<!-- foodpole-users Container / End -->