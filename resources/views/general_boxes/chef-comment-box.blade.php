<!-- Tab -->
<div class="popup-tab-content reason-area comment-box">

    <!-- Welcome Text -->
    <div class="welcome-text">
        <h3>Comment</h3>
    </div>

    <!-- Form -->
    <form method="post" id="leave-review-form">

        <textarea class="with-border" placeholder="Please write your comment here" name="comment" id="message2" cols="7" required></textarea>

    </form>

    <!-- Button -->
    <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="leave-review-form">Submit <i class="icon-material-outline-arrow-right-alt"></i></button>

</div>