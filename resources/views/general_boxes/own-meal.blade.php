<!-- Sign In Popup
================================================== -->
<div id="small-dialog-3" class="zoom-anim-dialog mfp-hide dialog-with-tabs order-otp">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
        </ul>

        <div class="popup-tabs-container">

            <!-- Order OTP Verification 01 -->
            <div class="popup-tab-content" id="forgot-password-01">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <div class="breathing-icon c-done"><i class="icon-feather-eye"></i></div>
                    <h3>You can not order your own meal!</h3>
                    <h3>Please remove your meals. Thank you.</h3>
                </div>


            </div>

            <!-- Forgot Password 02 -->
            <div class="popup-tab-content" id="forgot-password-02">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>You can not order your own meal!</h3>
                </div>



            </div>

            <!-- Forgot Password 03 -->
            <div class="popup-tab-content" id="forgot-password-03">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Phone Verified!</h3>
                    <span class="phone-successful"><i class="icon-material-outline-check-circle"></i></span>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- Sign In Popup / End -->