<!-- Result List Container -->
<div class="foodpole-users-container foodpole-users-grid-layout margin-top-35">
	
	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<div class="verified-badge rec-meal" title="FoodPole Recommended" data-tippy-placement="right"></div>
					<a href="#"><img src="images/food-placeholder.jpg" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Sindhi Biryani</a></h4>
					<span>Jurry Abbas</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Price <strong>Rs. 150</strong></li>
					<li>Ready in <strong>20 min</strong></li>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#sign-in-dialog" class="button button-sliding-icon ripple-effect popup-with-zoom-anim">Add to Order <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->
	
	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<a href="#"><img src="images/food-placeholder.jpg" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Sindhi Biryani</a></h4>
					<span>Jurry Abbas</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Price <strong>Rs. 150</strong></li>
					<li>Ready in <strong>20 min</strong></li>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#sign-in-dialog" class="button button-sliding-icon ripple-effect popup-with-zoom-anim">Add to Order <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->
	
	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<a href="#"><img src="images/food-placeholder.jpg" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Sindhi Biryani</a></h4>
					<span>Jurry Abbas</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Price <strong>Rs. 150</strong></li>
					<li>Ready in <strong>20 min</strong></li>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#sign-in-dialog" class="button button-sliding-icon ripple-effect popup-with-zoom-anim">Add to Order <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->

	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<div class="verified-badge rec-meal" title="FoodPole Recommended" data-tippy-placement="right"></div>
					<a href="#"><img src="images/food-placeholder.jpg" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Sindhi Biryani</a></h4>
					<span>Jurry Abbas</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Price <strong>Rs. 150</strong></li>
					<li>Ready in <strong>20 min</strong></li>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#sign-in-dialog" class="button button-sliding-icon ripple-effect popup-with-zoom-anim">Add to Order <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->

	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<a href="#"><img src="images/food-placeholder.jpg" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Sindhi Biryani</a></h4>
					<span>Jurry Abbas</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Price <strong>Rs. 150</strong></li>
					<li>Ready in <strong>20 min</strong></li>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#sign-in-dialog" class="button button-sliding-icon ripple-effect popup-with-zoom-anim">Add to Order <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->

	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<div class="verified-badge rec-meal" title="FoodPole Recommended" data-tippy-placement="right"></div>
					<a href="#"><img src="images/food-placeholder.jpg" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Sindhi Biryani</a></h4>
					<span>Jurry Abbas</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Price <strong>Rs. 150</strong></li>
					<li>Ready in <strong>20 min</strong></li>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#sign-in-dialog" class="button button-sliding-icon ripple-effect popup-with-zoom-anim">Add to Order <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->

	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<a href="#"><img src="images/food-placeholder.jpg" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Sindhi Biryani</a></h4>
					<span>Jurry Abbas</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Price <strong>Rs. 150</strong></li>
					<li>Ready in <strong>20 min</strong></li>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#sign-in-dialog" class="button button-sliding-icon ripple-effect popup-with-zoom-anim">Add to Order <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->

	<!--Result -->
	<div class="foodpole-user">

		<!-- Overview -->
		<div class="foodpole-user-overview">
			<div class="foodpole-user-overview-inner">
				
				<!-- Bookmark Icon -->
				<span class="bookmark-icon"></span>
				
				<!-- Avatar -->
				<div class="foodpole-user-avatar">
					<div class="verified-badge rec-meal" title="FoodPole Recommended" data-tippy-placement="right"></div>
					<a href="#"><img src="images/food-placeholder.jpg" alt=""></a>
				</div>

				<!-- Name -->
				<div class="foodpole-user-name">
					<h4><a href="#">Sindhi Biryani</a></h4>
					<span>Jurry Abbas</span>
				</div>

				<!-- Rating -->
				<div class="foodpole-user-rating">
					<div class="star-rating" data-rating="4.9"></div>
				</div>
			</div>
		</div>
		
		<!-- Details -->
		<div class="foodpole-user-details">
			<div class="foodpole-user-details-list search-results-dt">
				<ul>
					<li>Price <strong>Rs. 150</strong></li>
					<li>Ready in <strong>20 min</strong></li>
					<li>Mode <strong>Delivery & Take Away</strong></li>					
				</ul>
			</div>
			<a href="#sign-in-dialog" class="button button-sliding-icon ripple-effect popup-with-zoom-anim">Add to Order <i class="icon-material-outline-arrow-right-alt"></i></a>
		</div>
	</div>
	<!-- Result / End -->

	@include('general_boxes.meal-options-pop-up')

</div>
<!-- foodpole-users Container / End -->