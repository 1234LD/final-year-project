<!-- Pagination -->
<div class="clearfix"></div>
@if(($results->currentPage() < $results->lastPage() + 1) && $results->hasPages())
<div class="pagination-container margin-top-20 margin-bottom-20">
	<nav class="pagination">
		<ul>
			<?php if(($results->lastPage() > 4)){?>
			<li class="pagination-arrow"><a href="{{ $results->previousPageUrl() }}" class="ripple-effect"><i class="icon-material-outline-keyboard-arrow-left"></i></a></li>
				<?php };

				$pagecount = $results->currentPage();
                $urls = $results->getUrlRange($pagecount, $pagecount+3);
                $nextpage = $results->currentPage();
                $endDiff = $results->lastPage() - $results->currentPage();
                if($results->lastPage() > 3 && $endDiff < 4){
                for($i = ($results->lastPage() - 3); $i < $results->lastPage()+1; $i++){
                 ?>
				<li><a href="{{$results->url($i)}}" class="ripple-effect <?php if($results->currentPage() == $i) { ?>current-page <?php }?>">{{ $i }}</a></li>
                <?php

                }

                }elseif ($results->currentPage() < 4){
                    $startCount = 4;
                    if($results->lastPage() < 4){
                        $startCount = $results->lastPage();
					}
                for($i = 1; $i < $startCount+1; $i++){
                ?>
				<li><a href="{{$results->url($i)}}" class="ripple-effect <?php if($results->currentPage() == $i) { ?>current-page <?php }?>">{{ $i }}</a></li>
                <?php

                }

                }else{
			    for($i = $pagecount; $i < $pagecount+4; $i++){
					if($i < ($results->lastPage() + 1)){ ?>
				<li><a href="{{$urls[$i]}}" class="ripple-effect <?php if($results->currentPage() == $i) { ?>current-page <?php }?>">{{ $i }}</a></li>
						<?php $nextpage++;
					}
				}
				}

                 if(($results->lastPage() - $results->currentPage()) > 3) {?>
			<li class="pagination-arrow"><a href="{{ $results->nextPageUrl() }}" class="ripple-effect"><i class="icon-material-outline-keyboard-arrow-right"></i></a></li>
			<?php } ?>
		</ul>
	</nav>
</div>
<div class="clearfix"></div>
@endif
<!-- Pagination / End -->