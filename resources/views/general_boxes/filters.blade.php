<div class="full-page-sidebar hidden-sidebar">
	<div class="full-page-sidebar-inner" data-simplebar>

		<div class="sidebar-container">

			{{--<form method="GET" action="{{ URL::to('filter/request/'.$lat.'/'.$lng) }}">--}}
				{{--{{ csrf_field() }}--}}
			<!-- Location -->
			<div class="sidebar-widget">
				<h3>Location</h3>
						<div class="input-with-icon">
							<input id="autocomplete-input" type="text" placeholder="Your Location">
							<i class="icon-material-outline-location-on"></i>
						</div>
					<div class="intro-search-button locationbutton sr-btn">
						<a href="#small-dialog-2" id ="markLocation" class="popup-with-zoom-anim"><button class="popup-with-zoom-anim button ripple-effect location-btn"></button></a>
					</div>
			</div>

			<!-- Keywords -->
			<div class="sidebar-widget">
				<h3>Keyword</h3>
				<div class="keywords-container">
					<div class="keyword-input-container filter-keyword">
						<input type="text" class="keyword-input" name="keyword" placeholder="e.g. Biryani, Karahi etc."/>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			
			<!-- Category -->
			<div class="sidebar-widget">
				<h3>Category</h3>
				<select class="selectpicker default filter-category" title="All Categories" >
					@foreach(collect($allmeals)->unique('category_name') as $name)
						<option>{{ $name->category_name }}</option>
					@endforeach
				</select>
			</div>
			
			<!-- Price -->
			<div class="sidebar-widget filter-price">
				<h3>Price</h3>
				<div class="margin-top-55"></div>

				<!-- Range Slider -->
				<input class="range-slider " type="text" name="price" value="" data-slider-currency="Rs. " data-slider-min="0" data-slider-max="10000" data-slider-step="1" data-slider-value="[0,10000]"/>
			</div>	

			<!-- Distance -->
			<div class="sidebar-widget filter-distance">
				<h3>Distance</h3>
				<div class="margin-top-55"></div>

				<!-- Single Slider -->
				<input class="range-slider-single" type="text" name="distance" data-slider-min="1" data-slider-max="150" data-slider-step="5" data-slider-value="1"/>
			</div>		

			<!-- Preparation Time -->
			<div class="sidebar-widget filter-time">
				<h3>Preparation Time</h3>
				<div class="margin-top-55"></div>

				<!-- Single Slider -->
				<input class="range-slider-single" type="text" name="preparation_max_time" data-slider-min="1" data-slider-max="180" data-slider-step="1" data-slider-value="60"/>
			</div>	

			<!-- Ratings -->
			<div class="sidebar-widget">
				<h3>Rating</h3>
				<div class="margin-top-15"></div>
				<div class="leave-rating filter-rating">
					<input type="radio" name="rating" id="item1-radio-1" value="5">
					<label for="item1-radio-1" class="icon-material-outline-star"></label>
					<input type="radio" name="rating" id="item1-radio-2" value="4">
					<label for="item1-radio-2" class="icon-material-outline-star"></label>
					<input type="radio" name="rating" id="item1-radio-3" value="3">
					<label for="item1-radio-3" class="icon-material-outline-star"></label>
					<input type="radio" name="rating" id="item1-radio-4" value="2">
					<label for="item1-radio-4" class="icon-material-outline-star"></label>
					<input type="radio" name="rating" id="item1-radio-5" value="1">
					<label for="item1-radio-5" class="icon-material-outline-star"></label>
				</div><div class="clearfix"></div>
			</div>				

			<!-- Mode -->
			<div class="sidebar-widget filter-mode">
				<h3>Mode</h3>
				<div class="margin-top-15"></div>
				<div class="checkbox">
					<input type="checkbox" id="mode1" name="delivery_type" value="Delivery">
					<label for="mode1"><span class="checkbox-icon"></span>
						Delivery					
					</label>
				</div>
				<br>
				<div class="checkbox">
					<input type="checkbox" id="mode2" name="delivery_type" value="Take Away">
					<label for="mode2"><span class="checkbox-icon"></span>
						Take Away					
					</label>
				</div>		
			</div>

		</div>
		<!-- Sidebar Container / End -->

		<!-- Search Button -->
		<div class="sidebar-search-button-container">
			<button class="button ripple-effect search_click">Search</button>
		</div>
		<!-- Search Button / End-->

	</div>
</div>
<!-- Full Page Sidebar / End -->