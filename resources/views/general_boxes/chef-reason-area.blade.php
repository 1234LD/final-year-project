<!-- Tab -->
<div class="popup-tab-content reason-area reason-only">

    <!-- Welcome Text -->
    <div class="welcome-text">
        <h3>Order Decline Reason!</h3>
    </div>

    <!-- Form -->
    <form action="{{ URL::to('decline-reason/'.$order_item->id.'/'.config('constants.DEFAULT_ORDER_DECLINED'))}}"   method="GET"   id="leave-review-form">

        <textarea class="with-border" placeholder="Please share why would you like to decline this order?" name="reason" id="message2" cols="7" required></textarea>
        <input type="hidden" name="order_id" value="{{ $order_item->id}}" required/>


    <!-- Button -->
    <button class="button full-width button-sliding-icon ripple-effect"  type="submit"  >Submit <i class="icon-material-outline-arrow-right-alt"></i></button>
    </form>
</div>