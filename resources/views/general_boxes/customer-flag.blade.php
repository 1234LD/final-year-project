<div class="popup-tab-content reason-area customer-flag">
<div class="welcome-text">
    <h3>This order will be flagged now!</h3>
    <span>Please share more information regarding the food that you recieved.</span>
</div>

<!-- Form -->
<form method="get"   action="{{ URL::to('flag-rating/'.$order_item->id)}} id="leave-review-form">

    <textarea class="with-border" placeholder="Tell us more about it..." name="flag_reason" id="message2" cols="7" required></textarea>


<!-- Button -->
<button class="button full-width button-sliding-icon ripple-effect" type="submit" >Submit <i class="icon-material-outline-arrow-right-alt"></i></button>
    </form>

</div>