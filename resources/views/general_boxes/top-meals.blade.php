<!-- Highest Rated foodpole-users -->
<?php //dd($renderedArray->top_meal); ?>
<div class="section padding-top-50 full-width-carousel-fix">
	<div class="container">
		<div class="row">

			<div class="col-xl-12">
				<!-- Section Headline -->
				<div class="section-headline margin-top-0 margin-bottom-25">
					<h3>Top meals on FoodPole</h3>
				</div>
			</div>

			<div class="col-xl-12">
				<div class="default-slick-carousel foodpole-users-container foodpole-users-grid-layout">

					@foreach($renderedArray->top_meal as $meal)
					<!--Result -->
					<div class="foodpole-user">

						<!-- Overview -->
						<div class="foodpole-user-overview">
							<div class="foodpole-user-overview-inner">
								
								<!-- Bookmark Icon -->
								<span class="bookmark-icon"></span>
								
								<!-- Avatar -->

								<div class="foodpole-user-avatar">
									@if($meal->verified == true)
										<div class="verified-badge rec-meal" title="FoodPole Recommended" data-tippy-placement="right"></div>
									@endif
										@if($meal->item_image == null)
											<a href="{{ URL::to('meal/'.$meal->username.'/'.$meal->id).'/details' }}"><img src="{{ URL::asset('images/food-placeholder.jpg') }}" alt=""></a>
										@else
                                            <?php $exploded_image = explode(config('constants.DEFAULT_GUM'),$meal->item_image->item_image); ?>

											<a href="{{ URL::to('meal/'.$meal->username.'/'.$meal->id).'/details' }}"><img src="{{ URL::asset('item_images/'.$exploded_image[0]) }}" alt=""></a>
										@endif
								</div>

								<!-- Name -->
								<div class="foodpole-user-name">
									<h4><a href="{{ URL::to('meal/'.$meal->username.'/'.$meal->id).'/details' }}">{{ $meal->item_name }}</a></h4>
									<span>{{ $meal->full_name }}</span>
								</div>

								<!-- Rating -->
								<div class="foodpole-user-rating">
									<div class="star-rating" data-rating="{{ $meal->rating }}"></div>

								</div>
							</div>
						</div>
						
						<!-- Details -->
						<div class="foodpole-user-details">
							<div class="foodpole-user-details-list search-results-dt">
								<ul>
									<li>Price <strong>Rs. {{ $meal->price }}</strong></li>
									<li>Ready in <strong>{{ $meal->item_time }} min</strong></li>
									@if($meal->delivery_type == config('constants.DEFAULT_BOTH_DELIVERY'))
										<li>Mode <strong>Delivery & Take Away</strong></li>
									@else
										<li>Mode <strong>{{ $meal->delivery_type }}</strong></li>
									@endif
								</ul>
							</div>
							<a href="{{ URL::to('meal/'.$meal->username.'/'.$meal->id).'/details' }}" class="button button-sliding-icon ripple-effect ">Meal Details <i class="icon-material-outline-arrow-right-alt"></i></a>
						</div>
					</div>
					<!-- Result / End -->
					@endforeach

				</div>
			</div>

		</div>
	</div>
</div>

@include('general_boxes.meal-options-pop-up')