<!-- Tab -->
<div class="popup-tab-content reason-area reason-only">

    <!-- Welcome Text -->
    <div class="welcome-text">
        <h3>Order Cancle Reason!</h3>
    </div>

    <!-- Form -->
    <form method="get" action="{{ URL::to('decline-reason/'.$order_item->id.'/'.config('constants.DEFAULT_ORDER_CANCELLED'))}}" id="leave-review-form">

        <textarea class="with-border" placeholder="Please share why would you like to cancle this order?" name="reason" id="message2" cols="7" required></textarea>



    <!-- Button -->
    <button class="button full-width button-sliding-icon ripple-effect" type="submit" >Submit <i class="icon-material-outline-arrow-right-alt"></i></button>
    </form>
</div>