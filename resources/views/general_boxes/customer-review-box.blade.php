<div class="popup-tab-content reason-area customer-review">
    <div class="welcome-text">
    <h3>Leave a Review</h3>

    {{--<span>Rate <a href="#"> {{  $order_item->chef->user->full_name }}</a>'s meals for your order# <a href="#">{{ $order_item->id}}</a> </span>--}}

    <span>Rate <a href="#"> {{ $order_item->chef->user->full_name }}</a>'s meals for your order# <a href="#">{{$order_item->id}}</a> </span>

</div>

<!-- Form -->
<form method="get"  action="{{ URL::to('feedback-rating/'.$order_item->id)}}" id="leave-review-form">

    {{--<div class="feedback-yes-no">--}}
    {{--<strong>Was it prepared almost on time?</strong>--}}
    {{--<div class="radio">--}}
    {{--<input id="radio-1" name="radio" type="radio" required>--}}
    {{--<label for="radio-1"><span class="radio-label"></span> Yes</label>--}}
    {{--</div>--}}

    {{--<div class="radio">--}}
    {{--<input id="radio-2" name="radio" type="radio" required>--}}
    {{--<label for="radio-2"><span class="radio-label"></span> No</label>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="feedback-yes-no">--}}
    {{--<strong>Was this delivered on time?</strong>--}}
    {{--<div class="radio">--}}
    {{--<input id="radio-3" name="radio2" type="radio" required>--}}
    {{--<label for="radio-3"><span class="radio-label"></span> Yes</label>--}}
    {{--</div>--}}

    {{--<div class="radio">--}}
    {{--<input id="radio-4" name="radio2" type="radio" required>--}}
    {{--<label for="radio-4"><span class="radio-label"></span> No</label>--}}
    {{--</div>--}}
    {{--</div>--}}


    {{--@foreach($orders->data-> as $item)--}}
    {{--@foreach(order_line_items->data->menu_item as  $name)--}}

    <div class="feedback-yes-no">

        @foreach($order_item->order_line_items->data as $key=>$item)
            <strong>{{ $item->menu_item->item_name }}</strong>
            <div class="leave-rating"  >
                <input type="hidden" name="menu_item_id" value="{{$item->menu_item->id}}" required/>

                <input type="radio" name="order_rating[{{$item->menu_item->id}}]" id="item-radio-{{$order_item->id}}-{{ $key }}" value="5" required>
                <label for="item-radio-{{$order_item->id}}-{{ $key }}" class="icon-material-outline-star"></label>

                <input type="radio" name="order_rating[{{$item->menu_item->id}}]" id="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}" value="4" required>
                <label for="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>

                <input type="radio" name="order_rating[{{$item->menu_item->id}}]" id="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}" value="3" required>
                <label for="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>

                <input type="radio" name="order_rating[{{$item->menu_item->id}}]" id="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}{{ $key }}" value="2" required>

                <label for="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>

                <input type="radio" name="order_rating[{{$item->menu_item->id}}]" id="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}{{ $key }}{{ $key }}" value="1" required>
                <label for="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>
            </div>
            <div class="clearfix"></div>
        @endforeach

        {{--@foreach($order_item->order_line_items->data as $key=>$item)--}}
            {{--<strong>{{ $item->menu_item->item_name }}</strong>--}}
            {{--<div class="leave-rating"  >--}}
                {{--<input type="hidden" name="menu_item_id" value="{{$item->menu_item->id}}" required/>--}}

                {{--<input type="radio" name="order_rating[{{$item->menu_item->id}}]" id="item-radio-{{ $key }}" value="5" required>--}}
                {{--<label for="item-radio-{{ $key }}" class="icon-material-outline-star"></label>--}}

                {{--<input type="radio" name="order_rating[{{$item->menu_item->id}}]" id="item-radio-{{ $key }}{{ $key }}" value="4" required>--}}
                {{--<label for="item-radio-{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>--}}

                {{--<input type="radio" name="order_rating[{{$item->menu_item->id}}]" id="item-radio-{{ $key }}{{ $key }}{{ $key }}" value="3" required>--}}
                {{--<label for="item-radio-{{ $key }}{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>--}}

                {{--<input type="radio" name="order_rating[{{$item->menu_item->id}}]" id="item-radio-{{ $key }}{{ $key }}{{ $key }}{{ $key }}" value="2" required>--}}

                {{--<label for="item-radio-{{ $key }}{{ $key }}{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>--}}

                {{--<input type="radio" name="order_rating[{{$item->menu_item->id}}]" id="item-radio-{{ $key }}{{ $key }}{{ $key }}{{ $key }}{{ $key }}" value="1" required>--}}
                {{--<label for="item-radio-{{ $key }}{{ $key }}{{ $key }}{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>--}}
            {{--</div>--}}
            {{--<div class="clearfix"></div>--}}
        {{--@endforeach--}}

        {{--@foreach($order_item->order_line_items->data as $key=>$item)--}}
            {{--<strong>{{ $item->menu_item->item_name }}</strong>--}}
            {{--<div class="leave-rating">--}}
                {{--<input type="radio" name="item{{$order_item->id}}-{{ $key }}" id="item-radio-{{$order_item->id}}-{{ $key }}" value="5" required>--}}
                {{--<label for="item-radio-{{$order_item->id}}-{{ $key }}" class="icon-material-outline-star"></label>--}}
                {{--<input type="radio" name="item{{$order_item->id}}-{{ $key }}" id="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}" value="4" required>--}}
                {{--<label for="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>--}}
                {{--<input type="radio" name="item{{$order_item->id}}-{{ $key }}" id="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}" value="3" required>--}}
                {{--<label for="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>--}}
                {{--<input type="radio" name="item{{$order_item->id}}-{{ $key }}" id="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}{{ $key }}" value="2" required>--}}
                {{--<label for="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>--}}
                {{--<input type="radio" name="item{{$order_item->id}}-{{ $key }}" id="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}{{ $key }}{{ $key }}" value="1" required>--}}
                {{--<label for="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>--}}
            {{--</div><div class="clearfix"></div>--}}
        {{--@endforeach--}}

            {{--@foreach($order_item->order_line_items->data as $key=>$item)--}}
                {{--<strong>{{ $item->menu_item->item_name }}</strong>--}}
                {{--<div class="leave-rating"  >--}}
                    {{--<input type="hidden" name="menu_item_id" value="{{$item->menu_item->id}}" required/>--}}

                    {{--<input type="radio" name="order_rating[{{$item->menu_item->id}}]" id="item-radio-{{$order_item->id}}-{{ $key }}" value="5" required>--}}
                    {{--<label for="item-radio-{{$order_item->id}}-{{ $key }}" class="icon-material-outline-star"></label>--}}

                    {{--<input type="radio" name="order_rating[{{$item->menu_item->id}}]" id="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}" value="4" required>--}}
                    {{--<label for="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>--}}

                    {{--<input type="radio" name="order_rating[{{$item->menu_item->id}}]" id="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}" value="3" required>--}}
                    {{--<label for="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>--}}

                    {{--<input type="radio" name="order_rating[{{$item->menu_item->id}}]" id="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}{{ $key }}" value="2" required>--}}

                    {{--<label for="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>--}}

                    {{--<input type="radio" name="order_rating[{{$item->menu_item->id}}]" id="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}{{ $key }}{{ $key }}" value="1" required>--}}
                    {{--<label for="item-radio-{{$order_item->id}}-{{ $key }}{{ $key }}{{ $key }}{{ $key }}{{ $key }}" class="icon-material-outline-star"></label>--}}
                {{--</div>--}}
                {{--<div class="clearfix"></div>--}}
            {{--@endforeach--}}

    </div>

    {{--@endforeach--}}
    {{--<div class="feedback-yes-no">--}}
    {{--<strong>Rate Nali Nehari:</strong>--}}
    {{--<div class="leave-rating">--}}
    {{--<input type="radio" name="item2" id="item2-radio-1" value="1" required>--}}
    {{--<label for="item2-radio-1" class="icon-material-outline-star"></label>--}}
    {{--<input type="radio" name="item2" id="item2-radio-2" value="2" required>--}}
    {{--<label for="item2-radio-2" class="icon-material-outline-star"></label>--}}
    {{--<input type="radio" name="item2" id="item2-radio-3" value="3" required>--}}
    {{--<label for="item2-radio-3" class="icon-material-outline-star"></label>--}}
    {{--<input type="radio" name="item2" id="item2-radio-4" value="4" required>--}}
    {{--<label for="item2-radio-4" class="icon-material-outline-star"></label>--}}
    {{--<input type="radio" name="item2" id="item2-radio-5" value="5" required>--}}
    {{--<label for="item2-radio-5" class="icon-material-outline-star"></label>--}}
    {{--</div><div class="clearfix"></div>--}}
    {{--</div>		--}}

    {{--<div class="feedback-yes-no">--}}
    {{--<strong>Rate Chicken Pulao:</strong>--}}
    {{--<div class="leave-rating">--}}
    {{--<input type="radio" name="item3" id="item3-radio-1" value="1" required>--}}
    {{--<label for="item3-radio-1" class="icon-material-outline-star"></label>--}}
    {{--<input type="radio" name="item3" id="item3-radio-2" value="2" required>--}}
    {{--<label for="item3-radio-2" class="icon-material-outline-star"></label>--}}
    {{--<input type="radio" name="item3" id="item3-radio-3" value="3" required>--}}
    {{--<label for="item3-radio-3" class="icon-material-outline-star"></label>--}}
    {{--<input type="radio" name="item3" id="item3-radio-4" value="4" required>--}}
    {{--<label for="item3-radio-4" class="icon-material-outline-star"></label>--}}
    {{--<input type="radio" name="item3" id="item3-radio-5" value="5" required>--}}
    {{--<label for="item3-radio-5" class="icon-material-outline-star"></label>--}}
    {{--</div><div class="clearfix"></div>--}}
    {{--</div>						--}}

    <textarea class="with-border" placeholder="Comment" name="feedback" id="message2" cols="7" required></textarea>

<!-- Button -->
<button class="button full-width button-sliding-icon ripple-effect" type="submit" >Submit <i class="icon-material-outline-arrow-right-alt"></i></button>
</form>


</div>

