	<!-- Dashboard Sidebar
	================================================== -->
	<div class="dashboard-sidebar">
		<div class="dashboard-sidebar-inner" data-simplebar>
			<div class="dashboard-nav-container">

				<!-- Responsive Navigation Trigger -->
				<a href="#" class="dashboard-responsive-nav-trigger">
					<span class="hamburger hamburger--collapse" >
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</span>
					<span class="trigger-title">Dashboard Navigation</span>
				</a>
				
				<!-- Navigation -->
				<div class="dashboard-nav">
					<div class="dashboard-nav-inner">

						<ul data-submenu-title="Start">
							<li><a href="#"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>
						</ul>
						
						<ul data-submenu-title="Orders">
							<li><a href="{{ URL::asset('/general/user-all-orders')}}"><i class="icon-material-outline-restaurant"></i> My Orders</a></li>
						</ul>

						<ul data-submenu-title="Account">
							@if(Session::get('user_type') != config('constants.DEFAULT_CHEF_TYPE'))
								<li class="active"><a href="{{ URL::asset('/general/user-profile')}}"><i class="icon-material-outline-settings"></i> Settings</a></li>
							@endif
							<li><a href="{{ URL::asset('/general/user-logout')}}"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
						</ul>
						
					</div>
				</div>
				<!-- Navigation / End -->

			</div>
		</div>
	</div>
	<!-- Dashboard Sidebar / End -->