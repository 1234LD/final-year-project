<!-- Leave a Review for foodpole-user Popup
================================================== -->
<div id="small-dialog-1" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

	<!--Tabs -->
	<div class="sign-in-form">

		<ul class="popup-tabs-nav">
		</ul>

		<div class="popup-tabs-container">

			<!-- Tab -->
			<div class="popup-tab-content" id="tab2">
				
				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>This order will be flagged now!</h3>
					<span>Please share more information regarding the food that you recieved.</span>
				</div>
					
				<!-- Form -->
				<form method="post" id="leave-review-form">				

					<textarea class="with-border" placeholder="Tell us more about it..." name="message2" id="message2" cols="7" required></textarea>

				</form>
				
				<!-- Button -->
				<button class="button full-width button-sliding-icon ripple-effect" type="submit" form="leave-review-form">Submit <i class="icon-material-outline-arrow-right-alt"></i></button>

			</div>

		</div>
	</div>
</div>
<!-- Leave a Review Popup / End -->