<!doctype html>
<html lang="en">

@include('includes.header')
<body>
@include('includes.body-front')

<!-- Wrapper -->
<div id="wrapper">
@include('includes.header-dashboard')
@include('includes.top-bar-loggedin')
<!-- Content
    ================================================== -->
    @if (session('status'))
        <div class="alert alert-danger">
            <center><h3> {{ session('status') }} </h3></center>
        </div>
    @endif

    <div class="sign-in-form forgot-password-page">

        <div class="popup-tabs-container">
            <div class="popup-tab-content" id="forgot-password-01">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Let's recover your account!</h3>
                </div>

                <!-- Form 01 -->
                <form method="POST" id="forgot-password-form-01" action="{{ URL::to('submit/forgot_password/request') }}">
                    {{ csrf_field() }}
                    <div class="input-with-icon-left">
                        <i class="icon-feather-user"></i>
                        <input type="email" class="input-text with-border" name="email" id="fp-emailphone" placeholder="Email Address or Phone" required/>
                    </div>
                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="forgot-password-form-01">Send<i class="icon-material-outline-arrow-right-alt"></i></button>

                {{--<!-- Social Login -->--}}
                {{--<div class="social-login-separator"><span>or</span></div>--}}
                {{--<div class="social-login-buttons">--}}
                {{--<button class="facebook-login ripple-effect"><i class="icon-brand-facebook-f"></i> Log In via Facebook</button>--}}
                {{--<button class="google-login ripple-effect"><i class="icon-brand-google-plus-g"></i> Log In via Google+</button>--}}
                {{--</div>--}}

            </div>
        </div>
    </div>
    @include('includes.footer')

</div>
@include('includes.body-scripts')

</body>
</html>