@extends('layouts.admin_dashboard_layout)
@section('content')
	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Blocked Admins</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Admin</a></li>
						<li>Blocked Admins</li>
					</ul>
				</nav>
			</div>

			@include('admin.admin-boxes.admin-search-admins')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-business-center"></i> 1 Admin</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								<li>
									<!-- foodpole-xitem Listing -->
									<div class="foodpole-xitem-listing">

										<!-- foodpole-xitem Listing Details -->
										<div class="foodpole-xitem-listing-details">

											<!-- Logo -->
											<div class="foodpole-xitem-listing-foodpole-source-logo thumb">
												<img src="images/user-avatar-placeholder.png" alt="">
											</div>

											<!-- Details -->
											<div class="foodpole-xitem-listing-description">
												<h3 class="foodpole-xitem-listing-title">Naimat Shabbir <span class="dashboard-status-button red">Blocked</span></h3>

												<!-- foodpole-xitem Listing Footer -->
												<div class="foodpole-xitem-listing-footer">
													<ul>
														<li><i class="icon-material-outline-highlight-off"></i> 5 Blocks</li>
														<li><i class="icon-material-outline-highlight-off"></i> 10 Suspensions</li>
														<li><i class="icon-material-outline-highlight-off"></i> 3 Removals</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<!-- Buttons -->
									<div class="buttons-to-right">
										<a href="#" class="button red ripple-effect">Unblock</a>												
										<a href="#" class="button red ripple-effect">View</a>									
										<a href="#" class="button red ripple-effect">Edit</a>									
										<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
									</div>
								</li>								
							</ul>
						</div>
					</div>
				</div>

			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->
@endsection