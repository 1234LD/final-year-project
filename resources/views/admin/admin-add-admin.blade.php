@extends('layouts.admin_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Add Admin</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Admin</a></li>
						<li>Add Admin</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
            <form method="post" id="login-form"  action="{{URL::to('/admin/createadmin')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
			<div class="row">

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-account-circle"></i> Admin Intro</h3>
						</div>

						<div class="content with-padding padding-bottom-0">

							<div class="row">

								<div class="col-auto avatar-cont">
									<div class="avatar-wrapper" data-tippy-placement="bottom" title="Change Avatar">
										<img class="profile-pic" src="images/user-avatar-placeholder.png" alt="" />


										<div class="upload-button"></div>
										<input class="file-upload" type="file" name="image_name" accept="image/*"/>
									</div>
								</div>

								<div class="col">
									<div class="row">

										<div class="col-xl-6">



											<div class="submit-field">
												<h5>Full Name</h5>
												<input type="text" name="fullname" class="with-border" placeholder="Jurry Abbas">
											</div>

										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Username</h5>
												<input type="text" name="username" class="with-border"  placeholder="jurryabbas">
											</div>
										</div>											
										
										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Email</h5>
												<input type="email"  name="email" class="with-border" placeholder="jurryabbas@foodpole.com">
											</div>
										</div>										

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Phone</h5>
												<input type="number" name="phone" class="with-border" placeholder="03335583655">
											</div>
										</div>

									</div>
								</div>
							</div>

						</div>
					</div>
				</div>


				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div id="test1" class="dashboard-box">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-lock"></i> Password & Security</h3>
						</div>

						<div class="content with-padding">
							<div class="row">
								{{--<div class="col-xl-4">--}}
									{{--<div class="submit-field">--}}
										{{--<h5>Current Password</h5>--}}
										{{--<input  name="password" type="password" class="with-border">--}}
									{{--</div>--}}
								{{--</div>--}}

								<div class="col-xl-4">
									<div class="submit-field">
										<h5>New Password</h5>
										<input name="password" type="password"   class="with-border"  value="">
									</div>
								</div>

								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Repeat New Password</h5>
										<input type="password" class="with-border">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Button -->
				<div class="col-xl-12">
					<button class="button ripple-effect big margin-top-30" id="login-form" type="submit">Create Admin</button>
				</div>



			</div>
            </form>
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->
@endsection