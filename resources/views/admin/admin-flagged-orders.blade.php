@extends('layouts.admin_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Flagged Orders</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Orders</a></li>
						<li>Flagged Orders</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">

				@include('admin.admin-boxes.admin-search-orders')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">


						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-feather-shopping-bag"></i> {{ count($orders->data) }} Order</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								@foreach($orders->data as $order_item)
									<li>
										<!-- Overview -->
										<div class="foodpole-user-overview manage-candidates">
											<div class="foodpole-user-overview-inner">

												<!-- Avatar -->
												<div class="foodpole-user-avatar">
													<a href="#"><img src="{{URL::asset('images/food-placeholder.jpg') }}" alt=""></a>
												</div>

												<!-- Name -->
												<div class="foodpole-user-name">
													<h4><a href="#"><strong>Order#</strong> {{ $order_item->id }} | <strong>Chef:</strong> {{$order_item->chef->user->full_name}} | <strong>Customer:</strong> {{$order_item->customer->user->full_name}}</a>

														@if($order_item->flag == true)
															<span class="dashboard-status-button red">Flagged</span>
														@endif</h4>

													<!-- Details -->
													<span class="foodpole-user-detail-item"><strong>Subtotal:</strong> Rs. {{ $order_item->sub_total }}</span>
													{{--<span class="foodpole-user-detail-item"><strong>GST (17%):</strong> Rs. 210</span>--}}
													<span class="foodpole-user-detail-item"><strong>Delivery:</strong> Rs. {{ $order_item->delivery_fare }}</span>
													<span class="foodpole-user-detail-item"><strong>Grand Total:</strong> Rs. {{ $order_item->grand_total }}</span>

													<!-- Details -->
													<span class="foodpole-user-detail-item o-time-stamp">{{ \Carbon\Carbon::parse($order_item->created_at->date)->toDayDateTimeString() }}</span>

													<!-- fpOffer Details -->
													<ul class="dashboard-task-info fpOffer-info flagged-bg">
														@foreach($order_item->order_line_items->data as $item)
															<li><strong>{{ $item->menu_item->item_name }}</strong><span>{{ $item->item_option }} - x{{ $item->quantity }} - Rs.{{ $item->sub_total }}</span></li>
														@endforeach
													</ul>

													<!-- Rating -->
													<div class="foodpole-user-rating margin-top-25">
														<h4>Review</h4>
														<div class="star-rating margin-top-10" data-rating="0.0"></div>
														<p>Worst food ever. No one should buy anything from this scammer.</p>
													</div>

													<div class="foodpole-user-rating margin-top-30">
														<h4>Your Comment</h4>
														<p>Sorry for inconvinience. We will imrpove our quality.</p>
													</div>
												</div>
											</div>
										</div>
									</li>
								@endforeach
								{{--<li>--}}
									{{--<!-- Overview -->--}}
									{{--<div class="foodpole-user-overview manage-candidates">--}}
										{{--<div class="foodpole-user-overview-inner">--}}

											{{--<!-- Avatar -->--}}
											{{--<div class="foodpole-user-avatar">--}}
												{{--<a href="#"><img src="images/food-placeholder.jpg" alt=""></a>--}}
											{{--</div>--}}

											{{--<!-- Name -->--}}
											{{--<div class="foodpole-user-name">--}}
												{{--<h4><a href="#"><strong>Order#</strong> 15094 | <strong>Chef:</strong> Jurry Abbas | <strong>Customer:</strong> Naimat Shabbir</a> <span class="dashboard-status-button red">Flagged</span></h4>--}}

												{{--<!-- Details -->--}}
												{{--<span class="foodpole-user-detail-item"><strong>Subtotal:</strong> Rs. 1200</span>--}}
												{{--<span class="foodpole-user-detail-item"><strong>GST (17%):</strong> Rs. 210</span>--}}
												{{--<span class="foodpole-user-detail-item"><strong>Delivery:</strong> Rs. 25</span>--}}
												{{--<span class="foodpole-user-detail-item"><strong>Grand Total:</strong> Rs. 1435</span>--}}
												{{----}}
												{{--<!-- Details -->--}}
												{{--<span class="foodpole-user-detail-item o-time-stamp">02:35 PM | 25th September, 2018</span>--}}
												{{----}}
												{{----}}
												{{--<!-- fpOffer Details -->--}}
												{{--<ul class="dashboard-task-info fpOffer-info flagged-bg">--}}
													{{--<li><strong>Mughlai Handi</strong><span>Single - x2 - Rs.300</span><div class="star-rating margin-top-5 or-item-rating" data-rating="0.0"></div></li>--}}
													{{--<li><strong>Keema Naan</strong><span>Single - x2 - Rs.300</span><div class="star-rating margin-top-5 or-item-rating" data-rating="0.0"></div></li>--}}
													{{--<li><strong>Been Shahlik</strong><span>Single - x2 - Rs.600</span><div class="star-rating margin-top-5 or-item-rating" data-rating="0.0"></div></li>													--}}
												{{--</ul>												--}}

												{{--<!-- Rating -->--}}
												{{--<div class="foodpole-user-rating margin-top-25">--}}
												    {{--<h4>Customer Review</h4>--}}
													{{--<div class="star-rating margin-top-10" data-rating="0.0"></div>--}}
													{{--<p>Worst food ever. No one should buy anything from this scammer.</p>--}}
												{{--</div>	--}}
												{{----}}
										        {{--<div class="foodpole-user-rating margin-top-30">--}}
													{{--<h4>Chef Comment</h4>--}}
													{{--<p>Sorry for inconvinience. We will imrpove our quality.</p>--}}
												{{--</div>	--}}
												{{----}}
												{{--<!-- Buttons -->--}}
												{{--<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">--}}
													{{--<a href="#small-dialog-1" class="popup-with-zoom-anim button dark ripple-effect order-blue"><i class="icon-material-outline-thumb-up"></i> Leave a Comment</a>														--}}
												{{--</div>												--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
								{{--</li>								--}}
							</ul>
						</div>
					</div>
				</div>

			</div>
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->
	@include('admin.admin-boxes.admin-response')


	@endsection