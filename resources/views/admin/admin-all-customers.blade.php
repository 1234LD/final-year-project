@extends('layouts.admin_dashboard_layout')
@section('content')
	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>All Customers</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{URL::to('/')}}">Home</a></li>
						<li><a href="#">Customers</a></li>
						<li>All Customers</li>
					</ul>
				</nav>
			</div>

			@include('admin.admin-boxes.admin-search-customer')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-business-center"></i>{{count($allcustomer)}} Customers</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								@foreach($allcustomer as $customer )
								<li>
									<!-- foodpole-xitem Listing -->
									<div class="foodpole-xitem-listing">

										<!-- foodpole-xitem Listing Details -->
										<div class="foodpole-xitem-listing-details">

											<!-- Logo -->

											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="{{URL::asset('images/user-avatar-placeholder.png')}}" alt="">--}}

											<div class="foodpole-xitem-listing-foodpole-source-logo">
												@if($customer->user->image == null)
													<img src= "{{URL::asset('images/user-avatar-placeholder.png')}}"alt="">
												@else
													<img src="{{ URL::asset('chefimages/'.$customer->user->image )}}" alt="">
												@endif
												{{--<img src="{{URL::asset('images/user-avatar-placeholder.png')}}" alt="">--}}
											</div>

											<!-- Details -->
											<div class="foodpole-xitem-listing-description">
												<h3 class="foodpole-xitem-listing-title">{{$customer->user->full_name}}
												@if($customer->user->account_status == false)
													<span class="dashboard-status-button red">Blocked</span>
												</h3>
											@endif
												<!-- foodpole-xitem Listing Footer -->
												<div class="foodpole-xitem-listing-footer">
													<ul>
														<li><i class="icon-material-outline-location-on"></i>{{$customer->online_status}}</li>
														<li><i class="icon-feather-shopping-bag"></i>{{$customer->user->email}}</li>
														<li><i class="icon-material-outline-rate-review"></i> {{$customer->id}} id</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<!-- Buttons -->
									<div class="buttons-to-right">
										@if($customer->user->account_status == true)
											<a href="{{ URL::to('admin/block/'.$customer->user->id)}}" class="button red ripple-effect">Block</a>
										@else
											<a href="{{ URL::to('admin/unblock/'.$customer->user->id)}}" class="button red ripple-effect">Unblock</a>
										@endif
										{{--<a href="#" class="button red ripple-effect">Block</a>												--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										<a href="{{ URL::to('/admin/admin-profile/'.$customer->user->id)}}" class="button red ripple-effect">Edit</a>
										<a href="{{ URL::to('admin/delete/'.$customer->user->id)}}" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
									</div>
								</li>
								@endforeach
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/user-avatar-placeholder.png" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Naimat Shabbir <span class="dashboard-status-button red">Blocked</span></h3>--}}

												{{--<!-- foodpole-xitem Listing Footer -->--}}
												{{--<div class="foodpole-xitem-listing-footer">--}}
													{{--<ul>--}}
														{{--<li><i class="icon-material-outline-location-on"></i> Islamabad</li>--}}
														{{--<li><i class="icon-feather-shopping-bag"></i> 5 Orders</li>--}}
														{{--<li><i class="icon-material-outline-rate-review"></i> 2 reviews</li>--}}
													{{--</ul>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">--}}
										{{--<a href="#" class="button red ripple-effect">Unblock</a>												--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/user-avatar-placeholder.png" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Wajid Raza</h3>--}}

												{{--<!-- foodpole-xitem Listing Footer -->--}}
												{{--<div class="foodpole-xitem-listing-footer">--}}
													{{--<ul>--}}
														{{--<li><i class="icon-material-outline-location-on"></i> Islamabad</li>--}}
														{{--<li><i class="icon-feather-shopping-bag"></i> 5 Orders</li>--}}
														{{--<li><i class="icon-material-outline-rate-review"></i> 2 reviews</li>--}}
													{{--</ul>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">--}}
										{{--<a href="#" class="button red ripple-effect">Block</a>												--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>														--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/user-avatar-placeholder.png" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Wasim Akram</h3>--}}

												{{--<!-- foodpole-xitem Listing Footer -->--}}
												{{--<div class="foodpole-xitem-listing-footer">--}}
													{{--<ul>--}}
														{{--<li><i class="icon-material-outline-location-on"></i> Islamabad</li>--}}
														{{--<li><i class="icon-feather-shopping-bag"></i> 5 Orders</li>--}}
														{{--<li><i class="icon-material-outline-rate-review"></i> 2 reviews</li>--}}
													{{--</ul>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">--}}
										{{--<a href="#" class="button red ripple-effect">Block</a>												--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>	--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/user-avatar-placeholder.png" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Javed Miandad</h3>--}}

												{{--<!-- foodpole-xitem Listing Footer -->--}}
												{{--<div class="foodpole-xitem-listing-footer">--}}
													{{--<ul>--}}
														{{--<li><i class="icon-material-outline-location-on"></i> Islamabad</li>--}}
														{{--<li><i class="icon-feather-shopping-bag"></i> 5 Orders</li>--}}
														{{--<li><i class="icon-material-outline-rate-review"></i> 2 reviews</li>--}}
													{{--</ul>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">--}}
										{{--<a href="#" class="button red ripple-effect">Block</a>												--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>									--}}
							</ul>
						</div>
					</div>
				</div>

			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->
@endsection