@extends('layouts.admin_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>All Regular Meals</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Meals</a></li>
						<li>All Regular Meals</li>
					</ul>
				</nav>
			</div>

			@include('admin.admin-boxes.admin-search-meals')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-business-center"></i>
                            <?php  $count = 0;
                            foreach ($meals->data as $index){
                                if($index->menu->category_name != config('constants.DEFAULT_QUICK-MEAL_CATEGORY')){ $count++;}
                            }?>{{ $count }} Meals</h3>

						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								{{--@foreach($meals->data as $meal_item)--}}
									{{--@if($meal_item->menu->category_name != config('constants.DEFAULT_QUICK-MEAL_CATEGORY'))--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--@if($meal_item->item_image ==null)--}}
													{{--<img src="{{ URL::asset('images/food-placeholder.jpg') }}" alt="">--}}
												{{--@else--}}
													{{--<img src="{{ URL::asset('item_images/'.$meal_item->item_image->item_image) }}" alt="">--}}
												{{--@endif--}}


											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title"{{ $meal_item->item_name }} <div class="verified-badge rec-meal rec-inner" title="FoodPole Recommended" data-tippy-placement="right"></div></h3>--}}

												{{--<!-- foodpole-xitem Listing Footer -->--}}
												{{--<div class="foodpole-xitem-listing-footer">--}}
													{{--<ul>--}}
														{{--<li><i class="icon-material-outline-school"></i> {{ $meal_item->menu->category_name }}</li>--}}
														{{--<li><i class="icon-feather-align-justify"></i> {{ count($meal_item->item_option) }} Options</li>--}}
														{{--<li><i class="icon-material-outline-star-border"></i>{{ $meal_item->rating }}</li>--}}
														{{--<li><i class="icon-material-outline-access-time"></i> {{ $meal_item->item_time }} min</li>--}}
														{{--<li><i class="icon-feather-image"></i> 6 Photos</li>--}}
													{{--</ul>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">--}}
										{{--<a href="#" class="button red ripple-effect">Remove Badge</a>										--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>--}}
									{{--@endif--}}
								{{--@endforeach--}}
								@foreach($meals->data as $meal_item)
									@if($meal_item->menu->category_name != config('constants.DEFAULT_QUICK-MEAL_CATEGORY'))
								<li>
									<!-- foodpole-xitem Listing -->
									<div class="foodpole-xitem-listing">

										<!-- foodpole-xitem Listing Details -->
										<div class="foodpole-xitem-listing-details">

											<!-- Logo -->
											<div class="foodpole-xitem-listing-foodpole-source-logo thumb">
												@if($meal_item->item_image ==null)
													<img src="{{ URL::asset('images/food-placeholder.jpg') }}" alt="">
												@else
													<img src="{{ URL::asset('item_images/'.$meal_item->item_image->item_image[0]) }}" alt="">
												@endif
											</div>

											<!-- Details -->
											<div class="foodpole-xitem-listing-description">
												<h3 class="foodpole-xitem-listing-title">{{ $meal_item->item_name }}
													@if($meal_item->verified)
														<div class="verified-badge rec-meal rec-inner" title="FoodPole Recommended" data-tippy-placement="right"></div>
													@endif</h3>

												<!-- foodpole-xitem Listing Footer -->
												<div class="foodpole-xitem-listing-footer">
													<ul>
														<li><i class="icon-feather-image"></i> {{ $meal_item->menu->category_name }}</li>
														<li><i class="icon-feather-align-justify"></i> {{ count($meal_item->item_option) }} Options</li>
														@if($meal_item->item_recipe != null)
															<li><i class="icon-feather-list"></i> {{ count($meal_item->item_recipe->ingredient_name) }} Ingredients</li>
														@endif
														<li><i class="icon-material-outline-star-border"></i> {{ $meal_item->rating }}</li>
														<li><i class="icon-material-outline-access-time"></i> {{ $meal_item->item_time }} min</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<!-- Buttons -->
									<div class="buttons-to-right">
                                        @if($meal_item->verified == false)

										<a href="{{ URL::to('admin/verify-meal/'.$meal_item->id)}}" class="button red ripple-effect">Recommend</a>
                                        @else
                                            <a href="{{ URL::to('admin/verify-meal/'.$meal_item->id)}}" class="button red ripple-effect">Remove Bagde</a>
                                            @endif
										<a href="{{ URL::to('meal/'.$meal_item->chef->user->username.'/'.$meal_item->id.'/details') }}" class="button red ripple-effect">View</a>
										<a href="{{ URL::to('edit_meal/chef/'.$meal_item->chef->user->username.'/meal/'.$meal_item->id) }}" class="button red ripple-effect">Edit</a>
										<a href="{{ URL::to('admin/delete-meal/'.$meal_item->id)}}" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
									</div>
								</li>
									@endif
									@endforeach
							</ul>
						</div>
					</div>
				</div>

			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->
@endsection