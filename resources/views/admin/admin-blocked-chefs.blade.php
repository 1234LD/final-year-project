@extends('layouts.admin_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Blocked Chefs</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{URL::to('/')}}">Home</a></li>
						<li><a href="#">Chefs</a></li>
						<li>Blocked Chefs</li>
					</ul>
				</nav>
			</div>

			@include('admin.admin-boxes.admin-search-chefs')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-business-center"></i>Number of Blocked Chef {{count($blockchef->data)}}</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								@foreach($blockchef->data as $chef)
								<li>
									<!-- foodpole-xitem Listing -->
									<div class="foodpole-xitem-listing">

										<!-- foodpole-xitem Listing Details -->
										<div class="foodpole-xitem-listing-details">

											<!-- Logo -->

											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="{{URL::asset('images/user-avatar-placeholder.png')}}" alt="">--}}

											<div class="foodpole-xitem-listing-foodpole-source-logo">
												{{--<img src="{{URL::asset('images/user-avatar-placeholder.png')}}" alt="">--}}
												@if($chef->chef->picture == null)
													<img src= "{{URL::asset('images/user-avatar-placeholder.png')}}"alt="">
												@else
													<img src="{{ URL::asset('chefimages/'.$chef->chef->picture )}}" alt="">
												@endif

											</div>

											<!-- Details -->
											<div class="foodpole-xitem-listing-description">
												<h3 class="foodpole-xitem-listing-title">{{$chef->full_name}}
													@if($chef->chef->user->account_status == false)
														<span class="dashboard-status-button red">Blocked</span>
														<div class="verified-badge" title="Verified Chef" data-tippy-placement="right"></div>
													@elseif($chef->chef->verify == true)
														<div class="verified-badge" title="Verified Chef" data-tippy-placement="right"></div>
													@endif
												</h3>


												<!-- foodpole-xitem Listing Footer -->
												<div class="foodpole-xitem-listing-footer">
													<ul>
														<li><i class="icon-material-outline-location-on"></i>{{$chef->chef->city}}</li>
														<li><i class="icon-feather-align-justify"></i>{{$chef->chef->delivery_type}}</li>
														<li><i class="icon-material-outline-star-border"></i>{{$chef->chef->cnic}}</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<!-- Buttons -->

									<div class="buttons-to-right">

											<a href="{{ URL::to('admin/unblock/'.$chef->chef->user->id)}}" class="button red ripple-effect">Unblock</a>

										@if($chef->chef->verify == true)
											<a href="{{ URL::to('admin/verify-unverify/'.$chef->chef->id)}}" class="button red ripple-effect">Remove Badge</a>
										@else
											<a href="{{ URL::to('admin/verify-unverify/'.$chef->chef->id)}}" class="button red ripple-effect">Verify</a>
										@endif
										<a href=" {{ URL::to('chef/'.$chef->chef->user->id.'/'.$chef->chef->user->username.'/chef_page')}}" class="button red ripple-effect">View</a>
										<a href="{{ URL::asset('/chef/user-profile/'.$chef->chef->user->id)}}" class="button red ripple-effect">Edit</a>
										<a href="{{ URL::to('admin/delete/'.$chef->chef->user->id)}}" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
									</div>
										{{--<a href="#" class="button red ripple-effect">Unblock</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Verified</a>										--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}

								</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>

			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->
@endsection