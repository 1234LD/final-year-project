@extends('layouts.admin_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Flagged Reviews</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Reviews</a></li>
						<li>Flagged Reviews</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">

				{{--@include('admin.admin-boxes.admin-search-reviews')--}}

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">


						<!-- Headline -->

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-feather-shopping-bag"></i>
                                <?php $count = 0;
                                foreach ($reviews->data as $review_item)
                                {if($review_item->order != null)
                                    $count++;
                                }echo $count.' Reviews';
                                ?></h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								@foreach($reviews->data as $review_item)
									@if($review_item->order != null)
										<li>
											<!-- Overview -->
											<div class="foodpole-user-overview manage-candidates">
												<div class="foodpole-user-overview-inner">

													<!-- Avatar -->
													<div class="foodpole-user-avatar">
														<a href="#"><img src="{{ URL::asset('images/food-placeholder.jpg') }}" alt=""></a>
													</div>

													<!-- Name -->
													<div class="foodpole-user-name">
														<h4><a href="#">Order# {{ $review_item->orders_id }}</a> <span class="dashboard-status-button red">Flagged</span>
															<span class="dashboard-status-button red">by {{ $review_item->order->customer->user->full_name }}</span></h4>
														<!-- Details -->
														<span class="foodpole-user-detail-item o-time-stamp">{{ \Carbon\Carbon::parse($review_item->created_at->date)->toDayDateTimeString() }}</span>

                                                    <?php $avg = 0; $sum = 0; $count = 0;?>
													<!-- fpOffer Details -->
														<ul class="dashboard-task-info fpOffer-info flagged-bg">
															@foreach($review_item->order->order_line_items->data as $item)
																<li><strong>{{ $item->menu_item->item_name }}</strong><div class="star-rating margin-top-5 or-item-rating" data-rating="0.0"></div></li>
															@endforeach
														</ul>

														<!-- Rating -->
														<div class="foodpole-user-rating margin-top-25">
															<h4>Review</h4>
															<div class="star-rating margin-top-10" data-rating="0.0"></div>
															<p> {{ $review_item->feedback }} </p>
														</div>

														{{--<div class="foodpole-user-rating margin-top-30">--}}
															{{--<h4>Your Comment</h4>--}}
															{{--<p>Sorry for inconvinience. We will imrpove our quality.</p>--}}
														{{--</div>--}}
													</div>
												</div>
											</div>
										</li>
									@endif
								@endforeach
						{{--<div class="headline">--}}
							{{--<h3><i class="icon-feather-shopping-bag"></i> 1 Review</h3>--}}
						{{--</div>--}}

						{{--<div class="content">--}}
							{{--<ul class="dashboard-box-list">						--}}
								{{--<li>--}}
									{{--<!-- Overview -->--}}
									{{--<div class="foodpole-user-overview manage-candidates">--}}
										{{--<div class="foodpole-user-overview-inner">--}}

											{{--<!-- Avatar -->--}}
											{{--<div class="foodpole-user-avatar">--}}
												{{--<a href="#"><img src="images/food-placeholder.jpg" alt=""></a>--}}
											{{--</div>--}}

											{{--<!-- Name -->--}}
											{{--<div class="foodpole-user-name">--}}
												{{--<h4><a href="#"><strong>Order#</strong> 15094 | <strong>Chef:</strong> Jurry Abbas | <strong>Customer:</strong> Naimat Shabbir</a> <span class="dashboard-status-button red">Flagged</span></h4>--}}
												{{----}}
												{{--<!-- Details -->--}}
												{{--<span class="foodpole-user-detail-item o-time-stamp">02:35 PM | 25th September, 2018</span>--}}
												{{----}}
												{{--<!-- fpOffer Details -->--}}
												{{--<ul class="dashboard-task-info fpOffer-info flagged-bg">--}}
													{{--<li><strong>Mughlai Handi</strong><div class="star-rating margin-top-5 or-item-rating" data-rating="0.0"></div></li>--}}
													{{--<li><strong>Keema Naan</strong><div class="star-rating margin-top-5 or-item-rating" data-rating="0.0"></div></li>--}}
													{{--<li><strong>Been Shahlik</strong><div class="star-rating margin-top-5 or-item-rating" data-rating="0.0"></div></li>													--}}
												{{--</ul>												--}}

												{{--<!-- Rating -->--}}
												{{--<div class="foodpole-user-rating margin-top-25">--}}
												    {{--<h4>Customer Review</h4>--}}
													{{--<div class="star-rating margin-top-10" data-rating="0.0"></div>--}}
													{{--<p>Worst food ever. No one should buy anything from this scammer.</p>--}}
												{{--</div>	--}}
												{{----}}
										        {{--<div class="foodpole-user-rating margin-top-30">--}}
													{{--<h4>Chef Comment</h4>--}}
													{{--<p>Sorry for inconvinience. We will imrpove our quality.</p>--}}
												{{--</div>	--}}
												{{----}}
												{{--<!-- Buttons -->--}}
												{{--<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">--}}
													{{--<a href="#small-dialog-1" class="popup-with-zoom-anim button dark ripple-effect order-blue"><i class="icon-material-outline-thumb-up"></i> Leave a Comment</a>														--}}
												{{--</div>												--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
								{{--</li>								--}}
							</ul>
						</div>
					</div>
				</div>

			</div>
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>

			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->

	@include('admin.admin-boxes.admin-response')


	@endsection