<!-- Dashboard Box -->
<div class="col-xl-12">
	<div id="test1" class="dashboard-box margin-top-0">

		<!-- Headline -->
		<div class="headline">
			<h3><i class="icon-material-outline-search"></i> Search Chef</h3>
		</div>

		<div class="content with-padding">
			<div class="row">
				<div class="col-xl-12">
					<div class="submit-field search-dashboard">
						<div class="input-with-icon">
							<input type="text" placeholder="Enter Username">
							<i class="icon-material-outline-search"></i>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>
</div>