	<!-- Dashboard Sidebar
	================================================== -->
	<div class="dashboard-sidebar">
		<div class="dashboard-sidebar-inner" data-simplebar>
			<div class="dashboard-nav-container">

				<!-- Responsive Navigation Trigger -->
				<a href="#" class="dashboard-responsive-nav-trigger">
					<span class="hamburger hamburger--collapse" >
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</span>
					<span class="trigger-title">Dashboard Navigation</span>
				</a>
				
				<!-- Navigation -->
				<div class="dashboard-nav">
					<div class="dashboard-nav-inner">

						<ul data-submenu-title="Start">
							<li><a href="{{ URL::asset('/admin/dashboard') }}"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>
						</ul>

						<ul data-submenu-title="Organize and Manage">
							<li><a href="#"><i class="icon-feather-shopping-bag"></i> Orders</a>
								<ul>
									<li><a href="{{ URL::asset('/admin/all_orders') }}">All</a></li>
									<li><a href="{{URL::asset('/admin/pending_orders')}}"> Pending response</a></li>
									<li><a href="{{URL::asset('/admin/ready_orders')}}">Ready</a></li>
									<li><a href="{{URL::asset('/admin/processing')}}">Processing</a></li>
									<li><a href="{{URL::asset('/admin/deliver')}}">Delivered</a></li>
									<li><a href="{{URL::asset('/admin/cancelled')}}">Cancelled</a></li>
									<li><a href="{{URL::asset('/admin/Declined')}}">Declined</a></li>
									{{--<li><a href="{{URL::asset('/admin/Paid')}}">Paid</a></li>--}}
									{{--<li><a href="{{URL::asset('/admin/unPaid')}}">Unpaid</a></li>--}}

									<li><a href="{{URL::asset('/admin/flagged-order')}}">Flagged</a></li>
								</ul>
							</li>
							<li><a href="#"><i class="icon-material-outline-rate-review"></i> Reviews</a>
								<ul>
									<li><a href="{{URL::asset('/admin/all_review')}}">All</a></li>
									<li><a href="{{URL::asset('/admin/flagged')}}">Flagged</a></li>
									<li><a href="{{URL::asset('/admin/non_flagged')}}">Non-flagged</a></li>
								</ul>	
							</li>	
							<li><a href="#"><i class="icon-material-outline-restaurant"></i> Meals</a>
								<ul>
									<li><a href="{{ URL::asset('/admin/all_regular_meals')}}">All Regular Meals</a></li>
									<li><a href="{{ URL::asset('/admin/all_quick_meals')}}">All Quick Meals</a></li>
									{{--<li><a href="#">Add Regular Meal</a></li>--}}
									{{--<li><a href="#">Add Quick Meal</a></li>--}}
									<li><a href="{{ URL::asset('/admin/all_expired_meals')}}">Expired Quick Meals</a></li>
									<li><a href="{{ URL::asset('/admin/all_free_meals')}}">Free Meals</a></li>
								</ul>	
							</li>	
							<li><a href="#"><i class="icon-material-outline-reorder"></i> Categories</a>
								<ul>
									<li><a href="{{URL::asset('/admin/all_categories')}}">All</a></li>
									<li><a href="{{ URL::asset('/admin/add_categories')}}">Add New</a></li>
								</ul>	
							</li>

							<li><a href="#"><i class="icon-line-awesome-graduation-cap"></i> Chefs</a>
								<ul>
									<li><a href="{{ URL::asset('/admin/all_chefs')}}">All</a></li>
									<li><a href="{{ URL::asset('/admin/verified_chef')}}">Verified Chefs</a></li>
									<li><a href="{{ URL::asset('/admin/blocked_chef')}}">Blocked Chefs</a></li>
									{{--<li><a href="#">Suspended Chefs</a></li>--}}
									<li><a href="{{ URL::asset('/admin/online_chef')}}">Online Chefs</a></li>

								</ul>	
							</li>
							<li><a href="#"><i class="icon-material-outline-group"></i> Customers</a>
								<ul>
									<li><a href="{{ URL::asset('/admin/all_customers')}}">All</a></li>
									<li><a href="{{URL::asset('/admin/all_blocked_customers')}}">Blocked</a></li>
								</ul>	
							</li>
							@if(\Illuminate\Support\Facades\Session::get('user_type') == config('constants.DEFAULT_SUPER-ADMIN_TYPE'))
							<li><a href="#"><i class="icon-material-outline-business-center"></i> Admins</a>
								<ul>
									<li><a href="{{ URL::asset('/admin/all_admins') }}">All</a></li>
									<li><a href="{{ URL::asset('/admin/add_admin') }}">Add</a></li>
									{{--<li><a href="#">Blocked</a></li>--}}
								</ul>	
							</li>
								@endif
						</ul>

						<ul data-submenu-title="Account">
							<li class="active"><a href="{{ URL::asset('/admin/admin-profile/'.Session::get('user_id'))}}"><i class="icon-material-outline-settings"></i> Settings</a></li>
							<li><a href="{{ URL::asset('/admin/logged-out')}}"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
						</ul>
						
					</div>
				</div>
				<!-- Navigation / End -->

			</div>
		</div>
	</div>
	<!-- Dashboard Sidebar / End -->