<!-- Dashboard Box -->
<div class="col-xl-12">
	<div id="test1" class="dashboard-box margin-top-0">

		<!-- Headline -->
		<div class="headline">
			<h3><i class="icon-material-outline-search"></i> Search Order</h3>
		</div>

		<div class="content with-padding">
			<div class="row">
				<div class="col-xl-4">
					<div class="submit-field search-dashboard">
						<div class="input-with-icon">
							<input type="number"  placeholder="By Order #">
							<i class="icon-material-outline-search"></i>
						</div>
					</div>
				</div>
				<div class="col-xl-4">
					<div class="submit-field search-dashboard">
						<div class="input-with-icon">
							<input type="text" placeholder="By Chef username">
							<i class="icon-material-outline-search"></i>
						</div>
					</div>
				</div>
				<div class="col-xl-4">
					<div class="submit-field search-dashboard">
						<div class="input-with-icon">
							<input type="text" placeholder="By Customer username">
							<i class="icon-material-outline-search"></i>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>