@extends('layouts.admin_dashboard_layout)
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Suspended Chefs</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Chefs</a></li>
						<li>Suspended Chefs</li>
					</ul>
				</nav>
			</div>

			@include('admin.admin-boxes.admin-search-chefs')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-business-center"></i> 1 Chef</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								<li>
									<!-- foodpole-xitem Listing -->
									<div class="foodpole-xitem-listing">

										<!-- foodpole-xitem Listing Details -->
										<div class="foodpole-xitem-listing-details">

											<!-- Logo -->
											<div class="foodpole-xitem-listing-foodpole-source-logo thumb">
												<img src="images/user-avatar-placeholder.png" alt="">
											</div>

											<!-- Details -->
											<div class="foodpole-xitem-listing-description">
												<h3 class="foodpole-xitem-listing-title">Wajid Raza <span class="dashboard-status-button yellow">Suspended</span></h3>

												<!-- foodpole-xitem Listing Footer -->
												<div class="foodpole-xitem-listing-footer">
													<ul>
														<li><i class="icon-material-outline-location-on"></i> Islamabad</li>
														<li><i class="icon-feather-align-justify"></i> 5 Meals</li>
														<li><i class="icon-material-outline-star-border"></i> 4.5</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<!-- Buttons -->
									<div class="buttons-to-right">
										<a href="#" class="button red ripple-effect">Unsuspend</a>
										<a href="#" class="button red ripple-effect">Block</a>									
										<a href="#" class="button red ripple-effect">Verified</a>										
										<a href="#" class="button red ripple-effect">View</a>									
										<a href="#" class="button red ripple-effect">Edit</a>									
										<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
									</div>
								</li>																							
							</ul>
						</div>
					</div>
				</div>

			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->
@endsection