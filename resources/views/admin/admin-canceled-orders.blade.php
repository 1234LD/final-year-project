@extends('layouts.admin_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Canceled Orders</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Orders</a></li>
						<li>Canceled Orders</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">

				@include('admin.admin-boxes.admin-search-orders')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">


						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-feather-shopping-bag"></i> {{ count($orders->data) }} Order</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								@foreach($orders->data as $order_item)
								<li>
									<!-- Overview -->
									<div class="foodpole-user-overview manage-candidates">
										<div class="foodpole-user-overview-inner">

											<!-- Avatar -->
											<div class="foodpole-user-avatar">
												<a href="#"><img src="{{URL::asset('images/food-placeholder.jpg') }}" alt=""></a>
											</div>

											<!-- Name -->
											<div class="foodpole-user-name">
												<h4><a href="#"><strong>Order#</strong> {{ $order_item->id }} | <strong>Chef:</strong> {{$order_item->chef->user->full_name}} | <strong>Customer:</strong> {{$order_item->customer->user->full_name}}</a>
													<span class="dashboard-status-button green">{{ $order_item->order_status }}</span></h4>

												<!-- Details -->
												<span class="foodpole-user-detail-item"><strong>Subtotal:</strong> Rs. {{ $order_item->sub_total }}</span>
												{{--<span class="foodpole-user-detail-item"><strong>GST (17%):</strong> Rs. 210</span>--}}
												<span class="foodpole-user-detail-item"><strong>Delivery:</strong> Rs. {{ $order_item->delivery_fare }}</span>
												<span class="foodpole-user-detail-item"><strong>Grand Total:</strong> Rs. {{ $order_item->grand_total }}</span>

												<!-- Details -->
												<span class="foodpole-user-detail-item o-time-stamp">{{ \Carbon\Carbon::parse($order_item->created_at->date)->toDayDateTimeString() }}</span>

												<!-- fpOffer Details -->
												<ul class="dashboard-task-info fpOffer-info">
													@foreach($order_item->order_line_items->data as $item)
														<li><strong>{{ $item->menu_item->item_name }}</strong><span>{{ $item->item_option }} - x{{ $item->quantity }} - Rs.{{ $item->sub_total }}</span></li>
													@endforeach
												</ul>

													@if($order_item->order_status == config('constants.DEFAULT_ORDER_CANCELLED'))
													<div class="freelancer-rating margin-top-25">
														<h4>Customer Reason</h4>
														<p>{{$order_item->reason->reason}}</p>
													</div>
													@elseif($order_item->reason->order_status == "flagged")
														<div class="freelancer-rating margin-top-25">
															<h4>Your Reason</h4>
															<p>{{$order_item->reason->reason}}</p>
														</div>
												@endif

											</div>
										</div>
									</div>
								</li>
								@endforeach
									<!-- Overview -->
									{{--<div class="foodpole-user-overview manage-candidates">--}}
										{{--<div class="foodpole-user-overview-inner">--}}

											{{--<!-- Avatar -->--}}
											{{--<div class="foodpole-user-avatar">--}}
												{{--<a href="#"><img src="images/food-placeholder.jpg" alt=""></a>--}}
											{{--</div>--}}

											{{--<!-- Name -->--}}
											{{--<div class="foodpole-user-name">--}}
												{{--<h4><a href="#"><strong>Order#</strong> 15083 | <strong>Chef:</strong> Jurry Abbas | <strong>Customer:</strong> Naimat Shabbir</a> <span class="dashboard-status-button red">Cancelled</span></h4>--}}

												{{--<!-- Details -->--}}
												{{--<span class="foodpole-user-detail-item"><strong>Subtotal:</strong> Rs. 1200</span>--}}
												{{--<span class="foodpole-user-detail-item"><strong>GST (17%):</strong> Rs. 210</span>--}}
												{{--<span class="foodpole-user-detail-item"><strong>Delivery:</strong> Rs. 25</span>--}}
												{{--<span class="foodpole-user-detail-item"><strong>Grand Total:</strong> Rs. 1435</span>--}}
												{{----}}
												{{--<!-- Details -->--}}
												{{--<span class="foodpole-user-detail-item o-time-stamp">02:35 PM | 25th September, 2018</span>--}}
												{{----}}
												{{--<!-- fpOffer Details -->--}}
												{{--<ul class="dashboard-task-info fpOffer-info">--}}
													{{--<li><strong>Mughlai Handi</strong><span>Single - x2 - Rs.300</span></li>--}}
													{{--<li><strong>Keema Naan</strong><span>Single - x2 - Rs.300</span></li>--}}
													{{--<li><strong>Been Shahlik</strong><span>Single - x2 - Rs.600</span></li>													--}}
												{{--</ul>--}}
												{{----}}
												{{--<div class="foodpole-user-rating margin-top-25">--}}
												    {{--<h4>Customer Reason</h4>--}}
													{{--<p>Last moment plan change!</p>--}}
												{{--</div>	--}}

												{{--<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">--}}
													{{--<a href="#small-dialog-1" class="popup-with-zoom-anim button dark ripple-effect order-blue"><i class="icon-material-outline-thumb-up"></i> Leave a Comment</a>												--}}
												{{--</div>												--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
								{{--</li>																--}}
							</ul>
						</div>
					</div>
				</div>

			</div>
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->
	@include('admin.admin-boxes.admin-response')


	@endsection