@extends('layouts.admin_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Free Meals</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Meals</a></li>
						<li>Free Meals</li>
					</ul>
				</nav>
			</div>

			@include('admin.admin-boxes.admin-search-meals')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-business-center"></i><?php  $count = 0;
                                foreach ($meals->data as $index){
                                    if($index!= null)
                                    {
                                        if($index->menu->category_name == config('constants.DEFAULT_QUICK-MEAL_CATEGORY')){ $count++;}
                                    }
                                }?>{{ $count }} Meals</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								@foreach($meals->data as $meal_item)
									@if($meal_item != null)
										<li>
											<!-- foodpole-xitem Listing -->
											<div class="foodpole-xitem-listing">

												<!-- foodpole-xitem Listing Details -->
												<div class="foodpole-xitem-listing-details">

													<!-- Logo -->
													<div class="foodpole-xitem-listing-foodpole-source-logo thumb">
														@if($meal_item->item_image == null)
															<img src="{{ URL::asset('images/food-placeholder.jpg') }}" alt="">
														@else
															<img src="{{ URL::asset('item_images/'.$meal_item->item_image->item_image[0]) }}" alt="">
														@endif
													</div>

													<!-- Details -->
													<div class="foodpole-xitem-listing-description">
														<h3 class="foodpole-xitem-listing-title">{{ $meal_item->item_name }}</h3>

														<!-- foodpole-xitem Listing Footer -->
														<div class="foodpole-xitem-listing-footer">
															<ul>
																<li><i class="icon-feather-image"></i> {{ $meal_item->menu->category_name }}</li>
																<li><i class="icon-line-awesome-money"></i> Rs. {{ $meal_item->price }}</li>
																<li><i class="icon-feather-align-justify"></i> {{ $meal_item->item_quantity }} Plates</li>
																{{--<li><i class="icon-material-outline-access-time"></i> Remove in 30 min</li>--}}
															</ul>
														</div>
													</div>
												</div>
											</div>
											<!-- Buttons -->
											<div class="buttons-to-right">
												<a href="#" class="button red ripple-effect">View</a>
												<a href="#" class="button red ripple-effect">Edit</a>
												<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
											</div>
										</li>
									@endif
								@endforeach
						{{--<div class="headline">--}}
							{{--<h3><i class="icon-material-outline-business-center"></i> 5 Meals</h3>--}}
						{{--</div>--}}

						{{--<div class="content">--}}
							{{--<ul class="dashboard-box-list">--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/food-placeholder.jpg" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Chicken Biryani <div class="verified-badge rec-meal rec-inner" title="FoodPole Recommended" data-tippy-placement="right"></div></h3>--}}

												{{--<!-- foodpole-xitem Listing Footer -->--}}
												{{--<div class="foodpole-xitem-listing-footer">--}}
													{{--<ul>--}}
														{{--<li><i class="icon-material-outline-school"></i> Jurry Abbas</li>													--}}
														{{--<li><i class="icon-line-awesome-money"></i> Rs. 150</li>--}}
														{{--<li><i class="icon-feather-align-justify"></i> 15 Servings</li>--}}
														{{--<li><i class="icon-material-outline-access-time"></i> Remove in 30 min</li>--}}
													{{--</ul>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">--}}
										{{--<a href="#" class="button red ripple-effect">Remove Badge</a>--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/food-placeholder.jpg" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Nali Nehari</h3>--}}

												{{--<!-- foodpole-xitem Listing Footer -->--}}
												{{--<div class="foodpole-xitem-listing-footer">--}}
													{{--<ul>--}}
														{{--<li><i class="icon-material-outline-school"></i> Jurry Abbas</li>--}}
														{{--<li><i class="icon-line-awesome-money"></i> Rs. 150</li>--}}
														{{--<li><i class="icon-feather-align-justify"></i> 15 Servings</li>--}}
														{{--<li><i class="icon-material-outline-access-time"></i> Remove in 30 min</li>--}}
													{{--</ul>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">--}}
										{{--<a href="#" class="button red ripple-effect">Recommend</a>--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/food-placeholder.jpg" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Chicken Pulao</h3>--}}

												{{--<!-- foodpole-xitem Listing Footer -->--}}
												{{--<div class="foodpole-xitem-listing-footer">--}}
													{{--<ul>--}}
														{{--<li><i class="icon-material-outline-school"></i> Jurry Abbas</li>--}}
														{{--<li><i class="icon-line-awesome-money"></i> Rs. 150</li>--}}
														{{--<li><i class="icon-feather-align-justify"></i> 15 Servings</li>--}}
														{{--<li><i class="icon-material-outline-access-time"></i> Remove in 30 min</li>--}}
													{{--</ul>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">--}}
										{{--<a href="#" class="button red ripple-effect">Recommend</a>--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/food-placeholder.jpg" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Chinese Soup <div class="verified-badge rec-meal rec-inner" title="FoodPole Recommended" data-tippy-placement="right"></div></h3>--}}

												{{--<!-- foodpole-xitem Listing Footer -->--}}
												{{--<div class="foodpole-xitem-listing-footer">--}}
													{{--<ul>--}}
														{{--<li><i class="icon-material-outline-school"></i> Jurry Abbas</li>--}}
														{{--<li><i class="icon-line-awesome-money"></i> Rs. 150</li>--}}
														{{--<li><i class="icon-feather-align-justify"></i> 15 Servings</li>--}}
														{{--<li><i class="icon-material-outline-access-time"></i> Remove in 30 min</li>--}}
													{{--</ul>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">--}}
										{{--<a href="#" class="button red ripple-effect">Remove Badge</a>--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/food-placeholder.jpg" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Badami Kulfa <div class="verified-badge rec-meal rec-inner" title="FoodPole Recommended" data-tippy-placement="right"></div></h3>--}}

												{{--<!-- foodpole-xitem Listing Footer -->--}}
												{{--<div class="foodpole-xitem-listing-footer">--}}
													{{--<ul>--}}
														{{--<li><i class="icon-material-outline-school"></i> Jurry Abbas</li>--}}
														{{--<li><i class="icon-line-awesome-money"></i> Rs. 150</li>--}}
														{{--<li><i class="icon-feather-align-justify"></i> 15 Servings</li>--}}
														{{--<li><i class="icon-material-outline-access-time"></i> Remove in 30 min</li>--}}
													{{--</ul>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">--}}
										{{--<a href="#" class="button red ripple-effect">Remove Badge</a>--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>								--}}

							</ul>
						</div>
					</div>
				</div>

			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->


@endsection