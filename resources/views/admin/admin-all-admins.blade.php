@extends('layouts.admin_dashboard_layout')
@section('content')
	<!-- Dashboard Content

	================================================== -->





	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>All Admins</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{URL::to('/')}}">Home</a></li>
						<li><a href="#">Admin</a></li>
						<li>All Admins</li>
					</ul>
				</nav>
			</div>

			@include('admin.admin-boxes.admin-search-admins')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">

						<!-- Headline -->
						<div class="headline">


							<h3><i class="icon-material-outline-business-center"></i>Number of Admins {{count($alladmin)}}</h3>

						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								@foreach($alladmin as $admin)
									@if($admin->user->usertype != config('constants.DEFAULT_SUPER-ADMIN_TYPE'))
								<li>
									<!-- foodpole-xitem Listing -->
									<div class="foodpole-xitem-listing">

										<!-- foodpole-xitem Listing Details -->
										<div class="foodpole-xitem-listing-details">


											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}

											<div class="foodpole-xitem-listing-foodpole-source-logo">
												@if($admin->user->image == null)

												<img src= "{{URL::asset('images/user-avatar-placeholder.png')}}"alt="">
													@else
													<img src="{{ URL::asset('chefimages/'.$admin->user->image) }}" alt="">
													@endif
											</div>

											<!-- Details -->

											<div class="foodpole-xitem-listing-description">

												<h3 class="foodpole-xitem-listing-title">{{$admin->user->full_name}}
													@if($admin->user->account_status == false)
													<span class="dashboard-status-button red">Blocked</span>
														@endif
												</h3>

												<!-- foodpole-xitem Listing Footer -->
												<div class="foodpole-xitem-listing-footer">
													<ul>
														<li><i class="icon-material-outline-highlight-off"></i>{{$admin->user->email}}</li>
														<li><i class="icon-material-outline-highlight-off"></i>{{$admin->user->usertype}}</li>
														<li><i class="icon-material-outline-highlight-off"></i> id {{$admin->user->id}}</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<!-- Buttons -->
									<div class="buttons-to-right">
										@if($admin->user->account_status == true)
										<a href="{{ URL::to('admin/block/'.$admin->user->id)}}" class="button red ripple-effect">Block</a>
										@else
											<a href="{{ URL::to('admin/unblock/'.$admin->user->id)}}" class="button red ripple-effect">Unblock</a>
										@endif
											{{--<a href="#" class="button red ripple-effect">View</a>--}}
										<a href="{{ URL::to('/admin/admin-profile/'.$admin->user->id)}}" class="button red ripple-effect">Edit</a>
										<a href="{{ URL::to('admin/delete/'.$admin->user->id)}}" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
									</div>

								</li>
									@endif
								@endforeach

								{{--<li>--}}


									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/user-avatar-placeholder.png" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Naimat Shabbir <span class="dashboard-status-button red">Blocked</span></h3>--}}

												{{--<!-- foodpole-xitem Listing Footer -->--}}
												{{--<div class="foodpole-xitem-listing-footer">--}}
													{{--<ul>--}}
														{{--<li><i class="icon-material-outline-highlight-off"></i> 5 Blocks</li>--}}
														{{--<li><i class="icon-material-outline-highlight-off"></i> 10 Suspensions</li>--}}
														{{--<li><i class="icon-material-outline-highlight-off"></i> 3 Removals</li>--}}
													{{--</ul>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">--}}

										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/user-avatar-placeholder.png" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Wajid Raza</h3>--}}

												{{--<!-- foodpole-xitem Listing Footer -->--}}
												{{--<div class="foodpole-xitem-listing-footer">--}}
													{{--<ul>--}}
														{{--<li><i class="icon-material-outline-highlight-off"></i> 5 Blocks</li>--}}
														{{--<li><i class="icon-material-outline-highlight-off"></i> 10 Suspensions</li>--}}
														{{--<li><i class="icon-material-outline-highlight-off"></i> 3 Removals</li>--}}
													{{--</ul>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">--}}
										{{--<a href="#" class="button red ripple-effect">Block</a>												--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>														--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/user-avatar-placeholder.png" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Wasim Akram</h3>--}}

												{{--<!-- foodpole-xitem Listing Footer -->--}}
												{{--<div class="foodpole-xitem-listing-footer">--}}
													{{--<ul>--}}
														{{--<li><i class="icon-material-outline-highlight-off"></i> 5 Blocks</li>--}}
														{{--<li><i class="icon-material-outline-highlight-off"></i> 10 Suspensions</li>--}}
														{{--<li><i class="icon-material-outline-highlight-off"></i> 3 Removals</li>--}}
													{{--</ul>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">--}}
										{{--<a href="#" class="button red ripple-effect">Block</a>												--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>	--}}
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/user-avatar-placeholder.png" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Javed Miandad</h3>--}}

												{{--<!-- foodpole-xitem Listing Footer -->--}}
												{{--<div class="foodpole-xitem-listing-footer">--}}
													{{--<ul>--}}
														{{--<li><i class="icon-material-outline-highlight-off"></i> 5 Blocks</li>--}}
														{{--<li><i class="icon-material-outline-highlight-off"></i> 10 Suspensions</li>--}}
														{{--<li><i class="icon-material-outline-highlight-off"></i> 3 Removals</li>--}}
													{{--</ul>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">--}}
										{{--<a href="#" class="button red ripple-effect">Block</a>												--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>									--}}
							</ul>

						</div>

					</div>
				</div>

			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->
@endsection