@extends('layouts.admin_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Processing Orders</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Orders</a></li>
						<li>Processing Orders</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">

				@include('admin.admin-boxes.admin-search-orders')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">


						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-feather-shopping-bag"></i>  {{ count($orders->data) }} Order</h3>
						</div>
						<div class="content">
							<ul class="dashboard-box-list">
								@foreach($orders->data as $order_item)
									<li>
										<!-- Overview -->
										<div class="foodpole-user-overview manage-candidates">
											<div class="foodpole-user-overview-inner">

												<!-- Avatar -->
												<div class="foodpole-user-avatar">
													<a href="#"><img src="{{URL::asset('images/food-placeholder.jpg') }}" alt=""></a>
												</div>

												<!-- Name -->
												<div class="foodpole-user-name">
													<h4><a href="#"><strong>Order#</strong> {{ $order_item->id }} | <strong>Chef:</strong> {{$order_item->chef->user->full_name}} | <strong>Customer:</strong> {{$order_item->customer->user->full_name}}</a>
														@if($order_item->payment != null && $order_item->payment->payment_status == true)
															<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
															<span class="dashboard-status-button green">{{ $order_item->order_status }}</span>
														@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
															<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															<span class="dashboard-status-button green">{{ $order_item->order_status }}</span>
														@endif</h4>

													<!-- Details -->
													<span class="foodpole-user-detail-item"><strong>Subtotal:</strong> Rs. {{ $order_item->sub_total }}</span>
													{{--<span class="foodpole-user-detail-item"><strong>GST (17%):</strong> Rs. 210</span>--}}
													<span class="foodpole-user-detail-item"><strong>Delivery:</strong> Rs. {{ $order_item->delivery_fare }}</span>
													<span class="foodpole-user-detail-item"><strong>Grand Total:</strong> Rs. {{ $order_item->grand_total }}</span>

													<!-- Details -->
													<span class="foodpole-user-detail-item o-time-stamp">{{ \Carbon\Carbon::parse($order_item->created_at->date)->toDayDateTimeString() }}</span>

													<!-- fpOffer Details -->
													<ul class="dashboard-task-info fpOffer-info">
														@foreach($order_item->order_line_items->data as $item)
															<li><strong>{{ $item->menu_item->item_name }}</strong><span>{{ $item->item_option }} - x{{ $item->quantity }} - Rs.{{ $item->sub_total }}</span></li>
														@endforeach
													</ul>

													<!-- Buttons -->
													<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">
														{{--<span class="button ripple-effect order-green"><i class="icon-material-outline-check-circle"></i> Ready</span>--}}
														<a href="{{ URL::to('/accept/order/'.$order_item->id.'/'.config('constants.DEFAULT_ORDER_READY')) }}" class="button ripple-effect order-green"><i class="icon-material-outline-check-circle"></i> Ready</a>
														<a href="{{ URL::to('/address-info/'.$order_item->id)}}" target="_blank" class="button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Route  Info</a>
													</div>
												</div>
											</div>
										</div>
									</li>
								@endforeach
							</ul>
						</div>

						{{--<div class="content">--}}
							{{--<ul class="dashboard-box-list">--}}
								{{--<li>--}}
									{{--<!-- Overview -->--}}
									{{--<div class="foodpole-user-overview manage-candidates">--}}
										{{--<div class="foodpole-user-overview-inner">--}}

											{{--<!-- Avatar -->--}}
											{{--<div class="foodpole-user-avatar">--}}
												{{--<a href="#"><img src="images/food-placeholder.jpg" alt=""></a>--}}
											{{--</div>--}}

											{{--<!-- Name -->--}}
											{{--<div class="foodpole-user-name">--}}
												{{--<h4><a href="#"><strong>Order#</strong> 15071 | <strong>Chef:</strong> Jurry Abbas | <strong>Customer:</strong> Naimat Shabbir</a> <span class="dashboard-status-button green">Processing</span> <span class="dashboard-status-button yellow">Unpaid</span></h4>--}}

												{{--<!-- Details -->--}}
												{{--<span class="foodpole-user-detail-item"><strong>Subtotal:</strong> Rs. 1200</span>--}}
												{{--<span class="foodpole-user-detail-item"><strong>GST (17%):</strong> Rs. 210</span>--}}
												{{--<span class="foodpole-user-detail-item"><strong>Delivery:</strong> Rs. 25</span>--}}
												{{--<span class="foodpole-user-detail-item"><strong>Grand Total:</strong> Rs. 1435</span>--}}
												{{----}}
												{{--<!-- Details -->--}}
												{{--<span class="foodpole-user-detail-item o-time-stamp">02:35 PM | 25th September, 2018</span>--}}

												{{--<!-- fpOffer Details -->--}}
												{{--<ul class="dashboard-task-info fpOffer-info">--}}
													{{--<li><strong>Mughlai Handi</strong><span>Single - x2 - Rs.300</span></li>--}}
													{{--<li><strong>Keema Naan</strong><span>Single - x2 - Rs.300</span></li>--}}
													{{--<li><strong>Been Shahlik</strong><span>Single - x2 - Rs.600</span></li>													--}}
												{{--</ul>--}}

												{{--<!-- Buttons -->--}}
												{{--<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">--}}
													{{--<a href="#small-dialog-5" class="popup-with-zoom-anim button ripple-effect order-green"><i class="icon-material-outline-check-circle"></i> Ready</a>--}}
													{{--<a href="#small-dialog"  class="popup-with-zoom-anim button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Route info</a>													--}}
												{{--</div>												--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
								{{--</li>								--}}
							{{--</ul>--}}
						{{--</div>--}}
					</div>
				</div>

			</div>
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->

	@include('admin.admin-boxes.admin-ready-order')
	@include('admin.admin-boxes.admin-route-info')


	@endsection