@extends('layouts.admin_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Howdy, {{ Session::get('full_name')}}!</h3>
				<span>We are glad to see you again!</span>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li>Admin</li>
						<li>Dashboard</li>
					</ul>
				</nav>
			</div>
	
			<!-- Fun Facts Container -->
			<div class="fun-facts-container">
				<div class="fun-fact" data-fun-fact-color="#36bd78">
					<div class="fun-fact-text">
						<span>Orders Served</span>
						@if($renderedArray->orders_served == 0)
							<h4 class="rev-lac-plus">{{ $renderedArray->orders_served }}</h4>
						@else
						<h4>{{ $renderedArray->orders_served}}</h4>
							@endif
					</div>
					<div class="fun-fact-icon"><i class="icon-material-outline-restaurant"></i></div>
				</div>
				<div class="fun-fact" data-fun-fact-color="#b81b7f">
					<div class="fun-fact-text ff-revenue">
						<span>Revenue Rs.</span>
                        @if($renderedArray->revenue == 0)
                            <h4 class="rev-lac-plus">{{ $renderedArray->revenue }}</h4>
                        @else
                            <h4 class="rev-lac-plus">{{ $renderedArray->revenue[0]->Total }}</h4>
                        @endif
						{{--<h4 class="rev-lac-plus">9012</h4>--}}
					</div>
					<div class="fun-fact-icon"><i class="icon-line-awesome-money"></i></div>
				</div>
				<div class="fun-fact" data-fun-fact-color="#efa80f">
					<div class="fun-fact-text">
						<span>  Reviews</span>
						@if($renderedArray->total_rating == 0)
							<h4 class="rev-lac-plus">{{ $renderedArray->total_rating }}</h4>
						@else
						<h4>{{$renderedArray->total_rating}}</h4>
							@endif
					</div>
					<div class="fun-fact-icon"><i class="icon-material-outline-rate-review"></i></div>
				</div>

				<!-- Last one has to be hidden below 1600px, sorry :( -->
				<div class="fun-fact" data-fun-fact-color="#2a41e6">
					<div class="fun-fact-text ff-revenue">
						<span>Page Views</span>
						<h4 class="rev-lac-plus">12</h4>
					</div>
					<div class="fun-fact-icon"><i class="icon-feather-trending-up"></i></div>
				</div>
			</div>
			
			<!-- Fun Facts Container -->
			<div class="fun-facts-container margin-top-25">
				<div class="fun-fact" data-fun-fact-color="#bdb036">
					<div class="fun-fact-text">
						<span>Customers</span>
						@if($renderedArray->total_customer == 0)
							<h4 class="rev-lac-plus">{{ $renderedArray->total_customer }}</h4>
						@else
						<h4>{{ $renderedArray->total_customer}}</h4>
							@endif
					</div>
					<div class="fun-fact-icon"><i class="icon-material-outline-group"></i></div>
				</div>
				<div class="fun-fact" data-fun-fact-color="#36adbd">
					<div class="fun-fact-text ff-revenue">
						<span>Chefs</span>
						@if($renderedArray->total_chef == 0)
							<h4 class="rev-lac-plus">{{ $renderedArray->total_chef }}</h4>
						@else
						<h4 class="rev-lac-plus">{{ $renderedArray->total_chef}}</h4>
							@endif
					</div>
					<div class="fun-fact-icon"><i class="icon-material-outline-school"></i></div>
				</div>
				<div class="fun-fact" data-fun-fact-color="#ec2327">
					<div class="fun-fact-text ff-revenue">
						<span>Registered Users</span>
						@if($renderedArray->total_users == 0)
							<h4 class="rev-lac-plus">{{ $renderedArray->total_users }}</h4>
						@else
						<h4 class="rev-lac-plus">{{ $renderedArray->total_users}}</h4>
							@endif
					</div>
					<div class="fun-fact-icon"><i class="icon-material-outline-group"></i></div>
				</div>

				<!-- Last one has to be hidden below 1600px, sorry :( -->
				<div class="fun-fact" data-fun-fact-color="#bd3674">
					<div class="fun-fact-text">
						<span>Online Chefs</span>
						<h4>{{$renderedArray->online_chef}}</h4>
					</div>
					<div class="fun-fact-icon"><i class="icon-material-outline-add-location"></i></div>
				</div>
			</div>

			<!-- Row -->
			<div class="row">

				<div class="col-xl-6">
					<!-- Dashboard Box -->
					<div class="dashboard-box main-box-in-row">
						<div class="headline">
							<h3><i class="icon-feather-bar-chart-2"></i> Sales - Revenue - Profit Analysis - December 2018</h3>
						</div>
						<div class="content">
							<!-- Chart -->
							<div class="chart">
								<canvas id="chart-sales-profit" width="100" height="45"></canvas>
							</div>
						</div>
					</div>
					<!-- Dashboard Box / End -->
				</div>
				
				<div class="col-xl-6">
					<!-- Dashboard Box -->
					<div class="dashboard-box main-box-in-row">
						<div class="headline">
							<h3><i class="icon-feather-bar-chart-2"></i> Total/Regular/Quick Meals  Analysis - December 2018</h3>
						</div>
						<div class="content">
							<!-- Chart -->
							<div class="chart">
								<canvas id="chart-t-r-q" width="100" height="45"></canvas>
							</div>
						</div>
					</div>
					<!-- Dashboard Box / End -->
				</div>				
				
				
			</div>
			<!-- Row / End -->

			<!-- Row -->
			<div class="row">

				<div class="col-xl-6">
					<!-- Dashboard Box -->
					<div class="dashboard-box main-box-in-row">
						<div class="headline">
							<h3><i class="icon-feather-bar-chart-2"></i> Users - Customers - Chefs Analysis - December 2018</h3>
						</div>
						<div class="content">
							<!-- Chart -->
							<div class="chart">
								<canvas id="chart-u-c-c" width="100" height="45"></canvas>
							</div>
						</div>
					</div>
					<!-- Dashboard Box / End -->
				</div>
				
				<div class="col-xl-6">
					<!-- Dashboard Box -->
					<div class="dashboard-box main-box-in-row">
						<div class="headline">
							<h3><i class="icon-feather-bar-chart-2"></i> Delivery / Take Away Analysis</h3>
						</div>
						<div class="content">
							<!-- Chart -->
							<div class="chart">
								<canvas id="chart" width="100" height="45"></canvas>
							</div>
						</div>
					</div>
					<!-- Dashboard Box / End -->
				</div>				
				
				
			</div>
			<!-- Row / End -->			
			
			<!-- Row -->
			{{--<div class="row">--}}

				{{--<!-- Dashboard Box -->--}}
				{{--<div class="col-xl-6">--}}
					{{--<div class="dashboard-box">--}}
						{{--<table class="basic-table">--}}
							{{--<tr>--}}
								{{--<th>Top 5 Regions</th>--}}
								{{--<th>No. of Orders</th>--}}
							{{--</tr>--}}
							{{----}}
							{{--<tr>--}}
								{{--<td data-label="Column 1">Rawalpindi</td>--}}
								{{--<td data-label="Column 2">2300</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1">Islamabad</td>--}}
								{{--<td data-label="Column 2">2200</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1">Lahore</td>--}}
								{{--<td data-label="Column 2">2100</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1">Karachi</td>--}}
								{{--<td data-label="Column 2">1900</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1">Peshawar</td>--}}
								{{--<td data-label="Column 2">1500</td>--}}
							{{--</tr>							--}}
						{{--</table>--}}
					{{--</div>--}}
				{{--</div>--}}
				{{----}}
				{{--<div class="col-xl-6">--}}
					{{--<div class="dashboard-box">--}}
						{{--<table class="basic-table">--}}
							{{--<tr>--}}
								{{--<th>Top 5 Meals</th>--}}
								{{--<th>Orders</th>--}}
								{{--<th>Revenue</th>								--}}
							{{--</tr>--}}
							{{----}}
							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Sindhi Biryani</a></td>--}}
								{{--<td data-label="Column 2">23</td>--}}
								{{--<td data-label="Column 3">10,023</td>								--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Nali Nehari</a></td>--}}
								{{--<td data-label="Column 2">22</td>--}}
								{{--<td data-label="Column 3">9,563</td>								--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Siri Paye</a></td>--}}
								{{--<td data-label="Column 2">19</td>--}}
								{{--<td data-label="Column 3">6,568</td>								--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Chicken Pulao</a></td>--}}
								{{--<td data-label="Column 2">15</td>--}}
								{{--<td data-label="Column 3">5,600</td>								--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Chicken Korma</a></td>--}}
								{{--<td data-label="Column 2">06</td>--}}
								{{--<td data-label="Column 3">2,400</td>								--}}
							{{--</tr>							--}}
						{{--</table>--}}
					{{--</div>--}}
				{{--</div>				--}}

			{{--</div>--}}
			<!-- Row / End -->	

			<!-- Row -->
			<div class="row">

				<!-- Dashboard Box -->
				<div class="col-xl-6">
					<div class="dashboard-box">

						<table class="basic-table">
							<tr>
								<th>Top 5 Customers</th>
								<th>No. of Orders</th>
							</tr>
							@if($renderedArray->top_customer_orders != null)
							@foreach($renderedArray->top_customer_orders as $data)
							<tr>

								<td data-label="Column 1"><a href="#">{{$data->full_name}}</a></td>
								<td data-label="Column 2">{{$data->total_order}}</td>
							</tr>
							@endforeach
							@endif
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Hamza Dabeer</a></td>--}}
								{{--<td data-label="Column 2">200</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Naimat Shabbir</a></td>--}}
								{{--<td data-label="Column 2">100</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Wajid Raaz</a></td>--}}
								{{--<td data-label="Column 2">90</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Talha Tariq</a></td>--}}
								{{--<td data-label="Column 2">50</td>--}}
							{{--</tr>							--}}
						</table>

					</div>
				</div>
				
				<div class="col-xl-6">
					<div class="dashboard-box">
						<table class="basic-table">
							<tr>
								<th>Top 5 Chefs</th>
								<th>No. of Orders</th>
							</tr>
							@if($renderedArray->top_chef_orders != null)
							@foreach($renderedArray->top_chef_orders as $data)
							<tr>
								<td data-label="Column 1"><a href="#">{{$data->full_name}}</a></td>
								<td data-label="Column 2">{{$data->total_order}}</td>
							</tr>

							@endforeach
							@endif

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Hamza Dabeer</a></td>--}}
								{{--<td data-label="Column 2">700</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Jurry Abbas</a></td>--}}
								{{--<td data-label="Column 2">600</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Wajid Raaz</a></td>--}}
								{{--<td data-label="Column 2">500</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Naimat Shabbir</a></td>--}}
								{{--<td data-label="Column 2">400</td>--}}
							{{--</tr>							--}}
						</table>
					</div>
				</div>				

			</div>
			<!-- Row / End -->				

			<!-- Row -->
			<div class="row">

				<!-- Dashboard Box -->
				{{--<div class="col-xl-6">--}}
					{{--<div class="dashboard-box">--}}
						{{--<table class="basic-table">--}}
							{{--<tr>--}}
								{{--<th>Top 5 Users</th>--}}
								{{--<th>Rating</th>--}}
							{{--</tr>--}}
							{{----}}
							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Jurry Abbas</a></td>--}}
								{{--<td data-label="Column 2">5.0</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Hamza Dabeer</a></td>--}}
								{{--<td data-label="Column 2">4.9</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Naimat Shabbir</a></td>--}}
								{{--<td data-label="Column 2">4.8</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Wajid Raaz</a></td>--}}
								{{--<td data-label="Column 2">4.7</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Talha Tariq</a></td>--}}
								{{--<td data-label="Column 2">4.6</td>--}}
							{{--</tr>							--}}
						{{--</table>--}}
					{{--</div>--}}
				{{--</div>--}}
				{{----}}
				<div class="col-xl-6">
					<div class="dashboard-box">
						<table class="basic-table">
							<tr>
								<th>Top 5 Chefs</th>
								<th>No. of Ratings</th>
							</tr>
							@if($renderedArray->top_chef_rated != null)
							@foreach($renderedArray->top_chef_rated as $data)
							<tr>
								<td data-label="Column 1"><a href="#">{{$data->full_name}}</a></td>
								<td data-label="Column 2">{{$data->chef_rating}}</td>
							</tr>
							@endforeach
							@endif
							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Hamza Dabeer</a></td>--}}
								{{--<td data-label="Column 2">45</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Jurry Abbas</a></td>--}}
								{{--<td data-label="Column 2">38</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Wajid Raaz</a></td>--}}
								{{--<td data-label="Column 2">33</td>--}}
							{{--</tr>--}}

							{{--<tr>--}}
								{{--<td data-label="Column 1"><a href="#">Naimat Shabbir</a></td>--}}
								{{--<td data-label="Column 2">29</td>--}}
							{{--</tr>							--}}
						</table>

					</div>
				</div>				

			</div>
			<!-- Row / End -->	

			{{--@include('general_boxes.top-meals')--}}
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->
@endsection