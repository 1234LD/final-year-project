@extends('layouts.admin_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>All Orders</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Orders</a></li>
						<li>All Orders</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">

				@include('admin.admin-boxes.admin-search-orders')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">


						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-feather-shopping-bag"></i> {{ count($orders->data) }} Orders</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								@foreach($orders->data as $order_item)
								<li>
									<!-- Overview -->
									<div class="foodpole-user-overview manage-candidates">
										<div class="foodpole-user-overview-inner">

											<!-- Avatar -->
											<div class="foodpole-user-avatar">
												<a href="#"><img src="{{URL::asset('images/food-placeholder.jpg') }}" alt=""></a>
											</div>

											<!-- Name -->
											<div class="foodpole-user-name">
												<h4><a href="#"><strong>Order#</strong> {{ $order_item->id }} | <strong>Chef:</strong> {{$order_item->chef->user->full_name}} | <strong>Customer:</strong> {{$order_item->customer->user->full_name}}</a>
												@if($order_item->flag == true)
													<span class="dashboard-status-button red">Flagged</span>
												@else
													@if($order_item->order_status == config('constants.DEFAULT_ORDER_DELIVERED'))
														<span class="dashboard-status-button green">{{ $order_item->order_status }}</span>

														@if($order_item->payment != null && $order_item->payment->payment_status == true)
															<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
														@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
															<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
														@endif
													@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_PENDING'))
														<span class="dashboard-status-button yellow">{{ $order_item->order_status }}</span>
														@if($order_item->payment != null && $order_item->payment->payment_status == true)
															<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
														@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
															<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
														@endif
													@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_DECLINED'))
														<span class="dashboard-status-button red">{{ $order_item->order_status }}</span>
														@if($order_item->payment != null && $order_item->payment->payment_status == true)
															<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
														@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
															<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
														@endif
													@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_CANCELLED'))
														<span class="dashboard-status-button red">{{ $order_item->order_status }}</span>
														@if($order_item->payment != null && $order_item->payment->payment_status == true)
															<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
														@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
															<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
														@endif
													@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_READY'))
														<span class="dashboard-status-button green">{{ $order_item->order_status }}</span>
														@if($order_item->payment != null && $order_item->payment->payment_status == true)
															<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
														@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
															<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
														@endif
													@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_COMPLETED'))
														<span class="dashboard-status-button green">{{ $order_item->order_status }}</span>
														@if($order_item->payment != null && $order_item->payment->payment_status == true)
															<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
														@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
															<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
														@endif
													@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_PROCESSING'))
														<span class="dashboard-status-button green">{{ $order_item->order_status }}</span>
														@if($order_item->payment != null && $order_item->payment->payment_status == true)
															<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
														@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
															<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															@endif
															@endif
															@endif</h4>
												<!-- Details -->
												<span class="foodpole-user-detail-item"><strong>Subtotal:</strong> Rs.  {{ $order_item->sub_total }}</span>
												{{--<span class="foodpole-user-detail-item"><strong>GST (17%):</strong> Rs. 210</span>--}}
												<span class="foodpole-user-detail-item"><strong>Delivery:</strong> Rs.{{ $order_item->delivery_fare }}</span>
												<span class="foodpole-user-detail-item"><strong>Grand Total:</strong> Rs.  {{ $order_item->grand_total }}</span>
												
												<!-- Details -->
												<span class="foodpole-user-detail-item o-time-stamp">{{ \Carbon\Carbon::parse($order_item->created_at->date)->toDayDateTimeString() }}</span>
												
												<!-- fpOffer Details -->
												<ul class="dashboard-task-info fpOffer-info">
													@foreach($order_item->order_line_items->data as $item)
														<li><strong>{{ $item->menu_item->item_name }}</strong><span>{{ $item->item_option }} - x{{ $item->quantity }} - Rs.{{ $item->sub_total }}</span></li>
													@endforeach

													{{--<li><strong>Mughlai Handi</strong><span>Single - x2 - Rs.300</span></li>--}}
													{{--<li><strong>Keema Naan</strong><span>Single - x2 - Rs.300</span></li>--}}
													{{--<li><strong>Been Shahlik</strong><span>Single - x2 - Rs.600</span></li>													--}}
												</ul>

												@if($order_item->order_status == config('constants.DEFAULT_ORDER_PENDING'))

												<!-- Buttons -->
													<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">
														<button class="button ripple-effect order-green" onclick="adminReason($(this))"><i class="icon-material-outline-access-time"></i> Accept</button>
														<button class="button ripple-effect order-red" onclick="adminReason($(this))"><i class="icon-line-awesome-times-circle-o"></i> Decline</button>
														<button class="button ripple-effect order-red" onclick="adminReason($(this))"><i class="icon-line-awesome-times-circle-o"></i> Cancel</button>
														<a href="{{ URL::to('/address-info/'.$order_item->id)}}" target="_blank" class="button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Route  Info</a>
													</div>

												@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_DECLINED'

                                                         ))
													<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">
														{{--<button class="button dark ripple-effect order-blue" onclick="adminComment($(this))"><i class="icon-material-outline-thumb-up"></i> Leave a Comment</button>--}}
													</div>

												@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_DELIVERED'

                                                     ))
													<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">
														{{--<button class="button dark ripple-effect order-blue" onclick="adminComment($(this))"><i class="icon-material-outline-thumb-up"></i> Leave a Comment</button>--}}
													</div>
												@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_CANCELLED'

                                                     ))
													<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">
														{{--<button class="button dark ripple-effect order-blue" onclick="adminComment($(this))"><i class="icon-material-outline-thumb-up"></i> Leave a Comment</button>--}}
													</div>
												@elseif($order_item->flag == false))


												<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">
													{{--<button class="button dark ripple-effect order-blue" onclick="adminComment($(this))"><i class="icon-material-outline-thumb-up"></i> Leave a Comment</button>--}}
												</div>

												@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_PROCESSING'))

													<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">
														<button class="button ripple-effect order-green" onclick="adminReason($(this))"><i class="icon-material-outline-check-circle"></i> Ready</button>
															{{--<a href="#" target="_blank" class="button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Route info</a>--}}
														<a href="{{ URL::to('/address-info/'.$order_item->id)}}" target="_blank" class="button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Route  Info</a>
													</div>
												@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_READY'))
													<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">
														<button class="button ripple-effect order-green" onclick="adminReason($(this))"><i class="icon-material-outline-check-circle"></i> Delivered</button>
														{{--<a href="#" target="_blank" class="button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Route info</a>--}}
														<a href="{{ URL::to('/address-info/'.$order_item->id)}}" target="_blank" class="button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Route  Info</a>
													</div>
												@endif

												@include('general_boxes.admin-reason-area')
												@include('general_boxes.admin-comment-box')
												@include('general_boxes.customer-reason-area')
												@include('general_boxes.customer-review-box')
												@include('general_boxes.customer-flag')
											</div>
										</div>
									</div>
								</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>

			</div>
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->




	@endsection