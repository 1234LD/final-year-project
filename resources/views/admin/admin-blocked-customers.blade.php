@extends('layouts.admin_dashboard_layout')
@section('content')
	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Blocked Customers</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Customers</a></li>
						<li>Blocked Customers</li>
					</ul>
				</nav>
			</div>

			@include('admin.admin-boxes.admin-search-customer')

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-25">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-business-center"></i> {{count($blockcustomer)}} blocked Customer</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								@foreach($blockcustomer as $customer )
								<li>
									<!-- foodpole-xitem Listing -->
									<div class="foodpole-xitem-listing">

										<!-- foodpole-xitem Listing Details -->
										<div class="foodpole-xitem-listing-details">

											<!-- Logo -->

											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/user-avatar-placeholder.png" alt="">--}}

											<div class="foodpole-xitem-listing-foodpole-source-logo">
												{{--<img src="images/user-avatar-placeholder.png" alt="">--}}
												@if($customer->image == null)
													<img src= "{{URL::asset('images/user-avatar-placeholder.png')}}"alt="">
												@else
													<img src="{{ URL::asset('chefimages/'.$customer->image )}}" alt="">
												@endif

											</div>

											<!-- Details -->
											<div class="foodpole-xitem-listing-description">
												<h3 class="foodpole-xitem-listing-title">{{$customer->full_name}} <span class="dashboard-status-button red">Blocked</span></h3>

												<!-- foodpole-xitem Listing Footer -->
												<div class="foodpole-xitem-listing-footer">
													<ul>
														<li><i class="icon-material-outline-location-on"></i>Blocked</li>
														<li><i class="icon-feather-shopping-bag"></i>{{$customer->email}}</li>
														<li><i class="icon-material-outline-rate-review"></i>  {{$customer->id}} id</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<!-- Buttons -->
									<div class="buttons-to-right">
										<a href="{{ URL::to('admin/unblock/'.$customer->id)}}" class="button red ripple-effect">Unblock</a>
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										<a href="{{ URL::to('/admin/admin-profile/'.$customer->id)}}" class="button red ripple-effect">Edit</a>
										<a href="{{ URL::to('admin/delete/'.$customer->id)}}" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
									</div>
								</li>
								@endforeach
								{{--<li>--}}
									{{--<!-- foodpole-xitem Listing -->--}}
									{{--<div class="foodpole-xitem-listing">--}}

										{{--<!-- foodpole-xitem Listing Details -->--}}
										{{--<div class="foodpole-xitem-listing-details">--}}

											{{--<!-- Logo -->--}}
											{{--<div class="foodpole-xitem-listing-foodpole-source-logo thumb">--}}
												{{--<img src="images/user-avatar-placeholder.png" alt="">--}}
											{{--</div>--}}

											{{--<!-- Details -->--}}
											{{--<div class="foodpole-xitem-listing-description">--}}
												{{--<h3 class="foodpole-xitem-listing-title">Wajid Raza</h3>--}}

												{{--<!-- foodpole-xitem Listing Footer -->--}}
												{{--<div class="foodpole-xitem-listing-footer">--}}
													{{--<ul>--}}
														{{--<li><i class="icon-material-outline-location-on"></i> Islamabad</li>--}}
														{{--<li><i class="icon-feather-shopping-bag"></i> 5 Orders</li>--}}
														{{--<li><i class="icon-material-outline-rate-review"></i> 2 reviews</li>--}}
													{{--</ul>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
									{{--<!-- Buttons -->--}}
									{{--<div class="buttons-to-right">--}}
										{{--<a href="#" class="button red ripple-effect">Block</a>												--}}
										{{--<a href="#" class="button red ripple-effect">View</a>									--}}
										{{--<a href="#" class="button red ripple-effect">Edit</a>									--}}
										{{--<a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>--}}
									{{--</div>--}}
								{{--</li>																						--}}
							</ul>
						</div>
					</div>
				</div>

			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->
@endsection