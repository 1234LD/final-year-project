@extends('layouts.master_layout')
@section('content')
<!-- Titlebar
================================================== -->
<div class="single-page-header" data-background-image="{{ URL::asset('images/app-background.jpg') }}">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="single-page-header-inner">
					<div class="left-side">
						@if($chef_menu->data[0]->picture == null)
							<div class="header-image chef-header"><div class="user-avatar status-online"><img src="{{ URL::asset('images/user-avatar-placeholder.png') }}" alt=""></div></div>
						@else
							<div class="header-image chef-header"><div class="user-avatar status-online"><img src="{{ URL::asset('chefimages/'.$chef_menu->data[0]->picture) }}" alt=""></div></div>
						@endif
							<div class="header-details margin-top-20">
							<h3>{{ $chef_menu->data[0]->user->full_name }}</h3>
							<h5>{{ $chef_menu->data[0]->city }}, {{ $chef_menu->data[0]->state }}</h5>
							<ul>
								@if($chef_menu->data[0]->delivery_type == config('constants.DEFAULT_BOTH_TYPE'))
									<li><i class="icon-material-outline-redo"></i> Delivery & Take Away</li>
								@else
									<li><i class="icon-material-outline-redo"></i>{{ $chef_menu->data[0]->delivery_type }}</li>
								@endif
								<li><div class="star-rating" data-rating="{{ $chef_menu->data[0]->chef_rating }}"></div></li>
									<?php date_default_timezone_set('Asia/Karachi')?>
								<li><i class="icon-material-outline-access-time"></i> {{ \Carbon\Carbon::parse($chef_menu->data[0]->start_time)->format('g:i A') }} - {{ \Carbon\Carbon::parse($chef_menu->data[0]->end_time)->format('g:i A') }}</li>
								@if($chef_menu->data[0]->verify)
									<li><div class="verified-badge-with-title rec-meal-op">Verified Chef</div></li>
								@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Page Content
================================================== -->

<div class="container">
	<div class="row">
		
		<!-- Content -->
		<div class="col-xl-8 col-lg-8 content-right-offset">
		
			<!-- About me -->
			<div class="single-page-section">
				<h3 class="margin-bottom-25">About me</h3>
				<p>{{ $chef_menu->data[0]->description }}</p>
			</div>		
			
			<div class="clearfix"></div>

		</div>
		

		<!-- Sidebar -->
		<div class="col-xl-4 col-lg-4">
			<div class="sidebar-container">

				<!-- Sidebar Widget -->
				<div class="sidebar-widget">
					<h3>Bookmark or Share</h3>

					<!-- Bookmark Button -->
					<button class="bookmark-button margin-bottom-25">
						<span class="bookmark-icon"></span>
						<span class="bookmark-text">Bookmark</span>
						<span class="bookmarked-text">Bookmarked</span>
					</button>

					<!-- Copy URL -->
					<div class="copy-url">
						<input id="copy-url" type="text" value="{{ \Illuminate\Support\Facades\Route::currentRouteName() }}" class="with-border">
						<button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>
					</div>

					<!-- Share Buttons -->
					<div class="share-buttons margin-top-25">
						<div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
						<div class="share-buttons-content">
							<span>Interesting? <strong>Share It!</strong></span>
							<ul class="share-buttons-icons">
								<li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"><i class="icon-brand-facebook-f"></i></a></li>
								<li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
								<li><a href="#" data-button-color="#dd4b39" title="Share on Google Plus" data-tippy-placement="top"><i class="icon-brand-google-plus-g"></i></a></li>
								<li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"><i class="icon-brand-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>

@include('chef.chef-boxes.chef-meals')

<!-- Spacer -->
<div class="margin-top-15"></div>
<!-- Spacer / End-->



@endsection