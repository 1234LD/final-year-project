@extends('layouts.app')

@section('content')

    <h1>Forgot Password ?</h1>
    <div class="well">

        <form action="{{url('submit/reset_password/request/'.$data['email'])}}" method="POST" class="popup-form form-horizontal">

            {!! csrf_field() !!}
            <fieldset>
                <legend>Reset your Password</legend>

                <!-- Email -->
                <div class="form-group">
                    <div class="col-lg-10">
                        <input type="password" name="password" class="form-control form-white" placeholder="Enter New Password">
                    </div>
                </div>

                <!-- Submit Button -->
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-submit btn-lg btn-info pull-right">Set Password</button>
                    </div>
                </div>
            </fieldset>

        </form>
    </div>

@endsection
