@extends('layouts.user_dashboard_layout')
@section('content')

	<!-- Dashboard Content -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Howdy, {{ Session::get('username') }}</h3>
				<span>We are glad to see you again!</span>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li>Dashboard</li>
					</ul>
				</nav>
			</div>
	
			<!-- Fun Facts Container -->
			<div class="fun-facts-container">
				<div class="fun-fact" data-fun-fact-color="#36bd78">
					<div class="fun-fact-text">
						<span>Meals Ordered</span>
						<h4>{{ $renderedArray->meals_ordered }}</h4>
					</div>
					<div class="fun-fact-icon"><i class="icon-material-outline-restaurant"></i></div>
				</div>
				<div class="fun-fact" data-fun-fact-color="#b81b7f">
					<div class="fun-fact-text">
						<span>Meals Searched</span>
						{{--<h4>{{ $renderedArray->searched->search_count }}</h4>--}}
					</div>
					<div class="fun-fact-icon"><i class="icon-material-outline-search"></i></div>
				</div>
				<div class="fun-fact" data-fun-fact-color="#efa80f">
					<div class="fun-fact-text">
						<span>Meals Reviewed</span>
						<h4>{{ $renderedArray->meals_reviewed }}</h4>
					</div>
					<div class="fun-fact-icon"><i class="icon-material-outline-rate-review"></i></div>
				</div>
				<!-- Last one has to be hidden below 1600px, sorry :( -->
				<div class="fun-fact" data-fun-fact-color="#2a41e6">
					<div class="fun-fact-text">
						<span>Total Chefs</span>
						<h4>{{ $renderedArray->total_chefs }}</h4>
					</div>
					<div class="fun-fact-icon"><i class="icon-feather-trending-up"></i></div>
				</div>				
			</div>
			
			<!-- Row -->
			<div class="row">

				<div class="col-xl-12">
					<!-- Dashboard Box -->
					<div class="dashboard-box main-box-in-row">
						<div class="headline">
							<h3><i class="icon-feather-bar-chart-2"></i> Delivery / Take Away Analysis</h3>
							<div class="sort-by">
								<select class="selectpicker hide-tick">
									<option>Last 6 Months</option>
									<option>This Year</option>
									<option>This Month</option>
								</select>
							</div>
						</div>
						<div class="content">
							<!-- Chart -->
							<div class="chart">
								<canvas id="chart" width="100" height="45"></canvas>
							</div>
						</div>
					</div>
					<!-- Dashboard Box / End -->
				</div>
				
				
			</div>
			<!-- Row / End -->
			{{--@include('general_boxes.top-meals')--}}
			<!-- Row -->
			<div class="row">

				<!-- Dashboard Box -->
				<div class="col-xl-6">
					<div class="dashboard-box">						
						<table class="basic-table">
							<tr>
								<th>Quickest Meals</th>
								<th>Time</th>
								<th>Chef</th>								
							</tr>
							@foreach($renderedArray->quick_meal as $quick)
								<tr>
									<td data-label="Column 1"><a href="{{ URL::to(''.$quick->itemID) }}">{{ $quick->item_name }}</a></td>
									<td data-label="Column 2">{{ $quick->item_time }} min</td>
									<td data-label="Column 3"><a href="{{ URL::to(''.$quick->chefID) }}">{{ $quick->full_name }}</a></td>
								</tr>
							@endforeach

						</table>						
					</div>
				</div>
				
				<div class="col-xl-6">
					<div class="dashboard-box">
						<table class="basic-table">
							<tr>
								<th>Cheapest Meals</th>
								<th>Price</th>
								<th>Chef</th>								
							</tr>

							@foreach($renderedArray->cheap_meal as $cheap)
								<tr>
									<td data-label="Column 1"><a href="#">{{ $cheap->item_name }}</a></td>
									<td data-label="Column 2">Rs. {{ $cheap->price }}</td>
									<td data-label="Column 3"><a href="#">{{ $cheap->full_name }}</a></td>
								</tr>
							@endforeach

						</table>
					</div>
				</div>				

			</div>
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->



@endsection