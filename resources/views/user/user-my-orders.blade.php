@extends('layouts.user_dashboard_layout')
@section('content')

	<?php $xid = 99; ?>

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>My Orders</h3>
				<span class="margin-top-7">Do leave feedback for your orders. Thanks!</span>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Orders</a></li>
						<li>My Orders</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-feather-shopping-bag"></i>  {{ count($orders->data) }} Orders</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								@foreach($orders->data as $key=>$order_item)
								<li>
									<!-- Overview -->
									<div class="foodpole-user-overview manage-candidates">
										<div class="foodpole-user-overview-inner">

											<!-- Avatar -->
											<div class="foodpole-user-avatar">
												<a href="#"><img src={{URL::asset('images/food-placeholder.jpg') }} alt=""></a>
											</div>

											<!-- Name -->
											<div class="foodpole-user-name">

												<h4><a href="#">Order# {{ $order_item->id }}</a>
													@if($order_item->flag == true)
														<span class="dashboard-status-button red">Flagged</span>
													@else
														@if($order_item->order_status == config('constants.DEFAULT_ORDER_DELIVERED'))
															<span class="dashboard-status-button green">{{ $order_item->order_status }}</span>
															@if($order_item->payment != null && $order_item->payment->payment_status == true)
																<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
															@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
																<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															@endif
														@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_PENDING'))
															<span class="dashboard-status-button yellow">{{ $order_item->order_status }}</span>
															@if($order_item->payment != null && $order_item->payment->payment_status == true)
																<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
															@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
																<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															@endif
														@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_DECLINED'))
															<span class="dashboard-status-button red">{{ $order_item->order_status }}</span>
															@if($order_item->payment != null && $order_item->payment->payment_status == true)
																<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
															@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
																<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															@endif
														@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_CANCELLED'))
															<span class="dashboard-status-button red">{{ $order_item->order_status }}</span>
															@if($order_item->payment != null && $order_item->payment->payment_status == true)
																<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
															@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
																<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															@endif
														@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_READY'))
															<span class="dashboard-status-button green">{{ $order_item->order_status }}</span>
															@if($order_item->payment != null && $order_item->payment->payment_status == true)
																<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
															@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
																<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															@endif
														@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_COMPLETED'))
															<span class="dashboard-status-button green">{{ $order_item->order_status }}</span>
															@if($order_item->payment != null && $order_item->payment->payment_status == true)
																<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
															@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
																<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															@endif
														@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_PROCESSING'))
															<span class="dashboard-status-button green">{{ $order_item->order_status }}</span>
															@if($order_item->payment != null && $order_item->payment->payment_status == true)
																<span class="dashboard-status-button green">{{ config('constants.DEFAULT_PAYMENT_PAID') }}</span>
															@elseif($order_item->payment != null && $order_item->payment->payment_status == false)
																<span class="dashboard-status-button yellow">{{ config('constants.DEFAULT_PAYMENT_UNPAID') }}</span>
															@endif
														@endif
													@endif</h4>

												<!-- Details -->
												<span class="foodpole-user-detail-item"><strong>Subtotal:</strong> Rs. {{ $order_item->sub_total }}</span>
												{{--<span class="foodpole-user-detail-item"><strong>GST (17%):</strong> Rs. 210</span>--}}
												<span class="foodpole-user-detail-item"><strong>Delivery:</strong> Rs. {{ $order_item->delivery_fare }}</span>
												<span class="foodpole-user-detail-item"><strong>Grand Total:</strong> Rs. {{ $order_item->grand_total }}</span>
												

												<!-- fpOffer Details -->
												<ul class="dashboard-task-info fpOffer-info">
													@foreach($order_item->order_line_items->data as $item)

														<li><strong>{{ $item->menu_item->item_name }}</strong><span>{{ $item->item_option }} - x{{ $item->quantity }} - Rs.{{ $item->sub_total }}</span>
														@foreach($order_item->ratings as $stars)
															@if($item->menu_item->id == $stars->menu_item_id)
															<div class="star-rating margin-top-5 or-item-rating" data-rating="{{$stars->rating}}"></div>
																@endif
															@endforeach
														</li>
													@endforeach

												</ul>

												<!-- Buttons -->
												{{--@if($order_item->flag == false)--}}
													{{--<div class="freelancer-rating margin-top-25">--}}
														{{--<h4>Your Reason</h4>--}}
														{{--<p>{{$order_item->reason->reason}}</p>--}}
													{{--</div>--}}
												@if($order_item->flag == false)

												@if($order_item->order_status == config('constants.DEFAULT_ORDER_DELIVERED') &&
												($order_item->ratings ==null || $order_item->ratings == [] ) )

												<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">
													<button class="button dark ripple-effect order-blue" onclick="customerReview($(this))"><i class="icon-material-outline-thumb-up"></i> Leave a Review</button>
													<button class="button ripple-effect order-red" onclick="customerFlag($(this))"><i class="icon-material-outline-outlined-flag"></i> Flag</button>
												</div>
													@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_PENDING'))
													<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">
														<button class="button ripple-effect order-red" onclick="customerReason($(this))"><i class="icon-line-awesome-times-circle-o"></i> cancel</button>
														<a href="{{ URL::to('/address-info/'.$order_item->id)}}" target="_blank" class="button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Chef Info</a>
													</div>

												@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_PROCESSING') && ($order_item->payment != null && $order_item->payment->payment_status == true))
													<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">
															{{--<button class="button ripple-effect order-green"><i class="icon-line-awesome-money"></i> Clear Payment</a>--}}
														<a href="{{ URL::to('/address-info/'.$order_item->id)}}" target="_blank" class="button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Chef Info</a>
													</div>
												@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_PROCESSING'))
													<div class="buttons-to-right always-visible margin-top-40 margin-bottom-0">
														<a href="#" target="_blank" class="button ripple-effect order-orange"><i class="icon-material-outline-info"></i> Chef Info</a>
													</div>
												@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_DECLINED'))
													<div class="freelancer-rating margin-top-25">
														<h4>Chef Reason</h4>
														<p>{{$order_item->reason->reason}}</p>
													</div>
												@elseif($order_item->order_status == config('constants.DEFAULT_ORDER_CANCELLED'))
													<div class="freelancer-rating margin-top-25">
														<h4>Your Reason</h4>
														<p>{{$order_item->reason->reason}}</p>
													</div>
											@elseif($order_item->ratings !=null || $order_item->ratings != [])
												<!-- Rating -->
													<div class="foodpole-user-rating margin-top-25">
														<?php
															$sum =0 ;
															$count =1 ;
														foreach ($order_item->ratings as $foo ){
														    $sum += $foo->rating ;
														    $count++ ;

														}
														?>
														<h4>Your Review</h4>
														<div class="star-rating margin-top-10" data-rating="{{round($avg = $sum/$count,1) }}"></div>
														<p> {{ $order_item->ratings[0]->feedback }}</p>
													</div>
												@endif
												@else
													<h4>Your Review</h4>
													{{ $order_item->ratings[0]->feedback }}



													@endif
												@include('general_boxes.customer-reason-area')
												@include('general_boxes.customer-review-box')
												@include('general_boxes.customer-flag')


											</div>
										</div>
									</div>
								</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>

			</div>
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->



@endsection