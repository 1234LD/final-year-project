@extends('layouts.user_dashboard_layout')
@section('content')

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Settings</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{ URL::to('/') }}">Home</a></li>
						<li><a href="#">Account</a></li>
						<li>Settings</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<form method="post" id="profile-form"  action="{{URL::to('user/update-profile/'.$data->id)}}" enctype="multipart/form-data">
				{{ csrf_field()}}
			<div class="row">

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-account-circle"></i> My Account</h3>
						</div>

						<div class="content with-padding padding-bottom-0">

							<div class="row">

								<div class="col-auto avatar-cont">
									<div class="avatar-wrapper" data-tippy-placement="bottom" title="Change Avatar">
										@if($data->image == null)
										<img class="profile-pic" src="images/user-avatar-placeholder.png" alt="" />
										@else
											<img class="profile-pic" src="{{ URL::asset('chefimages/'.$data->image)}}" alt="" />
										@endif

										<div class="upload-button"></div>
										<input class="file-upload" type="file" name="image_name" accept="image/*"/>
											{{--<input type="hidden" name="_token" value="{{csrf_token()}}"><br>--}}
									</div>
								</div>

								<div class="col">
									<div class="row">

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Full Name</h5>
												<input type="text" name="name" class="with-border" value="{{$data->full_name}}">
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Username</h5>
												<input type="text" name="username" class="with-border" value="{{$data->username}}">
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Email</h5>
												<input type="email" name="email" class="with-border" value="{{$data->email}}">
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Phone</h5>
												<input type="number"  name="numb"class="with-border" value="{{$data->phone_number}}">
											</div>
										</div>

									</div>
								</div>
							</div>

						</div>
					</div>
				</div>


				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div id="test1" class="dashboard-box">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-lock"></i> Password & Security</h3>
						</div>

						<div class="content with-padding">
							<div class="row">
								{{--<div class="col-xl-4">--}}
								{{--<div class="submit-field">--}}
								{{--<h5>Current Password</h5>--}}
								{{--<input type="password"  name="pswd"  value=""  class="with-border">--}}
								{{--</div>--}}
								{{--</div>--}}

								<div class="col-xl-6">
									<div class="submit-field">
										<h5>New Password</h5>
										<input type="password" name="pass" class="with-border" value="">
									</div>
								</div>

								<div class="col-xl-6">
									<div class="submit-field">
										<h5>Repeat New Password</h5>
										<input type="password" name="pass" class="with-border " value="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Button -->
				<div class="col-xl-12">
					<button class="button ripple-effect big margin-top-30"  id="profile-form">  Save Changes</button>
				</div>

			</div>
			</form>
											{{--<div class="submit-field">--}}
												{{--<h5>Full Name</h5>--}}
												{{--<input type="text" name="" class="with-border" value="{{$data->full_name}}">--}}
											{{--</div>--}}
										{{--</div>--}}
										{{----}}
										{{--<div class="col-xl-6">--}}
											{{--<div class="submit-field">--}}
												{{--<h5>Username</h5>--}}
												{{--<input type="text" class="with-border" value="{{$data->username}}">--}}
											{{--</div>--}}
										{{--</div>											--}}
										{{----}}
										{{--<div class="col-xl-6">--}}
											{{--<div class="submit-field">--}}
												{{--<h5>Email</h5>--}}
												{{--<input type="email" class="with-border" value="{{$data->email}}">--}}
											{{--</div>--}}
										{{--</div>										--}}

										{{--<div class="col-xl-6">--}}
											{{--<div class="submit-field">--}}
												{{--<h5>Phone <span class="verified-badge vb-small" title="Verified Number" data-tippy-placement="top"></span></h5>--}}
												{{--<input type="number" class="with-border" value="03335583655">--}}
											{{--</div>--}}
										{{--</div>--}}

									{{--</div>--}}
								{{--</div>--}}
							{{--</div>--}}

						{{--</div>--}}
					{{--</div>--}}
				{{--</div>--}}


				{{--<!-- Dashboard Box -->--}}
				{{--<div class="col-xl-12">--}}
					{{--<div id="test1" class="dashboard-box">--}}

						{{--<!-- Headline -->--}}
						{{--<div class="headline">--}}
							{{--<h3><i class="icon-material-outline-lock"></i> Password & Security</h3>--}}
						{{--</div>--}}

						{{--<div class="content with-padding">--}}
							{{--<div class="row">--}}
								{{--<div class="col-xl-4">--}}
									{{--<div class="submit-field">--}}
										{{--<h5>Current Password</h5>--}}
										{{--<input type="password" class="with-border">--}}
									{{--</div>--}}
								{{--</div>--}}

								{{--<div class="col-xl-4">--}}
									{{--<div class="submit-field">--}}
										{{--<h5>New Password</h5>--}}
										{{--<input type="password" class="with-border">--}}
									{{--</div>--}}
								{{--</div>--}}

								{{--<div class="col-xl-4">--}}
									{{--<div class="submit-field">--}}
										{{--<h5>Repeat New Password</h5>--}}
										{{--<input type="password" class="with-border">--}}
									{{--</div>--}}
								{{--</div>--}}

								{{--<div class="col-xl-12">--}}
									{{--<div class="checkbox">--}}
										{{--<input type="checkbox" id="two-step" checked>--}}
										{{--<label for="two-step"><span class="checkbox-icon"></span> Enable Two-Step Verification via Email</label>--}}
									{{--</div>--}}
								{{--</div>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
				{{--</div>--}}
				{{----}}
				{{--<!-- Button -->--}}
				{{--<div class="col-xl-12">--}}
					{{--<a href="#" class="button ripple-effect big margin-top-30">Save Changes</a>--}}
				{{--</div>--}}

			{{--</div>--}}
			<!-- Row / End -->
			<div class="dashboard-footer-spacer"></div>
			@include('includes.footer-dashboard')

		</div>
	</div>
	<!-- Dashboard Content / End -->



@endsection