<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    //
//    public $table = "admin";

    protected $fillable=[
        'users_id',
       'online_status'
    ];

    public function user()
    {
        return $this->belongsTo(users::class ,'users_id');
    }

    public function menu()
    {
        return $this->hasOne(Menu::class);
    }


//    public function menu()
//    {
//        return $this->hasOne(Menu::class);
//    }

    public function menus()
    {
        return $this->hasMany(Menu::class);
    }

}
