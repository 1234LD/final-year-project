<?php

namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \Illuminate\Support\Facades\View::composer('index-logged-in','App\Http\ViewComposer\IndexComposer');
        \Illuminate\Support\Facades\View::composer('chef.chef-panel','App\Http\ViewComposer\Chef_PanelComposer');
        \Illuminate\Support\Facades\View::composer('admin.admin_dashboard','App\Http\ViewComposer\Admin_Composer');
        \Illuminate\Support\Facades\View::composer('user.user-dashboard','App\Http\ViewComposer\User_Composer');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
