<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paymentDetail extends Model
{
    public $table = 'payment_detail';

    protected $fillable = [
        'id','customer_id','chef_id', 'order_id',
        'paid_amount','remaining_amount','payment_status','payment_status'
    ];

    public function order()
    {
        return $this->belongsTo(Orders::class,'orders_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function chef()
    {
        return $this->belongsTo(chef::class);
    }
}
