<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class order_line_itemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'menu_item_id' => $this->menu__item_id,
            'order_id' => $this->order_id,
            'quantity' => $this->quantity,
            'item_price' => $this->item_price,
            'item_option' => $this->item_option,
            'sub_total' => $this->sub_total
        ];
    }
}
