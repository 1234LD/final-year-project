<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class chef_resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       // return parent::toArray($request);

        return [
            'code' => 200,
            'id'=>$this->id,
            'users_id'=>$this->users_id,
            'cnic'=>$this->cnic,
            'chef_rating'=>$this->chef_rating,
            'phoneNumber'=>$this->phoneNumber,
            'city'=>$this->city,
            'streetaddress'=>$this->streetaddress,
            'state'=>$this->state,
            'postalcode'=>$this->postalcode,
            'latitude'=>$this->latitude,
            'longitude'=>$this->longitude,
            'menu_name'=>$this->menu_name,
            'workingHrs'=>$this->workingHrs,
            'chef_level'=>$this->chef_level,
            'description'=>$this->description,
            'delivery_type'=>$this->delivery_type,
            'online_status'=>$this->online_status,
            'picture'=>$this->picture,
            'verify' => $this->verify,
            'start_time'=>$this->start_time,
            'end_time'=>$this->end_time,

            'user' => new loginResource($this->user)
        ];
    }
}
