<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class loginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       // return parent::toArray($request);
        return [
            'id'=>$this->id,
            'full_name' => $this->full_name,
            'username'=> $this->username,
            'usertype'=> $this->usertype,
            'email'=> $this->email,
            'image' => $this->image,
            'account_status' => $this->account_status,
            'phone_number'=>$this->phone_number,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'code' => 200
        ];
    }

//    public function with($request)
//    {
//        return[
//            'version'=>'127.0.0.1',
//            'author_url'=>url('www.google.com')
//        ];
//    }
}
