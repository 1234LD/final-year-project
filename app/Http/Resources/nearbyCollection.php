<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class nearbyCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data'=>$this->collection->transform(function($chef){

                return [
                    'id'=>$chef->id,
                    'users_id'=>$chef->users_id,
                    'cnic'=>$chef->cnic,
                    'phoneNumber'=>$chef->phoneNumber,
                    'city'=>$chef->city,
                    'streetaddress'=>$chef->streetaddress,
                    'state'=>$chef->state,
                    'postalcode'=>$chef->postalcode,
                    'latitude'=>$chef->latitude,
                    'longitude'=>$chef->longitude,
                    'menu_name'=>$chef->menu_name,
                    'workingHrs'=>$chef->workingHrs,
                    'chef_level'=>$chef->chef_level,
                    'description'=>$chef->description,
                    'delivery_type'=>$chef->delivery_type,
                    'online_status'=>$chef->online_status,
                    'picture'=>$chef->picture,
                    'start_time'=>$chef->start_time,
                    'end_time'=>$chef->end_time,


                    'user' => new loginResource($chef->user)

                ];
            })
        ];
    }
}
