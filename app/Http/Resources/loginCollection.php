<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class loginCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
        [
            'code' => 200,
            'data' => $this->collection->transform(function($user) {

                if($user->usertype == config('constants.DEFAULT_CHEF_TYPE') || $user->usertype == config('constants.DEFAULT_QUICK-CHEF_TYPE') )
                {
                    return [
                        'u_id'=>$user->id,
                        'full_name' => $user->full_name,
                        'username'=> $user->username,
                        'usertype'=> $user->usertype,
                        'email'=> $user->email,
                        'phone_number' => $user->phone_number,
                        'lat' => $user->lat,
                        'lng' => $user->lng,
                        'image' => $user->image,

                        'chef' => new chef_resource($user->chef)
                    ];
                }
                else if($user->usertype == config('constants.DEFAULT_ADMIN_TYPE') || $user->usertype == config('constants.DEFAULT_SUPER-ADMIN_TYPE'))
                {
                    return [
                        'u_id'=>$user->id,
                        'full_name' => $user->full_name,
                        'username'=> $user->username,
                        'usertype'=> $user->usertype,
                        'email'=> $user->email,
                        'phone_number' => $user->phone_number,
                        'lat' => $user->lat,
                        'lng' => $user->lng,
                        'image' => $user->image,

                        'admin' => new adminResource($user->admin)
                    ];
                }
                else if($user->usertype == config('constants.DEFAULT_CUSTOMER_TYPE'))
                {
                    return [
                        'u_id'=>$user->id,
                        'full_name' => $user->full_name,
                        'username'=> $user->username,
                        'usertype'=> $user->usertype,
                        'email'=> $user->email,
                        'phone_number' => $user->phone_number,
                        'lat' => $user->lat,
                        'lng' => $user->lng,
                        'image' => $user->image,

                        'customer' => new customerResource($user->customer)
                    ];
                }
                else {
                    return [
                        'u_id' => $user->id,
                        'full_name' => $user->full_name,
                        'username' => $user->username,
                        'usertype' => $user->usertype,
                        'email' => $user->email,
                        'phone_number' => $user->phone_number,
                        'lat' => $user->lat,
                        'lng' => $user->lng,
                        'image' => $user->image,
                    ];
                }
            })
        ];
    }
}
