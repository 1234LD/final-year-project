<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class payment_Collection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function($payment){

                return[
                    'id' => $payment->id,
                    'customer_id' => $payment->customer_id,
                    'chef_id' => $payment->chef_id,
                    'orders_id' => $payment->orders_id,
                    'paid_amount' => $payment->paid_amount,
                    'remaining_amount' => $payment->remaining_amount,
                    'payment_status' => $payment->payment_status,

                    'order' => new order_Resource($payment->order),
                ];

            })
        ];
    }
}
