<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class chef_profileCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       // chef_profileCollection::withoutWrapping();
        return [
//            'code' => 200,
            'data'=>$this->collection->transform(function($chef){
//                if($chef->ratings != null) {
                    return [
                        'id' => $chef->id,
                        'users_id' => $chef->users_id,
                        'chef_rating' => $chef->chef_rating,
                        'cnic' => $chef->cnic,
                        'phoneNumber' => $chef->phoneNumber,
                        'city' => $chef->city,
                        'streetaddress' => $chef->streetaddress,
                        'state' => $chef->state,
                        'postalcode' => $chef->postalcode,
                        'latitude' => $chef->latitude,
                        'longitude' => $chef->longitude,
                        'menu_name' => $chef->menu_name,
                        'workingHrs' => $chef->workingHrs,
                        'chef_level' => $chef->chef_level,
                        'description' => $chef->description,
                        'delivery_type' => $chef->delivery_type,
                        'online_status' => $chef->online_status,
                        'picture' => $chef->picture,
                        'verify' => $chef->verify,
                        'start_time' => $chef->start_time,
                        'end_time' => $chef->end_time,

                        'user' => new loginResource($chef->user),
                        'menu_items' => new menu_itemCollection($chef->menu_Items),
                        'ratings' => new rating_Collection($chef->ratings)
                    ];
//                }
//                else
//                {
//                    return [
//                        'id' => $chef->id,
//                        'users_id' => $chef->users_id,
//                        'chef_rating' => $chef->chef_rating,
//                        'cnic' => $chef->cnic,
//                        'phoneNumber' => $chef->phoneNumber,
//                        'city' => $chef->city,
//                        'streetaddress' => $chef->streetaddress,
//                        'state' => $chef->state,
//                        'postalcode' => $chef->postalcode,
//                        'latitude' => $chef->latitude,
//                        'longitude' => $chef->longitude,
//                        'menu_name' => $chef->menu_name,
//                        'workingHrs' => $chef->workingHrs,
//                        'chef_level' => $chef->chef_level,
//                        'description' => $chef->description,
//                        'delivery_type' => $chef->delivery_type,
//                        'online_status' => $chef->online_status,
//                        'picture' => $chef->picture,
//                        'verify' => $chef->verify,
//                        'start_time' => $chef->start_time,
//                        'end_time' => $chef->end_time,
//
//                        'user' => new loginResource($chef->user),
//                        'menu' => new menuCollection($chef->menus)
////                        'ratings' => new rating_reviews_Resource($chef->ratings)
//                    ];
//                }
            })
        ];
    }
}
