<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class order_line_itemCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function($order_line_item){

                return [
                    'id' => $order_line_item->id,
                    'menu_item_id' => $order_line_item->menu__item_id,
                    'order_id' => $order_line_item->order_id,
                    'quantity' => $order_line_item->quantity,
                    'item_price' => $order_line_item->item_price,
                    'item_option' => $order_line_item->item_option,
                    'sub_total' => $order_line_item->sub_total,

                    'menu_item' => new menu_itemResource($order_line_item->menu_item)
                ];

            })
        ];
    }
}
