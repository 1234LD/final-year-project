<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class item_imageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $exploded_image = explode(config('constants.DEFAULT_GUM'),$this->item_image);
        return [
            'id' => $this->id,
            'menu_item_id' => $this->menu__item_id,
            'item_image' => $exploded_image
        ];
    }
}
