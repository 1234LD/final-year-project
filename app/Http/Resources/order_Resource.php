<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class order_Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
//            'code'=>200,
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'chef_id' => $this->chef_id,
            'sub_total' => $this->sub_total,
            'delivery_type' => $this->delivery_type,
            'order_status' => $this->order_status,
            'delivery_fare' => $this->delivery_fare,
            'grand_total' => $this->grand_total,
            'flag' => $this->flag,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'customer' => new customerResource($this->customer),
            'chef' => new chef_resource($this->chef),
            'order_line_items' => new order_line_itemCollection($this->order_line_items),
        ];
    }
}
