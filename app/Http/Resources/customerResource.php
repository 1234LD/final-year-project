<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class customerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'users_id' => $this->users_id,
            'online_status' => $this->online_status,
            'latitude' => $this->latitude,
            'longitude'=> $this->longitude,

            'user' => new loginResource($this->user)
        ];
    }
}
