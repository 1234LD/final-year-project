<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class item_recpieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $exploded_name = explode(config('constants.DEFAULT_GUM'),$this->ingredient_name);
        $exploded_qty = explode(config('constants.DEFAULT_GUM'),$this->ingredient_qty);

        return [
            'id' =>$this->id,
            'menu_item_id' =>$this->menu__item_id,
            'description' =>$this->description,
            'ingredient_name' =>$exploded_name,
            'ingredient_qty' =>$exploded_qty
        ];
    }
}
