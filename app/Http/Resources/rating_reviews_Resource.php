<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class rating_reviews_Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      //  return parent::toArray($request);
        date_default_timezone_set('Asia/Karachi');

//        if($request->all() != null) {
            return [
                'id' => $this->id,
                'customer_id' => $this->customer_id,
                'order_id' => $this->orders_id,
                'chef_id' => $this->chef_id,
                'menu_item_id' => $this->menu__item_id,
                'headline' => $this->headline,
                'feedback' => $this->feedback,
                'rating' => $this->rating,
                'status' => $this->status,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,

//            'menu_items' => menu_itemResource::collection($this->menu_items),
            ];
//        }
//        else
//        {
//            return null;
//        }
    }
}
