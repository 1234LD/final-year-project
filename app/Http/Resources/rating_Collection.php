<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class rating_Collection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'data' => $this->collection->transform(function($rating){

                if($rating->order == null)
                {
                    return [
                        'id' => $rating->id,
                        'customer_id' => $rating->customer_id,
                        'orders_id' => $rating->orders_id,
                        'chef_id' => $rating->chef_id,
                        'menu_item_id' => $rating->menu__item_id,
                        'headline' => $rating->headline,
                        'feedback' => $rating->feedback,
                        'rating' => $rating->rating,
                        'status' => $rating->status,
                        'created_at' => $rating->created_at,
                        'updated_at' => $rating->updated_at,

                        'customer' => new customerResource($rating->customer),
                        'order' => null,
                    ];
                }
                else
                {
                    return [
                        'id' => $rating->id,
                        'customer_id' => $rating->customer_id,
                        'orders_id' => $rating->orders_id,
                        'chef_id' => $rating->chef_id,
                        'menu_item_id' => $rating->menu__item_id,
                        'headline' => $rating->headline,
                        'feedback' => $rating->feedback,
                        'rating' => $rating->rating,
                        'status' => $rating->status,
                        'created_at' => $rating->created_at,
                        'updated_at' => $rating->updated_at,

                        'customer' => new customerResource($rating->customer),
                        'order' => new order_Resource($rating->order),
                    ];
                }
            })
        ];
    }
}
