<?php

namespace App\Http\Resources;

use App\Item_Recipes;
use Illuminate\Http\Resources\Json\ResourceCollection;

class menu_itemCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /// we'll  be doing this with to array function later; rather than transforming the collection ....

        return [
            'code' => 200,
            'data'=>$this->collection->transform(function($menu_item){

                $exploded_price = explode(config('constants.DEFAULT_GUM'),$menu_item->item_price);
                $exploded_option = explode(config('constants.DEFAULT_GUM'),$menu_item->item_option);

//                if($menu_item->menu->category_name == config('constants.DEFAULT_QUICK-MEAL_CATEGORY'))
//                {
//                    return [
//                        'id'=> $menu_item->id,
//                        'item_name' => $menu_item->item_name,
//                        'price' => $menu_item->price,
//                        'item_price' => $exploded_price,
//                        'item_option' => $exploded_option,
//                        'item_time' => $menu_item->item_time,
//                        'rating' => $menu_item->rating,
//                        'item_quantity' => $menu_item->item_quantity,
//                        'meal_status' => $menu_item->meal_status,
//                        'verified' => $menu_item->verified,
//                        'created_at' => $menu_item->created_at,
//                        'updated_at' => $menu_item->updated_at,
//
//                        'chef'=> new chef_resource($menu_item->chef),
//                        'menu' => new menuResource($menu_item->menu),
//                        'ratings' => null,
//                        'item_image' => null,
//                        'item_recipe' => null
//                        ];
//                }

                if($menu_item->menu == null)
                {
                    return null;
                }

                if($menu_item->item_recipe == null && $menu_item->item_image == null)
                {
                    return [
                        'id'=> $menu_item->id,
                        'item_name' => $menu_item->item_name,
                        'price' => $menu_item->price,
                        'item_price' => $exploded_price,
                        'item_option' => $exploded_option,
                        'item_time' => $menu_item->item_time,
                        'rating' => $menu_item->rating,
                        'item_quantity' => $menu_item->item_quantity,
                        'meal_status' => $menu_item->meal_status,
                        'verified' => $menu_item->verified,
                        'created_at' => $menu_item->created_at,
                        'updated_at' => $menu_item->updated_at,

                        'chef'=> new chef_resource($menu_item->chef),
                        'menu' => new menuResource($menu_item->menu),
                        'ratings' => new rating_Collection($menu_item->ratings),
                        'item_image' => null,
                        'item_recipe' => null
                        ];
                }
                else if ($menu_item->item_recipe == null)
                {
                    return [
                        'id'=> $menu_item->id,
                        'item_name' => $menu_item->item_name,
                        'price' => $menu_item->price,
                        'item_price' => $exploded_price,
                        'item_option' => $exploded_option,
                        'item_time' => $menu_item->item_time,
                        'rating' => $menu_item->rating,
                        'item_quantity' => $menu_item->item_quantity,
                        'meal_status' => $menu_item->meal_status,
                        'verified' => $menu_item->verified,
                        'created_at' => $menu_item->created_at,
                        'updated_at' => $menu_item->updated_at,

                        'chef'=> new chef_resource($menu_item->chef),
                        'menu' => new menuResource($menu_item->menu),
                        'item_image' => new item_imageResource($menu_item->item_image),
                        'ratings' => new rating_Collection($menu_item->ratings),
                        'item_recipe' => null
                    ];
                }
                else if ($menu_item->item_image == null) {
                    return [
                        'id'=> $menu_item->id,
                        'item_name' => $menu_item->item_name,
                        'price' => $menu_item->price,
                        'item_price' => $exploded_price,
                        'item_option' => $exploded_option,
                        'item_time' => $menu_item->item_time,
                        'rating' => $menu_item->rating,
                        'item_quantity' => $menu_item->item_quantity,
                        'meal_status' => $menu_item->meal_status,
                        'verified' => $menu_item->verified,
                        'created_at' => $menu_item->created_at,
                        'updated_at' => $menu_item->updated_at,

                        'chef'=> new chef_resource($menu_item->chef),
                        'menu' => new menuResource($menu_item->menu),
                        'ratings' => new rating_Collection($menu_item->ratings),
                        'item_image' => null,
                        'item_recipe' => new item_recpieResource($menu_item->item_recipe)
                    ];
                }
                else
                {
                    return [
                        'id'=> $menu_item->id,
                        'item_name' => $menu_item->item_name,
                        'price' => $menu_item->price,
                        'item_price' => $exploded_price,
                        'item_option' => $exploded_option,
                        'item_time' => $menu_item->item_time,
                        'rating' => $menu_item->rating,
                        'item_quantity' => $menu_item->item_quantity,
                        'meal_status' => $menu_item->meal_status,
                        'verified' => $menu_item->verified,
                        'created_at' => $menu_item->created_at,
                        'updated_at' => $menu_item->updated_at,

                        'ratings' => new rating_Collection($menu_item->ratings),
                        'chef'=> new chef_resource($menu_item->chef),
                        'menu' => new menuResource($menu_item->menu),
//                        'ratings' => new rating_Collection($menu_item->ratings),
                        'item_image' => new item_imageResource($menu_item->item_image),
                        'item_recipe' => new item_recpieResource($menu_item->item_recipe)
                    ];
                }
            })
        ];
    }
}
