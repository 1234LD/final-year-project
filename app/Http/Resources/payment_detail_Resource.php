<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class payment_detail_Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'chef_id' => $this->chef_id,
            'orders_id' => $this->orders_id,
            'paid_amount' => $this->paid_amount,
            'remaining_amount' => $this->remaining_amount,
            'payment_status' => $this->payment_status
        ];
    }
}
