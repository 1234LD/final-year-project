<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class order_rating_Collection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        date_default_timezone_set('Asia/Karachi');

        return [
            'data'=>$this->collection->transform(function($order){

                    return [
                        'id' => $order->id,
                        'customer_id' => $order->customer_id,
                        'chef_id' => $order->chef_id,
                        'sub_total' => $order->sub_total,
                        'delivery_type' => $order->delivery_type,
                        'order_status' => $order->order_status,
                        'delivery_fare' => $order->delivery_fare,
                        'grand_total' => $order->grand_total,
                        'flag' => $order->flag,
                        'created_at' => $order->created_at,
                        'updated_at' => $order->updated_at,

                        'customer' => new customerResource($order->customer),
                        'chef' => new chef_resource($order->chef),
                        'order_line_items' => new order_line_itemCollection($order->order_line_items),
                    ];
                )}
                ];
    }
}
