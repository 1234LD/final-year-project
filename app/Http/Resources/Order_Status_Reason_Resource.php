<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Order_Status_Reason_Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'chef_id' => $this->chef_id,
            'orders_id' => $this->orders_id,
            'reason' => $this->reason,
            'order_status' => $this->order_status
        ];
    }
}
