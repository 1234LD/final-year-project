<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\ResourceCollection;

class orderCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'code' => 200,
            'data'=>$this->collection->transform(function($order){

                date_default_timezone_set('Asia/Karachi');


                if($order->reason == null && $order->ratings != null)
                {
                    return [
                        'id' => $order->id,
                        'customer_id' => $order->customer_id,
                        'chef_id' => $order->chef_id,
                        'sub_total' => $order->sub_total,
                        'delivery_type' => $order->delivery_type,
                        'order_status' => $order->order_status,
                        'delivery_fare' => $order->delivery_fare,
                        'grand_total' => $order->grand_total,
                        'flag' => $order->flag,
                        'created_at' => $order->created_at,
                        'updated_at' => $order->updated_at,

                        'customer' => new customerResource($order->customer),
                        'chef' => new chef_resource($order->chef),
                        'order_line_items' => new order_line_itemCollection($order->order_line_items),
                        'reason' => null,
                        'payment' => null,
                        'ratings' => rating_reviews_Resource::collection($order->ratings),
                    ];
                }

//                if($order->payment == null) {
//                    return [
//                        'id' => $order->id,
//                        'customer_id' => $order->customer_id,
//                        'chef_id' => $order->chef_id,
//                        'sub_total' => $order->sub_total,
//                        'delivery_type' => $order->delivery_type,
//                        'order_status' => $order->order_status,
//                        'delivery_fare' => $order->delivery_fare,
//                        'grand_total' => $order->grand_total,
//                        'flag' => $order->flag,
//                        'created_at' => $order->created_at,
//                        'updated_at' => $order->updated_at,
//
//                        'customer' => new customerResource($order->customer),
//                        'chef' => new chef_resource($order->chef),
//                        'order_line_items' => new order_line_itemCollection($order->order_line_items),
//                        'reason' => null,
//                        'payment' => null,
//                    ];
//                }
                else if($order->reason != null && $order->ratings == null)
                {
                    return [
                        'id' => $order->id,
                        'customer_id' => $order->customer_id,
                        'chef_id' => $order->chef_id,
                        'sub_total' => $order->sub_total,
                        'delivery_type' => $order->delivery_type,
                        'order_status' => $order->order_status,
                        'delivery_fare' => $order->delivery_fare,
                        'grand_total' => $order->grand_total,
                        'flag' => $order->flag,
                        'created_at' => $order->created_at,
                        'updated_at' => $order->updated_at,

                        'customer' => new customerResource($order->customer),
                        'chef' => new chef_resource($order->chef),
                        'order_line_items' => new order_line_itemCollection($order->order_line_items),
                        'payment' => null,
                        'ratings' => null,
                        'reason' => new Order_Status_Reason_Resource($order->reason),
                    ];
                }
                else if($order->ratings ==null && $order->reason ==null)
                {
                    return [
                        'id' => $order->id,
                        'customer_id' => $order->customer_id,
                        'chef_id' => $order->chef_id,
                        'sub_total' => $order->sub_total,
                        'delivery_type' => $order->delivery_type,
                        'order_status' => $order->order_status,
                        'delivery_fare' => $order->delivery_fare,
                        'grand_total' => $order->grand_total,
                        'flag' => $order->flag,
                        'created_at' => $order->created_at,
                        'updated_at' => $order->updated_at,

                        'customer' => new customerResource($order->customer),
                        'chef' => new chef_resource($order->chef),
                        'order_line_items' => new order_line_itemCollection($order->order_line_items),

                        'payment' => null,
                        'ratings' => null,
                        'reason' => null,
                    ];
                }
                else
                {
                    return [
                        'id' => $order->id,
                        'customer_id' => $order->customer_id,
                        'chef_id' => $order->chef_id,
                        'sub_total' => $order->sub_total,
                        'delivery_type' => $order->delivery_type,
                        'order_status' => $order->order_status,
                        'delivery_fare' => $order->delivery_fare,
                        'grand_total' => $order->grand_total,
                        'flag' => $order->flag,
                        'created_at' => $order->created_at,
                        'updated_at' => $order->updated_at,

                        'customer' => new customerResource($order->customer),
                        'chef' => new chef_resource($order->chef),
                        'order_line_items' => new order_line_itemCollection($order->order_line_items),

                        'payment' => null,
                        'ratings' => rating_reviews_Resource::collection($order->ratings),
                        'reason' => new Order_Status_Reason_Resource($order->reason),

                      //  'payment' => new payment_detail_Resource($order->payment),


                    ];
                }
            })
        ];
    }
}
