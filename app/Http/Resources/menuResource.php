<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class menuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request)

        return [
            'id' => $this->id,
            'chef_id' => $this->chef_id,
            'admin_id' => $this->admin_id,
            'category_name' => $this->category_name,
         //   'menu_item'=> new menu_itemResource($this->whenLoaded('menu_item'))
        ];
    }
}

