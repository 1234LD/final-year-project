<?php

namespace App\Http\Resources;

use App\Item_Images;
use Illuminate\Http\Resources\Json\JsonResource;

class menu_itemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $exploded_price = explode(config('constants.DEFAULT_GUM'),$this->item_price);
        $exploded_option = explode(config('constants.DEFAULT_GUM'),$this->item_option);

            return [
                'id' =>$this->id,
                'menu_id'=>$this->menu_id,
                'item_name' =>$this->item_name,
                'price' => $this->price,
                'item_price' =>$exploded_price,
                'item_option' =>$exploded_option,
                'item_time' =>$this->item_time,
                'rating' => $this->rating,
                'item_quantity' => $this->item_quantity,
                'meal_status' => $this->meal_status,
                'verified' => $this->verified,
            ];
    }
}
