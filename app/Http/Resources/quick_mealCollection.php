<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class quick_mealCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function($menu){

                return [
                    'id' => $menu->id,
                    'category_name' => $menu->category_name,
                    'menu_type' => $menu->menu_type,

                    'menu_item' => new menu_itemCollection($menu->menu_item)
                ];

            })
        ];
    }
}
