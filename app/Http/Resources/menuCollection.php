<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class menuCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($req)
    {
   // we need to add the condition to check the admin or chef resource for line 27 ....

        return [
            'data' => $this->collection->transform(function($menu){

                if($menu->admin_id)
                {
                    return [
                        'id' => $menu->id,
                        'admin_id' => $menu->admin_id,
                        'menu_name' => $menu->menu_name,
                        'category_name' => $menu->category_name,

                        'admin'=> new adminResource($menu->admin),
                        'menu_item'=> new menu_itemCollection($menu->menu_Items)
                    ];
                }
                else if($menu->chef_id)
                {
                    return [
                        'id' => $menu->id,
                        'chef_id' => $menu->chef_id,
                        'menu_name' => $menu->menu_name,
                        'category_name' => $menu->category_name,

                        'chef'=> new chef_resource($menu->chef),
                        'menu_item'=> new menu_itemCollection($menu->menu_Items)
                    ];
                }
            })
        ];
    }
}
