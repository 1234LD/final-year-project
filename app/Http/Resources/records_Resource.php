<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class records_Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'search_count' => $this->search_count
        ];
    }
}
