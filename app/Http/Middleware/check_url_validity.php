<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Carbon;

class check_url_validity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $decTime = Crypt::decrypt($request->route()->parameter('time'));

        date_default_timezone_set("Asia/Karachi");
//      $old = Carbon::now()->addDays(2);
        $current_time = Carbon::now();

        $diff_time = $current_time->diffInMinutes($decTime->toDateTimeString());

//        echo $current_time->toDateTimeString().'-----'.$diff_time.'-----'.$decTime->toDateTimeString().'</br>';

        if($diff_time > 60)
        {
            return Redirect::to('/forgot_password/request')->with('status','Timeout! Lets try again!');
        }
        return $next($request);
    }
}
