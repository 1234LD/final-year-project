<?php

namespace App\Http\Controllers;

use App\users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;
use GuzzleHttp\Client;

class web_admin_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_admin(Request $request)
    {
        $admin_info = [
            'password' => $request->input('password'),
            'name' => $request->input('user_name'),
            'email' => $request->input('email'),
            'fb_id' => $request->input('fb_id'),
            'gmail' => $request->input('gmail'),
            'twitter' => $request->input('twitteradm'),
            'status' => $request->input('status')
        ];

        $req = Request::create('api/admin/create','POST',$admin_info);

        $response = app()->handle($req)->getContent();
        $result = json_decode($response);

        if(is_null($result))
        {
            return app()->abort(404);
        }
        else
        {
            // returning some response on some view ...
        }

    }


    /**
     * to update the status of the admin ...
     */

    public function update(Request $request)
    {
        $data = [
            'user_id' => Session::getId(),
            'status' => $request->input('status')
        ];

        $req = Request::create('api/admin','PUT', $data);
        $response  = app()->handle($req)->getContent();

        $output = json_decode($response);

        if(is_null($output))
        {
            return app()->abort(404);
        }
        else
        {
            // returning some thing here on view  ....
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_admin($id)
    {
        $user =users::find($id) ;


            if($user->delete())
            {
                return Redirect::back();
            }
            else {

                return "user does not exist" ;
            }
        }




//        $request = Request::create('api/admin/'.$id,'DELETE');
//
//        $response = app()->handle($request)->getContent();
//
//        $result = json_decode($response);
//
//        if(is_null($result))
//        {
//            return app()->abort(404);
//        }
//        else
//        {
//            // will be returning something on view ...
//        }




    /**
     * Method to Block the customer or chef ...
     */

    public function block_user($id)
    {
    //    dd("a gya a gya");
        $user = users::find($id);
        if ($user->account_status == 'false') {
            return \response('This user is already blocked', '500');
        } else {
            $user->account_status = false;
            if ($user->save()) {


            }

        }
      return Redirect::back();
    }
//        $data = [
//            'block_id' => $request->input('id'),
//            'admin_id' => Session::get('user_id')
//        ];
////
//        $req = Request::create('api/block_request','POST',$data);
//
//        $response = app()->handle($req)->getContent();
//
//        $output = json_decode($response);
//
//        if(is_null($output))
//        {
//            return $response;
//        }
//        else
//        {
//            return "User has been Blocked ... ";
//        }


    /**
     * Method to unBlock the customer or chef ...
     */

    public function unblock_user($id)
   {

       $user = users::find($id);

           $user->account_status = true;
           if($user->save())
           {
               return Redirect::back();
           }
       }
//        $data = [
//            'unblock_id' => $request->input('id'),
//            'admin_id' => Session::get('user_id')
//        ];
//
//        $req = Request::create('api/unblock_request','POST',$data);
//
//        $response = app()->handle($req)->getContent();
//
//        $output = json_decode($response);
//
//        if(is_null($output))
//        {
//            return $response;
//        }
//        else
//        {
//            return "User has been unBlocked ... ";
//        }


    public function get_all_admin_details(){

        $url = 'http://localhost/foodpoleAPI/public/api/view_all_admins' ;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

          // dd($response[0]->user->full_name);
       // dd($request->Count());
       $numb = (count($response));
       return view('admin.admin-all-admins')->with('alladmin',$response);
    }

}

