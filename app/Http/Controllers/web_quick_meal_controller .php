<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use Illuminate\Support\Collection;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;


class web_quick_meal_controller extends Controller
{
    /**
     * Method to send request to API to create quick meal ...
     */
    public function create_quick_meal(Request $request,$id)
    {
//        $data = $request->all();

//        dd($request->all());

        $newData = [
            'user_id' => Session::get('user_id'),
            'user_type' => Session::get('user_type'),
            'username' => Session::get('username'),
            'phone_number' => $request->input('phone_number'),
            'item_name' => $request->input('item_name'),
            'price' => $request->input('price'),
            'quantity' => $request->input('quantity'),
            'description' => $request->input('description'),
            'item_image' => $request->file('item_image'),
            'switch' => $request->input('switch')
        ];

//        dd($data);

        $req = Request::create('api/store/quick_meal','POST',$newData);

        $response = app()->handle($req)->getContent();

        $output = json_decode($response);

        if(is_null($output))
        {
            return $response;
        }
        else
        {
            //  redirecting to view ...
            return Redirect::back()->with('response',$response);
        }
    }


    /**
     * Method to update the quick meal price ...
     */
    public function update_meal_price($username, $id)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/free_quick_meal/'.$username.'/'.$id;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        if(is_null($response))
        {
            return $response;
        }
        else
        {
            return Redirect::back()->with('response',$response);
        }
    }

    /**
     * Method to open the form of quick meal to edit the meal ....
     */
    public function edit_quick_meal($username, $id)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/meal_page/'.$username.'/'.$id;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        if(is_null($response))
        {
            return app()->abort(404);
        }
        else
        {
            return view('chef.chef-edit-quick-meal')->with('meal',$response);
        }
    }


    /**
     * Method to update the quick meal item ...
     */
    public function update_quick_meal($username, $id, Request $request)
    {
        $info = $request->all();

//        dd($request->all());

        $newData = [
            'username' => $username,
            'menu_item_id' => $id,
            'phone_number' => $request->input('phone_number'),
            'item_name' => $request->input('item_name'),
            'price' => $request->input('price'),
            'quantity' => $request->input('quantity'),
            'description' => $request->input('description'),
            'item_image' => $request->file('item_image')
        ];

//        dd($newData);
        $req = Request::create('api/update/quick_meal','PUT',$newData);

        $response = app()->handle($req)->getContent();

        $output = json_decode($response);

        if(is_null($output))
        {
            return $response;
        }
        else
        {
            return Redirect::back()->with('response',$response);
        }

    }

    /**
     * Method to drop the quick meal item ...
     */
    public function drop_quick_meal()
    {
    }


//    public function add_quick_meal(){
//      //  $name=Input::get('name') ;
//      //  $amount = Input::get('amount');
//      //  $price = Input::get('price');
//       $data = [
//        'names'=>$_POST['name'],
//        'amounts'=>$_POST['amount'],
//        'prices'=>$_POST['price'],
//        ];
//
//
//
//        $req =Request::create('api/quick_meals','POST',$data);
//       $response = app()->handle($req)->getContent();
//
//       $output = json_decode($response);
//
//       if(is_null($output)){
//          echo "returning null" ;
//           app()->abort('404');
//
//       }
//        else {
//        echo "QUICK MEAL HAVE BEEN PUBLISHED" ;
//        }
//    }
}
