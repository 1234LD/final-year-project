<?php

namespace App\Http\Controllers;

use App\chef;
use App\Http\Resources\loginResource;
use App\Http\Resources\order_ratingCollection;
use App\Http\Resources\rating_Collection;
use App\Http\Resources\rating_reviews_Resource;
use App\Menu_Item;
use App\Orders;
use App\rating;
use Illuminate\Http\Request;
use App\users;

class api_rating_reviews_controller extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return rating_reviews_Resource
     */
    public function store(Request $request)
    {
      //  dd($request->all()) ;
//dd("am in api controller");
        $item_array = $request->input('rating_array');
       // dd($item_array);


        foreach ($item_array as $key =>$item)
        {
          // dd($item);
            $rate= new rating();
            $rate->customer_id = $request->input("customerid");
            $rate->chef_id = $request->input('chef_id');
//            $rate->order_id = $request->input('order_id');
            $rate->menu__item_id = $key;
            $rate->feedback = $request->input('feedback');
            $rate->rating = $item;
//dd($rate);
            Orders::find($request->input('order_id'))->ratings()->save($rate);
           // $rate->save();
         //   dd($rate);

//
//            }else {
//               return "rating table main data  nai gya "  ;
//
//            }

         //   dd($rate->menu__item_id) ;
            $menuItem = Menu_Item::find($rate->menu__item_id);
//dd($menuItem) ;
            if(!is_null($menuItem))
            {
                $menuItem->rating = $menuItem->getRating();

                $menuItem->save();
              //  dd($menuItem);
            }
        }

        $chef = chef::find($request->input('chef_id'));
//dd($chef) ;
        if(!is_null($chef))
        {
            $chef->chef_rating = $chef->getRating();
            $chef->save();

        }

            $response = [
                'msg' => 'The password you entered is Invalid',
                'code' => 404
            ];
return $response ;
//   $rate->feedback = $request->input('feedback');
//   $rate->rating = $request->input('rating');
//   dd($rate);
//   $rate->save();
//   return new rating_reviews_Resource($rate);

    }

    /**
     * Method to get the all reviews ...
     */
    public function get_all_reviews($id,$username)
    {
        $user = users::where('id','=', $id)
            ->where('username','=', $username)
            ->where('account_status',true)->get();

        if(!is_null($user))
        {
            $chef = chef::where('users_id','=',$id)
                ->first();

            return new rating_Collection(rating::where('chef_id',$chef->id)->get());
        }
        else
        {
            $response = ['msg'=>'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response()->json($response);
        }
    }

    /**
     * Method to get the flagged reviews ...
     */
    public function get_flagged_reviews($id,$username)
    {
        $user = users::where('id','=',$id)
            ->where('username','=',$username)
            ->where('account_status',true)->get();

        if(!is_null($user))
        {
            $chef = chef::where('users_id','=',$id)
                ->first();

//            return response()->json(rating::where('chef_id',$chef->id)
//                            ->with(['order' => function($query)
//                                                {$query->with(['order_line_items' => function($item) {$item->with('menu_item');},
//                                                               'customer' => function($user){$user->with('user');}])
//                            ->where('flag',true);}])->get());

            return new rating_Collection(rating::where('chef_id',$chef->id)->with(['order' =>function($query){$query->where('flag',true);}])->get());
        }
        else
        {
            $response = ['msg'=>'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response($response);
        }
    }

    /**
     * Method to get the non flagged reviews ...
     */
    public function get_nonflagged_reviews($id,$username)
    {
        $user = users::where('id','=',$id)
            ->where('username','=',$username)
            ->where('account_status',true)->get();

        if(!is_null($user))
        {
            $chef = chef::where('users_id','=',$id)
                        ->first();

//            return response()->json(rating::where('chef_id',$chef->id)
//                                        ->with(['order' => function($query)
//                                                    {$query->with(['order_line_items' => function($item) {$item->with('menu_item');},
//                                                                   'customer' => function($user){$user->with('user');}])
//                                        ->where('flag','=',true)->first();}])->get());

            return new rating_Collection(rating::where('chef_id',$chef->id)->with(['order' =>function($query){$query->where('flag',false);}])->get());
        }
        else
        {
            $response = ['msg'=>'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response($response);
        }
    }
    public function get_admin_review_meals(){

       $rating = rating::all();

            return new rating_Collection($rating);


}

public function get_admin_flagged_reviews(){
//dd("dsfs");
    return new rating_Collection(rating::with(['order' =>function($query){$query->where('flag',true);}])->get());
}
public function get_admin_nonflagged_reviews(){

    return new rating_Collection(rating::with(['order' =>function($query){$query->where('flag',false);}])->get());

}
}
