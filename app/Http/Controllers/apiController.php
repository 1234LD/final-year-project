<?php

namespace App\Http\Controllers;

use App\Http\Resources\adminResource;
use App\Http\Resources\chef_resource;
use App\Http\Resources\customerResource;
use App\Http\Resources\loginCollection;
use App\Http\Resources\loginResource;
use App\Menu_Item;
use App\Orders;
use App\users;
use App\Admin;
use App\Customer;
use App\chef;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic;
use Symfony\Component\Debug\ExceptionHandler;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use GuzzleHttp;

use Illuminate\Routing\ResponseFactory;
use Intervention\Image\ImageManagerStatic as Image;

class apiController extends Controller
{

    /**
     * Method to get the top meals on the foodpole based on rating ...
     */
    public function get_top_meals()
    {
        $result = OrderLine_items::join('menu_items','menu_items.id','=','order_line_items.menu__item_id')
                                 ->join('chef','menu_items.chef_id','=','chef.id')
                                 ->join('users','chef.users_id','=','users.id')
                                 ->join('orders','orders.id','=','order_line_items.order_id')
                                 ->where('orders.order_status','=', config('constants.DEFAULT_ORDER_DELIVERED'))
                                 ->selectRaw('count(*) as total, menu_items.id, item_name, username, menu_items.rating')
                                 ->groupBy('menu_items.id', 'item_name', 'username', 'menu_items.rating')
                                 ->orderBy('total','DESC')
                                 ->take(5)
                                 ->get();

        if(!is_null($result))
        {
            return json_encode($result);
        }
        else
        {
            return response('Sorry! there is no cheapest meal on foodpole','404');
        }
    }

    /**
     * Method to get the quickest meal based on minimum time ...
     */
    public function get_quickest_meals()
    {
        $result = Menu_Item::join('chef','chef.id','=','menu_items.chef_id')
                           ->join('users','users.id','=','chef.users_id')
                           ->where('users.account_status','=',true)
                           ->where('menu_items.item_time','<=',30)
                           ->take(5)
                           ->get();

        if(!is_null($result))
        {
            return json_encode($result);
        }
        else
        {
            return response('Sorry! there is no cheapest meal on foodpole','404');
        }
    }

    /**
     * Method to get the cheapest meal based on minimum item price ....
     */
    public function get_cheapest_meal()
    {
        $result = Menu_Item::join('chef','chef.id','=','menu_items.chef_id')
                           ->join('users','users.id','=','chef.users_id')
                           ->where('users.account_status','=',true)
                           ->groupBy('menu_items.item_price')
                           ->orderBy('menu_items.item_price','ASC')
                           ->take(5)
                           ->get();

        if(!is_null($result))
        {
            return json_encode($result);
        }
        else
        {
            return response('Sorry! there is no cheapest meal on foodpole','404');
        }
    }

    /**
     * Method to get the analysis between delivery_type
     * (self delivery and take away) on Food Pole ...
     */
    public function get_analysis()
    {
        $totalOrders = Orders::where('order_status','=',config('constants.DEFAULT_ORDER_DELIVERED'))
                             ->count();

        $total_self_delivery = Orders::where('order_status','=',config('constants.DEFAULT_ORDER_DELIVERED'))
                                     ->where('delivery_type','=',config('constants.DEFAULT_SELF_DELIVERY'))
                                     ->count();

        $self_delivery_percentage = ($total_self_delivery / $totalOrders) * 100 ;

        $total_take_away = Orders::where('order_status','=',config('constants.DEFAULT_ORDER_DELIVERED'))
                                 ->where('delivery_type','=',config('constants.DEFAULT_TAKE-AWAY_DELIVERY'))
                                 ->count();

        $take_away_percentage = ($total_take_away / $totalOrders) * 100 ;

        $analysis = array(
            'self-delivery' => $self_delivery_percentage,
            'take-away' => $take_away_percentage
        );

        return json_encode($analysis);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userInfo = users::simplePaginate(5);

        return $userInfo->count();
        return loginResource::collection($userInfo);
    }

    /**
     * Handling the forgot Password request
     */
    public function forgot_password(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'email' => 'required|unique:users'
        ]);

        if($validate->fails())
        {
            $user = DB::table('users')->where('email','=',$request->get('email'))->first();

            $mail = Crypt::encrypt($user->email);

            date_default_timezone_set("Asia/Karachi");

            $time = Crypt::encrypt(Carbon::now());

            Mail::to($user->email)->send(new SendMailable($user->username,$mail,$time));

            $response = [
                'msg' => 'We have send you the mail, kindly check it',
                'code' => 200
            ];
            return \response($response);
        }
        else
        {
            $response = [
                'msg' => 'Sorry you have entered a wrong email',
                'code' => 404
            ];

            return \response($response);
        }

        /* Extra Code Comments */
        //    $timezone = date_default_timezone_get();
        //    $time = date("Y-m-d h:i:s");
    }


    /**
     * Resetting the new password of the User...
     */
    public function reset_pswd(Request $request)
    {
        $dec_email = Crypt::decrypt($request->get('email'));

        $user = DB::table('users')->where('email','=',$dec_email)->first();

        if($user)
        {
            $reset_user = users::find($user->id);

            $hash = Hash::make($request->get('pswd'));

            $reset_user->password = $hash;

            if($reset_user->save())
            {
                $response = [
                    'msg' => 'Your password has been updated please login to verify !',
                    'code' => 200
                ];
                return \response($response);
//                return new loginResource($reset_user);
            }
            else
            {
                $response = [
                    'msg' => 'Sorry there were some problem in updating !',
                    'code' => 200
                ];
                return \response($response);
            }
        }
    }

    /**
     * Call on Sign up Request ...
     * Method to store the new record of the user as a customer ...
     */
    public function create_customer(Request $request)
    {

        $validateEmail = Validator::make($request->all(),[
            'email' => 'required|unique:users'
        ]);
        $validateUsername = Validator::make($request->all(),[
            'username' => 'required|unique:users'
        ]);

        if($validateEmail->fails())
        {
            $response = [
                'msg' => 'Email has already taken',
                'code' => 404
            ];
            return \response($response);
        }
        else if($validateUsername->fails())
        {
            $response = [
                'msg' => 'Username has already taken',
                'code' => 404
            ];
            return \response($response);
        }
        else
            {
            $user = new users();

//            $passwords = $request->input('password');
//            $hashed = Hash::make($passwords);
//
//            $user->full_name = $request->input('fullname');
//            $user->username = $request->input('username');
//            $user->email = $request->input('email');
//            $user->usertype = $request->input('usertype');
//            $user->password = $hashed;


            $passwords = $request->input('password');
            $hashed = Hash::make($passwords);

            $user->full_name = $request->input('fullname');
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->usertype = $request->input('usertype');
            $user->lat = $request->input('latitude');
            $user->lng = $request->input('longitude');
            $user->password = $hashed;


            if ($user->save())
            {
                $response = [
                    'msg' => 'Your account has been created, please login to verify your credentials ..',
                    'code' => 200
                ];
                return \response($response);
//               return new loginCollection(users::where('id',$user->id)->get());
            }
            else
            {
                $response = [
                    'msg' => 'Whoops there were some problems with server',
                    'code' => 404
                ];
                return \response($response);
            }

        }

        /* Extra Code Comments */
        // $user->password = $request->input('password');
        //  bcrypt($request->input('password'));
    }    /**
     * Call on Sign up Request ...
     * Method to store the new record of the user as a chef ...
     */
    public function create_chef(Request $request)
    {

        $validateEmail = Validator::make($request->all(),[
            'email' => 'required|unique:users'
        ]);
        $validateUsername = Validator::make($request->all(),[
            'username' => 'required|unique:users'
        ]);

        if($validateEmail->fails())
        {
            $response = [
                'msg' => 'Email has already taken',
                'code' => 404
            ];
            return \response($response);
        }
        else if($validateUsername->fails())
        {
            $response = [
                'msg' => 'Username has already taken',
                'code' => 404
            ];
            return \response($response);
        }
        else
            {

            $user = new users();
            $passwords = $request->input('password');
            $hashed = Hash::make($passwords);

            $user->full_name = $request->input('fullname');
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->phone_number =$request->input('phone');
            $user->usertype = $request->input('usertype');
            $user->lat = $request->input('latitude');
            $user->lng = $request->input('longitude');
            $user->password = $hashed;

            if ($user->save())
            {
                $chef = new chef([
                    'phoneNumber' => $request->input('phone'),
                    'latitude' => $request->input('latitude'),
                    'longitude' => $request->input('longitude')
                ]);

                users::find($user->id)->chef()->save($chef);

//               return new loginCollection(users::where('id',$user->id)->get());

                $email = $chef->phoneNumber.'@txtlocal.co.uk' ;
                $data =array('name'=> $user->full_name) ;

                //$emial= 'hamza.dabeer@gmail.com';

                Mail::send('mail', $data,  function ($message ) use($email){

                    $message->to($email)->subject('Welcome to Foodpole ! verification message');
                    $message->from('food.pole4@gmail.com');

                });

//                $newMail = new MailController();
//                $newMail->message_to_number($chef->phoneNumber, $user->full_name);

                $response = [
                    'msg' => 'Your account has been created, please login to verify your credentials ..',
                    'code' => 200
                ];
                return \response($response);
            }
            else
            {
                $response = [
                    'msg' => 'Whoops there were some problems with server',
                    'code' => 404
                ];
                return \response($response);
            }

        }
    }

    /**
     * Call on Login request ...
     * Method to check the credentials of the User ...
     */
    public function show(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('pswd');

        // $hashed =  Hash::check($password);

        $user = DB::table('users')
            ->where('email', '=', $email)
            ->where('account_status',true)
            ->first();

        if (is_null($user)) {
            $response = [
                'msg' => 'The Email you entered is Invalid',
                'code' => 404
            ];
            return response($response);
        } else {
            if ($user && Hash::check($password, $user->password))
            {
//              return new loginResource($user);
                return new loginCollection(users::where('id',$user->id)->get());
            } else {
                $response = [
                    'msg' => 'The password you entered is Invalid',
                    'code' => 404
                ];
                return response($response);
//                return response('The password you entered is Invalid ... ');
            }
        }
    }

    /**
     * Update the password of the User ...
     */
    public function update_password(Request $request)
    {
        $user = users::findOrFail($request->input('user_id'));

        if(is_null($user))
        {
            return response('These credentials does not meet with the records ...','500');
        }
        else
        {
            if(Hash::check($request->input('password'),$user->password))
            {
                return response('Sorry! You need enter different password ...','403');
            }
            else
            {
                $hashed_pswd = Hash::make($request->input('password'));

                $user->password = $hashed_pswd;

                if($user->save())
                {
                    return new loginResource($user);
                }
                else
                {
                    return response('Whoops there wer some problems with server','404');
                }
            }
        }
    }

    /**
     * Method to update the profile picture of the User ...
     */
   public function update_picture(Request $request)
   {
       $user = users::find($request->input('user_id'));

       if(is_null($user))
       {
           return response('These credentials does not meet with the records ...','500');
       }
       else
       {
           $image = $request->input('profile_picture');

           $image->move(public_path('/item_images').'/',$image->getClientOriginalName());


           $user->picture = $image;

           if($user->save())
           {
               return new loginResource($user);
           }
           else
           {
               return response('Whoops there wer some problems with server','404');
           }
       }
   }

    /**
     * Method to update the current status of the user ...
     */
    public function update_status(Request $request)
    {
        $user = users::find($request->get('user_id'));

//return $request->get('user_id');

        if(is_null($user))
        {
            return response('These credentials does not meet with the records ... ','203');
        }
        else
        {
            if($user->usertype == config('constants.DEFAULT_ADMIN_TYPE'))
            {
                $admin = Admin::where('users_id','=',$request->input('user_id'))->first();

                if(is_null($admin))
                {
                    return \response('These credentials does not meet with the records ...','203');
                }
                else
                {
                    $admin->online_status = $request->input('status');

                    if($admin->save())
                    {
                        return new adminResource($admin);
                    }
                    else
                    {
                        return response('Whoops there wer some problems with server','404');
                    }
                }
            }
            else if($user->usertype == config('constants.DEFAULT_CHEF_TYPE') || $user->usertype == config('constants.DEFAULT_QUICK-CHEF_TYPE'))
            {
                $chef = chef::where('users_id','=',$user->id)->first();

                if(is_null($chef))
                {
                    return \response('These credentials does not meet with the records ...','203');
                }
                else
                {
                    $chef->online_status = $request->input('status');

                    if($chef->save())
                    {
                        return new chef_resource($chef);
                    }
                    else
                    {
                        return response('Whoops there wer some problems with server','404');
                    }
                }
            }
            else if($user->usertype == config('constants.DEFAULT_CUSTOMER_TYPE'))
            {
                $customer = Customer::where('users_id','=',$request->input('user_id'))->first();

                if(is_null($customer))
                {
                    return \response('These credentials does not meet with the records ...','203');
                }
                else
                {
                    $customer->online_status = $request->input('status');

                    if($customer->save())
                    {
                        return new customerResource($customer);
                    }
                    else
                    {
                        return response('Whoops there wer some problems with server','404');
                    }
                }
            }
        }
    }

    /**
     * Method to remove the User from the record ...
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if(is_null($user))
        {
            $response = [
                'msg' => 'These credentials does not meet with the records ...',
                'code' => 404
            ];

            return response($response);
        }
        else{
            if($user->delete())
            {
                return new loginResource($user);
            }
            else
            {
                $response = [
                    'msg' => 'Whoops there were some problems wuth server ...',
                    'code' => 404
                ];

                return response($response);
            }
        }

    }
    public function profile_data_populate($id){


        $user = users::find($id);

        return new loginResource($user);



    }
    public function get_admin_data_update(Request $request){
$id= $request->input("user_id") ;


//dd($request->input('fullname'));
        $pswd = $request->input('pass')   ;

        $file = $request->img;
 //  dd($file);

       // dd($file);
//        $file->move(public_path('/chefimages').'/', $file->getClientOriginalName());
//        return $file->getClientOriginalName();


        $user = users::find($request->input('user_id'));



         if(is_null($file) && is_null($pswd)){

             $user->full_name = $request->input('fullname');
             $user->username = $request->input('username');
             $user->email= $request->input('useremail');

             $user->phone_number= $request->input('number');
         }

else if($file !=null && is_null($pswd)){

    $user->full_name = $request->input('fullname');
    $user->username = $request->input('username');
    $user->email= $request->input('useremail');
//


    $user->phone_number= $request->input('number');
  //  $file= $request->img;
//    if($request->hasFile('img')) {
        $file->move(public_path('/chefimages') . '/', $file->getClientOriginalName());
        $user->image = $file->getClientOriginalName();
//        return $file->getClientOriginalName();
//    }
//    else {
//
//        return 'bhai file nai a  rahe';
//    }
//return $file->getClientOriginalName();
//    $user->image = $file->getClientOriginalName();


//    $image->move(public_path('/images').'/',$image->getClientOriginalName());
//    dd($image->getClientOriginalName());
//    $user->image = $image->getClientOriginalName();

//
//    $user->image = $image;

}
else if($pswd !=null && is_null($file) ){


    $user->full_name = $request->input('fullname');
    $user->username = $request->input('username');
    $user->email= $request->input('useremail');
    $user->phone_number= $request->input('number');
    $hashed = Hash::make($pswd);


    $user->password = $hashed;
//
//    $file = $file->move(public_path('/chefimages') . '/', $file->getClientOriginalName());
//
//    $user->image = $file->getClientOriginalName();

}
else {



    $user->full_name = $request->input('fullname');
    $user->username = $request->input('username');
    $user->email= $request->input('useremail');
    $user->phone_number= $request->input('number');
    $hashed = Hash::make($pswd);


    $user->password = $hashed;

    $file = $file->move(public_path('/chefimages') . '/', $file->getClientOriginalName());

    $user->image = $file->getClientOriginalName();


}

      //  return  $file;
if($user->save()) {

             Session::forget('picture');
             Session::put('picture',$user->image);

    return new loginResource($user) ;
        }



else {

             return "";
}



//      if($user->save()){
//
//          $data= [ 'code' => 200,
//
//               'msg' => 'sucessfull updated'
//              ];
//return
//    response($data) ;
//      }
//      else {
//
//          $data= [ 'code' => 200,
//
//              'msg' => 'error occur'
//          ];
//          return response($data);
//      }

//


}



}
