<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redirect;


class Web_superadmin_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //    dd($request->all()) ;
        //dd("sdadssa");
        $data = [
            'img' => $request->file('image_name'),
            'fullname' =>$_POST['fullname'],
            'username'=> $_POST['username'],
            'email' => $_POST['email'],
            'password' => $_POST['password'],
            'phone' => $_POST['phone'],
            'usertype' => config('constants.DEFAULT_ADMIN_TYPE'),
//            'gmail' => $_POST['username'] . '@gmail.com',
//            'twitter' => $_POST['username'], '@twitter.com'

        ];
//dd($data) ;
        $req = Request::create('api/admin/create', 'POST', $data);
        $response = app()->handle($req)->getContent();

        $output = json_decode($response);

        if (is_null($output)) {

            app()->abort('404');
//
        }
        else {

            return Redirect::back();
        }
//        dd($data);

//        $req = curl_init();
//        $url = 'http://localhost/foodpoleAPI/public/api/admin/create';
//
//        curl_setopt($req,CURLOPT_URL,$url);
//        curl_setopt($req,CURLOPT_POST,1);
//        curl_setopt($req,CURLOPT_POSTFIELDS,$data);
//        curl_setopt($req, CURLOPT_RETURNTRANSFER,true);
//
//        $response = curl_exec($req);
//        curl_close($req);


        //$response = json_decode($request->getBody());

       // return view('admin.admin-add-admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
