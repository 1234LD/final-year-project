<?php

namespace App\Http\Controllers;

use App\Admin;
use App\chef;
use App\Http\Requests;
use App\Http\Resources\adminResource;
use App\Http\Resources\chef_profileCollection;
use App\Http\Resources\loginCollection;
use App\Http\Resources\loginResource;
use App\Http\Resources\menu_itemResource;
use App\Http\Resources\menuResource;
use App\Http\Resources\nearbyCollection;
use App\Http\Resources\orderCollection;
use App\Item_Images;
use App\Menu;
use App\Menu_Item;
use App\Orders;
use App\rating;
use App\users;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Query\Builder ;
use App\Http\Resources\chef_resource;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class api_chef_controller extends Controller
{

    /**
     * View the complete profile of the chef along with his/her menu ...
     */
    public function view_complete_profile($id, $username)
    {
        $user = users::where('id', $id)->where('username', $username)->where('account_status',true)->first();

        if ($user) {

            $chef = chef::where('users_id',$user->id)->first();
            if($chef)
            {
                return new chef_profileCollection(chef::where('id', $chef->id)->with(['menu_Items', 'user','ratings'])->get());
            }
            else
            {
                $response = [
                    'msg' => 'Sorry no chef exist with these input fields or may be account is blocked.',
                    'code' => 404
                ];
                return response($response);
            }
        } else {
            $response = [
                'msg' => 'Sorry no chef exist with these input fields or may be account is blocked.',
                'code' => 404
            ];
            return response($response);
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        //
        $chef = chef::paginate(15);
        return chef_resource::collection($chef);
    }

    /**
     * Method to get the total sales today ...
     */
    public function get_total_sales($id)
    {
        $chef = users::find($id)->chef;

        if(!is_null($chef)) {

            date_default_timezone_set("Asia/Karachi");

            $order = OrderLine_items::join('orders', 'order_line_items.order_id', '=', 'orders.id')
                                    ->join('chef','chef.id','=','orders.chef_id')
                                    ->where('orders.order_status', '=', 'delivered')
                                    ->where('orders.created_at','=',Carbon::today())
                                    ->count();

            return $order;
        }
    }

    /**
     * Method to get the total sales of per item today ...
     */
    public function get_total_sale_items($id)
    {
        $chef = users::find($id)->chef;

        if(!is_null($chef)) {

            date_default_timezone_set("Asia/Karachi");

            $order = OrderLine_items::join('orders', 'order_line_items.order_id', '=', 'orders.id')
                ->where('orders.chef_id','=',$chef->id)
                ->join('menu_items','menu_items.id','=','order_line_items.menu__item_id')
                ->where('orders.order_status', '=', 'delivered')
                ->where('orders.created_at','=',Carbon::today())
                ->selectRaw('sum(order_line_items.item_price) as price , menu_items.item_name , menu_items.rating')
                ->groupBy('menu_items.item_name','menu_items.rating')
                ->get();

            return $order;
        }
    }

    /**
     * Method to get the total gross rate of the sale today ...
     */
    public function get_gross_total($id)
    {
        $chef = users::find($id)->chef;

        if(!is_null($chef))
        {
            date_default_timezone_set("Asia/Karachi");

            $order = Orders::where('chef_id',$chef->id)
                            ->whereDate('orders.created_at',Carbon::today())
                            ->selectRaw('SUM(orders.total) as Total')
                            ->groupBy('orders.total')
                            ->get();

            return $order;
        }
    }


    /**
     * Method to get the top meals on foodpole based on orders ...
     */
    public function get_top_meals()
    {
        $menu_item = OrderLine_items::join('menu_items','menu_items.id','=','order_line_items.menu__item_id')
                                    ->join('chef','menu_items.chef_id','=','chef.id')
                                    ->join('users','chef.users_id','=','users.id')
                                    ->join('orders','orders.id','=','order_line_items.order_id')
                                    ->where('orders.order_status','=', config('constants.DEFAULT_ORDER_DELIVERED'))
                                    ->selectRaw('count(*) as total, menu_items.id, item_name, username, menu_items.rating')
                                    ->groupBy('menu_items.id', 'item_name', 'username', 'menu_items.rating')
                                    ->orderBy('total','DESC')
                                    ->take(5)
                                    ->get();

        return $menu_item;
    }


    /**
     * Method to get the analysis between delivery_type
     * (self delivery and take away) on Food Pole ...
     */
    public function get_analysis($id)
    {

        $chef = users::find($id)->chef;

        if(is_null($chef))
        {
           return response('Sorry! you are not authorized to access this ... ');
        }


        $totalOrders = Orders::where('chef_id','=',$chef->id)
                             ->where('order_status','=',config('constants.DEFAULT_ORDER_DELIVERED'))
                             ->count();

        $total_self_delivery = Orders::where('chef_id','=',$chef->id)
                                     ->where('order_status','=',config('constants.DEFAULT_ORDER_DELIVERED'))
                                     ->where('delivery_type','=',config('constants.DEFAULT_SELF_DELIVERY'))
                                     ->count();

        $self_delivery_percentage = ($total_self_delivery / $totalOrders) * 100 ;

        $total_take_away = Orders::where('chef_id','=',$chef->id)
                                 ->where('order_status','=',config('constants.DEFAULT_ORDER_DELIVERED'))
                                 ->where('delivery_type','=',config('constants.DEFAULT_TAKE-AWAY_DELIVERY'))
                                 ->count();

        $take_away_percentage = ($total_take_away / $totalOrders) * 100 ;

        $analysis = array(
            'self-delivery' => $self_delivery_percentage,
            'take-away' => $take_away_percentage
        );

        return json_encode($analysis);
    }


    /**
     * Method to get the completed orders ...
     */
    public function get_completed_orderes($id)
    {
        $chef = users::find($id)->chef;

        if(!is_null($chef))
        {
            $orders = Orders::with('order_line_items')
                            ->where('chef_id','=',$chef->id)
                            ->where('order_status','=',config('constants.DEFAULT_ORDER_COMPLETED'))
                            ->get();

            if(!is_null($orders))
            {
                return new orderCollection($orders);
            }
            else
            {
                return response('Sorry! you do not have any completed order yet ... ');
            }

        }
    }


    /**
     * Method to get all the feedback ...
     */
    public function get_all_feedback($id)
    {
        $chef = users::find($id)->chef;

        if(is_null($chef))
        {
            return response('Sorry! you are not authorized user ... ','404');
        }

        $result = rating::join('chef','chef.id','=','rating_reviews.chef_id')
                        ->join('customer','customer.id','=','rating_reviews.customer_id')
                        ->join('menu_items','menu_items.id','=','rating_reviews.menu__item_id')
                        ->where('rating_reviews.chef_id',$chef->id)
                        ->get();

        return json_encode($result);
    }

    /**
     * Method to get all the feedback ...
     */
    public function get_flagged_feedback($id)
    {
        $chef = users::find($id)->chef;

        if(is_null($chef))
        {
            return response('Sorry! you are not authorized user ... ','404');
        }

        $result = rating::join('chef','chef.id','=','rating_reviews.chef_id')
                        ->join('customer','customer.id','=','rating_reviews.customer_id')
                        ->join('menu_items','menu_items.id','=','rating_reviews.menu__item_id')
                        ->where('rating_reviews.chef_id',$chef->id)
                        ->where('rating_reviews.flag','>=',1)
                        ->get();

        return json_encode($result);
    }

    /**
     * Method to get the delivered orders ...
     */
    public function get_delivered_orderes($id)
    {
        $chef = users::find($id)->chef;

        if(!is_null($chef))
        {
            $orders = Orders::with('order_line_items')
                ->where('chef_id','=',$chef->id)
                ->where('order_status','=',config('constants.DEFAULT_ORDER_DELIVERED'))
                ->get();

            if(!is_null($orders))
            {
                return new orderCollection($orders);
            }
            else
            {
                $response = [
                    'msg' => 'Sorry! you do not have any delivered order yet ... ',
                    'code' => 404
                ];
                return response($response);
                return response();
            }

        }

    }

    /**
     * Method to get the ready orders ...
     */
    public function get_ready_orderes($id)
    {
        $chef = users::find($id)->chef;

        if(!is_null($chef))
        {
            $orders = Orders::with('order_line_items')
                ->where('chef_id','=',$chef->id)
                ->where('order_status','=',config('constants.DEFAULT_ORDER_READY'))
                ->get();

            if(!is_null($orders))
            {
                return new orderCollection($orders);
            }
            else
            {
                $response = [
                    'msg' => 'Sorry! you do not have any completed order yet ... ',
                    'code' => 404
                ];
                return response($response);
            }

        }
    }

    /**
     * Method to get the completed orders ...
     */
    public function get_cancelled_orderes($id)
    {
        $chef = users::find($id)->chef;

        if(!is_null($chef))
        {
            $orders = Orders::with('order_line_items')
                ->where('chef_id','=',$chef->id)
                ->where('order_status','=',config('constants.DEFAULT_ORDER_CANCEL'))
                ->get();

            if(!is_null($orders))
            {
                return new orderCollection($orders);
            }
            else
            {
                $response = [
                    'msg' => 'Sorry! you do not have any completed order yet ... ',
                    'code' => 404
                ];
                return response($response);
            }

        }

    }

    /**
     * Method to get the flagged orders ...
     */
    public function get_flagged_orders($id)
    {
        $chef = users::find($id)->chef;

        if(!is_null($chef) && $chef->account_status != false)
        {
            $orders = Orders::with('order_line_items')
                            ->where('chef_id','=',$chef->id)
                            ->where('order_status','=',config('constants.DEFAULT_ORDER_DELIVERED'))
                            ->where('flag','=',true)
                            ->get();

            if(!is_null($orders))
            {
                return new orderCollection($orders);
            }
            else
            {
                $response = [
                    'msg' => 'Sorry! you do not have any completed order yet ... ',
                    'code' => 404
                ];
                return response($response);
            }
        }
        else
        {
            return response('Sorry! These credentials does not meet with the requirements ...');
        }
    }

//    /**
//     * Method to get all orders of the chef ...
//     */
    public function get_all_orders($id, $name)
    {
        $user = users::where('id','=',$id)
                    ->where('name','=',$name)->first();

        if(!is_null($user))
        {
            $chef = users::find($user->id)->chef;
            return new orderCollection(Orders::where('chef_id',$chef->id)->with('order_line_items','chef','customer')->get());
        }

    }

    /**
     * @param $lat
     * @param $long
     * @return nearbyCollectionMet
     */
    public function nearSearching($lat, $long)
    {
        $radius = 200;
        $unit = "km";
        $unit = ($unit === "km") ? 6378.10 : 3963.17;
        $lat = (float)$lat;
        $long = (float)$long;
        $radius = (double)$radius;

        $chef = chef::join('users', 'users.id', '=', 'chef.users_id')
            ->having('distance', '<=', $radius)
            ->select(DB::raw("*,

                            ($unit * ACOS(COS(RADIANS($lat))
                                * COS(RADIANS(latitude))
                                * COS(RADIANS($long) - RADIANS(longitude))
                                + SIN(RADIANS($lat))
                                * SIN(RADIANS(latitude)))) AS distance"))
            ->where('online_status', '=', 'online')
            ->orderBy('distance', 'asc')
            ->simplePaginate(3);

//dd($chef);
//    $chef = chef::where('online_status','online')->with('user')->paginate(1);

        return new nearbyCollection($chef);

//                                ($unit * ACOS(COS(RADIANS($lat))
//                                    * COS(RADIANS(latitude))
//                                    * COS(RADIANS($lng) - RADIANS(longitude))
//                                    + SIN(RADIANS($lat))
//                                    * SIN(RADIANS(latitude)))) AS distance"))
//            ->where('online_status','=','online')
//            ->orderBy('distance','asc')
//            ->get();
//
//      //  return $chef->total();
//        return new nearbyCollection(new Paginator($chef,1));
//
    }




//    public function nearby(Request $request, $query){
//
//        $chef = new chef();
//        $latitude = 33.632066;
//        $longitude = 73.0704067;
//
//       // dd($latitude) ;
//        $distance =10 ;
//
//
//        $unit = "km";
//        $radius = 100;
//        $unit = ($unit === "km") ? 6378.10 : 3963.17;
//        $lat =  $latitude;
//        $lng =  $longitude;
//        $radius = (double) $radius;
//        return $query->having('distance','<=',$radius)
//            ->select(DB::raw("*,
//                            ($unit * ACOS(COS(RADIANS($lat))
//                                * COS(RADIANS(latitude))
//                                * COS(RADIANS($lng) - RADIANS(longitude))
//                                + SIN(RADIANS($lat))
//                                * SIN(RADIANS(latitude)))) AS distance")
//            )->orderBy('distance','asc');
////        $chef =DB::table('chef')('SELECT*,
////        6371 * 2 * ASIN(SQRT(POWER(SIN(RADIANS(? - ABS(chef.latitude))), 2) + COS(RADIANS(?)) * COS(RADIANS(ABS(chef.latitude))) * POWER(SIN(RADIANS(? - chef.longitude)), 2))) AS distance
////    FROM users
////    HAVING distance < ?
////    ',
////            array(
////                $latitude,
////                $latitude,
////                $longitude,
////                $distance
////            )
////        );
////        echo $chef;
////        foreach ($chef as $user)
////        {
////            var_dump($user->city);
////
////        }
////        if (is_null($chef)) {
////            echo "Sorry Error";
////        }
////        else {
////            dd($chef);
////            return new chef_resource($chef);
////
////        }
//
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return chef_resource
     */
    public function store(Request $request , $id)
    {

        //  $user = DB::table('users')->where('id', '=',Session::get('user_id'))->first();
        //  $user = DB::table('users')->where('id','=',$request->input('user_id'))->first();
//        $data = ['user'=>$_POST['cnic'],
//            'cnic'=>$_POST['cnic'],
//            'phoneNumber'=>$_POST['number'],
//            'Address'=>$_POST ['address'],
//            'workinghours'=>$_POST['workinghrs'],
//            'level'=>$_POST['cnic'],
//            'descriptions'=>$_POST['description'],
//            'modes'=>$_POST['cnic'],
//            'online_statuss'=>$_POST['cnic'] ,
//            'start_times'=>$_POST['cnic'],
//            'end_times'=>$_POST['cnic'],
//        ];

        // if(is_null($user))

        $validate = Validator::make($request->all(), [
            'cnic' => 'required|unique:chef'
        ]);

        if ($validate->fails()) {
            $response = [
                'msg' => 'Sorry! records with this CNIC already exists ... ',
                'code' => 404
            ];
            return response($response);
        } else {

           // $user = DB::table('users')->where('id', '=', $request->input('user_id'))->first();

         $user =  users::findorfail($id);
        // dd($user);

            if(is_null($user))
            {
                $response = [
                    'msg' => 'These credentials does not meet with the records ... ',
                    'code' => 404
                ];
                return response($response);
            }
            else {
                if($user->usertype == config('constants.DEFAULT_GENERAL_TYPE')) {
                    $user->usertype = config('constants.DEFAULT_CHEF_TYPE');
                    $user->account_status = 0 ;

                    $user->save();
//                    dd($user);

                }

                $chef = new chef();
                $chef->users_id = $user->id;
                $chef->cnic = $request->input('cnic');
                $chef->phoneNumber = $request->input('phoneNumber');
                $chef->latitude = $request->input('late');
                $chef->longitude = $request->input('long');

//        $chef->address = $request->input('Address');
//        $chef->workingHrs = $request->input('workinghours');
//        $chef->chef_level = $request->input('cnic');
//        $chef->description = $request->input('descriptions') ;
//        $chef->mode = $request->input('cnic');

                $chef->city = $request->input('city');
                $chef->streetaddress = $request->input('street_address');
                $chef->state = $request->input('state');
                $chef->postalcode = $request->input('postalcode');
                $chef->online_status = 'online';
//dd($chef);
                if ($chef->save())
                {
                    return new chef_resource($chef);
                }
                else
                {
                    $response = [
                        'msg' =>'Whoops there wer some problems with server',
                        'code' => 404
                    ];
                    return response($response);
                }
            }
        }
    }


    public function basic_mail()
    {
//Session::get('user_email')
        Session::get('username');
        $data = array('name' => Session::get('username'));

        Mail::send('mail', $data, function ($message) {

            $message->to('hamza.dabeer@gmail.com')->subject('Verfication email');
            $message->from('foodpole@gmail.com', 'foodpole');

        });

        $response = [
            'msg' => 'verification email has been sent successfully ...',
            'code' => 200
        ];
        return response($response);
    }

//
//    public function basic_mail(){
////Session::get('user_email')
//        Session::get('username') ;
//        $data =array('name'=> Session::get('username')) ;
//
//        Mail::send('mail', $data,function ($message){
//
//            $message->to('hamza.dabeer@gmail.com')->subject('Verfication email') ;
//            $message->from('foodpole@gmail.com','foodpole') ;
//
//        }) ;
//        echo " basic email has been send to niamat " ;
//    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
        {
            //

        }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return chef_resource
     */
    public function edit(Request $request, $id)
    {
        $chef = users::find($id)->chef;
      //  $chef = chef::find($id);


        if (is_null($chef)) {
            return response('These credentials does not meet with the records ...', '403');

        } else {
//            $file = $request->image;
//            $file->move(public_path('/chefimages') . '/', $file->getClientOriginalName());


//            $chef->picture = $file->getClientOriginalName();
            $chef->description = $request->input('aboutme');
          //  $chef->workingHrs = $request->input('workinghrs');


    $chef->start_time=$request->input('starttime');
    $chef->end_time=$request->input('endtime');

    $chef->delivery_type =$request->input('deliverytype');
            $chef->city = $request->input('city');
            $chef->streetaddress = $request->input('street_address');
            $chef->state = $request->input('state');
            $chef->postalcode = $request->input('postalcode');

            $chef->cnic = $request->input('cnic');
//dd($chef);

            if ($chef->save()) {
                return new chef_resource($chef);
            } else {
                $response = [
                    'msg' => 'Whoops there were some problems with server',
                    'code' => 200
                ];
                return response($response);
            }
        }
    }
    public function get_all_chefs(){
         $chef = chef::all();

        if($chef){

            return chef_resource::collection($chef);
        }


    }
    public function get_verfied_chefs()
    {

        $chef = chef::where('verify', true)->get();
        if ($chef) {

            return chef_resource::collection($chef);

        }
    }
    public function get_blocked_chefs(){

        $chef = users::where('usertype','chef')->where('account_status',false)->get();

        if($chef){

            return new loginCollection($chef);
        }


    }

    public function chef_data_populate($id){
          //      $user = users::where('id', $id);
        //dd($user);
        return new loginCollection(users::where('id',$id)->get());
      //  return new loginCollection($user);

    }
    public function get_chef_data_update(Request $request)
    {

//   dd($request->all());
        $id = $request->input('userid');
     //   dd($id) ;
        $file = $request->img;
        $pswd = $request->input('pass')   ;
        $user = users::find($id);
        $chef = users::find($id)->chef;
       //dd($chef);
//        $validate = Validator::make($request->all(), [
//            'cnic' => 'required|unique:chef'
//        ]);
////
//        if ($validate->fails()) {
//            return response('Sorry! records with this CNIC already exists ... ', '202');
//        }
//        else {
//                if (is_null($chef)) {
//                    return response('These credentials does not meet with the records ...', '403');
//                } else {

//                    if(is_null($user)){
//
//                      return "user does not exist" ;
//                    }
//                    else {
                               if(is_null($pswd)) {
                                   $user->full_name = $request->input('fullname');
                                   $user->email = $request->input('useremail');
                                   $user->username = $request->input('username');

                                   $user->phone_number = $request->input('number');
                                 //  dd($user) ;
                                   if($user->save()){

                            //        dd( "ho jao" );
                                   }
                                   else {

                                //       dd("have to do");
                                   }
                               }
                               else {
                                   $user->full_name = $request->input('fullname');
                                   $user->email = $request->input('useremail');
                                   $user->username = $request->input('username');

                                   $user->phone_number = $request->input('numb');


                                   $hashed = Hash::make($pswd);


                                   $user->password = $hashed;
                                   $user->save();
                                   //   }
                               }
         //           }


                        if($file !=null) {
                            $chef->description = $request->input('description');
                            //  $chef->workingHrs = $request->input('workinghrs');


                            $chef->start_time = $request->input('starttime');
                            $chef->end_time = $request->input('endtime');

                            $chef->delivery_type = $request->input('deliverytype');
                            $chef->city = $request->input('city');
                            $chef->streetaddress = $request->input('street_address');
                            $chef->state = $request->input('state');
                            $chef->postalcode = $request->input('postalcode');

                            $chef->cnic = $request->input('cnic');
                            $file->move(public_path('/chefimages') . '/', $file->getClientOriginalName());
                            $chef->picture = $file->getClientOriginalName();
                            if ($chef->save()) {
                                $response = [
                                    'msg' => 'Your account has been created, please login to verify your credentials ..',
                                    'code' => 200
                                ];
                                return response($response);

                            } else {

                                dd("data bhai nai hua update");
                            }
                        }
                        else {


                            $chef->description = $request->input('description');
                            //  $chef->workingHrs = $request->input('workinghrs');


                            $chef->start_time = $request->input('starttime');
                            $chef->end_time = $request->input('endtime');

                            $chef->delivery_type = $request->input('deliverytype');
                            $chef->city = $request->input('city');
                            $chef->streetaddress = $request->input('street_address');
                            $chef->state = $request->input('state');
                            $chef->postalcode = $request->input('postalcode');

                            $chef->cnic = $request->input('cnic');


                          //  users::find($id)->chef()->save($chef);
//                            dd($chef);
                            if ($chef->save()) {
                                $response = [
                                    'msg' => 'Your account has been created, please login to verify your credentials ..',
                                    'code' => 200
                                ];
                                return response($response);


                            } else {

//                               dd("data bhai nai hua update");
                            }
                        }
                }
        //    }
        }
