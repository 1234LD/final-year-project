<?php

namespace App\Http\Controllers;

use App\Menu;
use App\users;
use App\Customer;
use App\Orders;
use Illuminate\Http\Request;
 use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Http\Resources\loginResource;
use App\Http\Resources\order_Resource;
use App\Http\Resources\orderCollection;
use App\Menu_Item;
use App\OrderLine_items;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class Web_admin_view_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public static $setvalue = 0;

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_all_customer()
    {


        $url = 'http://localhost/foodpoleAPI/public/api/all_customer';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());
        // dd($response[0]->user->full_name);
        // dd($request->Count());
        $numb = (count($response));
        return view('admin.admin-all-customers')->with('allcustomer', $response);

    }

    public function get_all_block_customer()
    {

        $url = 'http://localhost/foodpoleAPI/public/api/all_blocked_customer';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());
        // dd($response[0]->user->full_name);
        // dd($request->Count());
        $numb = (count($response));
        // dd($response);
        return view('admin.admin-blocked-customers')->with('blockcustomer', $response);
    }


    public function get_all_chef()
    {
        $url = 'http://localhost/foodpoleAPI/public/api/all_chefs';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());
        // dd($response[0]->user->full_name);
        // dd($request->Count());
        $numb = (count($response));
        //  dd($response);
        return view('admin.admin-all-chefs')->with('all_chef', $response);


    }

    public function get_verified_chef()
    {


        $url = 'http://localhost/foodpoleAPI/public/api/verified_chefs';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());
        // dd($response[0]->user->full_name);
        // dd($request->Count());
        $numb = (count($response));
        //  dd($response);
        return view('admin.admin-verified-chefs')->with('verify_chef', $response);


    }

    public function get_blocked_chef()
    {


        $url = 'http://localhost/foodpoleAPI/public/api/blocked_chefs';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());
        // dd($response[0]->user->full_name);
        // dd($request->Count());
        $numb = (count($response->data));
        // dd($response);
        return view('admin.admin-blocked-chefs')->with('blockchef', $response);

    }

    public function get_online_chef()
    {
        $url = 'http://localhost/foodpoleAPI/public/api/online_chefs';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());
        // dd($response[0]->user->full_name);
        // dd($request->Count());
        $numb = (count($response));
        // dd($response);
        return view('admin.admin-online-chefs')->with('onlinechef', $response);


    }

    public function get_all_categrories()
    {

        $url = 'http://localhost/foodpoleAPI/public/api/all_categories';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());
        // dd($response[0]->user->full_name);
        // dd($request->Count());
        $numb = (count($response));
        //dd($response);
        return view('admin.admin-all-categories')->with('allcategories', $response);


    }

    public function get_add_category()
    {

        $data = [
            'category_name' => $_POST['category_name'],
//        'username'=> $_POST['username'],
//
        ];
//        dd($data);

        $req = curl_init();
        $url = 'http://localhost/foodpoleAPI/public/api/admin/add_categories';

        curl_setopt($req, CURLOPT_URL, $url);
        curl_setopt($req, CURLOPT_POST, 1);
        curl_setopt($req, CURLOPT_POSTFIELDS, $data);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($req);
        curl_close($req);


        //$response = json_decode($request->getBody());

        return Redirect::back();

    }

    public function get_regular_meals()
    {

        $url = 'http://localhost/foodpoleAPI/public/api/all_meals';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());
        // dd($response[0]->user->full_name);
        // dd($request->Count());
        // $numb = (count($response));
        //dd($response);
        return view('admin.admin-all-regular-meals')->with('meals', $response);


    }

    public function get_all_orders()
    {

        $url = 'http://localhost/foodpoleAPI/public/api/all_orders/' . Session::get('user_id') . '/' . Session::get('user_type');
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        //   dd($response);
        return view('admin.admin-all-orders')->with('orders', $response);
    }

    public function get_admin_pending_orders()
    {
        // dd('yes we can do it');
        $url = 'http://localhost/foodpoleAPI/public/api/pending_orders';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        //   dd($response);
        return view('admin.admin-pending-response-orders')->with('orders', $response);
    }

    public function get_ready_orders()
    {


        $url = 'http://localhost/foodpoleAPI/public/api/admin/ready_orders';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        //  dd($response);
        return view('admin.admin-ready-orders')->with('orders', $response);

    }

    public function get_processsing_order()
    {


        $url = 'http://localhost/foodpoleAPI/public/api/process_orders';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        // dd($response);
        return view('admin.admin-processing-orders')->with('orders', $response);


    }

    public function get_delivered_order()
    {


        $url = 'http://localhost/foodpoleAPI/public/api/admin/deliver_orders';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        // dd($response);
        return view('admin.admin-delivered-orders')->with('orders', $response);


    }

    public function get_admin_cancel_orders()
    {
        $url = 'http://localhost/foodpoleAPI/public/api/admin/cancelled_orders';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        // dd($response);
        return view('admin.admin-canceled-orders')->with('orders', $response);


    }

    public function get_admin_Declined_orders()
    {

        $url = 'http://localhost/foodpoleAPI/public/api/admin/declined_orders';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        // dd($response);
        return view('admin.admin-declined-orders')->with('orders', $response);


    }

    public function get_admin_Paid_orders()
    {

        $url = 'http://localhost/foodpoleAPI/public/api/admin/paid_orders';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        // dd($response);
        return view('admin.admin-paid-orders')->with('orders', $response);


    }

    public function get_admin_unpaid_orders()
    {


        $url = 'http://localhost/foodpoleAPI/public/api/admin/unpaid_orders';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        // dd($response);
        return view('admin.admin-unpaid-orders')->with('orders', $response);


    }

    public function get_admin_flagged_orders()
    {


        $url = 'http://localhost/foodpoleAPI/public/api/admin/flagged_orders';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        // dd($response);
        return view('admin.admin-flagged-orders')->with('orders', $response);


    }

    public function get_admin_quick_orders()
    {


        $url = 'http://localhost/foodpoleAPI/public/api/admin/all_quick_meals';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());
        // dd($response[0]->user->full_name);
        // dd($request->Count());
        // $numb = (count($response));
        //dd($response);
        return view('admin.admin-all-quick-meals')->with('meals', $response);

    }

    public function get_admin_expire_orders()
    {


        $url = 'http://localhost/foodpoleAPI/public/api/admin/all_expire_meals';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());
        // dd($response[0]->user->full_name);
        // dd($request->Count());
        // $numb = (count($response));
        //dd($response);
        return view('admin.admin-expired-quick-meals')->with('meals', $response);
    }


    public function get_admin_free_orders()
    {

        $url = 'http://localhost/foodpoleAPI/public/api/admin/all_free_meals';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());
        // dd($response[0]->user->full_name);
        // dd($request->Count());
        // $numb = (count($response));
        //dd($response);
        return view('admin.admin-free-meals')->with('meals', $response);

    }

    public function get_admin_allreview_orders()
    {

        $url = 'http://localhost/foodpoleAPI/public/api/admin/all_review_meals';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());
        // dd($response[0]->user->full_name);
        // dd($request->Count());
        // $numb = (count($response));
        //dd($response);
        return view('admin.admin-all-reviews')->with('reviews', $response);


    }

    public function get_admin_flagged_reviews()
    {


        $url = 'http://localhost/foodpoleAPI/public/api/admin/all_flagged_meals';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());
        // dd($response[0]->user->full_name);
        // dd($request->Count());
        // $numb = (count($response));
       // dd($response);
        if(is_null($response)){

            return Redirect::back();
        }else {
            return view('admin.admin-flagged-reviews')->with('reviews', $response);
        }
    }

    public function get_admin_nonflagged_reviews()
    {


        $url = 'http://localhost/foodpoleAPI/public/api/admin/all_nonflagged_meals';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());
        // dd($response[0]->user->full_name);
        // dd($request->Count());
        // $numb = (count($response));
        //dd($response);
        return view('admin.admin-non-flagged-reviews')->with('reviews', $response);

    }

    public function get_admin_profile_load($id)
    {

//        $id = Session::get('user_id');
        $url = 'http://localhost/foodpoleAPI/public/api/updateprofile/' . $id;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        return view('admin.admin-profile')->with('data', $response);
    }

    public function get_user_data_population()
    {

        $id = Session::get('user_id');
        $url = 'http://localhost/foodpoleAPI/public/api/updateprofile/' . $id;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        return view('user.user-profile')->with('data', $response);

    }

    public function get_admin_profile_update(Request $request, $id)
    {
//dd($request->all());
//dd($request->file('img_name'));
//       // $id = Session::get('user_id');
//$file =$request->file('image_name');
//    $file->move(public_path('/chefimages').'/', $file->getClientOriginalName());
//    dd($file->getClientOriginalName()) ;
//        $data = [
//            'img' => $request->file('image_name'),
//            'fullname' => $_POST['name'],
//            'username' => $_POST['username'],
//            'useremail' => $_POST['email'],
//            'number' => $_POST['numb'],
//            'pswd' => $_POST['pass'],
//
//            'user_id' => $id
//
//        ];
//dd($data);
        $data = [
            'img' => $request->file('image_name'),
            'fullname' => $_POST['name'],
            'username' => $_POST['username'],
            'useremail' => $_POST['email'],
            'number' => $_POST['numb'],
            'pswd' => $_POST['pass'],

            'user_id' => $id

        ];
        //  dd($data);

        $req = Request::create('api/update-data-profile', 'POST', $data);
        $response = app()->handle($req)->getContent();

        $output = json_decode($response);

        if (is_null($output)) {

            app()->abort('404');
//
        }
//    else {
//dd($response);
        else {
            //   return Redirect::to('http://localhost/foodpoleAPI/public/user/dashboard');
            return Redirect::back();
        }

//
//    $req = curl_init();
//    $url ='http://localhost/foodpoleAPI/public/api/update-data-profile';
//
//    curl_setopt($req, CURLOPT_URL, $url);
//    curl_setopt($req, CURLOPT_POST, 1);
//    curl_setopt($req, CURLOPT_POSTFIELDS, $data);
//    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
//
//    $response = curl_exec($req);
//    curl_close($req);
//    $result = json_decode($response);
//
//if($result->code == 200){
//
//    return 'yahooooo' ;
//}else {
//    return 'ohooooo';
//
//}

    }

    public function get_user_profile_update(Request $request, $id)
    {
//    if($request->hasFile('image_name')) {
//        return 'bhai file hai ';
//    }
//    else {
//
//
//        return 'bhai ye file nai hai';
//    }
//dd("welcome");

//        $id = Session::get('user_id');
//$file =$request->file('image_name');
//    $file->move(public_path('/chefimages').'/', $file->getClientOriginalName());
//    dd($file->getClientOriginalName()) ;
        $data = [
            'img' => $request->file('image_name'),
            'fullname' => $_POST['name'],
            'username' => $_POST['username'],
            'useremail' => $_POST['email'],
            'number' => $_POST['numb'],
            'pswd' => $_POST['pass'],

            'user_id' => $id

        ];
        //  dd($data);

        $req = Request::create('api/update-data-profile', 'POST', $data);
        $response = app()->handle($req)->getContent();

        $output = json_decode($response);

        if (is_null($output)) {

            app()->abort('404');
//
        }
//    else {
//dd($response);
        else {
            return Redirect::back();
        }


    }

    public function get_chef_profile_update(Request $request, $id)
    {

//        dd($request->all());

//$file =$request->file('image_name');
//    $file->move(public_path('/chefimages').'/', $file->getClientOriginalName());
//    dd($file->getClientOriginalName()) ;
        $data = [
            'fullname' => $_POST['name'],
            'username' => $_POST['username'],
            'useremail' => $_POST['email'],
            'number' => $_POST['numb'],
            'pswd' => $_POST['pass'],
            'img' => $request->file('image_name'),
            'user_id' => $id,
            'description' => $_POST['aboutme'],
            'starttime' => $_POST['starttime'],
            'endtime' => $_POST['endtime'],
            'deliverytype' => $_POST['deliverymode'],
            'city' => $_POST['city'],
            'street_address' => $_POST['address'],
            'state' => $_POST['state'],
            'cnic' => $_POST['cnic'],
            'postalcode' => $_POST['postalcode'],
            'userid' => $id

        ];
//dd($data) ;
        $req = Request::create('api/update-chef-profile', 'POST', $data);
        $response = app()->handle($req)->getContent();
        //  dd($response);

        $output = json_decode($response);
//dd($output);
        if (is_null($output)) {

            app()->abort('404');
//
        } //    else {

        else {
//            dd($response);
            return Redirect::back();
        }
    }

    public function get_user_all_orders()
    {
        $id = Session::get('user_id');


        $url = 'http://localhost/foodpoleAPI/public/api/customer_all_orders/' . $id;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        // dd($response);


        return view('user.user-my-orders')->with('orders', $response);

//    }

    }

    public function setValue($value)
    {
//        $setvalue = $value;
//        dd($value);
        return view('user.user-my-orders')->with('tvalue', $value);
    }


    public function get_chef_data_population($id)
    {
        $user_id = $id;
        // dd($id);
//        $id = Session::get('user_id');
        $url = 'http://localhost/foodpoleAPI/public/api/chef_profile_populate/' . $user_id;
        $res = new Client();

        $request = $res->get($url);

        $response = json_decode($request->getBody());

        //    return $response;
        return view('chef.chef-profile')->with('data', $response);

    }

    public function get_chef_all_categories()
    {


        $url = 'http://localhost/foodpoleAPI/public/api/all_categories';
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());
        // dd($response[0]->user->full_name);
        // dd($request->Count());
        $numb = (count($response));
        //dd($response);
        return view('chef.chef-all-categories')->with('allcategories', $response);


    }

    public function get_meal_recomend($id)
    {

        $item = Menu_Item::find($id);
//dd($item);
        if ($item->verified == true) {
            $item->verified = false;
        } else {

            $item->verified = true;

        }
        if ($item->save()) {
            return Redirect::back();
        }


    }

    public function get_meal_delete($id)
    {


        $item = Menu_Item::find($id);
        if ($item->delete()) {
            return Redirect::back();
        } else {

        }

    }
     public function delete_category($id){

        $cate = Menu::find($id);
        $cate->delete($id) ;

        return Redirect::back();
     }
}
