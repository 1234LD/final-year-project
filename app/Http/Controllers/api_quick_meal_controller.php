<?php

namespace App\Http\Controllers;

use App\chef;
use App\Http\Resources\menu_itemResource;
use App\Http\Resources\menuResource;
use App\Http\Resources\quick_mealResource;
use App\Item_Recipes;
use App\Jobs\freeQuickMeal;
use App\Menu;
use App\Menu_Item;
use App\users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Item_Images;
use App\Jobs\expireQuickMeal;
use Illuminate\Support\Facades\Session;

class api_quick_meal_controller extends Controller
{
    /**
     * Method to store the quick meal ...
     */
    public function post_quick_meal(Request $request)
    {

//        dd($request->all());
        $menu = Menu::where('category_name', config('constants.DEFAULT_QUICK-MEAL_CATEGORY'))->first();

        // Make the user as chef and set the type as quick_chef ...

        if ($request->input('user_type') == config('constants.DEFAULT_GENERAL_TYPE') ||
            $request->input('user_type') == config('constants.DEFAULT_CUSTOMER_TYPE')) {
            $user = users::find($request->input('user_id'));

            if ($user) {
                $user->usertype = config('constants.DEFAULT_QUICK-CHEF_TYPE');
                $user->save();

                $chef = chef::create([
                    'online_status' => 'online',
                    'phoneNumber' => $request->input('phone_number'),
                ]);

                $newUser = users::findOrFail($user->id)->chef()->save($chef);
//                $user->chef()->save($chef);

                $menu_item = Menu_Item::create([
                    'chef_id' => $chef->id,
                    'item_name' => $request->input('item_name'),
                    'price' => $request->input('price'),
                    'item_quantity' => $request->input('quantity')
                ]);

                Menu::findOrFail($menu->id)->menu_item()->save($menu_item);

                $description = Item_Recipes::create([
                    'description' => $request->input('description')
                ]);

                Menu_Item::find($menu_item->id)->item_recipe()->save($description);

                $item_images = [];

                if($request->input('item_image') != null)
                {
                    foreach ($request->input('item_image') as $image) {
                        $image_file = $image;
                        $image_file->move(public_path('/item_images') . '/', $image_file->getClientOriginalName());

                        $item_images [] = $image_file->getClientOriginalName();

                    }

                    $it = implode('^', $item_images);
                    $item_image = new Item_Images([
                        'item_image' => $it,
                        'menu_id' => $menu->id
                    ]);

//                $item_images [] = new Item_Images([
//                    'item_image' => $image_file->getClientOriginalName(),
//                    'menu_id' => $menu->id
//                ]);

                    Menu_Item::find($menu_item->id)->item_image()->save($item_image);
                }

                if($request->input('switch'))
                {
                    date_default_timezone_set('Asia/Karachi');

                    $job = (new freeQuickMeal($request->input('username'),$menu_item->id))->delay(now()->addMinutes(3));

                    $this->dispatch($job);

//                return "yess triggered";
                }

                date_default_timezone_set('Asia/Karachi');

                $job = (new expireQuickMeal($menu_item->id))->delay(now()->addMinutes(5));

                $this->dispatch($job);

//                return new menu_itemResource($menu_item);
             Session::forget('user_type');
             Session::put('user_type',$newUser->usertype);
                $response = [
                    'msg' => 'Your quick meal has been posted successfully',
                    'code' => 200
                ];
                return response($response);
            }
        } else {
            // working as a chef type ...

            $chef = chef::where('users_id', $request->input('user_id'))->first();

            $menu_item = Menu_Item::create([
                'chef_id' => $chef->id,
                'item_name' => $request->input('item_name'),
                'price' => $request->input('price'),
                'item_quantity' => $request->input('quantity')
            ]);

            Menu::findOrFail($menu->id)->menu_item()->save($menu_item);

            $description = Item_Recipes::create([
                'description' => $request->input('description')
            ]);

            Menu_Item::find($menu_item->id)->item_recipe()->save($description);


            if($request->input('item_image'))
            {
                $item_images = [];

                foreach ($request->input('item_image') as $image) {
                    $image_file = $image;
                    $image_file->move(public_path('/item_images') . '/', $image_file->getClientOriginalName());

                    $item_images [] = $image_file->getClientOriginalName();

                }

                $it = implode('^', $item_images);
                $item_image = new Item_Images([
                    'item_image' => $it,
                    'menu_id' => $menu->id
                ]);

//                $item_images [] = new Item_Images([
//                    'item_image' => $image_file->getClientOriginalName(),
//                    'menu_id' => $menu->id
//                ]);

                Menu_Item::find($menu_item->id)->item_image()->save($item_image);
            }


            if($request->input('switch'))
            {
                date_default_timezone_set('Asia/Karachi');

                $job = (new freeQuickMeal($request->input('username'),$menu_item->id))->delay(now()->addMinutes(3));

                $this->dispatch($job);

//                return "yess triggered";
            }

            date_default_timezone_set('Asia/Karachi');

            $job = (new expireQuickMeal($menu_item->id))->delay(now()->addMinutes(5));

            $this->dispatch($job);

//          return new menu_itemResource($menu_item);

            $response = [
                'msg' => 'Your quick meal has been posted successfully',
                'code' => 200
            ];

            return response($response);
        }
    }

    /**
     * Method to Update the quantity of Quick Meal ...
     */
    public function update_meal_quantity(Request $request)
    {
        $menu_item = Menu_Item::find($request->input('meal_id'));

        if ($menu_item) {
            $menu_item->quantity = $request->input('updated_quantity');

            if ($menu_item->save()) {
                return new quick_mealResource($menu_item);
            }
        }
    }

    /**
     * Request on finishing the countdown timer ...
     * Method to Update the price of Quick Meal ...
     */
    public function update_meal_price($username, $id)
    {
        $user = users::where('username', $username)->first();
        $chef = chef::where('users_id', $user->id)->first();

        $menu_item = Menu_Item::where('chef_id', $chef->id)
            ->where('id', $id)->first();

        if ($menu_item) {
            $menu_item->price = 0;

            if ($menu_item->save()) {
                $response = [
                    'msg' => 'Your quick meal price has been updated',
                    'code' => 200
                ];
                return response($response);
            }
        }
    }



    /**
     * Method to get the details of the quick meal ...
     */
//    public function edit_quick_meal($username, $id)
//    {
//        $user = users::where('username',$username)->first();
//        $chef = chef::where('users_id',$user->id)->first();
//
//        $menu_item = Menu_Item::where('chef_id',$chef->id)
//            ->where('id',$id)->first();
//
//        if($menu_item)
//        {
//
//        }
//
//    }

    public function update_quick_meal(Request $request)
    {

//        dd($request->all());
        $user = users::where('username',$request->input('username'))->first();

        $chef = chef::where('users_id',$user->id)->first();

        $chef->phoneNumber = $request->input('phone_number');

        $chef->save();

//        dd($chef);
        $menu_item = Menu_Item::where('chef_id',$chef->id)
                            ->where('id',$request->input('menu_item_id'))->first();

        $menu_item->item_name = $request->input('item_name');
        $menu_item->price = $request->input('price');
        $menu_item->item_quantity = $request->input('quantity');

        chef::find($chef->id)->menu_item()->save($menu_item);

        $recipe = Item_Recipes::where('menu__item_id',$menu_item->id)->first();

        if($recipe)
        {
            $recipe->description = $request->input('description');

            Menu_Item::find($menu_item->id)->item_recipe()->save($recipe);
        }
        else {
            $recipe = new Item_Recipes([
                'description' => $request->input('description')
            ]);
            Menu_Item::find($menu_item->id)->item_recipe()->save($recipe);
        }

        if($request->input('item_image') != null)
        {
            $item_images = [];


            foreach ($request->input('item_image') as $image)
            {
                $image_file = $image;
                $image_file->move(public_path('/item_images') . '/', $image_file->getClientOriginalName());

                $item_images [] = $image_file->getClientOriginalName();

            }


            $menu = Menu::where('category_name',config('constants.DEFAULT_QUICK-MEAL_CATEGORY'))->first();

            $it = implode('^',$item_images);

            $item_image = Item_Images::where('menu__item_id',$menu_item->id)->first();

            if($item_image)
            {
                $item_image->item_image = $it;
                Menu_Item::find($menu_item->id)->item_image()->save($item_image);
            }
            else
            {
                $item_image = new Item_Images([
                    'item_image' => $it,
                    'menu_id' => $menu->id
                ]);

                Menu_Item::find($menu_item->id)->item_image()->save($item_image);
            }
        }

        $response = [
            'msg' => 'Your quick meal has been updated successfully',
            'code' => 200
        ];
        return response($response);
    }

    /**
     * Method to Drop quick meal ...
     */
    public function drop_quick_meal($id)
    {
        $menu_item = Menu_Item::where('id',$id)->first();
//        $menu_item = Menu_Item::find($id);

        if($menu_item)
        {
            $menu_item->meal_status = false;

            if($menu_item->save())
            {
                $response = [
                    'msg' => 'Your quick has been deleted',
                    'code' => 200
                ];
                return response($response);
//                return new quick_mealResource($menu_item);
            }
        }
    }
}
