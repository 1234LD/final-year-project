<?php

namespace App\Http\Controllers;

use App\Menu_Item;
use App\rating;
use App\Record;
use App\users;
use App\Customer;
use Illuminate\Http\Request;
use App\Orders;
use App\OrderLine_items;
use App\chef;
use Illuminate\Support\Carbon;


class api_viewBuild_controller extends Controller
{
    /**
     * Method to Build the data for the Index View ...
     */
    public function build_index_data()
    {
        $orders = Orders::where('order_status', config('constants.DEFAULT_ORDER_DELIVERED'))
            ->count();

        $meal = OrderLine_items::join('orders', 'orders.id', '=', 'order_line_items.order_id')
            ->where('orders.order_status', '=', config('constants.DEFAULT_ORDER_DELIVERED'))
            ->count();

        $chef = chef::all()->count();

        $meal_list = Menu_Item::with(['item_image' => function ($query) {
            $query->get();
        }])
            ->join('order_line_items', 'menu_items.id', '=', 'order_line_items.menu__item_id')
            ->join('chef', 'menu_items.chef_id', '=', 'chef.id')
            ->join('users', 'chef.users_id', '=', 'users.id')
            ->join('orders', 'orders.id', '=', 'order_line_items.order_id')
            ->where('orders.order_status', '=', config('constants.DEFAULT_ORDER_DELIVERED'))
            ->selectRaw('menu_items.id, item_name, full_name, username, menu_items.rating, chef.verify, city, state, item_time')
            ->groupBy('menu_items.id', 'item_name', 'full_name', 'username', 'menu_items.rating', 'chef.verify', 'city', 'state', 'item_time')
            ->orderBy('menu_items.rating', 'DESC')
            ->take(5)
            ->get();


          $chef_list = chef::with(['user' => function($query) { $query->where('account_status','=',true); }])
                            ->with(['orders' => function($query) { $query->get(); }])
                            ->with(['menu_items' => function($query) { $query->get(); } ])
                            ->orderBy('chef_rating','DESC')
                            ->take(5)
                            ->get();
//
//        $chef_list = chef::with(['user' => function ($query) {
//            $query->where('account_status', '=', true);
//        }])
//            ->with(['orders' => function ($query) {
//                $query->get();
//            }])
//            ->with(['menu_items' => function ($query) {
//                $query->get();
//            }])
//            ->orderBy('rating', 'DESC')
//            ->take(5)
//            ->get();
// merge conflict

//        $getChefOrder = [];
//        foreach($chef_list as $chef)
//        {
//            $getChefOrder[$chef->id] = Orders::where('chef_id','=',$chef->id)->count();
//        }

        $renderedArray = [
            'orders' => $orders,
            'meals' => $meal,
            'chefs' => $chef,
            'meal_list' => $meal_list,
            'chef_list' => $chef_list,
        ];

        return response($renderedArray);
    }

    /**
     * Method to Build the data for the Chef Panel View ...
     */
    public function build_chef_panel_data($id)
    {

        $renderedArray = [];
        // Order served ...
        $chef = users::find($id)->chef;

        $orderServed = Orders::where('chef_id', '=', $chef->id)
            ->where('order_status', '=', config('constants.DEFAULT_ORDER_DELIVERED'))
            ->count();
        //     return $orderServed;

        $renderedArray['orders'] = $orderServed;
        // Total Revenue per day (gross rate) ...
        date_default_timezone_set("Asia/Karachi");

        $order = Orders::where('chef_id', $chef->id)
            ->whereDate('created_at', Carbon::today())->count();

        if ($order > 0) {
            date_default_timezone_set("Asia/Karachi");
            $revenue = Orders::where('chef_id', $chef->id)
                ->whereDate('orders.created_at', Carbon::today())
                ->selectRaw('SUM(orders.grand_total) as Total')
                ->get();

            $renderedArray['revenue'] = $revenue;

            // Total sale of meals per day ...
            $meal_sale = OrderLine_items::join('orders', 'order_line_items.order_id', '=', 'orders.id')
                ->join('menu_items', 'menu_items.id', '=', 'order_line_items.menu__item_id')
                ->where('orders.chef_id', '=', $chef->id)
                ->where('orders.order_status', '=', config('constants.DEFAULT_ORDER_DELIVERED'))
                ->whereDate('orders.created_at', Carbon::today())
                ->selectRaw('sum(order_line_items.item_price) as price, count(orders.id) as total_order, menu_items.id, menu_items.item_name , menu_items.rating')
                ->groupBy('menu_items.id', 'menu_items.item_name', 'menu_items.rating')
                ->orderBy('price', 'desc')
                ->take(5)
                ->get();

            $renderedArray['meal_sale'] = $meal_sale;
        } else {
            $renderedArray['revenue'] = 0;
            $renderedArray['meal_sale'] = null;
        }

        // Delivery/Take away analysis ...
        $totalOrders = Orders::where('chef_id', '=', $chef->id)
            ->where('order_status', '=', config('constants.DEFAULT_ORDER_DELIVERED'))
            ->count();


        $total_self_delivery = Orders::where('chef_id', '=', $chef->id)
            ->where('order_status', '=', config('constants.DEFAULT_ORDER_DELIVERED'))
            ->where('delivery_type', '=', config('constants.DEFAULT_SELF_DELIVERY'))
            ->count();

        if ($total_self_delivery != null) {
            $self_delivery_percentage = ($total_self_delivery / $totalOrders) * 100;
        } else {
            $self_delivery_percentage = 0;
        }

        $total_take_away = Orders::where('chef_id', '=', $chef->id)
            ->where('order_status', '=', config('constants.DEFAULT_ORDER_DELIVERED'))
            ->where('delivery_type', '=', config('constants.DEFAULT_TAKE-AWAY_DELIVERY'))
            ->count();

        if ($total_take_away != null) {
            $take_away_percentage = ($total_take_away / $totalOrders) * 100;
        } else {
            $take_away_percentage = 0;
        }

        $analysis = array(
            'self_delivery' => $self_delivery_percentage,
            'take_away' => $take_away_percentage
        );

        $renderedArray['analysis'] = $analysis;
        // Peak Time Slot ...


        // Quickest Meal ...
        $quick_meal = Menu_Item::join('chef', 'chef.id', '=', 'menu_items.chef_id')
            ->join('users', 'users.id', '=', 'chef.users_id')
            ->where('users.account_status', '=', true)
            ->where('menu_items.item_time', '<=', 30)
            ->selectRaw('chef.id as chefID, menu_items.id as itemID, users.full_name, menu_items.item_time, menu_items.item_name')
            ->orderBy('menu_items.item_time', 'asc')
            ->take(5)
            ->get();

        $renderedArray['quick_meal'] = $quick_meal;

        // Cheapest Meal ...
        $cheap_meal = Menu_Item::join('chef', 'chef.id', '=', 'menu_items.chef_id')
            ->join('users', 'users.id', '=', 'chef.users_id')
            ->where('users.account_status', '=', true)
            ->selectRaw('chef.id as chefID, menu_items.id as itemID, users.full_name, menu_items.price, menu_items.item_name')
            ->orderBy('menu_items.price', 'asc')
            ->take(5)
            ->get();

        $renderedArray['cheap_meal'] = $cheap_meal;

//        return $cheap_meal;

        // Top meals on Foodpole ...
        $top_meals = Menu_Item::with(['item_image' => function ($query) {
            $query->first();
        }])
            ->join('chef', 'menu_items.chef_id', '=', 'chef.id')
            ->join('users', 'users.id', '=', 'chef.users_id')
            ->where('menu_items.meal_status', '=', true)
            ->where('users.account_status', '=', true)
            ->selectRaw('menu_items.id, item_name, full_name, username, menu_items.rating, menu_items.price, item_time, chef.delivery_type, menu_items.verified')
            //     ->groupBy( 'item_name', 'full_name', 'menu_items.rating', 'menu_items.price', 'item_time', 'chef.delivery_type')
            ->orderBy('menu_items.rating', 'desc')
            ->take(3)
            ->get();

        $renderedArray['top_meal'] = $top_meals;

//        return $top_meals;

//        $renderedArray = [
//            'orders' => $orderServed,
//            'revenue' => $revenue,
//      //      'meal_sale' => $meal_sale,
//            'analysis' => $analysis,
//            'quick_meal' => $quick_meal,
//            'cheap_meal' => $cheap_meal,
//            'top_meal' => $top_meals,
//        ];

        return response($renderedArray);

    }


    public function build_admin_dashboard(){

        $renderedArray = [];

        $orderServed = Orders::where('order_status', '=', config('constants.DEFAULT_ORDER_DELIVERED'))->count(); // served orders
        $renderedArray['orders_served'] = $orderServed;
        $chef = chef::all()->count();

        $renderedArray['total_chef'] = $chef;
        $customer =Customer::all()->count();

        $renderedArray['total_customer'] = $customer;

        $revenue = Orders::selectRaw('SUM(orders.grand_total) as Total')->get();

        $renderedArray['revenue'] = $revenue;
        $user = users::where('usertype', '=', config('constants.DEFAULT_GENERAL_TYPE'))->count();



        $renderedArray['total_users'] = $user;

        $review = rating::all()->count();

        $renderedArray['total_rating'] = $review;

        $online_chef =chef::where('online_status', '=', config('constants.DEFAULT_STATUS_ONLINE'))->count();

        $renderedArray['online_chef'] = $online_chef;

//
        // Top meals on Foodpole ...
//        $top_meals = Menu_Item::with(['item_image' => function ($query) {
//            $query->first();
//        }])
//            ->join('chef', 'menu_items.chef_id', '=', 'chef.id')
//            ->join('users', 'users.id', '=', 'chef.users_id')
//            ->where('menu_items.meal_status', '=', true)
//            ->where('users.account_status', '=', true)
//            ->selectRaw('menu_items.id, item_name, full_name, menu_items.rating, menu_items.price, item_time, chef.delivery_type, menu_items.verified')
//            //     ->groupBy( 'item_name', 'full_name', 'menu_items.rating', 'menu_items.price', 'item_time', 'chef.delivery_type')
//            ->orderBy('menu_items.rating', 'desc')
//            ->take(3)
//            ->get();
// tpp 5 chef with respect  to orders
        $chef_orders = chef::join('orders', 'chef.id', '=', 'orders.chef_id')
            ->join('users', 'users.id', '=', 'chef.users_id')
                    //join('menu')
            ->selectRaw('count(orders.id) as total_order, full_name')
            ->groupBy('full_name')
           // ->orderBy('total_order', 'desc')
            ->take(5)
            ->get();
        $renderedArray['top_chef_orders'] = $chef_orders;

//top 5 customer with respect to orders

        $customer_orders =customer::join('orders', 'customer.id', '=', 'orders.customer_id')
            ->join('users', 'users.id', '=', 'customer.users_id')
            //join('menu')
            ->selectRaw('count(orders.id) as total_order, full_name')
            ->groupBy('full_name')
            // ->orderBy('total_order', 'desc')
            ->take(5)
            ->get();
        $renderedArray['top_customer_orders'] = $customer_orders;

   //top 5 chef with respect to  ratings

        $chef_rated =chef::join('users', 'users.id', '=', 'chef.users_id')

            ->selectRaw('chef_rating, full_name')
            ->orderBy('chef_rating', 'desc', 'full_name', '')

            ->take(5)
            ->get();
        $renderedArray['top_chef_rated'] = $chef_rated;

        return response($renderedArray);
    }

    public function build_user_data()
    {
        $renderedArray = [];

        // total meals number of meals delivered ...
        $meals_ordered = OrderLine_items::join('orders','orders.id','=','order_line_items.order_id')
                                        ->where('orders.order_status',config('constants.DEFAULT_ORDER_DELIVERED'))
                                        ->selectRaw('count(order_line_items.id) as total')
                                       ->count();

        $renderedArray['meals_ordered'] = $meals_ordered;

//        dd($meals_ordered);

        // Meals searched today ...
        date_default_timezone_set('Asia/Karachi');
        $total_searched = Record::whereDate('created_at',Carbon::today())
                                ->selectRaw('search_count')->first();

        $renderedArray['searched'] = $total_searched;


        // Meals Reviewed ...
        $meals_reviewed = rating::all()->count();

        $renderedArray['meals_reviewed'] = $meals_reviewed;


        // total chefs ...
        $renderedArray['total_chefs'] = chef::all()->count();

        // Quickest Meal ...
        $quick_meal = Menu_Item::join('chef', 'chef.id', '=', 'menu_items.chef_id')
            ->join('users', 'users.id', '=', 'chef.users_id')
            ->where('users.account_status', '=', true)
            ->where('menu_items.item_time', '<=', 30)
            ->selectRaw('chef.id as chefID, menu_items.id as itemID, users.full_name, menu_items.item_time, menu_items.item_name')
            ->orderBy('menu_items.item_time', 'asc')
            ->take(5)
            ->get();

        $renderedArray['quick_meal'] = $quick_meal;

        // Cheapest Meal ...
        $cheap_meal = Menu_Item::join('chef', 'chef.id', '=', 'menu_items.chef_id')
            ->join('users', 'users.id', '=', 'chef.users_id')
            ->where('users.account_status', '=', true)
            ->selectRaw('chef.id as chefID, menu_items.id as itemID, users.full_name, menu_items.price, menu_items.item_name')
            ->orderBy('menu_items.price', 'asc')
            ->take(5)
            ->get();

        $renderedArray['cheap_meal'] = $cheap_meal;

        return response($renderedArray);
    }



}