<?php

namespace App\Http\Controllers;

use App\chef;
use App\Http\Resources\chef_resource;
use App\Http\Resources\loginResource;
use App\Http\Resources\adminResource;
use App\Http\Resources\menu_itemResource;
use App\Http\Resources\orderCollection;
use App\Http\Resources\records_Resource;
use App\Menu;
use App\Menu_Item;
use App\OrderLine_items;
use App\Orders;
use App\Record;
use App\users;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class api_admin_controller extends Controller
{

    /**
     * Method to retrieve the total meal list served/day
     */
    public function get_total_sales()
    {
        // where clauses (order status = delivered , date = today)
        // along with the chefs and their meals from order table ...

        date_default_timezone_set("Asia/Karachi");

//        $order = Orders::leftJoin('order_line_items' ,'order_line_items.order_id' ,'=' ,'orders.id')
//                        ->where('order_status','=','delivered')
//                        ->where('orders.created_at','=',Carbon::today())
////                        ->selectRaw('order_line_items.id, count(order_line_items.id) as itemCount')
//                        ->groupBy('order_line_items.id')
//                        ->get();

        $order = OrderLine_items::leftjoin('orders','order_line_items.order_id','=','orders.id')
                                ->where('orders.order_status','=','delivered')
                                ->where('orders.created_at','=',Carbon::today())
                                ->count();

//        $result = count($order);

        return $order;
    }

    /**
     * Method to get the profit per day ...
     */
    public function get_profit()
    {
        // pending ....
    }


    /**
     * Method to get the total Users registered per day ...
     */
    public function get_registered_users()
    {
        date_default_timezone_set("Asia/Karachi");

        $users = users::whereDate('created_at','=',Carbon::today())->count();

//       $result = count($users);

        return $users;
    }

    /**
     * Method to get the total chef online at the moment ...
     */
    public function get_online_chef()
    {
        $chef = chef::where('online_status','=','online')->get();

        if($chef) {
//        $result = count($chef);

            return chef_resource::collection($chef);
        }
        else
        {
            $response = [
                'msg' => 'Sorry no chef is online',
                'code' => 404
            ];
            return \response($response);
        }
    }

    /**
     * Method to increment the index of the items searched ...
     */
    public function search()
    {
        date_default_timezone_set("Asia/Karachi");

        $index = Record::whereDate('created_at',Carbon::today())->first();

        if(is_null($index))
        {
            $newSearch = Record::create([
                'search_count' => 1
            ]);
            $newSearch->save();
            return new records_Resource($newSearch);
        }
        else
        {
            $index->search_count += 1;
            $index->save();
            return new records_Resource($index);
        }
    }


    /**
     * Method to get the total meal searched today ...
     */
    public function get_total_meals_searched()
    {
        date_default_timezone_set("Asia/Karachi");

        $total = Record::whereDate('created_at',Carbon::today())->first();

        return $total->search_count;
    }


    /**
     * Method to get the top meals based on number of orders ....
     */
    public function get_top_meals()
    {

        // All menu items who are having order line items ...
        /*
         * total orders of each item ..
         * then those items who are having top number of orders ...
         */

        $menu_item = OrderLine_items::join('menu_items','menu_items.id','=','order_line_items.menu__item_id')
                                    ->join('chef','menu_items.chef_id','=','chef.id')
                                    ->join('users','chef.users_id','=','users.id')
                                    ->join('orders','orders.id','=','order_line_items.order_id')
                                    ->where('orders.order_status','=', config('constants.DEFAULT_ORDER_DELIVERED'))
                                    ->selectRaw('count(*) as total, menu_items.id, item_name, username, menu_items.rating')
                                    ->groupBy('menu_items.id', 'item_name', 'username', 'menu_items.rating')
                                    ->orderBy('total','DESC')
                                    ->take(5)
                                    ->get();

        return $menu_item;
    }

    /**
     * Method to get the top users based on the number of orders ...
     */
    public function get_top_users()
    {
        $customers = Orders::join('customer','customer.id','=','orders.customer_id')
                        ->join('users','users.id','=','customer.users_id')
                        ->where('orders.order_status','=', config('constants.DEFAULT_ORDER_DELIVERED'))
                        ->selectRaw('count(*) as total_order , orders.id , username')
                        ->groupBy('orders.id','username')
                        ->orderBy('total_order','DESC')
                        ->take(5)
                        ->get();

        return $customers;
    }

    /**
     * Method to get the top chefs based on the number of orders ...
     */
    public function get_top_ordered_chefs()
    {
        $chefs = Orders::join('chef','chef.id','=','orders.chef_id')
            ->join('users','users.id','=','chef.users_id')
            ->where('orders.order_status','=', config('constants.DEFAULT_ORDER_DELIVERED'))
            ->selectRaw('count(*) as total_order , orders.id , username')
            ->groupBy('orders.id','username')
            ->orderBy('total_order','DESC')
            ->take(5)
            ->get();

        return $chefs;
    }

    /**
     * Method to get the top chefs based on the top rating ...
     */
    public function get_top_rated_chefs()
    {
        $chefs = chef::join('users','users.id','=','chef.users_id')
                        ->where('account_status','=',true)
                        ->orderBy('rating','DESC')
                        ->take(5)
                        ->get();

        return $chefs;
    }

    /**
     * Method to get the Order
     */
    public function get_order($id)
    {
        $order = Orders::find($id)->get();

        if(!is_null($order))
        {
            return new orderCollection(Orders::where('id',$id)->with('order_line_items')->get());
        }
        else
        {
            $response = [
                'msg' => 'Sorry! these credentials does not meet with records ... ',
                'code' => 404
            ];
            return \response($response);
        }
    }

    /**
     * Method to get all orders ...
     */
    public function get_all_orders()
    {
        return new orderCollection(Orders::with('order_line_items','customer','chef')->get());
    }

    /**
     * Method to get cancelled orders ...
     */
    public function get_cancelled_orders()
    {
        return new orderCollection(Orders::where('order_status',config('constants.DEFAULT_ORDER_CANCELLED'))->with('order_line_items')->get());
    }

    /**
     * Method to search the User ...
     */
    public function search_user($id)
    {
        $user = users::find($id);

        if($user)
        {
            return new loginResource($user);
        }
        else
        {
            $response = [
                'msg' => 'Sorry! User not found ... ',
                'code' => 404
            ];
            return \response($response);
        }
    }

    /**
     * Method to view the all Users ....
     */
    public function view_all_users()
    {
        $userInfo = users::all();
//        return $userInfo->count();
        return loginResource::collection($userInfo);
    }

    /**
     * Method to verify the menu item ...
     */
    public function verify_menu_item(Request $request)
    {
        $menu_item = Menu_Item::find($request->input('item_id'));

        if(!is_null($menu_item))
        {
            $menu_item->verified = true;
            if($menu_item->save())
            {
                return new menu_itemResource($menu_item);
            }
        }
    }

    /**
     * Method to Unverify the menu item ...
     */
    public function un_verify_menu_item(Request $request)
    {
        $menu_item = Menu_Item::find($request->input('item_id'));

        if(!is_null($menu_item))
        {
            $menu_item->verified = false;
            if($menu_item->save())
            {
                return new menu_itemResource($menu_item);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
//        $user = users::findOrFail($id);

        $admin = Admin::where('user_id','=',$request->input('user_id'))->first();

        if(is_null($admin))
        {
            $response = [
                'msg' => 'These credentials does not meet with the records ...',
                'code' => 404
            ];
            return \response($response);
        }
        else
        {
            $status = $request->input('status');

            if($admin->online_status == $status)
            {
                echo "Plesae choose different one ";
            }
            else
            {
                $admin->online_status = $status;

                if($admin->save())
                {
                    return new adminResource($admin);
                }
            }
        }
    }

    /**
     * Method to block the user or chef ...
     */
    public function block_user(Request $request)
    {
        $info = $request->all();

        $user = users::find($request->input('block_id'));

        if(is_null($user))
        {
            return \response('These credentials does not meet with the records ...','203');
        }
        else {
            if ($user->usertype == config('constants.DEFAULT_ADMIN_TYPE')) {
                return \response('Sorry! you are not authorized to block an admin account ...', '203');
            } else {
                $admin_user = users::find($request->input('admin_id'));
                if (is_null($admin_user)) {
                    return \response('These credentials does not meet with the records ...', '203');
                } else {
                    if ($user->account_status == 'false') {
                        return \response('This user is already blocked', '500');
                    } else {
                        $user->account_status = false;

                        if ($user->save()) {
                            return new loginResource($user);
                        } else {
                            return response('Whoops there wer some problems with server', '404');
                        }
                    }
                }
            }
        }
    }

    /**
     * Method to unblock the user chef or customer ...
     */
    public function unblock_user(Request $request)
    {
        $info = $request->all();

        $user = users::find($request->input('unblock_id'));

        if(is_null($user))
        {
            return \response('These credentials does not meet with the records ...','203');
        }
        else {
            if ($user->usertype == config('constants.DEFAULT_ADMIN_TYPE')) {
                return \response('Sorry! you are not authorized to unblock an admin account ...', '203');
            } else {
                $admin_user = users::find($request->input('admin_id'));
                if (is_null($admin_user)) {
                    return \response('These credentials does not meet with the records ...', '203');
                } else {
                    if ($user->account_status == 'true') {
                        return \response("This user's account is already activated", '500');
                    } else {
                        $user->account_status = true;

                        if ($user->save()) {
                            return new loginResource($user);
                        } else {
                            return response('Whoops there wer some problems with server', '404');
                        }
                    }
                }
            }
        }
    }
}
