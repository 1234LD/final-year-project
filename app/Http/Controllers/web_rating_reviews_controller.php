<?php

namespace App\Http\Controllers;

use App\chef;
use App\Menu_Item;
use App\OrderLine_items;
use App\Orders;
use App\rating;
use Illuminate\Http\Request;
use App\Orderstatus_Reason ;
use Illuminate\Support\Facades\Redirect;
class web_rating_reviews_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//
    }

    public function check(Request $request,$id)
    {
//dd($request->all()) ;
        $order = Orders::find($id) ;

           $menu_item_id =OrderLine_items::where('order_id',$order->id)->get() ;
      //     dd($menu_item_id);
//        $data = [
//            'rating'=> $request->input('rating'),
//            'feedback'=> $_POST['feedback']
//        ];
//dd($request->all());
//foreach ($request->item as $item_line){
//
//    $item_line ;
//    endforeach}
//

        $data = [
            'feedback' => $request->input('feedback') ,
            'order_id' => $order->id,
            'customerid'=>$order->customer_id,
            'chef_id' => $order->chef_id,
            'menu_item' => $request->input('menu_item'),
            'rating_array' => $request->input('order_rating')
        ];
//dd($data);
        $req =Request::create('api/ratings','POST',$data);
        $response = app()->handle($req)->getContent();

        $decode = json_decode($response);
//dd($decode);
        if(is_null($decode)){

            echo " return response null" ;
        }
        else {
//            $chef = chef::find(1);
//
//            dd($chef->getRating());
            return Redirect::back();

      }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public  function get_order_flag(Request $request,$id){
        $order = Orders::find($id) ;
        $order->flag = true ;

        if($order->save()) {

            $order_reason= new rating();

            $order_reason->feedback = $request->input('flag_reason');
            $order_reason->orders_id = $order->id ;
            $order_reason->customer_id = $order->customer_id ;
            $order_reason->chef_id = $order->chef_id ;
            if($order_reason->save()){

                return Redirect::back();
            }
        }
else{
     return "order does not exist " ;


}


    }
}
