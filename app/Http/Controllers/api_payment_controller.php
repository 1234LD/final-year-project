<?php

namespace App\Http\Controllers;

use App\Http\Resources\payment_Collection;
use App\paymentDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\users;
use App\chef;
use App\Orders ;
use  App\Http\Resources\order_Resource;

class api_payment_controller extends Controller
{
    //
    public function maproute(){

        $data =array('name'=> Session::get('username')) ;

        Mail::send('routemap', $data,function ($message){

            $message->to(Session::get('user_email'))->subject('Delivery email') ;
            $message->from('food.pole4@gmail.com','foodpole') ;


        }) ;
        $flag = 'true' ;
        return $flag;
    }

    /**
     * Method to get the paid orders ...
     */
    public function get_paid_orders($id,$username)
    {
        $user = users::where('id','=',$id)
            ->where('username','=',$username)
            ->where('account_status',true)->get();

        if(!is_null($user))
        {
            $chef = chef::where('users_id','=',$id)
                ->first();

            return new payment_Collection(paymentDetail::where('chef_id',$chef->id)->where('payment_status',config('constants.DEFAULT_PAYMENT_PAID'))->with('order')->get());
        }
        else
        {
            $response = ['msg'=>'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response($response);
        }
    }

    /**
     * Method to get the unpaid orders ...
     */
    public function get_unpaid_orders($id,$username)
    {
        $user = users::where('id','=',$id)
            ->where('username','=',$username)
            ->where('account_status',true)->get();

        if(!is_null($user))
        {
            $chef = chef::where('users_id','=',$id)
                ->first();

            return new payment_Collection(paymentDetail::where('chef_id',$chef->id)->where('payment_status',config('constants.DEFAULT_PAYMENT_UNPAID'))->with('order')->get());
        }
        else
        {
            $response = ['msg'=>'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response($response);
        }
    }
    public function get_admin_paid_orders(){


            return new payment_Collection(paymentDetail::where('payment_status',config('constants.DEFAULT_PAYMENT_PAID'))->with('order')->get());

    }
public function get_admin_unpaid_orders(){




        return new payment_Collection(paymentDetail::where('payment_status',config('constants.DEFAULT_PAYMENT_UNPAID'))->with('order')->get());


}
public function  location_info($id) {

//dd($id) ;
//   return  new order_Resource($id) ;
 //   return new order_Resource(Orders::where('id',$id)->get());

}

}

