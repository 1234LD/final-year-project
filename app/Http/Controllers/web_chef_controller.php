<?php

namespace App\Http\Controllers;

use App\Chef;

use App\Http\Middleware\handle;
use App\Http\Resources\chef_resource;
use App\Orderstatus_Reason;
use App\users;
use App\orders ;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\MailController;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class web_chef_controller extends Controller
{

    /**
     * Method to get the complets chef profile ...
     * @param $id
     * @param $username
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Psr\Http\Message\ResponseInterface
     */
    public function view_complete_profile($id, $username)
    {
//        $request = Request::create('api/chef_profile/'.$id,'GET');
//        $response = app()->handle($request)->getContent();


        $url = 'http://localhost/foodpoleAPI/public/api/chef_profile/'.$id.'/'.$username;
        $client = new Client();
        $response = $client->get($url);

        $chef_menu = json_decode($response->getBody(),false);

        if(is_null($chef_menu))
        {
            return $response;
        }
        else
        {
//            dd( $chef_menu->data[0]->ratings);
            return view('chef-page')->with('chef_menu',$chef_menu);
        }

    }



    public function load(){

        chef::all();
        $nearby = new web_nearby_search_controller(); // intializing
      $shiw =  $nearby->scopeDistance($lat =33.632066, $lng=73.0704067 , $radius = 1, $unit = "km");
      dd($shiw);

//example usage.
        $data =chef::all() ;
//        foreach($data as $post) {
//           echo( $post->phoneNumber);
//
//        }
        return view('chef_list' );


    }
    /**
     * Method to open the chef registration form ...
     */
    public function create()
    {
        return (view('chef'));
    }

    /**
     * @throws \Exception
     */
    public function check(){

        $street=Input::get('streetaddress');
        $city = Input::get('city');
        $state = Input::get('state');
        $postalcode = Input::get('postalcode');
        $country = 'Pakistan';
        $id = Session::get('user_id');

        $address =$street.','.$city.','.$state.','.','.$postalcode.$country ;

        $formattedAddr = str_replace(' ','+',$address);
       // urlencode(strtolower($formattedAddr));
        //dd($formattedAddr);
//$address = 'pakistan';
      //  $geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false' );
//         $geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&key=AIzaSyDMShCNsf6wPh0TEq14vLG2X2uUmcFuh5E');
//        $output= json_decode($geocode);
//
//
//        $lat = $output->results[0]->geometry->location->lat;
//        $long = $output->results[0]->geometry->location->lng;

        $data = [//'us_id'=>$_POST[Session::get('user_id')],
//            'late'=>$lat ,
//            'long'=>$long,
            'cnic'=>$_POST['cnic'],
        'phoneNumber'=>$_POST['number'],
            'city'=>$_POST['city'],
            'street_address'=>$_POST['streetaddress'],
            'state'=>$_POST['state'],
            'postalcode'=>$_POST['postalcode']

        ];

        $req = Request::create('api/chef/'.$id,'POST',$data);
       $response = app()->handle($req)->getContent();

       $output = json_decode($response);

       if(is_null($output))
       {
           return $response;
       }
       else
       {
         //  return response('You have been registered successfully ... ');





$number = $output->phoneNumber ;
        //   echo "You have been registered" ;
  $mail  = new MailController() ;// commented due to University interent prob
    $mail->basic_mail();
    $mail->message_to_number($number);

 }



    }
    public function get_order_accepted ($id ,$status) {



         $order =Orders::find($id);
        $order->order_status = $status;

            //config('constants.DEFAULT_ORDER_PENDING');

//        $id = Session::get('user_id') ;
//
//        $idchef = users::find($id)->chef;
//
//        $status = Orders::find($idchef)->where('order_status', config('constants.DEFAULT_ORDER_PENDING'));
//        $status->order_status='hhahh';
  //    dd($order);
        if($order->save()){

         return   Redirect::back();
        }
else {

    return 'status never changed';
}
    }

    public function chef_verify_unverify($id){
 $chef =chef::find($id) ;
     if($chef->verify == true){

        $chef->verify = false ;
       $chef->save() ;
         return Redirect::back() ;
     }
     else {

          $chef->verify = true ;
         $chef->save() ;
         return Redirect::back() ;
     }

    }
    public function get_decline_reasons(Request $request,$id,$status){

       $order = Orders::find($id) ;
      // dd($order);
        if($order !=null){

               $order->order_status = $status ;
               $order->save();

        }
        else {

            return "ordr is not saving" ;
        }

       $order_reason = new Orderstatus_Reason();
        $order_reason->orders_id = $order->id ;
       $order_reason->customer_id = $order->customer_id ;
       $order_reason->chef_id  = $order->chef_id ;
       $order_reason->reason = $request->input('reason');
       $order_reason->order_status = $status ;
       if($order_reason->save()){

           return Redirect::back();

       }
       else {


           return "reason does not store in database" ;
       }




    }
}
