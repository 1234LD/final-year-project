<?php

namespace App\Http\Controllers;

use App\chef;
use App\Http\Resources\chef_resource;
use App\Menu;
use App\Menu_Item;
use App\rating;
use App\users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use Illuminate\Database\Query\Builder;
use MongoDB\Driver\Query;

class api_filter_sorting_controller extends Controller
{
    /**
     * Method to send request on API to get filtered Data ....
     */

    public $queryBuilder = null;

    public function response_filter_sort($lat, $lng)
    {
//        $latitude = 33.63483229404082;
//        $longitude = 73.06669186931151;

//        return Input::get('distance');
        $latitude = $lat;
        $longitude = $lng;

        $distance = Input::get('distance');
        $category = Input::get('category');
        $price_min = Input::get('price_min');
        $price_max = Input::get('price_max');
        $rating = Input::get('rating');
        $preparation_min_time = Input::get('preparation_min_time');
        $preparation_max_time = Input::get('preparation_max_time');
        $keyword = Input::get('keyword');
        $delivery_type = Input::get('delivery_type');

        /**
         * Building the Combinations of the Distance Filter ...
         */
        if(!is_null($distance))
        {
            $this->queryBuilder = (new chef())->newQuery();

            $this->queryBuilder->join('menu_items','chef.id','=','menu_items.chef_id')
                               ->join('menu','menu.id','=','menu_items.menu_id')
//                                ->where('menu.menu_type','=',config('constants.DEFAULT_PRIVATE_ACCESS'))
                                ->join('item_images','item_images.menu__item_id','=','menu_items.id')
                                ->join('users','users.id','=','chef.users_id');

            // Calling Category Filter ...
            $this->queryBuilder->raw($this->category_filter($this->queryBuilder,$category));

            // Calling Rating Filter ...
            $this->queryBuilder->raw($this->rating_filter($this->queryBuilder,$rating));
        }

//        /**
//         * Building the Combinations of the Price or Preparation or Keyword Filter ...
//         */
//         else if(!(is_null($price_min) || is_null($price_max)) || !(is_null($preparation_min_time) || is_null($preparation_max_time)) || !is_null($keyword) || !is_null($rating))
//        {
//            $this->queryBuilder = (new Menu_Item())->newQuery();
//
//            // Joining with Chef and User Table ...
//            $this->queryBuilder->join('chef','chef.id','=','menu_items.chef_id')
//                                ->join('users','users.id','=','chef.users_id');
//
//            // Calling Category Filter ...
//            $this->queryBuilder->raw($this->category_filter($this->queryBuilder,$category));
//        }
//
//        /**
//         * Building the Combinations of the Category Filter ...
//         */
//        else if(!is_null($category))
//        {
//            $this->queryBuilder = (new Menu())->newQuery();
//
//            // Joining with Chef , User and Menu_Item Table ...
//            $this->queryBuilder->join('chef','chef.id','=','menu.chef_id')
//                           //     ->join('menu','menu.id','=','menu_items.menu_id')
//                                ->join('users','users.id','=','chef.users_id')
//                                ->join('menu_items','menu.id','=','menu_items.menu_id')
//                                ->join('item_images','item_images.menu__item_id','=','menu_items.id')
//                                ->where('category_name','=',$category,'AND')
//                                ->where('meal_status','=',true);
//        }

        //        else if(!is_null($rating))
//        {
//            $this->queryBuilder = (new rating())->newQuery();
//
//            // Joining with Chef , User and Menu_Item Table ...
//            $this->queryBuilder->join('chef','chef.id','=','rating_reviews.chef_id')
//                                ->join('users','users.id','=','chef.users_id')
//                                ->join('menu_items','menu_items.id','=','rating_reviews.menu_item_id')
//                                ->where('rating','=',$rating);
//
//            // Calling Category Filter ...
//            $this->queryBuilder->raw($this->category_filter($this->queryBuilder,$category));
//        }

        else
        {
            return response('Sorry No filters are Selected ...','203');
        }

        // Calling Delivery Mode Filter ...
        $this->queryBuilder->raw($this->delivery_mode_filter($this->queryBuilder,$delivery_type));

        // Calling Distance Filter ...
        $this->queryBuilder->raw($this->distance_filter($this->queryBuilder,$distance,$latitude,$longitude));

        // Calling Price Filter ...
        $this->queryBuilder->raw($this->price_filter($this->queryBuilder,$price_min,$price_max));

        // Calling Keyword Filter ...
        $this->queryBuilder->raw($this->keyword_filter($this->queryBuilder,$keyword));

        // Calling Preparation Filter ...
        $this->queryBuilder->raw($this->preparation_filter($this->queryBuilder,$preparation_min_time,$preparation_max_time));

        // Calling Rating Filter ...
        $this->queryBuilder->raw($this->rating_filter($this->queryBuilder,$rating));

        // return the query builder instance with all possible combinations of Distance ...
//        $this->queryBuilder->select('chef.id as chef_id, full_name, username, menu_items.id as item_id, menu_items.rating, item_name, item_price, item_option, item_price, menu.category_name, chef.delivery_type, chef.verify, menu_item.verified ');


//       $menu_names = Menu::all('category_name');
//       $result =  $this->queryBuilder->get();
//
//        $response = [
//            'menu_names' => $menu_names,
//            'result' => $result
//        ];
//
//        return $response;
////        return $this->queryBuilder->get();

//        $menu_names = Menu::all('category_name');
//       $result =  $this->queryBuilder->get();
//
//        $response = [
//            'menu_names' => $menu_names,
//            'result' => $result
//        ];
//
//        return $response;
        return $this->queryBuilder->get();


    }

    public function delivery_mode_filter($query, $delivery_type)
    {
        if(!is_null($delivery_type))
        {
            $query->where('delivery_type','=',$delivery_type);
        }
    }

    public function distance_filter($query,$distance,$latitude,$longitude)
    {
        if(!is_null($distance)) {
            $radius = $distance;
            $unit = "km";
            $unit = ($unit === "km") ? 6378.10 : 3963.17;
            $lat = (float)$latitude;
            $lng = (float)$longitude;
            $radius = (double)$radius;

//        ->join('menu_items','chef.id','=','menu_items.chef_id')
//        ->join('users','users.id','=','chef.users_id')

            $query->having('distance', '<=', $radius)
                ->select([DB::raw("chef.id as chef_id, chef.picture as chef_image, chef.verify as chef_verify_status, users_id, latitude, longitude, cnic, chef_rating, phoneNumber, city, streetaddress, state, postalcode, delivery_type, online_status, verify,
                                ($unit * ACOS(COS(RADIANS($lat))
                                    * COS(RADIANS(latitude))
                                    * COS(RADIANS($lng) - RADIANS(longitude))
                                    + SIN(RADIANS($lat))
                                    * SIN(RADIANS(latitude)))) AS distance"), 'menu.id as menu_id', 'category_name', 'menu_items.id as menu_item_id', 'menu_items.rating as item_rating', 'verified', 'item_name', 'price', 'item_time', 'item_quantity' , 'item_option' , 'item_price', 'full_name', 'username', 'item_image'])
//                ->select(['chef.id as chef_id'])
//                ->where('online_status', '=', 'online')
                ->orderBy('distance', 'asc');
        }
    }

    public function category_filter($query , $category)
    {
        if(!is_null($category))
        {
//            if($category == config('constants.DEFAULT_QUICK-MEAL_CATEGORY') )
//        {
//            $query->join('menu','menu.id','=','menu_items.menu_id')
//                ->where('category_name','=',$category,'AND')
//                ->where('meal_status','=',true);
//        }
            $query
                ->where('menu.category_name','=',$category)
                ->where('menu_items.meal_status','=',true);
//                ->select(['chef.id as chef_id', 'users_id', 'latitude', 'longitude', 'cnic', 'chef_rating', 'phoneNumber', 'city', 'streetaddress', 'state', 'postalcode', 'delivery_type', 'online_status', 'verify',
//                            'menu.id as menu_id', 'category_name', 'menu_items.id as menu_item_id', 'menu_items.rating as item_rating', 'item_name', 'price', 'item_time', 'item_quantity' , 'item_option' , 'item_price', 'full_name', 'username']);
        }
    }

    public function price_filter($query , $price_min , $price_max)
    {
        if(!(is_null($price_min) || is_null($price_max)))
        {
            //->join('menu_items','menu.id','=','menu_items.chef_id')
            $query->where('price','>=',$price_min,'AND')
                ->where('price','<=',$price_max);
//                ->select(['chef.id as chef_id', 'users_id', 'latitude', 'longitude', 'cnic', 'chef_rating', 'phoneNumber', 'city', 'streetaddress', 'state', 'postalcode', 'delivery_type', 'online_status', 'verify',
//                    'menu.id as menu_id', 'category_name', 'menu_items.id as menu_item_id', 'menu_items.rating as item_rating', 'item_name', 'price', 'item_time', 'item_quantity' , 'item_option' , 'item_price', 'full_name', 'username']);
        }
    }

    public function rating_filter($query , $rating)
    {
        if(!is_null($rating))
        {
//            ->join('rating_reviews','rating_reviews.menu_item_id','=','menu_items.id')
            $query->where('menu_items.rating','<=',$rating);
//                ->select(['chef.id as chef_id', 'users_id', 'latitude', 'longitude', 'cnic', 'chef_rating', 'phoneNumber', 'city', 'streetaddress', 'state', 'postalcode', 'delivery_type', 'online_status', 'verify',
//                    'menu.id as menu_id', 'category_name', 'menu_items.id as menu_item_id', 'menu_items.rating as item_rating', 'item_name', 'price', 'item_time', 'item_quantity' , 'item_option' , 'item_price', 'full_name', 'username']);
        }
    }

    public function keyword_filter($query , $keyword)
    {
        if(!is_null($keyword))
        {
//            ->join('menu_items','chef.id','=','menu_items.chef_id')
            $query->where('item_name','LIKE',$keyword.'%');
//                ->select(['chef.id as chef_id', 'users_id', 'latitude', 'longitude', 'cnic', 'chef_rating', 'phoneNumber', 'city', 'streetaddress', 'state', 'postalcode', 'delivery_type', 'online_status', 'verify',
//                    'menu.id as menu_id', 'category_name', 'menu_items.id as menu_item_id', 'menu_items.rating as item_rating', 'item_name', 'price', 'item_time', 'item_quantity' , 'item_option' , 'item_price', 'full_name', 'username']);
        }
    }

    public function preparation_filter($query , $preparation_min_time , $preparation_max_time)
    {
        if(!(is_null($preparation_min_time) || is_null($preparation_max_time)))
        {
            //->join('menu_items','chef.id','=','menu_items.chef_id')
            $query->where('item_time','>=',$preparation_min_time,'AND')
                ->where('item_time','<=',$preparation_max_time);
//                ->select(['chef.id as chef_id', 'users_id', 'latitude', 'longitude', 'cnic', 'chef_rating', 'phoneNumber', 'city', 'streetaddress', 'state', 'postalcode', 'delivery_type', 'online_status', 'verify',
//                    'menu.id as menu_id', 'category_name', 'menu_items.id as menu_item_id', 'menu_items.rating as item_rating', 'item_name', 'price', 'item_time', 'item_quantity' , 'item_option' , 'item_price', 'full_name', 'username']);
        }
    }
}
