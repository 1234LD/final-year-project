<?php

namespace App\Http\Controllers;

use App\Http\Middleware\handle;
use App\file ;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class web_customer_controller extends Controller
{

    public function store_image(Request $request){

        $id =Session::get('user_id') ;
        $data =['image' => $request->file('picture')
        ];

        $req = Request::create('api/customerpic/'.$id ,'PUT',$data);
        $response = app()->handle($req)->getContent();

        $output = json_decode($response);

        dd($output);

        if(is_null($response))
        {
            return $response;
        }
        else
        {
            return $output;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
