<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class web_direction_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('createroute');
        //
    }

    public function rote()
    {


        $value = '';
        // $source_address = $value["BlockB1061Satellite"]." ".$value["rawalpindi"]." ".$value["Pakistan"]." ".$value["46000"];
        $source_address = "BlockB1061Satellite" . "rawalpindi" . "Pakistan" . "46000";
        $destination_address = "ShahFaisalAve,E-8," . "islamabad," . "Pakistan," . "46000";
        $url = "http://maps.googleapis.com/maps/api/directions/json?origin=" . str_replace(' ', '+', $source_address) . "&destination=" . str_replace(' ', '+', $destination_address) . "&sensor=false";
        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//        $resp = curl_exec($ch);
//        curl_close($ch);
        $json = file_get_contents($url);
        $response = json_decode($json);
       // dd($response) ;
        print_r($response);

        $check= $response->status;



//}

        // print_r($response);


 $distance = $response->routes[0]->legs[0]->distance->text;
//        if ($response->status == 'OK'){
//
//            var bounds = new google.maps.LatLngBounds($response_.routes[0].bounds.southwest, $response.routes[0].bounds.northeast);
//            $response.routes[0].bounds = bounds;
//
//            $response.routes[0].overview_path = google.maps.geometry.encoding.decodePath(response.routes[0].overview_polyline.points);
//
//            $response.routes[0].legs = response.routes[0].legs.map(function (leg) {
//                    leg.start_location = new google.maps.LatLng(leg.start_location.lat, leg.start_location.lng);
//                    leg.end_location = new google.maps.LatLng(leg.end_location.lat, leg.end_location.lng);
//                    leg.steps = leg.steps.map(function (step) {
//                            step.path = google.maps.geometry.encoding.decodePath(step.polyline.points);
//                            step.start_location = new google.maps.LatLng(step.start_location.lat, step.start_location.lng);
//                            step.end_location = new google.maps.LatLng(step.end_location.lat, step.end_location.lng);
//                            return step;
//                        });
//                    return leg;
//                });
//            directionsDisplay.setDirections($response);
//        }


    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
