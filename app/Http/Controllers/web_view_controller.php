<?php

namespace App\Http\Controllers;

use App\Http\Resources\order_Resource;
use App\Orders;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use App\users;
use App\chef;
use App\Customer ;
use App\Menu;

class web_view_controller extends Controller
{
    /**
     * Method to get the all orders of the chef ...
     */
    public function get_chef_all_orders($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/all_orders/'.$id.'/'.Session::get('user_type');
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

     //   dd($response);
        return view('chef.chef-all-orders')->with('orders',$response);
    }

    /**
     * Method to get the pending orders of the chef ...
     */
    public function get_chef_pending_orders($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/pending_orders/'.$id.'/'.Session::get('user_type');
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        //   dd($response);
        return view('chef.chef-pending-response-orders')->with('orders',$response);
    }

    /**
     * Method to get the ready orders of the chef ...
     */
    public function get_chef_ready_orders($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/ready_orders/'.$id.'/'.Session::get('user_type');
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        //   dd($response);
        return view('chef.chef-ready-orders')->with('orders',$response);
    }

    /**
     * Method to get the delivered orders of the chef ...
     */
    public function get_chef_delivered_orders($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/delivered_orders/'.$id.'/'.Session::get('user_type');
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        //   dd($response);
        return view('chef.chef-delivered-orders')->with('orders',$response);
    }

    /**
     * Method to get the processing orders of the chef ...
     */
    public function get_chef_processing_orders($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/processing_orders/'.$id.'/'.Session::get('user_type');
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        //   dd($response);
        return view('chef.chef-processing-orders')->with('orders',$response);
    }

    /**
     * Method to get the cancelled orders of the chef ...
     */
    public function get_chef_cancelled_orders($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/cancelled_orders/'.$id.'/'.Session::get('user_type');
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        //   dd($response);
        return view('chef.chef-canceled-orders')->with('orders',$response);
    }

    /**
     * Method to get the declined orders of the chef ...
     */
    public function get_chef_declined_orders($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/declined_orders/'.$id.'/'.Session::get('user_type');
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        //   dd($response);
        return view('chef.chef-declined-orders')->with('orders',$response);
    }

    /**
     * Method to get the flagged orders of the chef ...
     */
    public function get_chef_flagged_orders($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/flagged_orders/'.$id.'/'.Session::get('user_type');
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        //   dd($response);
        return view('chef.chef-flagged-orders')->with('orders',$response);
    }

    /**
     * Method to get the paid orders of the chef ...
     */
    public function get_chef_paid_orders($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/paid_orders/'.$id.'/'.$name;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

//           dd($response->data[0]->payment_status);
        return view('chef.chef-paid-orders')->with('orders',$response);
    }

    /**
     * Method to get the Unpaid orders of the chef ...
     */
    public function get_chef_unpaid_orders($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/unpaid_orders/'.$id.'/'.$name;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

        //   dd($response);
        return view('chef.chef-unpaid-orders')->with('orders',$response);
    }

    /**
     * Method to get the all reviews of the chef ...
     */
    public function get_chef_all_reviews($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/all_reviews/'.$id.'/'.$name;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

//           dd($response);
        return view('chef.chef-all-reviews')->with('reviews',$response);
    }

    /**
     * Method to get the flagged reviews of the chef ...
     */
    public function get_chef_flagged_reviews($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/flagged_reviews/'.$id.'/'.$name;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

//           dd($response);
        return view('chef.chef-flagged-reviews')->with('reviews',$response);
    }

    /**
     * Method to get the non flagged reviews of the chef ...
     */
    public function get_chef_nonflagged_reviews($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/nonflagged_reviews/'.$id.'/'.$name;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

//           dd($response);
        return view('chef.chef-nonflagged-reviews')->with('reviews',$response);
    }

    /**
     * Method to view add meal categories ...
     */
    public function view_add_meal($id)
    {
        $user = users::where('id',$id)->first();
        $chef = chef::where('users_id',$user->id)->first();

        $menu = Menu::where('chef_id',$chef->id)
            ->selectRaw('category_name')->get();
        $decoded = json_decode($menu);

        return view('chef.chef-add-meal')->with('categories',$decoded);
    }


    /**
     * Method to get the all regular meals of the chef ...
     */
    public function get_chef_all_regular_meals($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/all_regular_meals/'.$id.'/'.$name;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

//           dd($response);
        return view('chef.chef-all-regular-meals')->with('meals',$response);
    }

    /**
     * Method to get the all quick meals of the chef ...
     */
    public function get_chef_all_quick_meals($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/all_quick_meals/'.$id.'/'.$name;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

//        dd($response);
        return view('chef.chef-all-quick-meals')->with('meals',$response);
    }

    /**
     * Method to get expired quick meals of the chef ...
     */
    public function get_chef_expired_quick_meals($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/expired_quick_meals/'.$id.'/'.$name;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

//      dd($response);
        return view('chef.chef-expired-quick-meals')->with('meals',$response);
    }

    /**
     * Method to get free meals of the chef ...
     */
    public function get_chef_free_meals($id, $name)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/free_meals/'.$id.'/'.$name;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

//      dd($response);
        return view('chef.chef-free-meals')->with('meals',$response);
    }
 public function get_address_info($id){

 $order = Orders::find($id) ;
        $chef_detail =  chef::find($order->chef_id);
       // dd($chef_detail);
        $chef_user = users::find($chef_detail->users_id) ;

     //   dd($chef_user) ;
     $customer_detail =  Customer::find($order->customer_id);

     //dd($customer_detail) ;
     $customer_user = users::find($customer_detail->users_id) ;

     //dd($customer_user) ;

//
//
//     $url = 'http://localhost/foodpoleAPI/public/api/chef-customer-info/'. $id ;
//     $client = new Client();
//     $response = $client->get($url);
//
//     $all_detail= json_decode($response->getBody(),false);
//
//     if(is_null($all_detail))
//     {
//         return $response;
//     }
//     else
//     {
//            dd( $all_detail);
//         return view('chef-page')->with('chef_menu',$chef_menu);

         return view('route-map')->with(compact('chef_detail', 'chef_user', 'customer_detail','customer_user')); ;
     }

}
