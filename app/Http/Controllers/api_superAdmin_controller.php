<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Http\Resources\adminResource;
use App\Http\Resources\loginResource;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\users;

class api_superAdmin_controller extends Controller
{
    /**
     * Method to get the details of the admin...
     */
    public function get_admin_details($id)
    {
        $admin = DB::table('admins')->join('users','users.id','=','admins.users_id')
                    ->where('id','=',$id,'AND')
                    ->where('users.usertype','=',config('constants.DEFAULT_ADMIN_TYPE'))
                    ->get();

        if($admin)
        {
            return new adminResource($admin);
        }
    }

    /**
     * Method to get the details of all Admins ...
     */
    public function get_all_admin_details()
    {
        $admin  = Admin::all();
//        $admin = DB::table('admins')->join('users','users.id','=','admins.users_id')
//            ->where('users.usertype','=',config('constants.DEFAULT_ADMIN_TYPE'))
//            ->get();

        if($admin)
        {
            return adminResource::collection($admin);
        }
    }

    /**
     * Method to create the admin ...
     */
    public function store(Request $request)
    {

      // return $request->all();
      //  dd($request);
        $file = $request->img;
       $pswd = $request->input('password');
        $hash_pswd = Hash::make($pswd);

        $user = new users();

       // dd($user);
        if($file !=null ) {
            $user->full_name = $request->input('fullname');
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->password = $hash_pswd;
            $user->usertype = config('constants.DEFAULT_ADMIN_TYPE');
            $user->facebook_ID = $request->input('fb_id');
            $user->gmail = $request->input('gmail');
            $user->twitter = $request->input('twitter');
            $file->move(public_path('/chefimages') . '/', $file->getClientOriginalName());
            $user->image = $file->getClientOriginalName();
        }

        else {

            $user->full_name = $request->input('fullname');
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->password = $hash_pswd;
            $user->usertype = config('constants.DEFAULT_ADMIN_TYPE');
            $user->facebook_ID = $request->input('fb_id');
            $user->gmail = $request->input('gmail');
            $user->twitter = $request->input('twitter');

        }
        if($user->save())
        {
            $admin = new Admin();
            $admin->online_status = $request->input('status');

            users::findOrFail($user->id)->admin()->save($admin);
            return new adminResource($admin);
        }

    }

    /**
     * Method to delete an Admin ...
     */
    public function destroy_admin($id)
    {
        //
        $admin = Admin::findOrFail($id)->user;

        return $admin;
        if($admin)
        {
            if($admin->delete())
            {
                return new loginResource($admin);
            }
        }
        else
        {
            return response('Sorry There were some problem ...');
        }
    }

    /**
     * Method to block the admin ...
     */
    public function block_user(Request $request)
    {
        $admin = Admin::findOrFail($id)->user;

        if($admin)
        {
            if($admin->account_status == false)
            {
                return response('This Admin account is already blocked .. ');
            }
            $admin->account_status = false;
            if($admin->save())
            {
                return new adminResource($admin);
            }
        }
        else
        {
            return response('Sorry There were some problem ...');
        }
    }

    /**
     * Method to unblock the user chef or customer ...
     */
    public function unblock_user(Request $request)
    {
        $admin = Admin::findOrFail($id)->user;

        if($admin)
        {
            if($admin->account_status == true)
            {
                return response('This Admin account is already unblocked .. ');
            }
            $admin->account_status = true;
            if($admin->save())
            {
                return new adminResource($admin);
            }
        }
        else
        {
            return response('Sorry There were some problem ...');
        }
    }
}
