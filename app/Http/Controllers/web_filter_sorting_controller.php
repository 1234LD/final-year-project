<?php

namespace App\Http\Controllers;

use App\Menu;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Illuminate\Pagination\LengthAwarePaginator;

class web_filter_sorting_controller extends Controller
{
    /**
     * Method to send request on API to get filtered Data ....
     */

    public function request_filter_sort($lat, $lng)
    {
//        return Input::get('keyword');
        $url = 'http://localhost/foodpoleAPI/public/api/filter/response/'.$lat.'/'.$lng.'?';
        if(Input::get('distance') != null)
        {
            $url = $url.'distance='.Input::get('distance');
            if(Input::get('keyword') != null) { $url = $url.'&keyword='.Input::get('keyword'); }
            if(Input::get('category') != null) { $url = $url.'&category='.Input::get('category'); }
            if(Input::get('price_min') != null) { $url = $url.'&price_min='.Input::get('price_min'); }
            if(Input::get('price_max') != null) { $url = $url.'&price_max='.Input::get('price_max'); }
            if(Input::get('rating') != null) { $url = $url.'&rating='.Input::get('rating'); }
            if(Input::get('preparation_min_time') != null) { $url = $url.'&preparation_min_time='.Input::get('preparation_min_time'); }
            if(Input::get('preparation_max_time') != null) { $url = $url.'&preparation_max_time='.Input::get('preparation_max_time'); }
            if(Input::get('delivery_type') != null) { $url = $url.'&delivery_type='.Input::get('delivery_type'); }
        }

        $res = new Client();
        $request = $res->get($url);

//        $menu_names = Menu::all('category_name');

        $response = json_decode($request->getBody());

        $reqInc = new Client();
        $req = $reqInc->get('http://localhost/foodpoleAPI/public/api/increment');
        $resp = json_decode($req->getBody());

        ////////////////////////////////////////////////// Trying pagination

        // Set default page
        $page = request()->has('page') ? request('page') : 1;

// Set default per page
        $perPage = request()->has('per_page') ? request('per_page') : 4;

// Offset required to take the results
        $offset = ($page * $perPage) - $perPage;

// At here you might transform your data into collection
//        $url = "https://www.googleapis.com/webfonts/v1/webfonts?key={ my key here}";
        $newCollection = collect(json_decode(file_get_contents( $url ),true));

// Set custom pagination to result set
        $results =  new LengthAwarePaginator(
            $newCollection->slice($offset, $perPage),
            $newCollection->count(),
            $perPage,
            $page,
            ['path' => request()->url(), 'query' => request()->query()]
        );

        return view('search-results',['results' => $results, 'allmeals' => $response]);

//        $exploded_image = explode(config('constants.DEFAULT_GUM'),$response[0]->item_image);
//        dd($response);



//        return view('search-results')->with('meals',$response);

    }
}
