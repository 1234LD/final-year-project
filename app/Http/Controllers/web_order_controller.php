<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Testing\HttpException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class web_order_controller extends Controller
{

    /**
     * Store a newly created resource in storage.
     */
    public function create_order(Request $request)
    {

        //
//        $data=[
//            "customer_id" => "1",
//            "total" => "200",
//            "delivery_type" => "take away",
//            "discount" => "0",
//            "order_status" => "pending",
//            "order_line_item" =>[
//
//                "quantity" => '5,2,4',
//                "item_price"=> '100',
//                "sub_total" => "300,3,4",
//                "menu_item_id" => "2",
//               ] // Order line item array logic ...
//        ];
//        $data = json_decode($_POST['dataInfo']);

        $data = json_decode($_POST['dataInfo']);

//        $data_item = json_encode($data->order_line_item);
//dd($data);
        $newArray = [
            'customer_id' => Session::get('user_id'),
            'latitude' => $data->lat,
            'longitude'=> $data->lng,
            'chef_id' => $data->chef_id,
            'order_sub_total' => $data->order_subtotal,
            'grand_total' => $data->grand_total,
            'delivery_type' => $data->delivery_type,
            'delivery_fare' => $data->delivery_fare,
            'order_status' => config('constants.DEFAULT_ORDER_PENDING'),
             'order_line_item' => $data->order_line_item
        ];
//        dd($newArray);

//        $req =Request::create('api/orders/create','POST',$newArray);
//        $response = app()->handle($req)->getContent();
//
//        $decode = json_decode($response);
//        dd($decode);

        $req = curl_init();
        curl_setopt($req,CURLOPT_URL,'http://localhost/foodpoleAPI/public/api/orders/create');
        curl_setopt($req,CURLOPT_POST,1);
        curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($newArray));
        curl_setopt($req,CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($req);
        curl_close($req);

        $output = json_decode($response);

        Session::forget('customer_cart');
//        return Redirect::to('http://localhost/foodpoleAPI/public/order_confirmation')->with('output',$decode);
//        dd($output);
        return view('order-confirmation')->with('output',$output);
//        dd($output);

    }

    /**
     * Method to view the order details of the Chef or Customer ...
     */
    public function view_order()
    {
//      return view('order_history');
        $request = Request::create('api/customer_order/'.Session::get('user_id').'/'.Session::get('user_type'),'GET');

        $response = app()->handle($request)->getContent();

        $output = json_decode($response);

            if(is_null($output))
            {
                return $response;
            }
            else
            {
                return view('order_history',compact('output'));
            }
    }

    /**
     * Method for Re-ordering ...
     */
    public function re_order($order)
    {
        $re_build = unserialize($order);

        $CART = array();

        foreach ($re_build->order_line_item->data as $order_item)
        {
//            $CART['item_id'][] = $oi->menu_item->id;
            $CART[$order_item->menu_item_id]['name'] = $order_item->menu_item->item_name;
            $CART[$order_item->menu_item_id]['price'] = $order_item->item_price;
            $CART[$order_item->menu_item_id]['quantity'] = $order_item->quantity;
            $CART[$order_item->menu_item_id]['sub_total'] = $order_item->sub_total;
        }

        $re_cart = new web_cart_controller();
        $re_cart->re_order_cart($re_build->total,$re_build->delivery_type,$re_build->chef_id,$CART);

        return Redirect::to('http://localhost/foodpoleAPI/public/cart');
    }

    /**
     * Method for requesting to update the order status of the chef or customer ...
     */
    public function update_order_status($id,$order_id)
    {
        $request = Request::create('api/user/'.'$id'.'/order/'.$order_id.'/order_status','GET');

        $response = app()->handle($request)->getContent();

        $output = json_decode($response);

        if(is_null($output))
        {
            return $response;
        }
        else
        {
            return Redirect::back();
        }
    }

}
//           return Redirect::back();
     //   }





//}



