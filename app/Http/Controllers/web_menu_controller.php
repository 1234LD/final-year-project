<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use League\Flysystem\Config;
use GuzzleHttp\Client;

class web_menu_controller extends Controller
{

    public function wrap(Request $request)
    {
        $data  = $request->all();

        echo "Category Name :: ". $data['category_name'].'</br>';
        echo ' -- -- -- -- -- -- -- -- -- -- '."</br>";
        $menu_items = $data['menu_item'];
      //  dd($items);

        foreach ($menu_items as $items)
        {
            foreach ($items as $key => $item)
            {
                switch ($key)
                {
                    case 'item_name':
                        echo "Item Name :: ".$item.'</br>';
                        break;
                    case 'option_name':
                        echo "Option Names :: ";
                        foreach ($item as $name)
                        {
                            echo $name.' -- ';
                        }
                        echo '</br>';
                        break;
                    case 'option_price':
                        echo "Option Prices :: ";
                        foreach ($item as $price)
                        {
                            echo $price.' -- ';
                        }
                        echo '</br>';
                        break;
                    case 'description':
                        echo "Description :: ".$item."</br>";
                        break;
                    case 'ingredient_name':
                        echo "Ingredient Names :: ";
                        foreach ($item as $name)
                        {
                            echo $name.' -- ';
                        }
                        echo '</br>';
                        break;
                    case 'ingredient_qty':
                        echo "Ingredient Qtys :: ";
                        foreach ($item as $qty)
                        {
                            echo $qty.' -- ';
                        }
                        echo '</br>';
                        break;
                    case 'item_image':
                        echo "Item Images :: ";
                        foreach ($item as $name)
                        {
                            echo $name.' -- ';
                        }
                        echo '</br>';
                        break;
                }
            }
            echo '</br>'.'-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_'.'</br>'.'</br>';
        }


    }

    /**
     *  Sending the request on api to get the pop up menu for chef ...
     */

    public function pop_up_menu()
    {
        $request = Request::create('api/pop_menu','GET');

        $response = app()->handle($request)->getContent();

        $output = json_decode($response);

        if(is_null($output))
        {
            return app()->abort(404);
        }
        else
        {
            return $output;
        }
    }


    /**
     *  Method to send request for loading the complete Menu ...
     *  For Admin or Chef both ...
     */

    public function view_menu(Request $request)
    {
        $data = [
            'user_type' => Session::get('user_type'),
            'user_id' => Session::get('user_id')
        ];

        $req = Request::create('api/menu','POST', $data);

        $response = app()->handle($req)->getContent();

        $output = json_decode($response);

        if(is_null($output))
        {
            return "Sorry you have not created your menu ";
        }
        else
        {
            return $output;
        }
    }

    public function view_complete_meal($username, $id)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/meal_page/'.$username.'/'.$id;
        $client = new Client();
        $response = $client->get($url);

        $meal = json_decode($response->getBody(),false);

        if(is_null($meal))
        {
            return $response;
        }
        else
        {
            return view('meal-page')->with('meal',$meal);
        }
    }


    /**
     *  C-R-U-D operations for Menu name of Chef ...
     *  method to create menu name for chef
     */
    public function create_menu(Request $request)
    {
        $data = [
            'user_id' => Session::getId(),
            'menu_name' => $request->input('menu_name')
        ];

        $req = Request::create('api/menu/create','POST',$data);

        $response = app()->handle($req)->getContent();

        $output = json_decode($response);

        if(is_null($output))
        {
            return $response;
        }
        else
        {
            // menu name will be showing on some view ...
        }

    }

    /**
     * Method to update the Menu name of the Chef ...
     */

    public function update_menu(Request $request)
    {
        $info = [
            'user_id' => Session::getId(),
            'menu_name' => $request->input('menu_name')
        ];

        $req = Request::create('api/menu','PUT', $info);

        $response = app()->handle($req)->getContent();

        $output = json_decode($response);

        if(is_null($output))
        {
            return app()->abort(404);
        }
        else
        {
            echo 'alert(updated)';
        }
    }

    /**
     *  C-R-U-D operations for Menu Category ...
     *  To send request for complete Menu ...
     */
    public function create_category(Request $request)
    {
//        dd($request->all());
        $info = $request->all();
        $data = [
            'user_type' =>config('constants.DEFAULT_CHEF_TYPE'),
            'user_id' => Session::get('user_id'),
            'category_name' => $info['category'],
            'menu_item' => $info['menu_item']
        ];

        $req = Request::create('api/category/create','POST',$data);

        $response = app()->handle($req)->getContent();

//        $output = json_decode($response);

//        $req = curl_init();
//        $url = 'http://localhost/foodpoleAPI/public/api/category/create';
//
//        curl_setopt($req,CURLOPT_URL,$url);
//        curl_setopt($req,CURLOPT_POST,1);
//        curl_setopt($req,CURLOPT_POSTFIELDS,$data);
//        curl_setopt($req, CURLOPT_RETURNTRANSFER,true);
//
//        $response = curl_exec($req);
//        curl_close($req);

        $result = json_decode($response);

//        dd($output);
        if($result->code == 404)
        {
            return Redirect::back();
//            return Redirect::to('/chef_panel')->with('response_failed',$result->msg);
        }
        else
        {
            return Redirect::back();
//            return Redirect::to('/chef_panel')->with('response_success',$result->msg);
        }

    }


    /**
     * Method to send the request of deleting the complete single category ...
     */
    public function destroy_category(Request $request)
    {
        $info = [
            'menu_id' => ''// will be getting id of the menu ..''
        ];

        $req = Request::create('api/category','DELETE',$info);

        $response = app()->handle($req)->getContent();

        $output = json_decode($response);

        if(is_null($output))
        {
            return  app()->abort(404);
        }
        else
        {
            return $output;
        }
    }


    /**
     * C-R-U-D operations for Category Items ...
     * Method to create the single Category Item/8
     */
    public function create_category_item(Request $request)
    {
        $info = $request->all();
        $data = [
            'category_id' => $info['category_id'],
            'menu_item' => $info['menu_item'],
            'item_id' => $info['item_id'],
            'option_name' =>$info['option_name'],
            'option_price' =>$info['option_price'],
            'ingredient_name'=> $info['ingredient_name'],
            'ingredient_qty' => $info['ingredient_qty'],
            'description' => $info['description'],
            'item_image' => $info['item_imaage'],

            'item_recipe' => $info['item_recipe']
        ];
//dd($data);
        $req = Request::create('api/category_item','POST',$data);
        $response = app()->handle($req)->getContent();

        $output = json_decode($response);

        if(is_null($output))
        {
            return app()->abort(404);
        }
        else
        {
            return Redirect::back(); // with some views ...
        }
    }

    /**
     * Method to update the category item ...
     */
    public function update_category_item($username,$menu_id,$id, Request $request)
    {
        $info = $request->all();

//        dd($request->all());
        $data = [
            'menu_id' => $menu_id,
            'username' => $username,
            'menu_item_id' => $id,
            'menu_item' => $info['menu_item']
        ];

//        $encoded_data = json_encode($data);
//        $req = curl_init();
//        curl_setopt($req,CURLOPT_URL,'http://localhost/foodpoleAPI/public/api/update_meal');
//        curl_setopt($req, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($encoded_data)));
//        curl_setopt($req,CURLOPT_CUSTOMREQUEST,'PUT');
//        curl_setopt($req,CURLOPT_RETURNTRANSFER,true);
//        curl_setopt($req,CURLOPT_POSTFIELDS,$data);
//
//        $output = curl_exec($req);
//        $result = json_decode($output);
//        curl_close($req);

        $req = Request::create('api/update_meal','PUT', $data);

        $response = app()->handle($req)->getContent();

        $output = json_decode($response);

//        dd($output);

        if($output->code == 404)
        {
            return Redirect::back();
//            return Redirect::to('/chef_panel')->with('response_failed',$output->msg);
        }
        else
        {
            return Redirect::back();
//            return Redirect::to('/chef_panel')->with('response_success',$output->msg);
        }
    }

    /**
     * Method to update the category item ...
     */
    public function edit_category_item($username, $id)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/meal_page/'.$username.'/'.$id;
        $res = new Client();
        $request = $res->get($url);

        $response = json_decode($request->getBody());

//        dd($response);
        if(is_null($response))
        {
            return app()->abort(404);
        }
        else
        {
            return view('chef.chef-edit-meal')->with('meal',$response);
        }
    }

    /**
     * Method to delete the complete category item ...
     */
    public function delete_category_item($id)
    {
        $url = 'http://localhost/foodpoleAPI/public/api/category_item/delete/'.$id;
        $res = new Client();
        $request = $res->delete($url);

//        $req = Request::create('api/category_item/delete/'.$id,'DELETE');
//
//        $response = app()->handle($req)->getContent();

        $response = json_decode($request->getBody());

        if($response->code == 404)
        {
            return Redirect::back()->with('response_failed',$response->msg);
        }
        else
        {
            return Redirect::back()->with('response_success',$response->msg);
        }
    }

    /**
     *  C-R-U-D operations for item _ reicpe ..
     *  To view the item recipe of the specific item ...
     */

    public function view_item_recipe(Request $request)
    {
        $data = [
            'item_id' => $request->input('item_id')
        ];

        $req = Request::create('api/item_recipe','POST',$data);

        $response = app()->handle($req)->getContent();
    }

    /**
     * Method to create the item recipe  ...
     */
    public function create_item_recipe(Request $request)
    {
        $info = $request->all();

        $data = [
            'item_id' => $info['item_id'],
            'option_name' =>$info['option_name'],

            'item_recipe' => $info['item_recipe']
        ];

        $req = Request::create('api/item_recipe/create','POST',$data);
        $response = app()->handle($req)->getContent();

        $output = json_decode($response);

        if(is_null($output))
        {
            return app()->abort(404);
        }
        else
        {
            return $output; // along with some view ...
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create_item_images(Request $request)
    {
        $data = [
            'item_id' => $_POST['item_id'],
            'pictures' => $request->file('pictures')
            ];

        $request = Request::create('api/item_images/create/','POST',$data);

        $response = app()->handle($request)->getContent();

        $output = json_decode($response);

        if(is_null($output))
        {
            echo "returning null" ;
            return app()->abort(404);
        }
        else
        {
            echo " ho gya " ;
            return $output;
        }
    }

    public function view_item_images()
    {

        $data = [
            'item_id' => $_POST['itemId']
        ];

        $request = Request::create('api/item_images/','POST',$data);

        $response = app()->handle($request)->getContent();

        $output = json_decode($response);

       // dd($output);
        if(is_null($output))
        {
            echo "nhi aya kuch bhi";
        }
        else
        {
            echo "aa gya ";
         return view('show',compact('output'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
