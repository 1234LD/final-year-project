<?php

namespace App\Http\Controllers;

use App\chef;
use App\Customer;
use App\Http\Resources\chef_resource;
use App\Http\Resources\loginResource;
use App\Http\Resources\order_Resource;
use App\Http\Resources\orderCollection;
use App\Menu;
use App\Menu_Item;
use App\OrderLine_items;
use App\Orders;
use App\users;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class api_order_controller extends Controller
{

    /**
     * Viewing all the order details of the chef or customer ....
     */

    public function index(Request $request)
    {
        $info = $request->all();

        $order_line_items = $info['order_line_item'];

        foreach ($order_line_items as $ind => $order_line_item) {
            echo $ind . "</br>";
            foreach ($order_line_item as $key => $item) {
                echo $key . ' > ' . $item . ' ---- ';
            }
            echo "</br>" . "</br>";
        }
    }

    public function get_all_orders($id, $usertype)
    {
        if ($usertype == config('constants.DEFAULT_CHEF_TYPE')) {
            $user = users::find($id)->chef;
            $chef_orders = chef::find($user->id)->orders;

            if (is_null($chef_orders)) {
                return response('These credentials does not meet with the records ...', '203');
            } else {
                return new orderCollection($chef_orders);
            }
        } else if ($usertype == config('constants.DEFAULT_CUSTOMER_TYPE')) {
            $customer_orders = Customer::find($id)->orders;

            if (is_null($customer_orders)) {
                return response('These credentials does not meet with the records ...', '203');
            } else {
                return new orderCollection($customer_orders);
            }


        } else if ($usertype == config('constants.DEFAULT_ADMIN_TYPE') || $usertype == config('constants.DEFAULT_SUPER-ADMIN_TYPE')) {

            $all_orders = Orders::all();

//        if (is_null($custo_orders)) {
//            return response('These credentials does not meet with the records ...', '203');
//        } else {
            return new orderCollection($all_orders);
        }
    }


    /**
     * Method to get the pending orders ...
     */
    public function get_pending_orders($id, $username)
    {
        $user = users::where('id', '=', $id)
            ->where('username', '=', $username)
            ->where('account_status', true)->get();

        if (!is_null($user)) {
            $chef = chef::where('users_id', '=', $id)
                ->first();

            return new orderCollection(Orders::where('chef_id', $chef->id)->where('order_status', config('constants.DEFAULT_ORDER_PENDING'))->get());
        } else {
            $response = ['msg' => 'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response($response);
        }
    }

    /**
     * Method to get the ready orders ...
     */
    public function get_ready_orders($id, $username)
    {
        $user = users::where('id', '=', $id)
            ->where('username', '=', $username)
            ->where('account_status', true)->get();

        if (!is_null($user)) {
            $chef = chef::where('users_id', '=', $id)
                ->first();

            return new orderCollection(Orders::where('chef_id', $chef->id)->where('order_status', config('constants.DEFAULT_ORDER_READY'))->get());
        } else {
            $response = ['msg' => 'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response($response);
        }
    }

    /**
     * Method to get the processing orders ...
     */
    public function get_processing_orders($id, $username)
    {
        $user = users::where('id', '=', $id)
            ->where('username', '=', $username)
            ->where('account_status', true)->get();

        if (!is_null($user)) {
            $chef = chef::where('users_id', '=', $id)
                ->first();

            return new orderCollection(Orders::where('chef_id', $chef->id)->where('order_status', config('constants.DEFAULT_ORDER_PROCESSING'))->get());
        } else {
            $response = ['msg' => 'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response($response);
        }
    }

    /**
     * Method to get the delivered orders ...
     */
    public function get_delivered_orders($id, $username)
    {
        $user = users::where('id', '=', $id)
            ->where('username', '=', $username)
            ->where('account_status', true)->get();

        if (!is_null($user)) {
            $chef = chef::where('users_id', '=', $id)
                ->first();

            return new orderCollection(Orders::where('chef_id', $chef->id)->where('order_status', config('constants.DEFAULT_ORDER_DELIVERED'))->get());
        } else {
            $response = ['msg' => 'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response($response);
        }
    }

    /**
     * Method to get the cancelled orders ...
     */
    public function get_cancelled_orders($id, $username)
    {
        $user = users::where('id', '=', $id)
            ->where('username', '=', $username)
            ->where('account_status', true)->get();

        if (!is_null($user)) {
            $chef = chef::where('users_id', '=', $id)
                ->first();

            return new orderCollection(Orders::where('chef_id', $chef->id)->where('order_status', config('constants.DEFAULT_ORDER_CANCELLED'))->get());
        } else {
            $response = ['msg' => 'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response($response);
        }
    }

    /**
     * Method to get the declined orders ...
     */
    public function get_declined_orders($id, $username)
    {
        $user = users::where('id', '=', $id)
            ->where('username', '=', $username)
            ->where('account_status', true)->get();

        if (!is_null($user)) {
            $chef = chef::where('users_id', '=', $id)
                ->first();

            return new orderCollection(Orders::where('chef_id', $chef->id)->where('order_status', config('constants.DEFAULT_ORDER_DECLINED'))->get());
        } else {
            $response = ['msg' => 'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response($response);
        }
    }

    /**
     * Method to get the flagged orders ...
     */
    public function get_flagged_orders($id, $username)
    {
        $user = users::where('id', '=', $id)
            ->where('username', '=', $username)
            ->where('account_status', true)->get();

        if (!is_null($user)) {
            $chef = chef::where('users_id', '=', $id)
                ->first();

            return new orderCollection(Orders::where('chef_id', $chef->id)->where('flag', '=', true)->get());
        } else {
            $response = ['msg' => 'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response($response);
        }
    }

    /**
     * Method is remaining ...
     */
    public function view_order($id, $type)
    {

        // chef and customer can view his last order based on latest created date in the order table ....
        if ($type == config('constants.DEFAULT_CUSTOMER_TYPE')) {
            $customer = users::find($id)->customer;

            $order = DB::table('orders')
                ->where('customer_id', '=', $customer->id)->get();

            if ($order) {
                return new orderCollection(Orders::where('customer_id', $customer->id)->with('order_line_items')->get());
            } else {
                return response('Sorry You do not have any previous orders ... ', '404');
            }
        }

//            if($customer)
//            {
//                $order = DB::table('orders')
//                            ->where('customer_id','=',$customer->id)
//                            ->get();
//                $order = Customer::find($customer->id)->orders;
//            }

    }

    /**
     * Method for creating a new Order ...
     */
    public function create_order(Request $request)
    {
//        return $request->all();
//                dd($request->all());

        $data = $request->all();


//            return $data;
//        $poo = json_encode($data, true);
//        $soo = json_decode($data, true);
//        return $soo;

//        return $request->all();


//        $user = users::where('id',$request->input('customer_id'))->first();

        $customer = Customer::where('users_id', $request->input('customer_id'))->first();


        if (is_null($customer)) {
            $customer = new Customer([
                'online_status' => 'online',
                'latitude' => $request->input('latitude'),
                'longitude' => $request->input('longitude')
            ]);

            users::findOrFail($data['customer_id'])->customer()->save($customer);

            $user = users::findOrFail($data['customer_id']);

            if (is_null($user)) {
                return response('These credentials does not meet with the records ...', '203');
            } else {
                if ($user->usertype == config('constants.DEFAULT_GENERAL_TYPE')) {
                    $user->usertype = config('constants.DEFAULT_CUSTOMER_TYPE');
                    if ($user->save()) {
                    }
                }
                $order = new Orders([
                    'chef_id' => $data['chef_id'],
                    'sub_total' => $data['order_sub_total'],
                    'delivery_type' => $data['delivery_type'],
                    'order_status' => $data['order_status'],
                    'delivery_fare' => $data['delivery_fare'],
                    'grand_total' => $data['grand_total'],
                ]);

                Customer::findOrFail($customer->id)->order()->save($order);

//                $order_items = $data['order_line_item'];
//                return $data['order_line_item'];

                foreach ($data['order_line_item'] as $index=>$order_item) {


                    $order_line_item = new OrderLine_items([
                        'order_id' => $order->id,
                        'quantity' => $order_item[0],
                        'item_price' => $order_item[1],
                        'item_option' => $order_item[2],
                        'sub_total' => $order_item[4]
                    ]);

                    Menu_Item::find($order_item[3])->order_line_item()->save($order_line_item);
                }

                $orderitem = Orders::find($order->id)->order_line_items;

                $item_id = [];
                foreach ($orderitem as $o) {
                    $item = Menu_Item::join('menu', 'menu.id', '=', 'menu_items.menu_id')
                        ->join('order_line_items', 'menu_items.id', '=', 'order_line_items.menu__item_id')
                        ->where('menu_items.id', $o->menu__item_id)
                        ->where('menu.category_name', '=', config('constants.DEFAULT_QUICK-MEAL_CATEGORY'))
                        ->selectRaw('menu_items.id, order_line_items.quantity')->first();

                    if ($item) {
                        $item_id[] = $item->id;
                        $item_id[] = $item->quantity;

                        $menuitem = Menu_Item::find($item->id);
                        if ($menuitem) {
                            $menuitem->item_quantity -= $item->quantity;
                            $menuitem->save();
                        }

                    }
                }
            }


            $response = [
                'order' => $order,
                'code' => 200
            ];
            return response($response);
        } else {

            $customer->latitude = $request->input('latitude');
            $customer->longitude = $request->input('longitude');
            $customer->save();


            $order = new Orders([
                'chef_id' => $data['chef_id'],
                'sub_total' => $data['order_sub_total'],
                'delivery_type' => $data['delivery_type'],
                'order_status' => $data['order_status'],
                'delivery_fare' => $data['delivery_fare'],
                'grand_total' => $data['grand_total'],
            ]);

            Customer::findOrFail($customer->id)->order()->save($order);

//            $order_items = $data['order_line_item'];
//            return $data['order_line_item'];

//            return $order_items->menu_item_id;

            foreach ($data['order_line_item'] as $index=>$order_item) {


                $order_line_item = new OrderLine_items([
                    'order_id' => $order->id,
                    'quantity' => $order_item[0],
                    'item_price' => $order_item[1],
                    'item_option' => $order_item[2],
                    'sub_total' => $order_item[4]
                ]);

                Menu_Item::find($order_item[3])->order_line_item()->save($order_line_item);
            }


//            dd{}
            $response = [
                'order' => $order,
                'code' => 200
            ];
            return response($response);
        }
    }


    /**
     * Method to update the status of the Order from Chef or Customer ...
     */
    public function update_order_status($id, $order_id)
    {
        $check = users::find($id);
        if (is_null($check)) {
            if ($check->account_status == false) {
                $response = [
                    'msg' => 'Sorry your account has been blocked ... ',
                    'code' => 404
                ];
                return response($response);
            }
        } else {

            if (Input::get('usertype') == config('constants.DEFAULT_CHEF_TYPE')) {
                $chef = users::findorFail($id)->chef;

                if ($chef) {
                    $order = DB::table('orders')
                        ->where('id', '=', $order_id, 'AND')
                        ->where('chef_id', '=', $chef->id)
                        ->first();
                    $updated_order = Orders::find($order->id);

                    if (!is_null($updated_order)) {
                        $updated_order->order_status = Input::get('status');
                        if ($updated_order->save()) {
                            return new order_Resource($updated_order);
                        }
                    } else {
                        $response = [
                            'msg' => 'Sorry! These records does not meet the requirements ... ',
                            'code' => 404
                        ];
                        return response($response);
                    }
                }
            } else if (Input::get('usertype') == config('constants.DEFAULT_CUSTOMER_TYPE')) {
                $customer = users::find($id)->customer;

                if ($customer) {
                    $order = DB::table('orders')
                        ->where('id', '=', $order_id, 'AND')
                        ->where('customer_id', '=', $customer->id)
                        ->first();
                    $updated_order = Orders::find($order->id);

                    if (!is_null($updated_order)) {
                        $updated_order->order_status = Input::get('status');
                        if ($updated_order->save()) {
                            return new order_Resource($updated_order);
                        }
                    } else {
                        $response = [
                            'msg' => 'Sorry! These records does not meet the requirements ... ',
                            'code' => 404
                        ];
                        return response($response);
                    }
                }
            }
        }
    }


    /**
     * Method to update the status of the from Chef ...
     */
    public function order_status_ByCustomer($id, $order_id)
    {
        $customer = chef::find($id);

        if ($customer) {
            $order = DB::table('orders')
                ->where('id', '=', $order_id, 'AND')
                ->where('customer_id', '=', $customer->id)
                ->get();

            if (is_null($order)) {
                $order->order_status = Input::get('status');

                if ($order->save()) {
                    return new order_Resource($order);
                }
            }
        }
    }

    public function get_pending_admin_order()
    {


        return new orderCollection(Orders::where('order_status', config('constants.DEFAULT_ORDER_PENDING'))->get());
    }

    public function get_admin_ready_orders()
    {


        return new orderCollection(Orders::where('order_status', config('constants.DEFAULT_ORDER_READY'))->get());
    }

    public function get_admin_process_orders()
    {


        return new orderCollection(Orders::where('order_status', config('constants.DEFAULT_ORDER_PROCESSING'))->get());


    }

    public function get_admin_deliver_orders()
    {


        return new orderCollection(Orders::where('order_status', config('constants.DEFAULT_ORDER_DELIVERED'))->get());


    }

    public function get_admin_canceled_orders()
    {


        return new orderCollection(Orders::where('order_status', config('constants.DEFAULT_ORDER_CANCELLED'))->get());
    }


    public function get_admin_declined_orders()
    {


        return new orderCollection(Orders::where('order_status', config('constants.DEFAULT_ORDER_DECLINED'))->get());

    }

    public function get_admin_flagged_orders()
    {

        return new orderCollection(Orders::where('flag', '=', true)->get());
    }


    public function get_all_customer_orders($id)
    {

        {
            $user = users::find($id)->customer;
            $customer_orders = Customer::find($user->id)->orders;
            //  dd($customer_orders);

            if (is_null($customer_orders)) {
                return response('These credentials does not meet with the records ...', '203');
            } else {

                return new orderCollection($customer_orders);
            }


        }

    }

//
//    public function get_all_customer_orders($id)
//
//
//    {
//        $user = users::find($id)->customer;
//        $customer_orders = Customer::find($user->id)->orders;
//        //  dd($customer_orders);
//
//        if (is_null($customer_orders)) {
//            return response('These credentials does not meet with the records ...', '203');
//        } else {
//
//            return new orderCollection($customer_orders);
//        }
//
//
//    }


//
//new order_Resource($order);
//                }
//            }
//

//    public function get_pending_admin_order()
//    {
//
//
//        return new orderCollection(Orders::where('order_status', config('constants.DEFAULT_ORDER_PENDING'))->get());
//    }
//
//    public function get_admin_ready_orders()
//    {
//
//
//        return new orderCollection(Orders::where('order_status', config('constants.DEFAULT_ORDER_READY'))->get());
//    }
//
//    public function get_admin_process_orders()
//    {
//
//
//        return new orderCollection(Orders::where('order_status', config('constants.DEFAULT_ORDER_PROCESSING'))->get());
//
//
//    }
//
//    public function get_admin_deliver_orders()
//    {
//
//
//        return new orderCollection(Orders::where('order_status', config('constants.DEFAULT_ORDER_DELIVERED'))->get());
//
//
//    }
//
//    public function get_admin_canceled_orders()
//    {
//
//
//        return new orderCollection(Orders::where('order_status', config('constants.DEFAULT_ORDER_CANCELLED'))->get());
//    }
//
//
//    public function get_admin_declined_orders()
//    {
//
//
//        return new orderCollection(Orders::where('order_status', config('constants.DEFAULT_ORDER_DECLINED'))->get());
//
//    }
//
//    public function get_admin_flagged_orders()
//    {
//
//        return new orderCollection(Orders::where('flag', '=', true)->get());
//    }
//
//
//    public function get_all_customer_orders($id)
//    {
//
//        {
//            $user = users::find($id)->customer;
//            $customer_orders = Customer::find($user->id)->orders;
//            //  dd($customer_orders);
//
//            if (is_null($customer_orders)) {
//                return response('These credentials does not meet with the records ...', '203');
//            } else {
//
//                return new orderCollection($customer_orders);
//            }
//
//
//        }




}