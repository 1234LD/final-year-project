<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//require_once("2heckout.php/lib/Twocheckout.php");
//require_once("php-examples\payment-api\lib\Twocheckout.php");


use Twocheckout;
use Twocheckout_Charge;
use Twocheckout_Error;

class web_Paymentcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return (view('onlinepayment'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function route(){
//
//        $data = ['name' => $_POST['username'],
//
//        ];

        // posting the request from internal api

        $req = Request::create('api/maproute', 'POST');

        $resp = app()->handle($req)->getContent();


        $output = json_decode($resp);

        if (is_null($output))
        {
            return $resp;
        }
        else
        {
            dd($output);
        }
    }


    public function  payment(){

       Twocheckout::privateKey('DE1B20AE-DB6E-480F-9572-68C5DE0DF083'); //Private Key
        Twocheckout::sellerId('901391094'); // 2Checkout Account Number
        Twocheckout::sandbox(true); // Set to false for production accounts.

        try {
            $charge = Twocheckout_Charge::auth(array(

                        "merchantOrderId" => "123",
                        "token"      => $_GET['token'],
                        "currency"   => 'USD',
                        "total"      => '10.00',
                        "billingAddr" => array(
                            "name" => 'Testing Tester',
                            "addrLine1" => '123 Test St',
                            "city" => 'Columbus',
                            "state" => 'OH',
                            "zipCode" => '43123',
                            "country" => 'USA',
                            "email" => 'example@2co.com',
                            "phoneNumber" => '555-555-5555'
                        )// from customer profile
            ));

            if ($charge['response']['responseCode'] == 'APPROVED') {
                echo "Thanks for your Order!";
                echo "<h3>Return Parameters:</h3>";
                echo "<pre>";

                print_r($charge->total);
                echo "</pre>";

            }
        } catch (Twocheckout_Error $e) {

            print_r($e->getMessage());
        }

    }
}
