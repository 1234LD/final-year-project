<?php

namespace App\Http\Controllers;

use Gloudemans\Shoppingcart\CartItem;
use Illuminate\Http\Request;
use App\Chef;
use App\Http\Middleware\handle;
use App\Http\Resources\chef_resource;
use Illuminate\Support\Collection;
use App\Http\Controllers\MailController;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Gloudemans\Shoppingcart\Facades\Cart;

class web_cart_controller extends Controller
{

    public function addToCart(Request $request)

    {
//        $CUSTOMER_CART [] = array('item_id'=> '1',
//            'item_name' => 'Biryani',
//            'price' => '200',
//            'subtotal' => '1500');
//
//        $CUSTOMER_CART [] = array('item_id'=> '2',
//            'item_name' => 'Pulao',
//            'price' => '250',
//            'subtotal' => '2000');
//
//        $CUSTOMER_CART [] = array('item_id'=> '3',
//            'item_name' => 'Zinger Burger',
//            'price' => '320',
//            'subtotal' => '280');

//        dd($request->all());

       // Session::forget('cart');
        $cart_option = explode('^',$request->input('selection'));

        $item_option = $cart_option[0];


        $item_price = (int)$cart_option[1];
        $quantity = (int)$request->input('qty');
//        $item_id = $request->input('item_id');
//        $chef_id = $request->input('chef_id');
//        $delivery_type = $request->input('delivery_type');
//        $item_name =  $request->input('item_name');
//        $item_image = $request->input('item_image');

        $subtotal = $quantity * $item_price;
//$cart[5]=
//        $product = array();
        $item = session()->get('item_id');
        if (empty($item)) {
            $item = [];
        }
//        $check = session()->get('$item');
//        if (empty($cart)) {
//            $cart = [];
//        }
        $item [] = [
            'item_id' => $request->input('item_id'),
            'chef_id' => $request->input('chef_id'),
            'delivery_type' => $request->input('delivery_type'),
            'item_name' => $request->input('item_name'),
            'item_image' => $request->input('item_image'),
            'quantity' => $quantity,
            'item_price' => $item_price,
            'item_option' => $item_option,
            'subtotal' => $subtotal
        ];
//
//        $cart = session()->get('item');
//        if (empty($cart)) {
//            $cart = [];
//        }
//
//        $cart[] = $item;

//        $cart = [
//        'item' => [$item]
//    ];

//         $cart = session()->get('item');
//        if (empty($cart)) {
//            $cart = [];
//        }

//        $cartItems = session()->get('cart');
//        if (empty($cartItems)) {
//            $cartItems = [];
//        }
//        $cartItems[] = $cart;

//        $req->session()->put('cart', $cartItems);
//
//       $CUSTOMER_CART =  array(
//            'item_id' => $request->input('item_id'),
//            'chef_id' => $request->input('chef_id'),
//            'delivery_type' => $request->input('delivery_type'),
//            'item_name' => $request->input('item_name'),
//            'item_image' => $request->input('item_image'),
//            'quantity' => $quantity,
//            'item_price' => $item_price,
//            'item_option' => $item_option,
//            'subtotal' => $subtotal
//        );

//        Cart::instance(Session::get('username'))->add(['id' => $request->input('item_id'), 'name' => $request->input('item_name'),
//                    'price' => (float)$item_price, 'qty' => $quantity, 'subtotal' => $subtotal,
//                    'options' => ['item_option'=>$item_option, 'sub_total'=>$subtotal, 'item_image'=>$request->input('item_image')]]);

//      Cart::instance(Session::get('username'));
//        Cart::destroy();

//        dd(Cart::content());

//        $to = $_SESSION['cart']= array_push($CUSTOMER_CART,'');

//dd(Cart::getcontent());
//<<<<<<< Updated upstream
//
////        $_SESSION['customer_cart'][$request->input('item_id')]
//
//        $request->session()->put('customer_cart',$CUSTOMER_CART);
//=======
////        $cartvalues[] = $CUSTOMER_CART;
        $request->session()->push('customer_cart',$item);
//>>>>>>> Stashed changes
//        Session::put('customer_cart',$CUSTOMER_CART);

//        dd(\session()->get('customer_cart'));


//        foreach (Session::get('customer_cart') as $cart)
//        {
//            dd($cart);
//        }


        return redirect()->back();
    }

    public function updateCart(Request $request)
    {
        $myCart = Session::get('customer_cart');

        foreach ($myCart as $cartItem)
        {
            if($cartItem['item_id'] == Input::get('item_id'))
            {
                $cartItem['price'] = Input::get('price');
                $cartItem['quantity'] = Input::get('quantity');
                $cartItem['subtotal'] = Input::get('price') * Input::get('quantity');
            }
        }
    }

    public function re_order_cart($total, $delivery_type, $chef_id, $CART)
    {
        $CUSTOMER_CART = array();
        Session::forget('customer_cart');
        foreach ($CART as $key => $i)
        {
            $CUSTOMER_CART['item_id'] = $key;
            $CUSTOMER_CART['item_name'] = $i['name'];
            $CUSTOMER_CART['quantity'] = $i['quantity'];
            $CUSTOMER_CART['price'] = $i['price'];
            $CUSTOMER_CART['subtotal'] = $i['sub_total'];

            $CUSTOMER_CART['chef_id'] = $chef_id;
            $CUSTOMER_CART['deliver_type'] = $delivery_type;
            $CUSTOMER_CART['total'] = $total;

            Session::push('customer_cart',$CUSTOMER_CART);
        }
    }
}
