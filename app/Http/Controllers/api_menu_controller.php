<?php

namespace App\Http\Controllers;

use App\Admin;
use App\chef;
use App\Customer;
use App\Http\Resources\chef_resource;
use App\Http\Resources\item_imageResource;
use App\Http\Resources\item_recpieResource;
use App\Http\Resources\menu_itemCollection;
use App\Http\Resources\menu_itemResource;
use App\Http\Resources\menuCollection;
use App\Http\Resources\menuResource;
use App\Http\Resources\menuResource_Collection;
use App\Item_Images;
use App\Item_Recipes;
use App\Menu_Item;
use App\Menu;
use App\users;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Requests;


class api_menu_controller extends Controller
{
    /**
     * Display a listing of the resource ...
     * @return \Illuminate\Http\Response
     */

    public function pop_up_menu()
    {
        return new menuCollection(Menu::where('menu_type', 'public')->with('menu_Items')->get());
    }

    /**
     * Method to get the all regular meals of the chef ...
     * @param $id
     * @param $username
     * @return menuCollection|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function get_all_regular_meals($id, $username)
    {
        $user = users::where('id', '=', $id)
            ->where('username', '=', $username)
            ->where('account_status', true)->get();

        if (!is_null($user)) {
            $chef = chef::where('users_id', '=', $id)
                ->first();

            return new menu_itemCollection(Menu_Item::where('chef_id', $chef->id)->with('menu', 'item_image', 'item_recipe')->get());
        } else {
            $response = ['msg' => 'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response($response);
        }
    }

    /**
     * Method to get the all quick meals of the chef ...
     * @param $id
     * @param $username
     * @return menuCollection|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function get_all_quick_meals($id, $username)
    {
        $user = users::where('id', '=', $id)
            ->where('username', '=', $username)
            ->where('account_status', true)->get();

        if (!is_null($user)) {
            $chef = chef::where('users_id', '=', $id)
                ->first();

            return new menu_itemCollection(Menu_Item::where('chef_id', $chef->id)->where('meal_status', true)->with(['menu' => function ($query) {
                $query->where('category_name', config('constants.DEFAULT_QUICK-MEAL_CATEGORY'));
            }, 'item_image'])->get());
//          return new menuCollection(Menu::where('chef_id', $chef->id)->where('category_name',config('constants.DEFAULT_QUICK-MEAL_CATEGORY'))->with('menu_Items')->get());
        } else {
            $response = ['msg' => 'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response($response);
        }
    }

    /**
     * Method to get expired quick meals of the chef ...
     * @param $id
     * @param $username
     * @return menuCollection|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function get_expired_quick_meals($id, $username)
    {
        $user = users::where('id', '=', $id)
            ->where('username', '=', $username)
            ->where('account_status', true)->get();

        if (!is_null($user)) {
            $chef = chef::where('users_id', '=', $id)
                ->first();

            return new menu_itemCollection(Menu_Item::where('chef_id', $chef->id)->where('meal_status', false)->with(['menu' => function ($query) {
                $query->where('category_name', config('constants.DEFAULT_QUICK-MEAL_CATEGORY'));
            }, 'item_image'])->get());
//            return new menuCollection(Menu::where('chef_id', $chef->id)->where('category_name',config('constants.DEFAULT_QUICK-MEAL_CATEGORY'))->with('menu_Items')->get());
        } else {
            $response = ['msg' => 'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response($response);
        }
    }

    /**
     * Method to get free meals of the chef ...
     * @param $id
     * @param $username
     * @return menuCollection|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function get_free_meals($id, $username)
    {
        $user = users::where('id', '=', $id)
            ->where('username', '=', $username)
            ->where('account_status', true)->get();

        if (!is_null($user)) {
            $chef = chef::where('users_id', '=', $id)
                ->first();

            return new menu_itemCollection(Menu_Item::where('chef_id', $chef->id)->where('price', 0)->where('meal_status',true)->with(['menu' => function ($query) {
                $query->where('category_name', config('constants.DEFAULT_QUICK-MEAL_CATEGORY'));
            }, 'item_image'])->get());
//            return new menuCollection(Menu::where('chef_id', $chef->id)->where('category_name',config('constants.DEFAULT_QUICK-MEAL_CATEGORY'))->with('menu_Items')->get());
        } else {
            $response = ['msg' => 'Sorry User does not exist or account may be deactivated',
                'code' => 203];
            return response($response);
        }
    }

    public function create_menu(Request $request)
    {
        $chef = chef::findOrFail($request->input('user_id'));

        if (is_null($chef)) {
            return null;
        } else {
            $chef->menu_name = $request->input('menu_name');

            if ($chef->save()) {
                return new chef_resource($chef);
            }
        }
    }


    /**
     *  To update the menu name of the Chef ...
     */

    public function update_menu(Request $request)
    {
        $chef = chef::findOrFail($request->input('user_id'));

        if (is_null($chef)) {
            echo "Record not found .... ";
        } else {
            $chef->menu_name = $request->input('menu_name');

            if ($chef->save()) {
                return new chef_resource($chef);
            }
        }
    }


    /**
     * C-R-U-D operations for categories ...
     */

    public function create_category(Request $request)
    {
//        dd($request->all());

        $info = $request->all();

        $all_items = $request->input('menu_item');

//        dd($all_items);
//        dd($info['user_id']);
        if($info['user_type'] == config('constants.DEFAULT_CHEF_TYPE'))
        {
            $user = users::find($info['user_id'])->chef;

            $category = DB::table('menu')
                ->where('chef_id','=',$user->id,'AND')
                ->where('category_name','=',$info['category_name'])->first();

        if ($info['user_type'] == config('constants.DEFAULT_ADMIN_TYPE')) {
            $category = DB::table('menu')
                ->where('admin_id', '=', $info['user_id'], 'AND')
                ->where('category_name', '=', $info['category_name'])->first();

            if (is_null($category)) {
                $menu = Menu::create([
                    'category_name' => $info['category_name'],
                    'menu_type' => 'public'
                ]);

                Admin::findOrFail($info['user_id'])->menu()->save($menu);

                foreach ($all_items as $items) {
                    $menu_item = new Menu_Item();
                    $item_recipe = new Item_Recipes();
                    $item_images = [];

                    foreach ($items as $key => $item) {

                        switch ($key) {
                            case 'item_name':
                                $menu_item->item_name = $item;
                                break;
                            case 'item_time':
                                $menu_item->item_time = $item;
                                break;
                            case 'option_price':
                                $menu_item->item_price = implode(config('constants.DEFAULT_GUM'), $item);
                                $menu_item->price = $item[0];
                                break;
                            case 'option_name':
                                $menu_item->item_option = implode(config('constants.DEFAULT_GUM'), $item);
                                break;
                            case 'description':
                                $item_recipe->description = $item;
                                break;
                            case 'ingredient_name':
                                $item_recipe->ingredient_name = implode(config('constants.DEFAULT_GUM'), $item);
                                break;
                            case 'ingredient_qty':
                                $item_recipe->ingredient_qty = implode(config('constants.DEFAULT_GUM'), $item);
                                break;
                            case 'item_image':
                                foreach ($item as $i) {
                                    $image_file = $i;
                                    $image_file->move(public_path('/item_images') . '/', $image_file->getClientOriginalName());

                                    $item_images [] = new Item_Images([
                                        'item_image' => $image_file->getClientOriginalName(),
                                        'menu_id' => $menu->id
                                    ]);
                                }
                                break;
                        }
                    }

                    Menu::findOrFail($menu->id)->menu_Items()->save($menu_item);

                    Menu_Item::findOrFail($menu_item->id)->item_recipe()->save($item_recipe);

                    Menu_Item::findOrFail($menu_item->id)->item_images()->saveMany($item_images);
                }
            } else {
                $response = [
                    'msg' => 'This category already exists in records ... ',
                    'code' => 404
                ];
                return response($response);
            }
        } else if ($info['user_type'] == config('constants.DEFAULT_CHEF_TYPE')) {
            $category = DB::table('menu')
                ->where('chef_id', '=', $info['user_id'], 'AND')
                ->where('category_name', '=', $info['category_name'])->first();


            if (is_null($category)) {

                $menu = Menu::create([
                    'category_name' => $info['category_name'],
                    'menu_type' => 'private'
                ]);

//                $new = users::findOrFail($info['user_id'])->chef;
               chef::find($user->id)->menu()->save($menu);

//               dd($all_items);
//                dd($new);
                foreach ($all_items as $items) {
                    $menu_item = new Menu_Item();
                    $item_recipe = new Item_Recipes();
                    $item_images = [];

                    foreach ($items as $key => $item) {

                        switch ($key) {
                            case 'item_name':
                                $menu_item->item_name = $item;
                                break;
                            case 'option_price':
                                $menu_item->item_price = implode(config('constants.DEFAULT_GUM'), $item);
                                break;
                            case 'option_name':
                                $menu_item->item_option = implode(config('constants.DEFAULT_GUM'), $item);
                                break;
                            case 'description':
                                $item_recipe->description = $item;
                                break;
                            case 'ingredient_name':
                                $item_recipe->ingredient_name = implode(config('constants.DEFAULT_GUM'), $item);
                                break;
                            case 'ingredient_qty':
                                $item_recipe->ingredient_qty = implode(config('constants.DEFAULT_GUM'), $item);
                                break;
                            case 'item_image':
//                                dd($item);
                                foreach ($item as $i) {
//                                dd($item[0]->getClientOriginalName());
                                    $image_file = $i;
//                                    dd($image_file);
                                    $image_file->move(public_path('/item_images') . '/', $image_file->getClientOriginalName());
//
//
//                                    $ti = $image_file->getClientOriginalName();
//                                    $it = implode('^', $ti);
//                                    $poo = "joo";
//                                    $it = implode('^', $poo);
//
//                                    $item_images->menu_id = $menu->id;

                                    $item_images [] = $image_file->getClientOriginalName();

                                }
                                break;
                        }
                    }

                    $it = implode('^',$item_images);
                    $item_imag = new Item_Images([
                                        'item_image' => $it,
                                        'menu_id' => $menu->id
                                    ]);

                    $new = users::findOrFail($info['user_id'])->chef;

                    $menu_item->chef_id = $new->id;

                    Menu::find($menu->id)->menu_item()->save($menu_item);

                    Menu_Item::find($menu_item->id)->item_recipe()->save($item_recipe);

                    Menu_Item::find($menu_item->id)->item_image()->save($item_imag);

                }

                $response = [
                    'msg' => 'Meal has been successfully added',
                    'code' => 200
                ];

                return response($response);
            }
            else
            {
                $response = [
                    'msg' => 'This category already exists in records ...',
                    'code' => 404
                ];
                return response($response);
            }
//            else
//            {
////                dd($category);
//                $user = users::find($info['user_id'])->chef;
////                chef::find($user->id)->menu()->save($menu);
//
////                dd($user->id);
//                foreach ($all_items as $items) {
//                    $menu_item = new Menu_Item();
//                    $item_recipe = new Item_Recipes();
//                    $item_images = new Item_Images();
//
//                    foreach ($items as $key => $item) {
//
//                        switch ($key) {
//                            case 'item_name':
//                                $menu_item->item_name = $item;
//                                break;
//                            case 'option_price':
//                                $menu_item->item_price = implode(config('constants.DEFAULT_GUM'), $item);
//                                break;
//                            case 'option_name':
//                                $menu_item->item_option = implode(config('constants.DEFAULT_GUM'), $item);
//                                break;
//                            case 'description':
//                                $item_recipe->description = $item;
//                                break;
//                            case 'ingredient_name':
//                                $item_recipe->ingredient_name = implode(config('constants.DEFAULT_GUM'), $item);
//                                break;
//                            case 'ingredient_qty':
//                                $item_recipe->ingredient_qty = implode(config('constants.DEFAULT_GUM'), $item);
//                                break;
//                            case 'item_image':
//                                foreach ($item as $i) {
//                                    $image_file = $i;
//                                    $image_file->move(public_path('/item_images') . '/', $image_file->getClientOriginalName());
//
//                                    $item_images->item_image = implode(config('constants.DEFAULT_GUM'),$image_file->getClientOriginalName());
//                                    $item_images->menu_id = $category->id;
//
////                                    $item_images [] = new Item_Images([
////                                        'item_image' => $image_file->getClientOriginalName(),
////                                        'menu_id' => $menu->id
////                                    ]);
//                                }
//                                break;
//                        }
//                    }
////                    dd($category);
//                    $menu_item->chef_id = $user->id;
//
//                    Menu::findOrFail($category->id)->menu_Items()->save($menu_item);
//
//                    Menu_Item::findOrFail($menu_item->id)->item_recipe()->save($item_recipe);
//
//                    Menu_Item::findOrFail($menu_item->id)->item_images()->saveMany($item_images);
//
//                }
//                $response = [
//                    'msg' => 'Meal successfully added',
//                    'code' => 200
//                ];
//
//                return response($response);
////                $response = [
////                    'msg' => 'This Category already exists in records ...',
////                    'code' => 203
////                ];
////                return response($response);
//            }

            } else {
            $response = [
                'msg' => 'In order to add meals in your profile your have to become a Chef first .',
                'code' => 404
            ];
            return response($response);
            }

        }
    }

//    public function create_category(Request $req)
//    {
//        $info = $req->all();
//
//        // $cat_name = $info['category_name'];
//
//        $allItems = $info['menu_item'];
//
//        $menu = Menu::create([
//            'category_name' => $info['category_name']
//        ]);
//
//        Admin::findOrFail($info['admin_id'])->menu()->save($menu);
//
////        $item_recipes = [];
//
//        foreach ($allItems as $items) {
//            $menu_item = new Menu_Item([
//                'item_name' => $items['item_name'],
//                'item_price' => $items['item_price'],
//                'item_option' => $items['item_option']
//            ]);
//
//            Menu::findOrFail($menu->id)->menu_Items()->save($menu_item);
//
//            $images = $items['item_images'];
//
//            $item_images = [];
//
//            foreach ($images as $image)
//            {
//                $image_file = $image;
//                $image_file->move(public_path('/item_images').'/', $image_file->getClientOriginalName());
//
//                $item_images [] = new Item_Images([
//                    'item_image' => $image_file->getClientOriginalName(),
//                    'menu_id' => $menu->id
//                ]);
//            }
//
//            Menu_Item::findOrFail($menu_item->id)->item_images()->saveMany($item_images);
//
//            $ingredients_names = array();
//            $ingredients_qty = array();
//
//            $ingredient_array = $items['item_recipe']['ingredients'];
//
//            foreach ($ingredient_array as $index) {
//                $ingredients_names [] = $index['ingredient_name'];
//                $ingredients_qty [] = $index['ingredient_qty'];
//            }
//
//            $imploded_name = implode('^', $ingredients_names);
//            $imploded_qty = implode('^', $ingredients_qty);
//
//            echo $imploded_name . ' ------- ';
//            echo $imploded_qty;
//
//            $item_recipe = new Item_Recipes([
//                'description' => $items['item_recipe']['description'],
//                'ingredient_name' => $imploded_name,
//                'ingredient_qty' => $imploded_qty
//            ]);
//
//            echo "heee hooo haaa";
//
//            Menu_Item::findOrFail($menu_item->id)->item_recipe()->save($item_recipe);
//        }
//
//    }

    public function view_category(Request $request)
    {
        if ($request->input('user_type') == config('constants.DEFAULT_ADMIN_TYPE')) {

            $admin_menu = Admin::findOrFail($request->input('user_id'));

            return new menuCollection(Menu::where('admin_id', $admin_menu->id)->with('menu_Items')->get());
        } else if ($request->input('user_type') == config('constants.DEFAULT_CHEF_TYPE')) {
            $chef_menu = chef::findOrFail($request->input('user_id'));

            return new menuCollection(Menu::where('chef_id', $chef_menu->id)->with('menu_Items')->get());
        } else {
            return response('These credentials does not meet with the records ...', '500');
        }
    }

    public function destroy_category(Request $req)
    {
        $menu = Menu::find($req->input('menu_id'));

        if (is_null($menu)) {
            return response('Category does not exist in your records ...', '403');
        } else {
            if ($menu->delete()) {
                return new menuResource($menu);
            } else {
                return response('Whoops there wer some problems with server', '404');
            }
        }
    }

    /**
     * C-R-U-D operations for category item ...
     */
    public function view_category_item($username, $id)
    {
        $user = users::where('username',$username)->first();
        $chef = chef::where('users_id',$user->id)->first();

//<<<<<<< Updated upstream
//        if (is_null($menu_item)) {
//            return response('There is no details for this item in your records ...', '500');
//        } else {
//            return new menu_itemCollection(Menu_Item::where('id', $id)->with('item_recipe', 'item_image', 'ratings', 'chef')->get());
//=======
        if(is_null($chef))
        {
            $response = [
                'msg' => 'Sorry chef does not exists ....',
                'code' => 404
            ];
            return response($response);
        }
        else
        {
            $menu_item = Menu_Item::where('chef_id',$chef->id)
                                  ->where('id',$id)->first();
//        dd($id);
//        $menu_item = Menu_Item::find($id);

            if(is_null($menu_item))
            {
                $response = [
                    'msg' => 'Sorry meal does not exists ....',
                    'code' => 404
                ];
                return response($response);
            }
            else {
                return new menu_itemCollection(Menu_Item::where('id', $id)->with('item_recipe','item_image','ratings','chef')->get());
            }
//>>>>>>> Stashed changes
        }
    }

    public function create_category_items(Request $request)
    {
        $data = $request->all();

        $all_items = $data['menu_item'];

        $id = null;

        if ($request->input('user_type') == config('constants.DEFAULT_CHEF_TYPE')) {
            $chef = chef:: findOrFail($request->input('user_id'));
            $id = $chef->id;
        }

        foreach ($all_items as $items) {
            $menu_item = new Menu_Item();
            $item_recipe = new Item_Recipes();
            $item_images = [];

            foreach ($items as $key => $item) {

                switch ($key) {
                    case 'item_name':
                        $menu_item->item_name = $item;

                        $check_item = DB::table('menu_items')
                            ->where('menu_id', '=', $data['category_id'], 'AND')
                            ->where('item_name', '=', $item['item_name'])->first();

                        if (is_null($check_item)) {
                            $response = [
                                'msg' => 'Sorry! item name already exists in your records ... ',
                                'code' => 404
                            ];
                            return response($response);
                        }
                        break;
                    case 'option_price':
                        $menu_item->item_price = implode(config('constants.DEFAULT_GUM'), $item);
                        break;
                    case 'option_name':
                        $menu_item->item_option = implode(config('constants.DEFAULT_GUM'), $item);
                        break;
                    case 'description':
                        $item_recipe->description = $item;
                        break;
                    case 'ingredient_name':
                        $item_recipe->ingredient_name = implode(config('constants.DEFAULT_GUM'), $item);
                        break;
                    case 'ingredient_qty':
                        $item_recipe->ingredient_qty = implode(config('constants.DEFAULT_GUM'), $item);
                        break;
                    case 'item_image':
                        foreach ($item as $i) {
                            $image_file = $i;
                            $image_file->move(public_path('/item_images') . '/', $image_file->getClientOriginalName());

                            $item_images [] = new Item_Images([
                                'item_image' => $image_file->getClientOriginalName(),
                                'menu_id' => $menu->id
                            ]);
                        }
                        break;
                }
            }
            $menu_item->chef_id = $data['user_id'];

            Menu::findOrFail($menu->id)->menu_Items()->save($menu_item);

            Menu_Item::findOrFail($menu_item->id)->item_recipe()->save($item_recipe);

            Menu_Item::findOrFail($menu_item->id)->item_images()->saveMany($item_images);
        }
        return new menuCollection(Menu::where('id', $data['category_id'])->with('menu_Items', 'item_images')->get());
    }


    /**
     * Method to update the details of the category item ...
     */
    public function update_category_item(Request $request)
    {
//        dd($info[0]['item_name']);

        $user = users::where('username',$request->input('username'))->first();

        if(is_null($user))
        {
            $response = [
                'msg' => 'User does not exist ....',
                'code' => 401
            ];
            return response($response);
        }
        else {

            $menu_item = Menu_Item::findOrFail($request->input('menu_item_id'));


            $info = $request->input('menu_item');

            if (!is_null($menu_item)) {


                $menu_item->item_name = $info[0]['item_name'];

                $menu_item->item_price = implode(config('constants.DEFAULT_GUM'), $info[0]['option_price']);

                $menu_item->item_option = implode(config('constants.DEFAULT_GUM'), $info[0]['option_name']);

                $chef = chef::where('users_id',$user->id)->first();


                chef::find($chef->id)->menu_item()->save($menu_item);

//<<<<<<< Updated upstream
//        if (is_null($menu_item)) {
//
//            $menu_item->item_price = implode(config('constants.DEFAULT_GUM'), $request->input('option_price'));
//            $menu_item->item_option = implode(config('constants.DEFAULT_GUM', $request->input('option_name')));
//
//            if ($menu_item->save()) {
//                return new menu_itemResource($menu_item);
//            } else {
//                return response('Whoops there wer some problems with server', '404');
//            }
//        } else {
//            return response('These credentials does not meet with the records ... ', '500');
//=======
//                if ($menu_item->save()) {

                     $item_recipe = Item_Recipes::where('menu__item_id',$menu_item->id)->first();

                    if(!is_null($item_recipe))
                    {
                        $item_recipe->ingredient_name = implode(config('constants.DEFAULT_GUM'), $info[0]['ingredient_name']);
                        $item_recipe->ingredient_qty = implode(config('constants.DEFAULT_GUM'), $info[0]['ingredient_qty']);
                        $item_recipe->description  = $info[0]['description'];

                        Menu_Item::findOrFail($menu_item->id)->item_recipe()->save($item_recipe);
                    }
                    else
                    {
                        $item_recipe = new Item_Recipes([
                            'ingredient_name' => implode(config('constants.DEFAULT_GUM'), $info[0]['ingredient_name']),
                            'ingredient_qty' => implode(config('constants.DEFAULT_GUM'), $info[0]['ingredient_qty']),
                            'description' => $info[0]['description']
                        ]);

                        Menu_Item::findOrFail($menu_item->id)->item_recipe()->save($item_recipe);
                    }

                    if($info[0]['item_image'] != null)
                    {
                        $item_image = Item_Images::where('menu__item_id',$menu_item->id)->first();

                        if(!is_null($item_image))
                        {
                            $image_array = [];
                            foreach ($info[0]['item_image'] as $item)
                            {
                                $image_file = $item;

                                $image_file->move(public_path('/item_images') . '/', $image_file->getClientOriginalName());

                                $image_array [] = $image_file->getClientOriginalName();
                            }
                            $img = implode('^',$image_array);

                            $item_image->item_image = $img;
                            Menu_Item::find($menu_item->id)->item_image()->save($item_image);
                            $response = [
                                'msg' => 'Meal has been updated successfully',
                                'code' => 200
                            ];
                            return response($response);
                    }
//                    $item_image = Item_Images::where('menu__item_id',$menu_item->id)->first();
//
//                    if(!is_null($item_image))
//                    {
//                        $image_array = [];
//                        foreach ($info[0]['item_image'] as $item)
//                        {
//                            $image_file = $item;
//
//                            $image_file->move(public_path('/item_images') . '/', $image_file->getClientOriginalName());
//
//                            $image_array [] = $image_file->getClientOriginalName();
//                        }
//                        $img = implode('^',$image_array);
//
//                       $item_image->item_image = $img;
//                        Menu_Item::find($menu_item->id)->item_image()->save($item_image);
                    }
                    else
                    {
                        $image_array = [];
                        foreach ($info[0]['item_image'] as $item)
                        {
                            $image_file = $item;

                            $image_file->move(public_path('/item_images') . '/', $image_file->getClientOriginalName());

                            $image_array [] = $image_file->getClientOriginalName();
                        }
                        $img = implode('^',$image_array);

                        $item_imag = new Item_Images([
                            'item_image' => $img,
                            'menu_id' => $request->input('menu_id')
                        ]);
                        Menu_Item::find($menu_item->id)->item_image()->save($item_imag);
                    }

                    $response = [
                        'msg' => 'Meal has been updated successfully',
                        'code' => 200
                    ];
                    return response($response);
//                    return new menu_itemResource($menu_item);
//                }

            }
            else
                {
                    $response = [
                        'msg' => 'These credentials does not meet with the records ...',
                        'code' => 404
                    ];
                    return response($response);
            }
//>>>>>>> Stashed changes
        }

    }

    public function destroy_category_item($id)
    {
        $menu_item = Menu_Item::find($id);

//<<<<<<< Updated upstream
//        if (is_null($menu_item)) {
//            return response('These credentials does not meet with the records ....', '203');
//        } else {
//
//            if ($menu_item->delete()) {
//                return new menu_itemResource($menu_item);
//            } else {
//                return response('Whoops there wer some problems with server', '404');
//=======
        if(is_null($menu_item))
        {
            $response = [
                'msg' => 'Sorry meal does not exists ...',
                'code' => 404
            ];
            return response($response);
//            return response('These credentials does not meet with the records ....','203');
        }
        else {

            if ($menu_item->delete()) {
                $response = [
                    'msg' => 'Meal has been deleted successfully ...',
                    'code' => 200
                ];
                return response($response);
//                return new menu_itemResource($menu_item);
            }
            else
            {
                $response = [
                    'msg' => 'Whoops there wer some problems with server',
                    'code' => 404
                ];
                return response($response);
//>>>>>>> Stashed changes
            }
        }

    }

    /**
     * C-R-U-D operations for Item Recipe ...
     */

    public function view_item_recipe(Request $request)
    {
        $check_item = Menu_Item::find($request->input('item_id'));

        if (is_null($check_item)) {
            return response('These credentials does not meet with the records ...', '203');
        } else {
            return new item_recpieResource(Menu_Item::find($request->input('item_id'))->item_recipe);
        }
    }

    public function create_item_recipe(Request $request)
    {
        $data = $request->all();

        $menu_item = Menu_Item::find($request->input('item_id'));

        if (is_null($menu_item)) {
            return response('These credentials does not meet with the records ...', '203');
        } else {
            $ingredients_names = array();
            $ingredients_qty = array();

            $ingredient_array = $data['ingredients'];

            foreach ($ingredient_array as $index) {
                $ingredients_names [] = $index['ingredient_name'];
                $ingredients_qty [] = $index['ingredient_qty'];
            }

            $imploded_name = implode('^', $ingredients_names);
            $imploded_qty = implode('^', $ingredients_qty);

            echo $imploded_name . ' ------- ';
            echo $imploded_qty;

            $item_recipe = new Item_Recipes([
                'description' => $data['description'],
                'ingredient_name' => $imploded_name,
                'ingredient_qty' => $imploded_qty
            ]);

            Menu_Item::findOrFail($menu_item->id)->item_recipe()->save($item_recipe);

            return new item_recpieResource($item_recipe);
        }
    }

    public function update_item_recipe(Request $request)
    {

        $data = $request->all();

        $item_recipe = Menu_Item::find($request->input('item_id'))->item_recipe;

        if (is_null($item_recipe)) {
            return response('These credentials does not meet with the records ...', '203');
        } else {
            $ingredients_names = array();
            $ingredients_qty = array();

            $ingredient_array = $data['ingredients'];

            foreach ($ingredient_array as $index) {
                $ingredients_names [] = $index['ingredient_name'];
                $ingredients_qty [] = $index['ingredient_qty'];
            }

            $imploded_name = implode('^', $ingredients_names);
            $imploded_qty = implode('^', $ingredients_qty);

            echo $imploded_name . ' ------- ';
            echo $imploded_qty;

            $item_recipe->description = $data['description'];
            $item_recipe->ingredient_name = $imploded_name;
            $item_recipe->ingredient_qty = $imploded_qty;

            if ($item_recipe->save()) {
                return new item_recpieResource($item_recipe);
            } else {
                return response('Whoops there wer some problems with server', '404');
            }

        }
    }

    public function destroy_item_recipe($id)
    {
        $item_recipe = Item_Recipes::find($id);

        if (is_null($item_recipe)) {
            return response('These credentials does not meet with the records ... ', '203');
        } else {
            if ($item_recipe->delete()) {
                return new Item_Recipes($item_recipe);
            } else {
                return response('Whoops there wer some problems with server', '404');
            }
        }
    }


    /**
     * C-R-U-D operations for Item Images ...
     */

    public function view_item_images(Request $request)
    {
        $item_images = Menu_Item::find($request->input('item_id'))->item_images;

        if (is_null($item_images)) {
            return response('These credentials does not meet with the records ...', '203');
        } else {
            return item_imageResource::collection($item_images);
        }
    }


    public function create_item_images(Request $request)
    {

        $item_id = $request->input('item_id');

        $menu_item = Menu_Item::find($item_id);
        $menu = Menu_Item::find($menu_item->id)->menu;

        if (is_null($menu_item)) {
            return response('These credentials does not meet with the records ... ', '203');
        } else if (is_null($menu)) {
            return response('These credentials does not meet with the records ...', '203');
        } else {
            $images = $request->pictures;

            $item_images = [];

            foreach ($images as $image) {
                $image_file = $image;
                $image_file->move(public_path('/item_images') . '/', $image_file->getClientOriginalName());

                $item_images [] = new Item_Images([
                    'item_image' => $image_file->getClientOriginalName(),
                    'menu_id' => $menu->id
                ]);
            }

            Menu_Item::findOrFail($menu_item->id)->item_images()->saveMany($item_images);

            return item_imageResource::collection($item_images);
        }
    }

    public function update_item_image(Request $request)
    {
        $item_image = Item_Images::findOrFail($request->input('image_id'));

        if (is_null($item_image)) {
            return response('These credentials does not meet with the records ...', '203');
        } else {
            $image_file = $request->input('item_image');
            $image_file->move(public_path('/item_images_' . $request->input('item_id')) . '/', $image_file->getClientOriginalName());

            $item_image->item_image = $image_file->getClientOriginalName();

            if ($item_image->save()) {
                return new item_imageResource($item_image);
            } else {
                return response('Whoops there wer some problems with server', '404');
            }
        }

    }

    public function destroy_item_image($id)
    {
        $item_image = Item_Images::find($id);

        if (is_null($item_image)) {
            return response('These credentials does not meet with the records ...', '203');
        } else {
            if ($item_image->delete()) {
                return new item_imageResource($item_image);
            } else {
                return response('Whoops there wer some problems with server', '404');
            }
        }
    }

    public function get_all_category()
    {

        $meal = Menu::all();

        if ($meal) {

            return menuResource::collection($meal);
        }


    }

    public function get_add_category(Request $request)
    {

        $meal = $request->input('category_name');

//        $user = new users();
//        // dd($user);
//        $user->full_name = $request->input('fullname');
//        $user->username = $request->input('username');
//        $user->email = $request->input('email');
//        $user->password = $hash_pswd;
//        $user->usertype = config('constants.DEFAULT_ADMIN_TYPE');
//        $user->facebook_ID = $request->input('fb_id');
//        $user->gmail = $request->input('gmail');
//        $user->twitter = $request->input('twitter');

        $menu = new Menu();
        $menu->category_name = $meal;
        if ($menu->save()) {

            return menuResource::collection($menu);
        }
    }

    public function get_admin_meals()
    {


        return new menu_itemCollection(Menu_Item::where('meal_status', true)->with(['menu' => function ($query) {
            $query->where('category_name', config('constants.DEFAULT_QUICK-MEAL_CATEGORY'));
        }, 'item_image'])->get());
    }

    public function get_admin_expire_meals()
    {

        return new menu_itemCollection(Menu_Item::where('meal_status', false)->with(['menu' => function ($query) {
            $query->where('category_name', config('constants.DEFAULT_QUICK-MEAL_CATEGORY'));
        }, 'item_image'])->get());

    }

    public function get_admin_free_meals()
    {

        return new menu_itemCollection(Menu_Item::where('price', 0)->with(['menu' => function ($query) {
            $query->where('category_name', config('constants.DEFAULT_QUICK-MEAL_CATEGORY'));
        }, 'item_image'])->get());

    }

    public function get_regular_meals()
    {

        return new menu_itemCollection(Menu_Item::with('menu', 'item_image', 'item_recipe')->get());
    }
}