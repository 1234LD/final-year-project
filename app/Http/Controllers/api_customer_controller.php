<?php

namespace App\Http\Controllers;

use App\users;
use App\Customer;
use Illuminate\Http\Request;
use App\Http\Resources\loginResource;
use App\Http\Resources\customerResource;

class api_customer_controller extends Controller
{
    /**
     *
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return loginResource
     */
    public function edit(Request $request ,$id){

        $user =users::find($id);

        if(is_null($user))
        {
            return response('These credentials does not meet with the records ...','500');
        }
        else
            {
            $file = $request->image;

            $file->move(public_path('/customer_images') . '/', $file->getClientOriginalName());

            $user->picture = $file->getClientOriginalName();

            if($user->save())
            {
                return new loginResource($user) ;
            }
            else
            {
                return response('Whoops there wer some problems with server','404');
            }
        }
    }
    public function  get_all_customers(){

        $customer  = Customer::all();
//        $admin = DB::table('admins')->join('users','users.id','=','admins.users_id')
//            ->where('users.usertype','=',config('constants.DEFAULT_ADMIN_TYPE'))
//            ->get();

        if($customer)
        {
            return customerResource::collection($customer);
        }
    }
    public function get_blocked_customers(){
      //  $user = new users();
        $tyoe = 'constants.DEFAULT_CUSTOMER_TYPE';
        $user = users::where('usertype','customer')->where('account_status',false)->get();

        if($user){

            return  loginResource::collection($user);
        }



    }
}
