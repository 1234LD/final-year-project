<?php
/**
 * Created by PhpStorm.
 * User: a
 * Date: 12/24/2018
 * Time: 7:57 AM
 */

namespace App\Http\ViewComposer;

use GuzzleHttp\Client;
use Illuminate\View\View;

class User_Composer
{
    public $renderedData = null;

    public function __construct()
    {
        $url = 'http://localhost/foodpoleAPI/public/api/userComposer';
        $client = new Client();
        $request = $client->get($url);

//        $request = curl_init();
//        curl_setopt($request,CURLOPT_URL,$url);
//        curl_setopt($request,CURLOPT_RETURNTRANSFER,true);
//        $output = curl_exec($request);
//        curl_close($request);


        $this->renderedData = json_decode($request->getBody());

        //   dd($this->renderedData->meal_list);
    }

    public function compose(View $view)
    {
        $view->with('renderedArray',$this->renderedData);
    }
}