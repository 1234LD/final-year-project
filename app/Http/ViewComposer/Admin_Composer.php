<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 12/19/2018
 * Time: 7:39 AM
 */

namespace App\Http\ViewComposer;

use GuzzleHttp\Client;
use Illuminate\View\View;

class Admin_Composer
{
    public $renderedData = null;


    public function __construct()
    {
//        $request = curl_init();
        $url = 'http://localhost/foodpoleAPI/public/api/admin_dashboard';
        $client = new Client();
        $request = $client->get($url);

//        curl_setopt($request,CURLOPT_URL,$url);
//        curl_setopt($request,CURLOPT_RETURNTRANSFER,true);
//        $output = curl_exec($request);
//        curl_close($request);

        $this->renderedData = json_decode($request->getBody());

//        dd($this->renderedData);
    }


    public function compose(View $view)
    {
        $view->with('renderedArray',$this->renderedData);
    }

}