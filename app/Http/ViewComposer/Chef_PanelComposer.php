<?php
/**
 * Created by PhpStorm.
 * User: a
 * Date: 12/14/2018
 * Time: 1:03 AM
 */

namespace App\Http\ViewComposer;

use App\chef;
use App\OrderLine_items;
use App\Orders;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\View\View;
use GuzzleHttp\Client;

class Chef_PanelComposer
{
    public $renderedData = null;

    public function __construct()
    {
//        $request = curl_init();
        $url = 'http://localhost/foodpoleAPI/public/api/chef_panelComposer/'.Session::get('user_id');
        $client = new Client();
        $request = $client->get($url);

//        curl_setopt($request,CURLOPT_URL,$url);
//        curl_setopt($request,CURLOPT_RETURNTRANSFER,true);
//        $output = curl_exec($request);
//        curl_close($request);

        $this->renderedData = json_decode($request->getBody());

//        dd($this->renderedData);
    }


    public function compose(View $view)
    {
        $view->with('renderedArray',$this->renderedData);
    }
}