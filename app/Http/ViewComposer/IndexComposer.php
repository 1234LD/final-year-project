<?php

namespace  App\Http\ViewComposer;

use App\chef;
use App\OrderLine_items;
use App\Orders;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\View\View;
use GuzzleHttp\Client;


class IndexComposer
{

    public $renderedData = null;

    public function __construct()
    {
        $url = 'http://localhost/foodpoleAPI/public/api/indexComposer';
        $client = new Client();
        $request = $client->get($url);

//        $request = curl_init();
//        curl_setopt($request,CURLOPT_URL,$url);
//        curl_setopt($request,CURLOPT_RETURNTRANSFER,true);
//        $output = curl_exec($request);
//        curl_close($request);


        $this->renderedData = json_decode($request->getBody());

     //   dd($this->renderedData->meal_list);
    }

    public function compose(View $view)
    {
        $view->with('renderedArray',$this->renderedData);
    }
}