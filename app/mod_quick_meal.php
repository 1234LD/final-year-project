<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class chef extends Model
{
    //
    public $table = "chef";
//    public $timestamps = false;

    protected $fillable = [
        'chef_id ', 'item_name','amount','price',
    ];

    protected $hidden = [
       'remember_token'
    ];

    public function user()
    {
        return $this->belongsTo(users::class);
    }

    public function menu()
    {
        return $this->hasOne(Menu::class);
    }

    public function menus()
    {
        return $this->hasMany(Menu::class);
    }

    public function menu_item()
    {
        return $this->hasOne(Menu_Item::class);
    }

    public function menu_Items()
    {
        return $this->hasMany(Menu_Item::class);
    }

    public function order()
    {
        return $this->hasOne(Orders::class);
    }

    public function orders()
    {
        return $this->hasMany(Orders::class);
    }

}
