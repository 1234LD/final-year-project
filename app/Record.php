<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    public $table = "records";

    protected $fillable = [
        'id',
        'search_count','created_at','updated_at'
    ];
}
