<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu_Item extends Model
{
    //
    public $table = 'menu_items';

    protected $fillable =[
        'id','menu_id','chef_id',
        'item_name','price','item_price',
        'item_option','item_time',
        'rating','item_quantity','meal_status','verified',
        'created_at', 'updated_at'
    ];


    public function menu()
    {
        return $this->belongsTo(Menu::class,'menu_id');
    }

    public function item_image()
    {
        return $this->hasOne(Item_Images::class);
    }

//    public function item_images()
//    {
//        return $this->hasMany(Item_Images::class);
//    }

    public function item_recipe()
    {
        return $this->hasOne(Item_Recipes::class);
    }

    public function chef()
    {
        return $this->belongsTo(chef::class);
    }

    public function order_line_item()
    {
        return $this->hasOne(OrderLine_items::class, 'menu__item_id');
    }

    public function order_line_items()
    {
        return $this->hasMany(OrderLine_items::class,'menu__item_id');
    }

    public function ratings()
    {
        return $this->hasMany(rating::class,'menu__item_id');
    }


    public function getRating()
    {
        $ratingsum =$this->ratings()->sum('rating');
        $avgrating =$ratingsum/$this->ratings()->count();

        return $avgrating;
    }

}
