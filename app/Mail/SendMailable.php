<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $name,$email;
    public $time;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$email,$time)
    {
        $this->name = $name;
        $this->email = $email;
        $this->time = $time;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('food.pole4@gmail.com','FoodPole')
            ->subject('Reset - Password')
            ->view('email_name');
    }
}
