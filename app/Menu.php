<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

    public $DEFAULT_MENU_TYPE = 'public';

    protected $fillable =[
        'id',
        'chef_id','admin_id','menu_name','category_name','menu_type'
    ];

    public $table = 'menu';

    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }

    public function menu_item()
    {
        return $this->hasOne(Menu_Item::class);
    }

    public function menu_Items()
    {
        return $this->hasMany(Menu_Item::class,'menu__item_id');
    }

    public function item_images()
    {
        return $this->hasMany(Item_Images::class);
    }

    public function chef()
    {
        return $this->belongsTo(chef::class,'chef_id');
    }
}
