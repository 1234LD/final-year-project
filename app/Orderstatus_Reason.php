<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderstatus_Reason extends Model
{
    public $table = 'order_status_reason';

    protected $fillable = [
        'id','customer_id','chef_id', 'orders_id',
        'reason' , 'order_status'
    ];

    public function order()
    {
        return $this->belongsTo(Orders::class,'orders_id');
    }

    public function chef()
    {
        return $this->belongsTo(chef::class,'chef_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id');
    }

}
