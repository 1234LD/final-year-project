<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public $table = 'customer';

    protected $fillable = [
        'id','users_id','online_status','latitude','longitude'
    ];

    public function user()
    {
        return $this->belongsTo(users::class,'users_id');
    }

    public function order()
    {
        return $this->hasOne(Orders::class);
    }

    public function orders()
    {
        return $this->hasMany(Orders::class);
    }

    public function ratings()
    {
        return $this->hasMany(rating::class);
    }
}
