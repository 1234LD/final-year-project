<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rating extends Model
{

    public $table = "rating_reviews";
    // public $timestamps = false;

    protected $fillable = [
        'id', 'customer_id', 'chef_id', 'orders_id', 'menu__item_id',
        'headline', 'feedback', 'rating', 'status', 'approved',
        'created_at','updated_at'
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];

    public function reviews(){

        return  $this->hasMany(rating::class);
    }

    public function menu_item()
    {
        return $this->belongsTo(Menu_Item::class);
    }

    public function menu_items()
    {
        return $this->belongsToMany(Menu_Item::class,'menu__item_id');
    }

    public function order()
    {
        return $this->belongsTo(Orders::class,'orders_id');
    }

    public function orders()
    {
        return $this->belongsToMany(Orders::class);
    }

    public function chef()
    {
        return $this->belongsTo(chef::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }


    public function getRating()
    {
        $ravings =$this->reviews()->sum('rating');
//        dd($ravings);
        $avg =$ravings/$this->reviews()->count();

        return $avg ;
    }
}
