<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Menu_Item;
use GuzzleHttp\Client;

class freeQuickMeal implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $item_id;
    protected $username;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($name, $id)
    {
        $this->username = $name;
        $this->item_id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $url = 'http://localhost/foodpoleAPI/public/api/free_quick_meal/'.$this->username.'/'.$this->item_id;
        $res = new Client();
        $request = $res->get($url);
        $response = json_decode($request->getBody());
    }
}
