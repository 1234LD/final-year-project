<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use GuzzleHttp\Client;

class expireQuickMeal implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

//    protected $mail, $name;

    protected $item_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->item_id = $id;
//        $this->mail = $email;
//        $this->name = $n;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $url = 'http://localhost/foodpoleAPI/public/api/drop/quick_meal/'.$this->item_id;
        $res = new Client();
        $request = $res->get($url);
        $response = json_decode($request->getBody());

//      date_default_timezone_set('Asia/Karachi');
//      Mail::to($this->mail)->send(new SendMailable($this->name,$this->mail,'Hey There !'));
//      dispatch(Mail::to($this->mail)->send(new SendMailable($this->name,$this->mail,'Hey There !')))->delay(now()->addSeconds(10));
//      $this->delay(now()->addMinutes(1));
    }
}
