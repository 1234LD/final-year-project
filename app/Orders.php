<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    public $table = 'orders';
    protected $fillable = [
        'id','customer_id','chef_id',
        'sub_total','delivery_type','order_status','delivery_fare', 'grand_total','flag',
        'created_at', 'updated_at'
    ];

    public function order_line_item()
    {
        return $this->hasOne(OrderLine_items::class);
    }

    public function order_line_items()
    {
        return $this->hasMany(OrderLine_items::class,'order_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function chef()
    {
        return $this->belongsTo(chef::class);
    }

    public function payment()
    {
        return $this->hasOne(paymentDetail::class);
    }

    public function ratings()
    {
        return $this->hasMany(rating::class,'orders_id');
    }

    public function reason()
    {
        return $this->hasOne(Orderstatus_Reason::class,'orders_id');
    }

}
