<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class chef extends Model
{
    //
    public $table = "chef";
//    public $timestamps = false;

    protected $fillable = [
        'id', 'users_id' ,'cnic', 'chef_rating',
        'phoneNumber', 'city', 'streetaddress', 'state', 'postalcode',
        'workingHrs', 'chef_level', 'description', 'mode', 'latitude', 'longitude',
        'online_status', 'picture', 'verify', 'start_time', 'end_time',
        'created_at', 'updated_at'
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];

    public function user()
    {
        return $this->belongsTo(users::class,'users_id');
    }

    public function menu()
    {
        return $this->hasOne(Menu::class,'chef_id');
    }

    public function menus()
    {
        return $this->hasMany(Menu::class);
    }

    public function menu_item()
    {
        return $this->hasOne(Menu_Item::class,'chef_id');
    }

    public function menu_Items()
    {
        return $this->hasMany(Menu_Item::class);
    }

    public function order()
    {
        return $this->hasOne(Orders::class);
    }

    public function orders()
    {
        return $this->hasMany(Orders::class);
    }

    public function ratings(){

        return  $this->hasMany(rating::class);
    }

    public function getRating(){

        $ratingsum =$this->ratings()->sum('rating');
        $avgrating =$ratingsum/$this->ratings()->count();
        return $avgrating ;
    }

}
