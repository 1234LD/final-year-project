<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item_Images extends Model
{

    public $table = 'item_images';

    protected $fillable= [
        'id','menu__item_id','menu_id',
        'item_image'
    ];


    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }

    public function menu_item()
    {
        return $this->belongsTo(Menu_Item::class);
    }

}
