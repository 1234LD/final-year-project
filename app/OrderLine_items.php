<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderLine_items extends Model
{
    public $table = 'order_line_items';

    protected $fillable = [
        'id','menu__item_id','order_id',
        'quantity','item_price','item_option','sub_total'
    ];

    public function menu_item()
    {
        return $this->belongsTo(Menu_Item::class,'menu__item_id');
    }

    public function order()
    {
        return $this->belongsTo(Orders::class,'order_id');
    }

    public function reviews()
    {
        return $this->hasMany(rating::class);
    }

}
