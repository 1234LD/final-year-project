<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item_Recipes extends Model
{
    //

    public $table = 'item_recipes';

    protected $fillable = [
        'id','menu__item_id',
        'description','ingredient_name','ingredient_qty'
    ];

    public function menu_item()
    {
        return $this->belongsTo(Menu_Item::class,'menu__item_id');
    }

}
