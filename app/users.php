<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    // the attributes that are mass assignable ...
    public $table = "users";
    public $DEFAULT_USER_TYPE = 'general';

    protected $fillable = [
        'id','full_name',
        'username','email','password','phone_number', 'lat', 'lng',
        'usertype','facebook_ID','gmail','twitter','image','Activation'

    ];

    // the attributes that are  hidden for an array ...

    protected $hidden = [
        'password','remember_token'
    ];

    public function admin()
    {
     return $this->hasOne(Admin::class);
    }

    public function admins()
    {
        return $this->hasMany(Admin::class);
    }

    public function chef()
    {
        return $this->hasOne(chef::class);
    }

    public function chefs()
    {
        return $this->hasMany(chef::class);
    }

    public function customer()
    {
        return $this->hasOne(Customer::class,'users_id');
    }

    public function customers()
    {
        return $this->hasMany(Customer::class,'users_id');
    }

}
