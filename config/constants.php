<?php

return [

// Default Chef status ...

    'DEFAULT_STATUS_ONLINE' => 'online',

    'DEFAULT_STATUS_BUSY' => 'busy',

    'DEFAULT_STATUS_OFFLINE' => 'offline',

// Default User Types ...

    'DEFAULT_SUPER-ADMIN_TYPE' => 'Super Admin',

    'DEFAULT_ADMIN_TYPE' => 'Admin',

    'DEFAULT_CHEF_TYPE' => 'Chef',

    'DEFAULT_QUICK-CHEF_TYPE' => 'Quick Chef',

    'DEFAULT_GENERAL_TYPE' => 'General',

    'DEFAULT_CUSTOMER_TYPE' => 'Customer',

// Default Menu Types ...

    'DEFAULT_PUBLIC_ACCESS' => 'public',

    'DEFAULT_PRIVATE_ACCESS' => 'private',

    'DEFAULT_QUICK-MEAL_CATEGORY' => 'Quick meal',

// Default Delivery types ...

    'DEFAULT_SELF_DELIVERY' => 'Home Delivery',

    'DEFAULT_TAKE-AWAY_DELIVERY' => 'Take away',

    'DEFAULT_BOTH_DELIVERY' => 'Both',

// Default Order Status ...

    'DEFAULT_ORDER_PENDING' => 'Pending',

    'DEFAULT_ORDER_ACCEPT' => 'Accepted',

//    'DEFAULT_ORDER_IN-QUEUE' => 'in-queue',

    'DEFAULT_ORDER_CANCELLED' => 'Cancelled',

    'DEFAULT_ORDER_DECLINED' => 'Declined',

    'DEFAULT_ORDER_DELIVERED' => 'Delivered',

    'DEFAULT_ORDER_READY' => 'Ready',

    'DEFAULT_ORDER_COMPLETED' => 'Completed',

    'DEFAULT_ORDER_PROCESSING' => 'Processing',

// Default Order Status ...

    'DEFAULT_PAYMENT_PAID' => 'Paid',

    'DEFAULT_PAYMENT_UNPAID' => 'Unpaid',

    'DEFAULT_PAYMENT_COD' => 'COD',

// Default GUM for concatenation ...

    'DEFAULT_GUM' => '^'
];