
    function changeVal(el) {
        var qt = parseFloat(el.parent().children(".qt").html());
        var price = parseFloat(el.parent().parent().children().children(".price").html());
        var eq = Math.round(price * qt * 100) / 100;
		
        el.parent().parent().children().children(".full-price").html(eq);
        totalQty();
        changeTotal();
    }

    function changeTotal() {
		var price = 0;

		function totalPrice(){
			$(".full-price").each(function(index) {
					price += parseFloat($(".full-price").eq(index).html());
			});
		}

		totalPrice();
		price = Math.round(price * 100) / 100;
		var delivery = parseFloat($(".delivery span").html());
		var fullPrice = Math.round((price + delivery) * 100) / 100;

		if (price == 0) {
			fullPrice = 0;
		}

		$(".subtotal span").html(price);
		$(".total span").html(fullPrice);
    }
	
	function totalQty(){
		var qty = 0;
		$(".qt").each(function(index) {
				qty += parseInt($(".qt").eq(index).html());
		});
		
		if(qty == 0){
			$(".total-cart-items span").remove();			
		}
		else{
			if($(".total-cart-items span")[0]){
				$(".total-cart-items span").html(qty);
			}
			else{
			    $(".total-cart-items").append("<span>"+ qty +"</span>");	
			}
		}
		
	}

    // function create_order()
    // {
    //     alert("sad;aksjd;");
    // }
	
$(document).ready(function() {
	changeTotal();
	
	$('.mode').on('change', function() {
	  var mode = $(".mode").val();
		if(mode == "Take away"){
			$(".delivery span").html("0");
		}
		else if (mode == "Home Delivery"){
			$(".delivery span").html("25");
		}
        changeTotal();		
	});

    $(".cart-checkout").click(function()
	{
        var qty = [];
        var price = [];
        var option = [];
        var item_id = [];
        var item_subtotal = [];

        var count = 0;

        $(".quantities span.qt").each(function(index) {
            qty.push($(this).text());
            count++;
        });

        $(".notification-text span.price").each(function(index) {
            price.push($(this).text());
        });

        $(".notification-text span.option").each(function(index) {
            option.push($(this).text());
        });

        $(".notification-text .item_id").each(function(index) {
            item_id.push($(this).text());
        });

        $(".notification-text span.full-price").each(function(index) {
            item_subtotal.push($(this).text());
        });



         var order_line_item = [];

         for(t = 0; t < count ; t++){
             order_line_item[t] = [
                 qty[t],
                 price[t],
                 option[t],
                 item_id[t],
                  item_subtotal[t]
         ]
		 }

        var chef_id;

        $(".chef_id").each(function(index) {
            chef_id = $(this).text();
        });


        var lat = $('.latlong span#blat').text();
        var lng = $('.latlong span#blong').text();
        var order_subtotal = $('.cart-info span.subtotal span').text();
        var delivery_type = $('.cart-info .mode').val();
        var delivery_fare = $('.cart-info .delivery span').text();
        var grand_total = $('.cart-info .total span').text();



         // alert(lat);
        var v_order_line_item = order_line_item;

		var dataInfo = {
             chef_id: chef_id,
            delivery_type:delivery_type, delivery_fare:delivery_fare,
            order_subtotal:order_subtotal, grand_total:grand_total,
            lat: lat, lng: lng,
			order_line_item: v_order_line_item
		};
		// var dataI = {};
		// // dataI.push(item_id,'');
		// // dataI.push(chef_id);
		// dataI['item_id'] = item_id;
		// dataI['chef_id'] = chef_id;

		// dataInfo.serializeArray();

        var newData =  JSON.stringify(dataInfo);
		// var parsed = JSON.parse(newData);
		 console.log(newData);

		// $.form('order/create',dataInfo,'POST').submit();

       var token = $('meta[name="_token"]').attr('content');

        $('.cart-content').append('<form action="http://localhost/foodpoleAPI/public/order/create" method="POST" class="checkout-fly hide">' +
			'<input type="hidden" name="_token" value="'+ token +'">'+
            '<input type="hidden" name="dataInfo" value="">' +
		    '<button type="submit"></button>' +
            '</form>');


        $('input[name="dataInfo"]').val(newData);


        $('.checkout-fly').trigger('submit');


        // $.ajax({
        //     type:'POST',
        //     data: {
        //         item_id: item_id, chef_id: chef_id,
			// 	quantity: qty, price:price, option:option,
			// 	lat: lat, lng: lng,
			// 	item_subtotal: item_subtotal, delivery_type:delivery_type, delivery_fare:delivery_fare,
			// 	order_subtotal:order_subtotal, grand_total:grand_total
        //     },
			// url:'order/create',
        //     success: function(response){ // What to do if we succeed
        //         // if(data == "success")
        //         alert(response);
        //     },
        //     error: function(response){
        //          // alert('Error : '+response);
        //     }
        // });

    });

    $(".clear-cart").click(function() {
		window.setTimeout(
			function() {
				$(".meal-bag-items").slideUp('fast', function() {
					$(".meal-bag-items").remove();
					$(".meal-bag").prepend("<span class=\"no-meal\">No meals!</span>");
					totalQty();
					changeTotal();
				});
			}, 200);
	});
	
    $(".remove-cart-item").click(function() {
        var el = $(this);
        window.setTimeout(
            function() {
                el.parent().slideUp('fast', function() {
                    el.parent().remove();
                    if($(".meal-item")[0]) {
                    }
					else{
						$(".meal-bag-items").remove();
						$(".meal-bag").prepend("<span class=\"no-meal\">No meals!</span>");
					}
					totalQty();
                    changeTotal();
                });
            }, 200);
    });
	

    // $(".qt-plus").click(function() {
    //     $(this).parent().children(".qt").html(parseInt($(this).parent().children(".qt").html()) + 1);
    //
    //     var el = $(this);
    //     window.setTimeout(function() {
    //         changeVal(el);
    //     }, 150);
    // });

    // $(".qt-minus").click(function() {
    //
    //     child = $(this).parent().children(".qt");
		//
    //
    //     if (parseInt(child.html()) > 1) {
    //         child.html(parseInt(child.html()) - 1);
    //     }
		//
    //     var el = $(this);
    //     window.setTimeout(function() {
    //         changeVal(el);
    //     }, 150);
    //
    // });
});

// $('#create_order').click(function()
// {
// 	alert("yesssss");
// });ndow.setTimeout(function() {
    //         changeVal(el);
    //     }, 150);
    // });

    // $(".qt-minus").click(function() {
    //
    //     child = $(this).parent().children(".qt");
		//
    //
    //     if (parseInt(child.html()) > 1) {
    //         child.html(parseInt(child.html()) - 1);
    //     }
		//
    //     var el = $(this);
    //     window.setTimeout(function() {
    //         changeVal(el);
    //     }, 150);
    //
    // });
// });

// $('#create_order').click(function()
// {
// 	alert("yesssss");
// });                  changeTotal();
//                 });
//             }, 200);
//     });
	

    // $(".qt-plus").click(function() {
    //     $(this).parent().children(".qt").html(parseInt($(this).parent().children(".qt").html()) + 1);
    //
    //     var el = $(this);
    //     window.setTimeout(function() {
    //         changeVal(el);
    //     }, 150);
    // });

    // $(".qt-minus").click(function() {
    //
    //     child = $(this).parent().children(".qt");
		//
    //
    //     if (parseInt(child.html()) > 1) {
    //         child.html(parseInt(child.html()) - 1);
    //     }
		//
    //     var el = $(this);
    //     window.setTimeout(function() {
    //         changeVal(el);
    //     }, 150);
    //
    // });
// });

// $('#create_order').click(function()
// {
// 	alert("yesssss");
// });