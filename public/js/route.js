function initMap() {
  var pointA = new google.maps.LatLng(33.5970666, 73.011922),
    pointB = new google.maps.LatLng(33.6002939, 73.0313812),
    myOptions = {
	  center: pointA,	
      zoom: 18
    },
    map = new google.maps.Map(document.getElementById('map-canvas'), myOptions),
    // Instantiate a directions service.
    directionsService = new google.maps.DirectionsService,
    directionsDisplay = new google.maps.DirectionsRenderer({
	  preserveViewport: true,
      map: map
    });

  // get route from A to B
  calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB);
}

function calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB) {
  directionsService.route({
    origin: pointA,
    destination: pointB,
    travelMode: google.maps.TravelMode.WALKING
  }, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
      pointsArray = response.routes[0].legs[0].steps;
      route = "";
      for (var i in pointsArray) {
        route += "<li>"+pointsArray[i].instructions+"</li>" ;
      }
      document.getElementById("route").innerHTML = "<ul>"+route+"</ul>";
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}

initMap();