    function addItem() {
		var number = parseInt($(".xwnmokp").html());
		number++;
		$("#f-add-meal").append('<!-- Dashboard Box -->'
			+'<div class="col-xl-12">'
				+'<div class="dashboard-box">'
					+'<!-- Headline -->'
					+'<div class="headline">'
						+'<h3><i class="icon-material-outline-restaurant"></i> Meal# '+ (number+1) +'</h3>'
						+'<div class="keyword-input-container meal-plus-cont">'
							+'<a class="keyword-input-button ripple-effect meal-plus meal-item-add" data-tippy-placement="left" title="Add another Meal" onclick="addItem()"><i class="icon-material-outline-add"></i></a>'
						+'</div>'	
					+'</div>'
					+'<div class="content with-padding padding-bottom-0">'
						+'<div class="row">'
							+'<div class="col-auto avatar-cont">'
								+'<div class="avatar-wrapper new-meal" data-tippy-placement="bottom" title="Upload Featured Photo">'
									+'<img class="profile-pic" src="images/food-placeholder.jpg" alt="" />'
									+'<div class="upload-button"></div>'
									+'<input class="file-upload" type="file" name="menu_item['+ number +'][item_image][0]" accept="image/*"/>'
								+'</div>'
							+'</div>'
							+'<div class="col">'
								+'<div class="row">'
									+'<div class="col-xl-4">'
										+'<div class="submit-field">'
											+'<h5>Title</h5>'
											+'<input type="text" name="menu_item['+ number +'][item_name]" class="with-border" placeholder="e.g. Mc Zinger Burger">'
										+'</div>'
									+'</div>'
									+'<div class="col-xl-4">'
									+'<div class="submit-field">'
									+'<h5>Preparation Time (minutes)</h5>'
									+'<input type="number" name="menu_item['+ number +'][item_time]" class="with-border" placeholder="e.g. 60">'
									+'</div>'
									+'</div>'
									+'<div class="col-xl-4">'
										+'<div class="submit-field">'
											+'<h5>More Photos</h5>'
											+'<!-- Upload Button -->'
											+'<div class="uploadButton margin-top-0">'
												+'<input class="uploadButton-input meal-'+ number +'" type="file" name="menu_item['+ number +'][item_image][1]" accept="image/*" id="upload'+ number +'" multiple/>'
												+'<label class="uploadButton-button ripple-effect" for="upload'+ number +'">Upload Photos</label>'
												+'<span class="uploadButton-file-name">Maximum file size: 10 MB</span>'
											+'</div>'
										+'</div>'
									+'</div>'	
								+'</div>'
							+'</div>'
						+'</div>'
					+'</div>'
				+'</div>'
			+'</div>'
			+'<!-- Dashboard Box -->'
			+'<div class="col-xl-12">'
				+'<div class="dashboard-box">'
					+'<!-- Headline -->'
					+'<div class="headline">'
						+'<h3><i class="icon-material-outline-restaurant"></i> Options</h3>'
						+'<div class="keyword-input-container meal-plus-cont">'
							+'<a class="keyword-input-button ripple-effect meal-plus add-option" data-tippy-placement="left" title="Add Option" onclick="addOption($(this))"><i class="icon-material-outline-add"></i></a>'
						+'</div>'					
					+'</div>'
					+'<div class="content with-padding padding-bottom-0 options-cont">'
					+'<div class="hide qawsed">0</div>'
						+'<div class="row">'
							+'<div class="col">'
								+'<div class="row">'
									+'<div class="col-xl-6">'
										+'<div class="submit-field">'
											+'<h5>Option Name</h5>'
											+'<input type="text" class="with-border" name="menu_item['+ number +'][option_name][0]" placeholder="e.g. Single Patty Burger" requied/>'
										+'</div>'
									+'</div>'
									+'<div class="col-xl-6">'
										+'<div class="submit-field">'
											+'<h5>Price Rs.</h5>'
											+'<input type="number" class="with-border" name="menu_item['+ number +'][option_price][0]" placeholder="e.g. 300" requied/>'
										+'</div>'
									+'</div>'									
								+'</div>'
							+'</div>'
						+'</div>'
					+'</div>'
				+'</div>'
			+'</div>'
			+'<!-- Dashboard Box -->'
			+'<div class="col-xl-12">'
				+'<div class="dashboard-box">'
					+'<!-- Headline -->'
					+'<div class="headline">'
						+'<h3><i class="icon-material-outline-restaurant"></i> Ingredients</h3>'
						+'<div class="keyword-input-container meal-plus-cont">'
							+'<a class="keyword-input-button ripple-effect meal-plus add-ingredient" data-tippy-placement="left" title="Add Ingredient" onclick="addIngredient($(this))"><i class="icon-material-outline-add"></i></a>'
						+'</div>'								
					+'</div>'
					+'<div class="content with-padding padding-bottom-0 ingredients-cont">'
					+'<div class="hide rftgyh">0</div>'
						+'<div class="row">'
							+'<div class="col">'
								+'<div class="row">'
									+'<div class="col-xl-6">'
										+'<div class="submit-field">'
											+'<h5>Name</h5>'
											+'<input type="text" class="with-border" name="menu_item['+ number +'][ingredient_name][0]"  placeholder="e.g. Mustard Sauce">'
										+'</div>'
									+'</div>'
									+'<div class="col-xl-6">'
										+'<div class="submit-field">'
											+'<h5>Quantity</h5>'
											+'<input type="text" class="with-border" name="menu_item['+ number +'][ingredient_qty][0]" placeholder="e.g. 1 tea spoon">'
										+'</div>'
									+'</div>'
								+'</div>'
							+'</div>'
						+'</div>'
					+'</div>'
				+'</div>'
			+'</div>'
			+'<!-- Dashboard Box -->'
			+'<div class="col-xl-12">'
				+'<div class="dashboard-box">'
					+'<!-- Headline -->'
					+'<div class="headline">'
						+'<h3><i class="icon-material-outline-restaurant"></i> Recipe</h3>'
					+'</div>'
					+'<div class="content with-padding padding-bottom-0">'
						+'<div class="row">'
							+'<div class="col">'
								+'<div class="row">'
									+'<div class="col-xl-12">'
										+'<div class="submit-field">'
											+'<textarea name="menu_item['+ number +'][description]" cols="30" rows="5" class="with-border">Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.</textarea>'
										+'</div>'
									+'</div>'
								+'</div>'
							+'</div>'
						+'</div>'
					+'</div>'
				+'</div>'
			+'</div>');
		if($(".xwnmokp")[0]) {
			$(".xwnmokp").html(number);
		}
		else{
			$(body).append('<div class="hide xwnmokp">'+ number +'</div>');
		}				
    }
	
    function addOption(el) {
		var item_count = parseInt($(".xwnmokp").html());
		var option_count = (parseInt(el.parent().parent().parent().children().children(".qawsed").html())) + 1;
		el.parent().parent().parent().children(".options-cont").append('<div class="row">'
							+'<div class="col">'
								+'<div class="row">'
									+'<div class="col-xl-6">'
										+'<div class="submit-field">'
											+'<h5>Option Name</h5>'
											+'<input type="text" class="with-border" name="menu_item['+ item_count +'][option_name]['+ option_count +']" placeholder="e.g. Single Patty Burger">'
										+'</div>'
									+'</div>'
									+'<div class="col-xl-6">'
										+'<div class="submit-field">'
											+'<h5>Price Rs.</h5>'
											+'<input type="number" class="with-border" name="menu_item['+ item_count +'][option_price]['+ option_count +']" placeholder="e.g. 300">'
										+'</div>'
									+'</div>'									
								+'</div>'
							+'</div>'
						+'</div>');
		el.parent().parent().parent().children().children(".qawsed").html(option_count);
	}

    function addIngredient(el) {
		var item_count = parseInt($(".xwnmokp").html());
		var ingredient_count = (parseInt(el.parent().parent().parent().children().children(".rftgyh").html())) + 1;
		el.parent().parent().parent().children(".ingredients-cont").append('<div class="row">'
							+'<div class="col">'
								+'<div class="row">'
									+'<div class="col-xl-6">'
										+'<div class="submit-field">'
											+'<h5>Name</h5>'
											+'<input type="text" class="with-border" name="menu_item['+ item_count +'][ingredient_name]['+ ingredient_count +']"  placeholder="e.g. Mustard Sauce">'
										+'</div>'
									+'</div>'
									+'<div class="col-xl-6">'
										+'<div class="submit-field">'
											+'<h5>Quantity</h5>'
											+'<input type="text" class="with-border" name="menu_item['+ item_count +'][ingredient_qty]['+ ingredient_count +']" placeholder="e.g. 1 tea spoon">'
										+'</div>'
									+'</div>'
								+'</div>'
							+'</div>'
						+'</div>');
		el.parent().parent().parent().children().children(".rftgyh").html(ingredient_count);
	}	
	
$(document).ready(function() {
    $(".meal-item-add").click(function() {
        addItem();
    });
	
    $(".add-option").click(function() {
        addOption($(this));
    });

    $(".add-ingredient").click(function() {
        addIngredient($(this));
    });

	$('.all-cats').on('change', function() {
	  var cat = this.options[this.selectedIndex].value;
		if(cat == "None"){
			$(".cat-field").prop('disabled', false);
			$(".cat-field").val("");
		}
		else{
			$(".cat-field").val(cat);
			$(".cat-field").prop('disabled', true);
		}	
	});		
});